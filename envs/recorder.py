import sys
sys.path.append('..')
sys.path.append('../utils/')
from pynput.keyboard import Listener, Key
from gym import spaces
from string import ascii_lowercase
import baselines.common.tf_util as U
import os
import datetime
from gym.wrappers.monitoring.video_recorder import VideoRecorder
import tensorflow as tf

RESULT_PATH = '../results/'

keys = {}

for i in ascii_lowercase:
    keys[i] = False
for i in range(10):
    keys[str(i)] = False
keys['left'], keys['right'], keys['up'], keys['down'] = [False] * 4


def on_press(key):
    if key == Key.left:
        keys['left'] = True
    elif key == Key.right:
        keys['right'] = True
    elif key == Key.up:
        keys['up'] = True
    elif key == Key.down:
        keys['down'] = True
    else:
        keys[key.char] = True

def on_release(key):
    if key == Key.left:
        keys['left'] = False
    elif key == Key.right:
        keys['right'] = False
    elif key == Key.up:
        keys['up'] = False
    elif key == Key.down:
        keys['down'] = False
    else:
        keys[key.char] = False
    return True


class GameRunner(object):

    def __init__(self, env, policy_func=None,
        max_timesteps=0, callback=None, identifier='', record_video=False, cnn=None):
        self.env = env
        self.pi = None if policy_func is None else \
            policy_func('pi', self.env.observation_space, self.env.action_space)
        self.max_timesteps = max_timesteps
        self.callback = callback
        self.identifier = identifier

        os.makedirs(RESULT_PATH + "video", exist_ok=True)
        curr_time = datetime.datetime.now().strftime("%y%m%d%H%M%S")
        self.rec = VideoRecorder(env, base_path=RESULT_PATH + 'video/' + curr_time, enabled=record_video)

        self.cnn = cnn

        self.states_cnn = None
        speed = self.env.sample_speed()
        if self.cnn:
            self.states_cnn = self.cnn.get_smart_logits([speed])[0, :]

        self.total_reward = 0
        self.timestep = 0
        self.keymap = env.keymap    # [{key -> action, ...}, ...]
        
        self.run_func = {'h': self.run_human, 'n': self.run_nn}
        
    def run(self, option):
        Listener(on_press=on_press, on_release=on_release).start()

        if 'n' in option:
            saver = tf.train.Saver()
            saver.restore(tf.get_default_session(), RESULT_PATH + 'saved_model/model_data/' + self.identifier + '.ckpt')
            assert self.pi is not None
            
        while True:
            if self.callback:
                self.callback(locals(), globals())
            if self.max_timesteps and self.timestep >= self.max_timesteps:
                break
            if self.rec.functional:
                self.rec.capture_frame()

            self.run_func[option]()
            self.act()

    def run_human(self):
        keymap_len = len(self.keymap)
        self.action = [0] * keymap_len
        for i in range(keymap_len):
            for key in self.keymap[i]:
                if key in keys and keys[key]:
                    self.action[i] = self.keymap[i][key]
                    break
            else:
                if 'else' in self.keymap[i]:
                    self.action[i] = self.keymap[i]['else']

    def run_nn(self):
        raise NotImplementedError

    def act(self):
        raise NotImplementedError


class GameSingleRunner(GameRunner):

    def __init__(self, env, policy_func=None, 
        max_timesteps=0, callback=None, identifier='', record_video=False, cnn=None):
        self.state = env.reset()
        GameRunner.__init__(self, env, policy_func,
            max_timesteps, callback, identifier, record_video, cnn)

    def run_nn(self):
        self.action, _ = self.pi.act(False, self.state, self.states_cnn)

    def act(self):
        self.state, reward, done, info = self.env.step(self.action)

        speed = info['speed']
        if self.cnn:
            self.states_cnn = self.cnn.get_smart_logits([speed])[0, :]

        self.env.render()
        self.total_reward += reward
        self.timestep += 1

        if done:
            print(["{:+0.2f}".format(x) for x in self.state])
            print("step {} total_reward {:+0.2f}".format(self.timestep, self.total_reward))
            self.state = self.env.reset()
            self.total_reward = 0
            self.timestep = 0

            speed = self.env.sample_speed()
            if self.cnn:
                self.states_cnn = self.cnn.get_smart_logits([speed])[0, :]


class GameDoubleRunner(GameRunner): 

    def __init__(self, env, policy_func=None, 
        max_timesteps=0, callback=None, identifier='', record_video=True, cnn=None):
        self.state1, self.state2 = env.reset()
        GameRunner.__init__(self, env, policy_func, 
            max_timesteps, callback, identifier, record_video, cnn)

        assert len(self.keymap) % 2 == 0
        self.run_func['hn'] = self.run_human_with_nn

    def run_human(self):
        GameRunner.run_human(self)
        action_len = len(self.action)
        self.action = [self.action[:action_len // 2], self.action[action_len // 2:]]

    def run_nn(self):
        action1, _ = self.pi.act(False, self.state1, self.states_cnn)
        action2, _ = self.pi.act(False, self.state2, self.states_cnn)
        self.action = [action1, action2]

    def run_human_with_nn(self):
        self.run_human()
        action1 = self.action[0]
        action2, _ = self.pi.act(False, self.state2, self.states_cnn)
        self.action = [action1, action2]

    def act(self):
        self.state1, self.state2, reward1, reward2, done, info = self.env.step(self.action)
        
        speed = info['speed']
        if self.cnn:
            self.states_cnn = self.cnn.get_smart_logits([speed])[0, :]

        self.env.render()
        self.total_reward += reward1
        self.timestep += 1

        if done:
            print(["{:+0.2f}".format(x) for x in self.state1])
            print("step {} total_reward {:+0.2f}".format(self.timestep, self.total_reward))
            self.state1, self.state2 = self.env.reset()
            self.total_reward = 0
            self.timestep = 0

            speed = self.env.sample_speed()
            if self.cnn:
                self.states_cnn = self.cnn.get_smart_logits([speed])[0, :]


