from music_params import *

### MUSIC UTILS ###

def music_calc_reward(die, x=None, target_x=None, time=None):
    if die:
        return WD
    assert x is not None
    assert target_x is not None
    assert time is not None
    if x == target_x:
        return WW - WT * abs(time)
    else:
        return WN

def music_note(key, time):
    if key == 7:
        return ('c5', 4 * TBASE / time)
    else:
        return (chr(99 + int(key - int(key > 4) * 7)), 4 * TBASE / time)


### FLUID_FUNC ###

import ctypes as ct

GRAVITY = -9.81

def liquid_func_declare(fluid):

    fluid.destroyLiquid.       			argtypes = []

    fluid.createLiquid.        			argtypes = [ct.c_float, ct.c_float, ct.c_float, ct.c_float, ct.c_float * 4, ct.c_bool]
    fluid.createLiquidSingle.			argtypes = [ct.c_float, ct.c_float, ct.c_float, ct.c_float, ct.c_float * 4, ct.c_bool]
    fluid.createLiquidTriple.           argtypes = [ct.c_float, ct.c_float, ct.c_float, ct.c_float, ct.c_float, ct.c_float * 4]
    fluid.createLiquidQuadruple.        argtypes = [ct.c_float, ct.c_float, ct.c_float, ct.c_float, ct.c_float, ct.c_float * 4]
    fluid.createLiquidMulti.			argtypes = [ct.c_float, ct.c_float, ct.c_float, ct.c_float, ct.c_float, ct.c_float * 4]
    fluid.createLiquidBattle.			argtypes = [ct.c_float, ct.c_float, ct.c_float, ct.c_float, ct.c_float * 4]
    fluid.createLiquidBottomBattle.		argtypes = [ct.c_float, ct.c_float, ct.c_float, ct.c_float, ct.c_float * 4]

    fluid.addLiquidSolid.      			argtypes = [ct.c_float, ct.c_float, ct.c_float, ct.c_float, ct.c_bool]

    fluid.setRigidLiquid.      			argtypes = [ct.c_float, ct.c_float, ct.c_float, ct.c_float, ct.c_float]
    fluid.setRigidBoxLiquid.			argtypes = [ct.c_float, ct.c_float, ct.c_float, ct.c_float, ct.c_float, ct.c_bool]
    fluid.setRigidVelLiquid.   			argtypes = [ct.c_float, ct.c_float]

    fluid.getRigidPosLiquid.   			argtypes = [ct.POINTER(ct.c_float), ct.POINTER(ct.c_float)]
    fluid.getRigidRotLiquid.			argtypes = [ct.c_float * 4]
    fluid.getRigidRotAngleLiquid.		argtypes = [ct.POINTER(ct.c_float)]
    fluid.getRigidVelLiquid.   			argtypes = [ct.POINTER(ct.c_float), ct.POINTER(ct.c_float)]
    fluid.getRigidRotVelLiquid.			argtypes = [ct.POINTER(ct.c_float)]
    fluid.getRigidForceLiquid. 			argtypes = [ct.POINTER(ct.c_float)]
    fluid.getRigidTorqueLiquid.			argtypes = [ct.POINTER(ct.c_float)]
    fluid.getRigidBBLiquid.				argtypes = [ct.POINTER(ct.c_float), ct.POINTER(ct.c_float)]

    fluid.transferLiquid.				argtypes = [ct.c_float, ct.c_bool, ct.c_bool, ct.POINTER(ct.c_float), ct.c_float, ct.c_float, ct.POINTER(ct.c_bool)]
    fluid.transferLiquidSingle.			argtypes = [ct.c_float, ct.c_bool, ct.POINTER(ct.c_float), ct.POINTER(ct.c_float), ct.c_float, ct.POINTER(ct.c_int), ct.POINTER(ct.c_int), ct.c_bool, ct.POINTER(ct.c_bool)]
    fluid.transferLiquidDouble.			argtypes = [ct.c_float, ct.c_bool, ct.c_bool, ct.POINTER(ct.c_float), ct.POINTER(ct.c_float), ct.POINTER(ct.c_float), ct.c_float, ct.c_float, ct.c_float, ct.POINTER(ct.c_int), ct.POINTER(ct.c_int), ct.c_bool, ct.POINTER(ct.c_bool)]
    fluid.transferLiquidTriple.         argtypes = [ct.c_float, ct.c_bool * 3, ct.c_float * 3, ct.c_float * 3, ct.c_float, ct.c_float, ct.c_float, ct.POINTER(ct.c_int), ct.POINTER(ct.c_int), ct.POINTER(ct.c_bool)]
    fluid.transferLiquidQuadruple.		argtypes = [ct.c_float, ct.c_bool * 4, ct.POINTER(ct.c_float), ct.POINTER(ct.c_float), ct.POINTER(ct.c_float), ct.POINTER(ct.c_float), ct.POINTER(ct.c_float), ct.c_float, ct.c_float, ct.c_float, ct.c_float, ct.POINTER(ct.c_bool)]
    fluid.transferLiquidMulti.          argtypes = [ct.c_float, ct.c_bool * 10,  ct.POINTER(ct.c_float), ct.POINTER(ct.c_float), ct.c_float, ct.c_float, ct.POINTER(ct.c_bool)]
    fluid.transferLiquidMultiSingle.	argtypes = [ct.c_float, ct.c_bool, ct.c_bool, ct.POINTER(ct.c_float), ct.POINTER(ct.c_float), ct.c_float, ct.POINTER(ct.c_int), ct.POINTER(ct.c_int)]
    fluid.transferLiquidBattle.			argtypes = [ct.c_float, ct.c_bool, ct.c_bool, ct.POINTER(ct.c_float), ct.POINTER(ct.c_float), ct.POINTER(ct.c_float), ct.POINTER(ct.c_float), ct.c_float, ct.POINTER(ct.c_int), ct.POINTER(ct.c_int), ct.c_float, ct.POINTER(ct.c_bool)]
    fluid.transferLiquidBottomBattle.	argtypes = [ct.c_float, ct.c_bool, ct.c_bool, ct.POINTER(ct.c_float), ct.POINTER(ct.c_float), ct.POINTER(ct.c_float), ct.POINTER(ct.c_float), ct.c_float, ct.POINTER(ct.c_int), ct.POINTER(ct.c_int), ct.c_float, ct.POINTER(ct.c_bool)]

    fluid.getLiquidMesh.				argtypes = [ct.POINTER(ct.c_float), ct.POINTER(ct.c_int)]
    # fluid.getFeatureLiquid.				argtypes = [ct.c_float, ct.POINTER(ct.c_float), ct.POINTER(ct.c_float), ct.POINTER(ct.c_float), ct.POINTER(ct.c_int)]
    # fluid.getFeatureLiquidSimple.		argtypes = [ct.c_float, ct.POINTER(ct.c_float), ct.POINTER(ct.c_int)]
    fluid.getParticlesLiquid.  			argtypes = [ct.POINTER(ct.c_float), ct.POINTER(ct.c_int)]
    
def smoke_func_declare(fluid):

    fluid.destroySmoke.                 argtypes = []

    fluid.createSmoke.                  argtypes = [ct.c_float, ct.c_float, ct.c_float, ct.c_float]
    fluid.createSmokeSingle.            argtypes = [ct.c_float, ct.c_float, ct.c_float, ct.c_float]

    fluid.addSmokeSolid.                argtypes = [ct.c_float, ct.c_float, ct.c_float, ct.c_float]

    fluid.setRigidSmoke.                argtypes = [ct.c_float, ct.c_float, ct.c_float, ct.c_float]
    fluid.setRigidVelSmoke.             argtypes = [ct.c_float, ct.c_float]
    fluid.setRigidBoxSmoke.             argtypes = [ct.c_float, ct.c_float, ct.c_float, ct.c_float, ct.c_float, ct.c_bool]

    fluid.getRigidPosSmoke.             argtypes = [ct.POINTER(ct.c_float), ct.POINTER(ct.c_float)]
    fluid.getRigidRotSmoke.             argtypes = [ct.c_float * 4]
    fluid.getRigidRotAngleSmoke.        argtypes = [ct.POINTER(ct.c_float)]
    fluid.getRigidVelSmoke.             argtypes = [ct.POINTER(ct.c_float), ct.POINTER(ct.c_float)]
    fluid.getRigidRotVelSmoke.          argtypes = [ct.POINTER(ct.c_float)]
    fluid.getRigidForceSmoke.           argtypes = [ct.POINTER(ct.c_float)]
    fluid.getRigidTorqueSmoke.          argtypes = [ct.POINTER(ct.c_float)]
    fluid.getRigidBBSmoke.              argtypes = [ct.POINTER(ct.c_float), ct.POINTER(ct.c_float)]

    fluid.transferSmoke.                argtypes = [ct.c_float, ct.c_bool, ct.c_bool, ct.POINTER(ct.c_float), ct.c_float, ct.c_float]
    fluid.transferSmokeSingle.          argtypes = [ct.c_float, ct.c_bool, ct.POINTER(ct.c_float), ct.POINTER(ct.c_float), ct.c_float, ct.POINTER(ct.c_bool)]
    
    # fluid.getFeatureSmoke.              argtypes = [ct.c_float, ct.POINTER(ct.c_float), ct.POINTER(ct.c_float), ct.POINTER(ct.c_float), ct.POINTER(ct.c_int)]
    # fluid.getFeatureSmokeSimple.        argtypes = [ct.c_float, ct.POINTER(ct.c_float), ct.POINTER(ct.c_int)]
    fluid.getRhoSmoke.                  argtypes = [ct.POINTER(ct.c_float), ct.POINTER(ct.c_float), ct.POINTER(ct.c_float), ct.POINTER(ct.c_int), ct.POINTER(ct.c_int), ct.POINTER(ct.c_int)]
    fluid.setFade.                      argtypes = [ct.c_float]


### SPEED EXTRACTION ###

import numpy as np

def preprocess(field, field_w, field_h):
    speed_x = np.array(field[::2]).reshape((field_h, field_w))
    speed_y = np.array(field[1::2]).reshape((field_h, field_w))
    return np.stack((speed_x, speed_y), axis=-1)