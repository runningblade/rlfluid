import pyFluidOpt as fo
import numpy as np
import math, random, os, shutil


def setTrans(T, v):
    for i in range(3):
        T(i, 3, v[i])


def setRot(T, r):
    for i in range(3):
        for j in range(3):
            T(i, j, r(i, j))


if __name__ == '__main__':
    sz = 64.0
    trap_h = 0.5
    trap_t = 0.2
    # setup
    sol = fo.SolverSWE2DConstant1()
    sol.initDomain(fo.BBox(fo.Vec3(0, 0, 0), fo.Vec3(3, 3, 3)), fo.Vec3(1.0 / sz, 1.0 / sz, 1.0 / sz), fo.Vec3i(0, 0, 0))
    # add floor
    rigid = fo.RigidSolver(3, 1 / sz)
    rigid.BB = fo.BBox(fo.Vec3(1 / sz, 1 / sz, 1 / sz), fo.Vec3(3.0 - 1 / sz, 3.0 - 1 / sz, 3.0 - 1 / sz))
    T = fo.Mat4()
    R = fo.Mat3()
    T.setIdentity()
    setTrans(T, fo.Vec3(1.5, 1.5, 0))
    rigid.addSolid(fo.BoxGeomCell(T, 3, fo.Vec3(3, 3, 0.1), 0, False))
    # add solid 0
    T = fo.Mat4()
    R = fo.Mat3()
    T.setIdentity()
    setTrans(T, fo.Vec3(1.5, 1.5 - trap_t/2, 0))
    R.fromLog(fo.Vec3(math.pi / 4.0, 0, 0))
    setRot(T, R)
    rigid.addSolid(fo.BoxGeomCell(T, 3, fo.Vec3(3, np.sqrt(2) * trap_h / 2, np.sqrt(2) * trap_h / 2), 0, False))
    # add solid 1
    T = fo.Mat4()
    R = fo.Mat3()
    T.setIdentity()
    setTrans(T, fo.Vec3(1.5, 1.5 + trap_t/2, 0))
    R.fromLog(fo.Vec3(math.pi / 4.0, 0, 0))
    setRot(T, R)
    rigid.addSolid(fo.BoxGeomCell(T, 3, fo.Vec3(3, np.sqrt(2) * trap_h / 2, np.sqrt(2) * trap_h / 2), 0, False))
    # add solid 2
    T = fo.Mat4()
    R = fo.Mat3()
    T.setIdentity()
    setTrans(T, fo.Vec3(1.5, 1.5, 0))
    R.fromLog(fo.Vec3(0, 0, 0))
    setRot(T, R)
    rigid.addSolid(fo.BoxGeomCell(T, 3, fo.Vec3(3, trap_t/2, trap_h), 0, False))
    # add body
    rigid.addBall3(fo.Vec3(0.1, 0.1, 0.1), fo.Vec3(1.5, 0.5, 1), fo.Vec3(0, 0, 0))
    rigid.getBody(0).rho = 100.0
    sol.initSolid(rigid)
    sol.initLiquidLevel(0.37, 1)
    # add source
    src_num = 11
    jet = True
    for i in range(src_num):
        # source
        s = fo.RandomSource()
        sf = fo.ImplicitRect(fo.Vec3(i * 3 / src_num, 0, 0.4), 
                             fo.Vec3((i + 1) * 3 / src_num, 1 / sz, 0.4), 3, 1.0 / sz, 
                             fo.Vec3(0, 1 if jet else 0, 0), 0, True, False)
        # jet ^= True
        s.addSource(sf)
        sol.addSource(s)
    # init
    sol.globalId = 0
    sol.globalTime = 0
    sol.CDrag = 10000
    sol.theta = 1
    # simulate
    if os.path.exists('SWE'):
        shutil.rmtree('SWE')
    os.mkdir('SWE')
    for i in range(500):
        print('Simulating frame: %d!' % i)
        sol.advance(1e-2)
        sol.focusOn2(0, 1)
        #write rigid
        if sol.getRigid().nrBody()>0:
            sol.getRigid().getBody(0).writeMeshVTK('SWE/rigid' + str(i) + '.vtk')
        #write fluid
        wsObj = sol.getWS().toObj(True)
        wsObj.writeVTK('SWE/W' + str(i) + '.vtk')
        wsObj.writePov('SWE/W' + str(i) + '.pov', True)
        #write bottom
        if i==0:
            bs = sol.getBS()
            bs.writeVTK('SWE/B' + str(i) + '.vtk', True)
        
