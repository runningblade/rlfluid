#ifndef PARTICLE_SET_HASH_H
#define PARTICLE_SET_HASH_H

#include <CommonFile/CollisionFunc.h>

PRJ_BEGIN

template <typename T,typename PS_TYPE,typename EXTRACT_POS>
class ParticleSetHash;
template <typename T,typename PS_TYPE,typename EXTRACT_POS>
class ParticleSetList;
template <typename T,typename PS_TYPE,typename EXTRACT_POS>
class PSetImplicitFunc;

//adaptor
class NeighQueryN
{
  typedef ParticleSetList<scalar,ParticleSetN,ExtractPosParticle<ParticleN<scalar> > > IMPL;
  //typedef ParticleSetHash<scalar,ParticleSetN,ExtractPosParticle<ParticleN<scalar> > > IMPL;
public:
  NeighQueryN(const ParticleSetN& pset);
  void update(scalar cell);
  void find(const Vec3& ctr,scalar sqrRad,CollisionFunc<ParticleN<scalar> >& f) const;
  sizeType count(const Vec3& ctr,scalar sqrRad) const;
private:
  boost::shared_ptr<IMPL> _impl;
};
class PSetImplicitFuncN
{
  typedef PSetImplicitFunc<scalar,ParticleSetN,ExtractPosParticle<ParticleN<scalar> > > IMPL;
public:
  PSetImplicitFuncN(const ParticleSetN& pset,scalar rad,scalar qRad);
  void update();
  scalar operator()(const Vec3& pos) const;
  const ParticleSetN& getPSet() const;
  scalar getRad() const;
  scalar& getQRad();
  const scalar& getQRad() const;
  BBox<scalar> getBB() const;
private:
  boost::shared_ptr<IMPL> _impl;
};

PRJ_END

#endif
