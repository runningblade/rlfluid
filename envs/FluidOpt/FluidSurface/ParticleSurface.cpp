#include "ParticleSurface.h"
#include "ParticleSetHash.h"
#include "DTGrid.h"
#include <CommonFile/MarchingCube3DTable.h>

USE_PRJ_NAMESPACE

struct VertInfo {
  VertInfo():_trid(-1) {}
  void interp(Vec3 b,scalar ia,scalar ib) {
    b+=_pos;
    _pos=(b*ia-_pos*ib)/(ia-ib);
  }
  sizeType _off;
  sizeType _trid;
  Vec3 _pos;
};
struct TriInfo {
  TriInfo():_trid(-1) {
    for(int d=0; d<5; d++)
      _tid[d].setConstant(-1);
  }
  Vec3i _tid[5];
  sizeType _off;
  sizeType _trid;
};
template <int dim>
struct Choose {
  BOOST_STATIC_ASSERT(dim == 1);
  static sizeType nr(int marker,bool vol) {
    return vol ? nrTriangles2DV[marker] : nrTriangles2D[marker];
  }
  static sizeType choose(sizeType edge,const Eigen::Matrix<sizeType,4,1>& cid,const vector<VertInfo>& vid,char it,bool vol) {
    if(vol) {
      edge=idTriangles2DV[edge+it];

      switch(edge) {
      case 0:
        return vid[cid[0]*(dim+1)+1]._off;
      case 1:
        return vid[cid[1]*(dim+1)+1]._off;
      case 2:
        return vid[cid[0]*(dim+1)+0]._off;
      case 3:
        return vid[cid[3]*(dim+1)+0]._off;

      case 4:
        return cid[0];
      case 5:
        return cid[1];
      case 6:
        return cid[2];
      case 7:
        return cid[3];
      }
      return -1;
    } else {
      if(it == 2)
        return 0;
      else edge=idTriangles2D[edge+it];

      switch(edge) {
      case 0:
        return vid[cid[0]*(dim+1)+1]._off;
      case 1:
        return vid[cid[1]*(dim+1)+1]._off;
      case 2:
        return vid[cid[0]*(dim+1)+0]._off;
      case 3:
        return vid[cid[3]*(dim+1)+0]._off;
      }
      return -1;
    }
  }
  static int stride(bool vol) {
    return vol ? 12 : 4;
  }
  static int off(bool vol) {
    return vol ? 3 : 2;
  }
};
template <>
struct Choose<2> {
  static const int dim=2;
  static sizeType nr(int marker,bool vol) {
    return nrTriangles3D[marker];
  }
  static sizeType choose(sizeType edge,const Eigen::Matrix<sizeType,8,1>& cid,const vector<VertInfo>& vid,char it,bool vol) {
    edge=idTriangles3D[edge+it];
    switch(edge) {
    case 0:
      return vid[cid[0]*(dim+1)+2]._off;
    case 1:
      return vid[cid[1]*(dim+1)+0]._off;
    case 2:
      return vid[cid[3]*(dim+1)+2]._off;
    case 3:
      return vid[cid[0]*(dim+1)+0]._off;

    case 4:
      return vid[cid[4]*(dim+1)+2]._off;
    case 5:
      return vid[cid[5]*(dim+1)+0]._off;
    case 6:
      return vid[cid[7]*(dim+1)+2]._off;
    case 7:
      return vid[cid[4]*(dim+1)+0]._off;

    case 8:
      return vid[cid[0]*(dim+1)+1]._off;
    case 9:
      return vid[cid[1]*(dim+1)+1]._off;
    case 10:
      return vid[cid[2]*(dim+1)+1]._off;
    case 11:
      return vid[cid[3]*(dim+1)+1]._off;
    }
    return -1;
  }
  static int stride(bool vol) {
    return 20;
  }
  static int off(bool vol) {
    return 4;
  }
};
template <int dim>
class DTMarchingCube {
  typedef DTHash<dim> HASH;
 public:
  DTMarchingCube(ObjMesh& mesh,const PSetImplicitFuncN& func,scalar coef)
    :_mesh(mesh),_cellSz(func.getRad()*coef),_func(func) {}
  void marchingCube(bool debug,bool vol) {
    if(vol) {
      ASSERT(dim == 1)
    }
    //build grid
    buildGrid();
    if(debug) {
      DTHashIO<HASH>::writeScalarHash("./hash.vtk",_hash,_cellSz,_hashVal);
      _func.getPSet().writeVTK("./pset.vtk");
    }

    //build vertex
    vector<VertInfo> vid;
    buildVertex(vid,vol);
    if(debug) {
      VTKWriter<scalar> os("Vert","./vert.vtk",true);
      os.appendPoints(_mesh.getV().begin(),_mesh.getV().end());
      os.appendCells(VTKWriter<scalar>::IteratorIndex<Vec3i>(0,0,0),
                     VTKWriter<scalar>::IteratorIndex<Vec3i>(_mesh.getV().size(),0,0),
                     VTKWriter<scalar>::POINT);
    }

    //build index
    buildIndex(vid,vol);
    _mesh.setDim(vol ? 3 : dim+1);
    _mesh.smooth();
    if(debug)
      _mesh.writeVTK("./mesh.vtk",true);
  }
 private:
  void buildGrid() {
    _hash.clearCache();
    OMP_PARALLEL_FOR_
    for(sizeType i=0; i<_func.getPSet().size(); i++) {
      const ParticleN<scalar>& p=_func.getPSet()[i];
      _hash.insertCache(floorV(Vec3(p._pos/_cellSz)));
    }
    _hash.constructFromCache();
    _hash.dilateBy((int)convert<double>()(ceil(_func.getQRad()/_cellSz)+1));

    //build cell value
    _hashVal.resize(_hash.size());
    const sizeType nrPI=_hash.nrPIter();
    Vec3i id=Vec3i::Zero();
    OMP_PARALLEL_FOR_I(OMP_FPRI(id))
    for(sizeType i=0; i<nrPI; i++)
      for(typename HASH::PCITER it=_hash.getPIter(i); !it.isEnd(); it++) {
        it.getId(id);
        _hashVal[it.cellId()]=_func(id.cast<scalar>()*_cellSz);
      }
  }
  void buildVertex(vector<VertInfo>& vid,bool vol) {
    VertInfo inf;
    Vec3i id=Vec3i::Zero();
    Eigen::Matrix<sizeType,dim+2,1> cid;
    const sizeType nrPI=_hash.nrPIter();

    Vec3i stencil[4];
    stencil[0]=Vec3i(0,0,0);
    stencil[1]=Vec3i(1,0,0);
    stencil[2]=Vec3i(0,1,0);
    stencil[3]=Vec3i(0,0,1);
    vector<sizeType> nrV(OmpSettings::getOmpSettings().nrThreads(),0);
    vid.assign(_hash.size()*(dim+1),VertInfo());
    OMP_PARALLEL_FOR_I(OMP_FPRI(id,inf,cid))
    for(sizeType i=0; i<nrPI; i++) {
      inf._trid=OmpSettings::getOmpSettings().threadId();
      sizeType& nrVI=nrV[inf._trid];
      //this is valid for 2D/3D cases alike, of course at the cost of readability
      for(SPDTIter<dim+2,dim> it(_hash,stencil,i); !it.isEnd(); it++)
        if(it.cellId(cid)) {
          it.getId(id,0);
          for(char d=0; d<=dim; d++)
            if(_hashVal[cid[0]]*_hashVal[cid[1+d]] <= 0) {
              inf._off=nrVI++;
              inf._pos=id.cast<scalar>()*_cellSz;
              inf.interp(Vec3::Unit(d)*_cellSz,_hashVal[cid[0]],_hashVal[cid[1+d]]);
              vid[cid[0]*(dim+1)+d]=inf;
            }
        }
    }

    //insert vertex
    vector<sizeType> offV(OmpSettings::getOmpSettings().nrThreads()+1,0);
    offV[0]=vol ? _hash.size() : 0;
    for(sizeType i=1; i<(sizeType)offV.size(); i++)
      offV[i]=offV[i-1]+nrV[i-1];
    _mesh.getV().resize(offV.back());
    OMP_PARALLEL_FOR_
    for(sizeType i=0; i<_hash.size()*(dim+1); i++) {
      VertInfo& p=vid[i];
      if(p._trid == -1)
        continue;
      p._off+=offV[p._trid];
      ASSERT(p._off >= 0 && p._off < (sizeType)_mesh.getV().size())
      _mesh.getV()[p._off]=p._pos;
    }
    if(vol) {
      OMP_PARALLEL_FOR_I(OMP_FPRI(id))
      for(sizeType i=0; i<nrPI; i++)
        for(typename HASH::PCITER it=_hash.getPIter(i); !it.isEnd(); it++) {
          it.getId(id);
          _mesh.getV()[it.cellId()]=id.cast<scalar>()*_cellSz;
        }
    }
  }
  void buildIndex(const vector<VertInfo>& vid,bool vol) {
    Eigen::Matrix<sizeType,4*dim,1> cid;
    const sizeType nrPI=_hash.nrPIter();

    Vec3i stencil[8];
    if(dim == 1) {
      stencil[0]=Vec3i(0,0,0);
      stencil[1]=Vec3i(1,0,0);
      stencil[2]=Vec3i(1,1,0);
      stencil[3]=Vec3i(0,1,0);
    } else {
      stencil[0]=Vec3i(0,0,0);
      stencil[1]=Vec3i(0,0,1);
      stencil[2]=Vec3i(1,0,1);
      stencil[3]=Vec3i(1,0,0);
      stencil[4]=Vec3i(0,1,0);
      stencil[5]=Vec3i(0,1,1);
      stencil[6]=Vec3i(1,1,1);
      stencil[7]=Vec3i(1,1,0);
    }
    vector<sizeType> nrI(OmpSettings::getOmpSettings().nrThreads(),0);
    vector<TriInfo> iid(_hash.size());
    OMP_PARALLEL_FOR_I(OMP_PRI(cid))
    for(sizeType i=0; i<nrPI; i++)
      //this is valid for 2D/3D cases alike, of course at the cost of readability
      for(SPDTIter<4*dim,dim> it(_hash,stencil,i); !it.isEnd(); it++)
        if(it.cellId(cid)) {
          TriInfo& tinf=iid[cid[0]];
          tinf._trid=OmpSettings::getOmpSettings().threadId();
          tinf._off=nrI[tinf._trid];

          int marker=0;
          for(int f=0; f<4*dim; f++)
            if(_hashVal[cid[f]] < 0.0f)
              marker|=1<<f;

          int nrT=Choose<dim>::nr(marker,vol);
          for(int t=0,j=marker*Choose<dim>::stride(vol); t<nrT; t++,j+=Choose<dim>::off(vol)) {
            tinf._tid[t][0]=Choose<dim>::choose(j,cid,vid,0,vol);
            tinf._tid[t][1]=Choose<dim>::choose(j,cid,vid,1,vol);
            tinf._tid[t][2]=Choose<dim>::choose(j,cid,vid,2,vol);
          }
          nrI[tinf._trid]+=nrT;
        }

    //insert index
    vector<sizeType> offI(OmpSettings::getOmpSettings().nrThreads()+1,0);
    for(sizeType i=1; i<(sizeType)offI.size(); i++)
      offI[i]=offI[i-1]+nrI[i-1];
    _mesh.getI().resize(offI.back());
    OMP_PARALLEL_FOR_
    for(sizeType i=0; i<_hash.size(); i++) {
      const TriInfo& p=iid[i];
      if(p._trid == -1)
        continue;
      for(int i=0,j=p._off+offI[p._trid]; i<5 && p._tid[i][0] != -1; i++,j++) {
        ASSERT(j >= 0 && j < (sizeType)_mesh.getI().size())
        _mesh.getI()[j]=p._tid[i];
      }
    }
  }
  ObjMesh& _mesh;
  const scalar _cellSz;
  const PSetImplicitFuncN& _func;
  vector<scalar> _hashVal;
  HASH _hash;
};

ParticleSurface::ParticleSurface(const ParticleSetN& pset):_pset(pset) {}
void ParticleSurface::generate(unsigned char dim,scalar s,scalar h,bool vol,bool debug) {
  if(dim == 2) {
    PSetImplicitFuncN func(_pset,s,h);
    DTMarchingCube<1> mc(_mesh,func,0.5);
    mc.marchingCube(debug,vol);
  } else if(dim == 3) {
    PSetImplicitFuncN func(_pset,s,h);
    DTMarchingCube<2> mc(_mesh,func,0.5);
    mc.marchingCube(debug,false);
  } else {
    ASSERT_MSG(false,"dim must be 2 or 3!")
  }
}
const ObjMesh& ParticleSurface::getMesh() const {
  return _mesh;
}
