#ifndef PARTICLE_SURFACE_H
#define PARTICLE_SURFACE_H

#include <CommonFile/ObjMesh.h>
#include <CommonFile/ParticleSet.h>

PRJ_BEGIN

class ParticleSurface
{
public:
  ParticleSurface(const ParticleSetN& pset);
  void generate(unsigned char dim,scalar s,scalar h,bool vol,bool debug=false);
  const ObjMesh& getMesh() const;
private:
  ObjMesh _mesh;
  const ParticleSetN& _pset;
};

PRJ_END

#endif
