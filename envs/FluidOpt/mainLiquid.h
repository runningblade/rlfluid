#include <Fluid/Utils.h>
#include <Fluid/LiquidSolver.h>
#include <Fluid/ScriptedSource.h>
#include <Fluid/VariableSizedGridOp.h>
#include <FluidSurface/ParticleSurface.h>
#include <CommonFile/geom/StaticGeomCell.h>

USE_PRJ_NAMESPACE

void mainLiquid(bool BOTE,bool unilateral)
{
  //add fluid
  scalar sz=64.0f;
  Vec3i warpMode(1,0,0);
  LiquidSolverFLIP<2,EncoderConstant<1> > sol;
  sol.initDomain(BBox<scalar>(Vec3(0.0f,0.0f,0.0f),Vec3(3.0f,3.0f,0.0f)),Vec3(1.0f/sz,1.0f/sz,0.0f),&warpMode);
  //add solid
  {
    boost::shared_ptr<RigidSolver> rigid(new RigidSolver(2,1/sz));
    rigid->setBB(BBox<scalar>(Vec3::Constant(1/sz),Vec3::Constant(3.0f-1/sz)));
    {
      Mat4 T=Mat4::Identity();
      T.block<3,1>(0,3)=Vec3(1,0.5f,0);
      T.block<3,3>(0,0)=makeRotation<scalar>(Vec3(0,0,M_PI/4));
      boost::shared_ptr<StaticGeomCell> cell(new CapsuleGeomCell(T,2,0.1f,0.5f));
      rigid->addSolid(cell);
    }
    {
      rigid->addBox(Vec3(0.5f,0.1f,0),Vec3(2.1f,0.5f,0),Vec3::Zero());
      RigidBody& body=rigid->getBody(rigid->nrBody()-1);
      boost::shared_ptr<RigidBodyTrajectory> traj(new RigidBodyTrajectory);
      scalar V=-1,W=2;
      RigidBodyState state;
      state._x=Vec3(2,0.5f,0);
      state._q=RigidBody::Quat(Eigen::AngleAxis<scalar>(M_PI/2,Vec3::UnitZ()));
      state._p=body.getM()*Vec3(V,0,0);
      state._L=body.I()*Vec3::UnitZ()*W;
      //keyframe
      for(scalar i=0; i<10; i+=0.1f) {
        state._globalTime=i;
        state._x=Vec3(2+V*state._globalTime,0.5f,0);
        state._q=RigidBody::Quat(Eigen::AngleAxis<scalar>(M_PI/2+W*state._globalTime,Vec3::UnitZ()));
        traj->addKeyframe(state,0);
      }
      body.setTraj(traj);
    }
    sol.initSolid(rigid);
    sol.initLiquid(ImplicitRect(Vec3(0.0f,0.0f,0.0f),Vec3(1.5f,1.5f,0.0f),2,sz));
  }
  sol.setGlobalId(0);
  sol.setGlobalTime(0);
  //add source
  {
    boost::shared_ptr<SourceRegion> s(new RandomSource(1.0f));
    boost::shared_ptr<SourceImplicitFunc> sf(new ImplicitSphere(Vec3(12.0f,1.0f,0.0f),0.1f,2,1.0f/sz,Vec3(-3.0f,0.0f,0.0f)));
    boost::dynamic_pointer_cast<ImplicitFuncSource>(s)->_sources.push_back(sf);
    sol.addSource(s);
  }
  {
    boost::shared_ptr<SourceRegion> s(new RandomSource(1.0f));
    boost::shared_ptr<SourceImplicitFunc> sf(new ImplicitSphere(Vec3(15.0f,0.5f,0.0f),0.1f,2,1.0f/sz));
    boost::dynamic_pointer_cast<ImplicitFuncSource>(s)->_sources.push_back(sf);
    sf->_source=false;
    sol.addSource(s);
  }
  //save load
  {
    boost::filesystem::ofstream os("liquid.dat",ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    sol.write(os,dat.get());
  }
  {
    sol=LiquidSolverFLIP<2,EncoderConstant<1> >();
    boost::filesystem::ifstream is("liquid.dat",ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    sol.read(is,dat.get());
  }
  //simulate
  sol.focusOn(BBox<scalar>(Vec3(0,0,0),Vec3(1.5f,2.0f,0)));
  //sol.setUseVariational(false);
  sol.setUseUnilateral(unilateral);
  sol.setUseBOTE(BOTE);
  recreate("liquid");
  sol.getPSet().writeVTK("liquid/pSet.vtk");
  sol.getRigid().writeMeshVTK("liquid/rigid.vtk");
  VariableSizedGridOp<scalar,2,EncoderConstant<1> >::writeGridCellCenterVTK("liquid/phi.vtk",sol.getGrid(),sol.getPhi());
  GridOp<scalar,scalar>::write2DScalarGridVTK("liquid/solid.vtk",sol.getSolid(),false);
  for(sizeType i=0; i<500; i++) {
    INFOV("Simulating frame: %ld!",i)
    sol.advance(1E-2f);
    {
      std::ostringstream oss;
      oss << "liquid/pset" << i << ".vtk";
      sol.getPSet().writeVTK(oss.str());
    }
    {
      std::ostringstream oss;
      oss << "liquid/rigid" << i << ".vtk";
      sol.getRigid().writeMeshVTK(oss.str());
    }
    {
      const ObjMesh& mesh=sol.getSurfaceMesh(true);
      std::ostringstream oss;
      oss << "liquid/surface" << i << ".vtk";
      mesh.writeVTK(oss.str(),true);
    }
  }
}
