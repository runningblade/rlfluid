import pyFluidOpt as fo
import numpy as np
import math,random,os,shutil

def setTrans(T, v):
    for i in range(3):
        T(i,3,v[i])
def setRot(T, r):
    for i in range(3):
        for j in range(3):
            T(i,j,r(i,j))
if __name__ == '__main__':
    #setup
    sz=64.0
    sol=fo.SolverFLIP2DConstant1()
    sol.initDomain(fo.BBox(fo.Vec3(0,0,0),fo.Vec3(3,3,0)),fo.Vec3(1.0/sz,1.0/sz,0),fo.Vec3i(1,0,0))
    #add solid
    rigid=fo.RigidSolver(2,1/sz)
    rigid.BB=fo.BBox(fo.Vec3(1/sz,1/sz,1/sz),fo.Vec3(3.0-1/sz,3.0-1/sz,3.0-1/sz))
    rigid.addBall(0.2,fo.Vec3(1.5,2.5,0))
    b=rigid.getBody(0)
    b.setP(fo.Vec3(b.getM(),0,0))
    b.rho=1500.0
    sol.initSolid(rigid)
    #add liquid
    sol.initLiquid(fo.ImplicitRect(fo.Vec3(-1.0,0,0),fo.Vec3(4.0,1.5,0),2,sz,fo.Vec3(0,0,0),0,True,False),1.0)
    #init
    sol.globalId=0
    sol.globalTime=0
    #simulate
    feature=fo.VectorField()
    sol.focusOn1(fo.BBox(fo.Vec3(0,0,0),fo.Vec3(1.5,2.0,0)))
    sol.setUseUnilateral(True)
    sol.setUseVariational(False)
    sol.setUseBOTE(True)
    if os.path.exists('rigidLiquid'):
        shutil.rmtree('rigidLiquid')
    os.mkdir('rigidLiquid')
    sol.getPSet().writeVTK('rigidLiquid/pSet.vtk')
    for i in range(500):
        print('Simulating frame: %d!'%i)
        sol.focusOn2(0,1)
        sol.advance(1e-2)
        sol.getFeature(feature,1)
        feature.writeVTK('rigidLiquid/feature'+str(i)+'.vtk')
        sol.getPSet().writeVTK('rigidLiquid/pset'+str(i)+'.vtk')
        sol.getRigid().writeMeshVTK('rigidLiquid/rigid'+str(i)+'.vtk')
        sol.getSurfaceMesh(True,6).writeVTK('rigidLiquid/surface'+str(i)+'.vtk')