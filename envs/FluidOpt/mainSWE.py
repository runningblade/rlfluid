import pyFluidOpt as fo
import numpy as np
import math, random, os, shutil


def setTrans(T, v):
    for i in range(3):
        T(i, 3, v[i])


def setRot(T, r):
    for i in range(3):
        for j in range(3):
            T(i, j, r(i, j))


if __name__ == '__main__':
    # setup
    sz = 64.0
    sol = fo.SolverSWE2DConstant1()
    sol.initDomain(fo.BBox(fo.Vec3(0, 0, 0), fo.Vec3(3, 3, 0)), fo.Vec3(1.0 / sz, 1.0 / sz, 0), fo.Vec3i(1, 0, 0))
    # add solid
    rigid = fo.RigidSolver(3, 1 / sz)
    rigid.BB = fo.BBox(fo.Vec3(1 / sz, 1 / sz, 1 / sz), fo.Vec3(3.0 - 1 / sz, 3.0 - 1 / sz, 3.0 - 1 / sz))
    T = fo.Mat4()
    R = fo.Mat3()
    T.setIdentity()
    setTrans(T, fo.Vec3(1.5, 1.5, -0.1))
    rigid.addSolid(fo.BoxGeomCell(T, 3, fo.Vec3(2, 2, 0.1), 0, False))
    setTrans(T, fo.Vec3(1.5, 1.5, 0.1))
    R.fromLog(fo.Vec3(math.pi / 6.0, 0, 0))
    setRot(T, R)
    rigid.addSolid(fo.BoxGeomCell(T, 3, fo.Vec3(2, 2, 0.1), 0, False))
    sol.initSolid(rigid)
    sol.initLiquidLevel(1.0, 1)
    # rigid
    rigid.addBox2(fo.Vec3(0.1, 0.1, 0.1), fo.Vec3(1.5, 2.5, 2.5),
                  fo.Vec3(random.random(), random.random(), random.random()))
    rigid.getBody(0).rho = 500.0
    rigid.getBody(0).setP(fo.Vec3(5, 0, 0))
    # source
    s = fo.RandomSource()
    sf = fo.ImplicitRect(fo.Vec3(2.4, 2.4, 0.9), fo.Vec3(2.6, 2.6, 1.05), 3, 1.0 / sz, fo.Vec3(0, 0, 0), 0, True, False)
    s.addSource(sf)
    sol.addSource(s)
    # source2
    s2 = fo.RandomSource()
    sf2 = fo.ImplicitRect(fo.Vec3(0.4, 0.4, 0.9), fo.Vec3(0.6, 0.6, 1.05), 3, 1.0 / sz, fo.Vec3(0, 0, 0), 0, True,
                          False)
    sf2._source = False
    s2.addSource(sf2)
    sol.addSource(s2)
    # init
    sol.globalId = 0
    sol.globalTime = 0
    # sol.CDrag = 0
    # sol.CDragLiquid = 5
    # sol.CLift = 0
    # simulate
    sol.focusOn1(fo.BBox(fo.Vec3(0, 0, 0), fo.Vec3(1.5, 2.0, 0)))
    if os.path.exists('SWE'):
        shutil.rmtree('SWE')
    os.mkdir('SWE')
    sol.getGrid().writeCellCenterVTK(sol.getW(), 'SWE/W.vtk', True)
    sol.getGrid().writeNodalVTK(sol.getB(), 'SWE/B.vtk', True)
    for i in range(500):
        print('Simulating frame: %d!' % i)
        sol.advance(1e-2)
        sol.focusOn2(0, 1)
        sol.getRigid().writeMeshVTK('SWE/rigid' + str(i) + '.vtk')
        # I added a function getWS(), the returned ws is a ScalarField (representing the bottom)
        ws = sol.getWS()
        # ws.writeVTK('SWE/W'+str(i)+'.vtk',True)
        wsObj = ws.toObj(True)
        wsObj.subdivide(1)
        wsObj.writeVTK('SWE/W' + str(i) + '.vtk')
        wsObj.writePov('SWE/W' + str(i) + '.pov', True)
        # I added a function getBS(), the returned bs is a ScalarField (representing the bottom)
        bs = sol.getBS()
        bs.writeVTK('SWE/B' + str(i) + '.vtk', True)
        comment = True
        if comment:
            # I will explain more about bs/ws. Let's use bs as an example
            # bs is a 2D uniform grid, the number grid points in x direction is:
            print(bs.getNrPoint()[0])
            # the number grid points in y direction is:
            print(bs.getNrPoint()[1])
            # the position of grid point (i=5,j=5) is:
            ij = fo.Vec3i(5, 5, 0)
            pos = bs.getPt(ij)  # this will give you the xy position
            pos[2] = bs[ij]  # this will give you the z position, aka value of B/W
            print(pos)
