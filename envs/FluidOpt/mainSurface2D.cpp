#include <FluidSurface/ParticleSurface.h>

USE_PRJ_NAMESPACE

int main(int argc, char** argv) {
  if(argc < 2) {
    WARNING("Usage: [path] [s] [h]")
    return EXIT_FAILURE;
  }

  //read particle set
  ParticleSetN pset;
  std::string line,path(argv[1]);
  std::ifstream infile(path);
  while (std::getline(infile, line)) {
    scalar x, y;
    std::istringstream iss(line);
    if(!(iss >> x >> y)) {
      break;
    }
    //add to particle set
    ParticleN<scalar> p;
    p._pos[0]=x;
    p._pos[1]=y;
    p._pos[2]=0;
    pset.addParticle(p);
  }

  //reconstruct
  ParticleSurface surface(pset);
  scalar s=argc>2 ? atof(argv[2]) : 0.001;
  scalar h=argc>3 ? atof(argv[3]) : s*5;
  INFOV("Generating mesh using parameters: s=%f, h=%f!",s,h)
  surface.generate(2,s,h,true);
  //vtk
  path=path.substr(0,path.size()-4)+".vtk";
  INFOV("Writing to %s!",path.c_str())
  surface.getMesh().writeVTK(path,true);
  //pov
  path=path.substr(0,path.size()-4)+".pov";
  INFOV("Writing to %s!",path.c_str())
  surface.getMesh().writePov(path,true);
  return EXIT_SUCCESS;
}
