#include <Fluid/Utils.h>
#include <Fluid/SmokeSolver.h>
#include <Fluid/ScriptedSource.h>
#include <Fluid/VariableSizedGridOp.h>
#include <CommonFile/geom/StaticGeomCell.h>

USE_PRJ_NAMESPACE

void mainRigidSmoke(bool BOTE)
{
  //add fluid
  scalar sz=64.0f;
  Vec3i warpMode(1,0,0);
  SmokeSolver<2,EncoderConstant<1> > sol;
  sol.initDomain(BBox<scalar>(Vec3(0.0f,0.0f,0.0f),Vec3(3.0f,4.0f,0.0f)),Vec3(1.0f/sz,1.0f/sz,0.0f),&warpMode);
  //add solid
  {
    boost::shared_ptr<RigidSolver> rigid(new RigidSolver(2,1/sz));
    rigid->setBB(BBox<scalar>(Vec3(1/sz,1/sz,0),Vec3(3.0f-1/sz,4.0f+100/sz,0)));
    rigid->addCross(Vec3(0.4f,0.1f,0),Vec3(1.0f,2.5f,0),Vec3(0,0,0));
    //rigid->addCross(Vec3(0.4f,0.1f,0),Vec3(2.0f,2.5f,0),Vec3(0,0,0));
    rigid->getBody(0).rho()=2000.0f;
    //rigid->getBody(1).rho()=2000.0f;
    sol.initSolid(rigid);
  }
  sol.setGlobalId(0);
  sol.setGlobalTime(0);
  sol.setVelDamping(0);
  sol.setDamping(0);
  //add source
  {
    boost::shared_ptr<RandomSource> s(new RandomSource(1.0f));
    boost::shared_ptr<SourceImplicitFunc> sf(new ImplicitSphere(Vec3(1.1f,2.0f,0.0f),0.1f,2,1.0f/sz,Vec3(0.0f,25.0f,0.0f)));
    boost::dynamic_pointer_cast<ImplicitFuncSource>(s)->_sources.push_back(sf);
    s->setT(20);
    s->setRho(0.8f);
    sol.addSource(s);
  }
  //save load
  {
    boost::filesystem::ofstream os("rigidSmoke.dat",ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    sol.write(os,dat.get());
  }
  {
    sol=SmokeSolver<2,EncoderConstant<1> >();
    boost::filesystem::ifstream is("rigidSmoke.dat",ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    sol.read(is,dat.get());
  }
  //simulate
  VectorField feature;
  sol.focusOn(BBox<scalar>(Vec3(0,0,0),Vec3(1.5f,2.0f,0)));
  //sol.setUseVariational(false);
  sol.setUseBOTE(BOTE);
  recreate("rigidSmoke");
  for(sizeType i=0; i<500; i++) {
    INFOV("Simulating frame: %ld!",i)
    sol.LiquidSolverPIC<2,EncoderConstant<1> >::focusOn(0);
    sol.advance(1E-2f);
    sol.getFeature(feature,0);
    {
      std::ostringstream oss;
      oss << "rigidSmoke/rho" << i << ".vtk";
      VariableSizedGridOp<scalar,2,EncoderConstant<1> >::writeGridCellCenterVTK(oss.str(),sol.getGrid(),sol.getRho());
    }
    {
      std::ostringstream oss;
      oss << "rigidSmoke/T" << i << ".vtk";
      VariableSizedGridOp<scalar,2,EncoderConstant<1> >::writeGridCellCenterVTK(oss.str(),sol.getGrid(),sol.getT());
    }
    {
      std::ostringstream oss;
      oss << "rigidSmoke/rigid" << i << ".vtk";
      sol.getRigid().writeMeshVTK(oss.str());
    }
    {
      std::ostringstream oss;
      oss << "rigidSmoke/feature" << i << ".vtk";
      GridOp<scalar,scalar>::write2DVectorGridVTK(oss.str(),feature,0.01f);
    }
  }
}
