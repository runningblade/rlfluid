#include <Fluid/Utils.h>
#include <Fluid/ScriptedSource.h>
#include <Fluid/ShallowWaterSolver.h>
#include <Fluid/VariableSizedGridOp.h>
#include <FluidSurface/ParticleSurface.h>
#include <CommonFile/geom/StaticGeomCell.h>
#include <CommonFile/RotationUtil.h>

USE_PRJ_NAMESPACE

void mainShallowEnv()
{
  //add fluid
  scalar sz=64.0f;
  Vec3i warpMode(0,0,0);
  ShallowWaterSolver<2,EncoderConstant<1> > sol;
  sol.initDomain(BBox<scalar>(Vec3(0.0f,0.0f,0.0f),Vec3(3.0f,3.0f,3.0f)),Vec3(1.0f/sz,1.0f/sz,0.0f),&warpMode);
  //add floor
  boost::shared_ptr<RigidSolver> rigid(new RigidSolver(3,-1));
  rigid->setBB(BBox<scalar>(Vec3::Constant(1/sz),Vec3::Constant(3.0f-1/sz)));
  Mat4 T=Mat4::Identity();
  T.block<3,1>(0,3)=Vec3(1.5f,1.5f,0);
  rigid->addSolid(boost::shared_ptr<StaticGeomCell>(new BoxGeomCell(T,3,Vec3(3,3,0.1f),0,false)));
  //add solid
  scalarD trapH=0.5f;
  scalarD trapT=0.2f;
  //solid0
  T.block<3,1>(0,3)=Vec3(1.5f,1.5f-trapT/2,0);
  T.block<3,3>(0,0)=expWGradV<scalarD>(Vec3(M_PI/4,0,0),NULL,NULL,NULL,NULL);
  rigid->addSolid(boost::shared_ptr<StaticGeomCell>(new BoxGeomCell(T,3,Vec3(3,sqrt(2.0f)*trapH/2,sqrt(2.0f)*trapH/2),0,false)));
  //solid1
  T.block<3,1>(0,3)=Vec3(1.5f,1.5f+trapT/2,0);
  T.block<3,3>(0,0)=expWGradV<scalarD>(Vec3(M_PI/4,0,0),NULL,NULL,NULL,NULL);
  rigid->addSolid(boost::shared_ptr<StaticGeomCell>(new BoxGeomCell(T,3,Vec3(3,sqrt(2.0f)*trapH/2,sqrt(2.0f)*trapH/2),0,false)));
  //solid2
  T.block<3,1>(0,3)=Vec3(1.5f,1.5f,0);
  T.block<3,3>(0,0)=expWGradV<scalarD>(Vec3(0,0,0),NULL,NULL,NULL,NULL);
  rigid->addSolid(boost::shared_ptr<StaticGeomCell>(new BoxGeomCell(T,3,Vec3(3,trapT/2,trapH),0,false)));
  //add ball
  rigid->addBall(Vec3(0.1,0.1,0.1),Vec3(1.5,0.5,1),Vec3(0,0,0));
  rigid->getBody(0).rho()=100.0;
  sol.initSolid(rigid);
  sol.initLiquidLevel(0.37,1);
  sol.setTheta(1.0f);
  sol.setCDrag(2000.0f);
  //add source
  sizeType srcNum=11;
  bool jet=true;
  for(sizeType i=0;i<srcNum;i++) {
    boost::shared_ptr<SourceRegion> s(new RandomSource());
    boost::shared_ptr<SourceImplicitFunc> sf(new ImplicitRect(Vec3((i+0)*3/srcNum-1/sz,-1/sz,0.4),
                                                              Vec3((i+1)*3/srcNum+1/sz, 2/sz,0.4),
                                                              3,1.0f/sz,Vec3(0,jet?1:0,0),
                                                              0,true,false));
    boost::dynamic_pointer_cast<ImplicitFuncSource>(s)->_sources.push_back(sf);
    sol.addSource(s);
  }

  sol.setGlobalId(0);
  sol.setGlobalTime(0);
  //simulate
  recreate("SWE");
  //sol.focusOn(BBox<scalar>(Vec3(0,0,0),Vec3(3.0f,3.0f,0)));
  VariableSizedGridOp<scalar,2,EncoderConstant<1> >::writeGridNodalVTK("SWE/B.vtk",sol.getGrid(),sol.getB(),true);
  VariableSizedGridOp<scalar,2,EncoderConstant<1> >::writeGridCellCenterVTK("SWE/W.vtk",sol.getGrid(),sol.getW(),true);
  sol.getRigid().writeMeshVTK("SWE/rigid.vtk");
  for(sizeType i=0; i<500; i++) {
    INFOV("Simulating frame: %ld!",i)
    sol.advance(1E-2f);
    sol.LiquidSolverPIC<2,EncoderConstant<1> >::focusOn(0);
    if(sol.getRigid().nrBody() > 0) {
      std::ostringstream oss;
      oss << "SWE/rigid" << i << ".vtk";
      sol.getRigid().getBody(0).writeMeshVTK(oss.str());
    }
    {
      std::ostringstream oss;
      oss << "SWE/W" << i << ".vtk";
      GridOp<scalar,scalar>::write2DScalarGridVTK(oss.str(),sol.getWS(),true);
      //VariableSizedGridOp<scalar,2,EncoderConstant<1> >::writeGridCellCenterVTK(oss.str(),sol.getGrid(),sol.getW(),true);
    }
    if(i == 0) {
      std::ostringstream oss;
      oss << "SWE/B" << i << ".vtk";
      GridOp<scalar,scalar>::write2DScalarGridVTK(oss.str(),sol.getBS(),true);
      //VariableSizedGridOp<scalar,2,EncoderConstant<1> >::writeGridCellCenterVTK(oss.str(),sol.getGrid(),sol.getW(),true);
    }
  }
}
