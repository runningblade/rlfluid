#ifndef IO_BASIC_H
#define IO_BASIC_H

#include "MathBasic.h"
#include <iostream>
#include <boost/weak_ptr.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/unordered_map.hpp>
#include <Eigen/Sparse>

PRJ_BEGIN

//io for basic type
struct IOData;
typedef Eigen::Triplet<scalarF,sizeType> TripF;
typedef Eigen::Triplet<scalarD,sizeType> TripD;
#define IO_BASIC_DECL(T)	\
ostream& writeBinaryData(const T& val,ostream& os,IOData* dat=NULL);	\
istream& readBinaryData(T& val,istream& is,IOData* dat=NULL);
IO_BASIC_DECL(char)
IO_BASIC_DECL(unsigned char)
IO_BASIC_DECL(short)
IO_BASIC_DECL(unsigned short)
IO_BASIC_DECL(int)
IO_BASIC_DECL(unsigned int)
//IO_BASIC_DECL(scalarD)
IO_BASIC_DECL(bool)
IO_BASIC_DECL(sizeType)
IO_BASIC_DECL(TripF)
IO_BASIC_DECL(TripD)
#undef IO_BASIC_DECL

//io for string
istream& readBinaryData(string& str,istream& is,IOData* dat=NULL);
ostream& writeBinaryData(const string& str,ostream& os,IOData* dat=NULL);

//minimal serializable system
struct IOData;
struct Serializable {
  explicit Serializable(const string& className):_type(className) {}
  virtual ~Serializable() {}
  virtual bool read(istream& is) {
    ASSERT_MSG(false,"Not Implemented!");
    return false;
  }
  virtual bool write(ostream& os) const {
    ASSERT_MSG(false,"Not Implemented!");
    return false;
  }
  virtual bool read(istream& is,IOData* dat) {
    return read(is);
  }
  virtual bool write(ostream& os,IOData* dat) const {
    return write(os);
  }
  virtual boost::shared_ptr<Serializable> copy() const {
    ASSERT_MSG(false,"Not Implemented!");
    return boost::shared_ptr<Serializable>(new Serializable(_type));
  }
  const string type() const {
    return _type;
  }
  bool operator<(const Serializable& other) const {
    return type() < other.type();
  }
protected:
  void setType(const string& type) {
    _type=type;
  }
  void setSerializableType(const string& type) {
    _type=type;
  }
  string _type;
};
boost::shared_ptr<IOData> getIOData();
ostream& writeSerializableData(boost::shared_ptr<Serializable> val,ostream& os,IOData* dat=NULL);
istream& readSerializableData(boost::shared_ptr<Serializable>& val,istream& is,IOData* dat=NULL);
void registerType(IOData* dat,boost::shared_ptr<Serializable> type);
template <typename T>FORCE_INLINE void registerType(IOData* dat)
{
  registerType(dat,boost::shared_ptr<Serializable>(new T));
}

//io for shared_ptr
template <typename T>FORCE_INLINE ostream& writeBinaryData(boost::shared_ptr<T> val,ostream& os,IOData* dat=NULL)
{
  boost::shared_ptr<Serializable> valS=boost::dynamic_pointer_cast<Serializable>(val);
  return writeSerializableData(valS,os,dat);
}
template <typename T>FORCE_INLINE istream& readBinaryData(boost::shared_ptr<T>& val,istream& is,IOData* dat=NULL)
{
  boost::shared_ptr<Serializable> valS;
  readSerializableData(valS,is,dat);
  val=boost::dynamic_pointer_cast<T>(valS);
  return is;
}
template <typename T>FORCE_INLINE ostream& writeBinaryData(boost::weak_ptr<T> val,ostream& os,IOData* dat=NULL)
{
  boost::shared_ptr<T> val_shared=val.lock();
  boost::shared_ptr<Serializable> valS=boost::dynamic_pointer_cast<Serializable>(val_shared);
  return writeSerializableData(valS,os,dat);
}
template <typename T>FORCE_INLINE istream& readBinaryData(boost::weak_ptr<T>& val,istream& is,IOData* dat=NULL)
{
  boost::shared_ptr<Serializable> valS;
  readSerializableData(valS,is,dat);
  val=boost::dynamic_pointer_cast<T>(valS);
  return is;
}
FORCE_INLINE ostream& writeBinaryData(const Serializable& val,ostream& os,IOData* dat=NULL)
{
  val.write(os,dat);
  return os;
}
FORCE_INLINE istream& readBinaryData(Serializable& val,istream& is,IOData* dat=NULL)
{
  val.read(is,dat);
  return is;
}

//io for float is double
ostream& writeBinaryData(scalarF val,ostream& os,IOData* dat=NULL);
istream& readBinaryData(scalarF& val,istream& is,IOData* dat=NULL);
ostream& writeBinaryData(scalarD val,ostream& os,IOData* dat=NULL);
istream& readBinaryData(scalarD& val,istream& is,IOData* dat=NULL);

//io for fixed matrix
#define NO_CONFLICT
#define FIXED_ONLY
#include "BeginAllEigen.h"
//redefine atomic operation
#undef NAME_EIGEN
#define NAME_EIGEN(type,NAME,size1,size2) \
ostream& writeBinaryData(const Eigen::Matrix<type,size1,size2>& v,ostream& os,IOData* dat=NULL);	\
istream& readBinaryData(Eigen::Matrix<type,size1,size2>& v,istream& is,IOData* dat=NULL);
//realize
NAME_EIGEN_ROWCOL_ALLTYPES_SPECIALSIZE()
NAME_EIGEN_MAT_ALLTYPES_SPECIALSIZE()
#include "EndAllEigen.h"

//io for non-fixed matrix
#define NO_CONFLICT
#define NON_FIXED_ONLY
#include "BeginAllEigen.h"
//redefine atomic operation
#undef NAME_EIGEN
#define NAME_EIGEN(type,NAME,size1,size2) \
ostream& writeBinaryData(const Eigen::Matrix<type,size1,size2>& v,ostream& os,IOData* dat=NULL);	\
istream& readBinaryData(Eigen::Matrix<type,size1,size2>& v,istream& is,IOData* dat=NULL);
//realize
NAME_EIGEN_ROWCOL_ALLTYPES_SPECIALSIZE()
#include "EndAllEigen.h"

//io for quaternion
#define IO_FIXED_QUAT_DECL(NAMEQ,NAMET,NAMEA)	\
ostream& writeBinaryData(const NAMEQ& v,ostream& os,IOData* dat=NULL);	\
istream& readBinaryData(NAMEQ& v,istream& is,IOData* dat=NULL);         \
ostream& writeBinaryData(const NAMET& v,ostream& os,IOData* dat=NULL);	\
istream& readBinaryData(NAMET& v,istream& is,IOData* dat=NULL);         \
ostream& writeBinaryData(const NAMEA& v,ostream& os,IOData* dat=NULL);	\
istream& readBinaryData(NAMEA& v,istream& is,IOData* dat=NULL);
IO_FIXED_QUAT_DECL(Quatd,Transd,Affined)
IO_FIXED_QUAT_DECL(Quatf,Transf,Affinef)
#undef IO_FIXED_QUAT_DECL

//io for bounding box
#define IO_BB_DECL(NAME,D)	\
ostream& writeBinaryData(const BBox<NAME,D>& b,ostream& os,IOData* dat=NULL);	\
istream& readBinaryData(BBox<NAME,D>& b,istream& is,IOData* dat=NULL);
IO_BB_DECL(scalarD,3)
IO_BB_DECL(scalarD,2)
IO_BB_DECL(scalarF,3)
IO_BB_DECL(scalarF,2)
IO_BB_DECL(unsigned char,3)
IO_BB_DECL(unsigned char,2)
IO_BB_DECL(sizeType,3)
IO_BB_DECL(sizeType,2)
#undef IO_BB_DECL

//io for unordered_map
template <typename K,typename V,typename H>
FORCE_INLINE ostream& writeBinaryData(const boost::unordered_map<K,V,H>& val,ostream& os,IOData* dat=NULL)
{
  sizeType nr=(sizeType)val.size();
  writeBinaryData(nr,os);
  for(typename boost::unordered_map<K,V,H>::const_iterator beg=val.begin(),end=val.end();beg!=end;beg++) {
    writeBinaryData(beg->first,os,dat);
    writeBinaryData(beg->second,os,dat);
  }
  return os;
}
template <typename K,typename V,typename H>
FORCE_INLINE istream& readBinaryData(boost::unordered_map<K,V,H>& val,istream& is,IOData* dat=NULL)
{
  K k;
  V v;
  sizeType nr;
  val.clear();
  readBinaryData(nr,is);
  for(sizeType i=0;i<nr;i++) {
    readBinaryData(k,is,dat);
    readBinaryData(v,is,dat);
    val[k]=v;
  }
  return is;
}

PRJ_END

#endif
