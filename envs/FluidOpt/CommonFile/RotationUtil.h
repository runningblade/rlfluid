#ifndef ROTATION_UTIL_H
#define ROTATOIN_UTIL_H

#include "MathBasic.h"
//#include <iostream>

PRJ_BEGIN

//basic
template <typename T>
typename ScalarUtil<T>::ScalarMat3 cross(const typename ScalarUtil<T>::ScalarVec3& v);
template <typename T>
typename ScalarUtil<T>::ScalarMat3 crossD(sizeType d);
template <typename T>
typename Eigen::Matrix<T,9,9> rotationCoef(const typename ScalarUtil<T>::ScalarMat3& r);
template <typename T>
typename ScalarUtil<T>::ScalarVec3 invCross(const typename ScalarUtil<T>::ScalarMat3& wCross);
template <typename T> //trace([w]*m) = w.dot(invCrossMatTrace(m))
typename ScalarUtil<T>::ScalarVec3 invCrossMatTrace(const typename ScalarUtil<T>::ScalarMat3& m);
template <typename T> //trace([wA]*m*[wB]) = wA.dot(invDoubleCrossMatTrace(m)*wB)
typename ScalarUtil<T>::ScalarMat3 invDoubleCrossMatTrace(const typename ScalarUtil<T>::ScalarMat3& m);
//rotation along X,Y
template <typename T>
typename ScalarUtil<T>::ScalarMat3 expWYZ(T y,T z,
    typename ScalarUtil<T>::ScalarVec3* DRDY,
    typename ScalarUtil<T>::ScalarVec3* DRDZ,
    typename ScalarUtil<T>::ScalarVec3* DRDYDZ);
//rotation using rodriguez formulation
template <typename T>
typename ScalarUtil<T>::ScalarMat3 expWGradInte(const typename ScalarUtil<T>::ScalarVec3& w);
template <typename T>
typename ScalarUtil<T>::ScalarVec3 invExpW(const typename ScalarUtil<T>::ScalarMat3& R);
template <typename T>
typename ScalarUtil<T>::ScalarMat3 expWGradV(const typename ScalarUtil<T>::ScalarVec3& w,
    typename ScalarUtil<T>::ScalarMat3 diffW[3],
    typename ScalarUtil<T>::ScalarMat3 ddiffW[3][3],
    const typename ScalarUtil<T>::ScalarVec3* wDotT,
    typename ScalarUtil<T>::ScalarMat3 diffWDotT[3],
    typename ScalarUtil<T>::ScalarVec3 diffVOut[3]=NULL,
    typename ScalarUtil<T>::ScalarVec3 vOut[3][3]=NULL);
template <typename T>
void debugExpWGrad(bool debugBasic=true,bool debugDiff=true,bool debugDiffDotT=true,bool debugInte=true,int maxIt=1000);
template <typename T>
void debugInvExpW(int maxIt=1000);
//euler-angle
template <typename T>
typename ScalarUtil<T>::ScalarMat3 rotationX(T angle,bool zero);
template <typename T>
typename ScalarUtil<T>::ScalarMat3 rotationY(T angle,bool zero);
template <typename T>
typename ScalarUtil<T>::ScalarMat3 rotationZ(T angle,bool zero);
template <typename T>
typename ScalarUtil<T>::ScalarMat3 rotationEulerDeriv(typename ScalarUtil<T>::ScalarVec3 xyz,typename ScalarUtil<T>::ScalarMat3 dR[3],typename ScalarUtil<T>::ScalarMat3 ddR[3][3]);
template <typename T>
void debugRotationEulerDeriv();
//se/SE(3) operations
template <typename T>
typename ScalarUtil<T>::ScalarMat4 exponentialSE3(const typename ScalarUtil<T>::ScalarVec6& se3);
template <typename T>
typename ScalarUtil<T>::ScalarVec6 logarithmSE3(const typename ScalarUtil<T>::ScalarMat4& SE3);
template <typename T>
void debugExpLogSE3(int maxIt=1000);
//rotation-strain transformation
template <typename T>
typename ScalarUtil<T>::ScalarMat3 calcFGrad(const typename ScalarUtil<T>::ScalarMat3& RS,
    typename ScalarUtil<T>::ScalarMat3 diffRS[9],
    typename ScalarUtil<T>::ScalarMat3 ddiffRS[9][9],
    const typename ScalarUtil<T>::ScalarMat3* RSDotT,
    typename ScalarUtil<T>::ScalarMat3 diffRSDotT[9]);
template <typename T>
typename ScalarUtil<T>::ScalarMat3 calcFGradDU(const typename ScalarUtil<T>::ScalarMat3& U);
template <typename T>
typename ScalarUtil<T>::ScalarMat3 calcFGradDDU(const typename ScalarUtil<T>::ScalarMat3& U);
template <typename T>
typename ScalarUtil<T>::ScalarMat3 calcFGradDUDV(const typename ScalarUtil<T>::ScalarMat3& U,const typename ScalarUtil<T>::ScalarMat3& V);
template <typename T>
typename ScalarUtil<T>::ScalarMat3 calcFGrad(const typename ScalarUtil<T>::ScalarMat3& F,typename ScalarUtil<T>::ScalarMat9* diffRS=NULL);
template <typename T>
void debugCalcFGrad(bool debugDiff=true,bool debugDDiff=true,bool debugDiffDotT=true,bool debugDUDV=true,int maxIt=1000);
//svd gradient
template <typename T>
void adjustF3D(typename ScalarUtil<T>::ScalarMat3& F,
                      typename ScalarUtil<T>::ScalarMat3& U,
                      typename ScalarUtil<T>::ScalarMat3& V,
                      typename ScalarUtil<T>::ScalarVec3& S);
template <typename T>
void adjustF2D(typename ScalarUtil<T>::ScalarMat3& F,
                      typename ScalarUtil<T>::ScalarMat3& U,
                      typename ScalarUtil<T>::ScalarMat3& V,
                      typename ScalarUtil<T>::ScalarVec3& S);
template <typename T>
void derivSVD(typename ScalarUtil<T>::ScalarMat3& derivU,
                     typename ScalarUtil<T>::ScalarVec3& derivD,
                     typename ScalarUtil<T>::ScalarMat3& derivV,
                     const typename ScalarUtil<T>::ScalarMat3& LHS,
                     const typename ScalarUtil<T>::ScalarMat3& U,
                     const typename ScalarUtil<T>::ScalarVec3& D,
                     const typename ScalarUtil<T>::ScalarMat3& V);
template <typename T>
void derivSVDDir(typename ScalarUtil<T>::ScalarMat3& derivU,
                        typename ScalarUtil<T>::ScalarVec3& derivD,
                        typename ScalarUtil<T>::ScalarMat3& derivV,
                        const typename ScalarUtil<T>::ScalarMat3& dir,
                        const typename ScalarUtil<T>::ScalarMat3& U,
                        const typename ScalarUtil<T>::ScalarVec3& D,
                        const typename ScalarUtil<T>::ScalarMat3& V);
template <typename T>
void derivRDF(typename ScalarUtil<T>::ScalarMat9& DRDF,
                     const typename ScalarUtil<T>::ScalarMat3& F,
                     const typename ScalarUtil<T>::ScalarMat3& U,
                     const typename ScalarUtil<T>::ScalarVec3& D,
                     const typename ScalarUtil<T>::ScalarMat3& V);
template <typename T>
void debugDerivSVD(bool debugSVD=true,bool debugRDF=true,int maxIt=1000);

PRJ_END

#endif
