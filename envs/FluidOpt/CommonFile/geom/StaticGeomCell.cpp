#include "StaticGeomCell.h"
#include "BVHBuilder.h"
#include "../ImplicitFunc.h"
#include "../MakeMesh.h"
#include <boost/filesystem/operations.hpp>
#include <boost/lexical_cast.hpp>

USE_PRJ_NAMESPACE

//BoxGeomCell
BoxGeomCell::BoxGeomCell():StaticGeomCell(typeid(BoxGeomCell).name()) {}
BoxGeomCell::BoxGeomCell(const Mat4& T,sizeType dim,const Vec3& ext,scalar depth,bool insideOut)
  :StaticGeomCell(T,dim,typeid(BoxGeomCell).name()),_ext(ext),_depth(depth),_insideOut(insideOut)
{
  build();
}
bool BoxGeomCell::read(std::istream& is,IOData* dat)
{
  StaticGeomCell::read(is,dat);
  readBinaryData(_ext,is);
  readBinaryData(_depth,is);
  readBinaryData(_insideOut,is);
  return is.good();
}
bool BoxGeomCell::write(std::ostream& os,IOData* dat) const
{
  StaticGeomCell::write(os,dat);
  writeBinaryData(_ext,os);
  writeBinaryData(_depth,os);
  writeBinaryData(_insideOut,os);
  return os.good();
}
boost::shared_ptr<Serializable> BoxGeomCell::copy() const
{
  return boost::shared_ptr<Serializable>(new BoxGeomCell(*this));
}
const Vec3& BoxGeomCell::getExt() const
{
  return _ext;
}
//helper
void BoxGeomCell::getMeshInner(ObjMesh& mesh) const
{
  if(_dim == 2)MakeMesh::makeBox2D(mesh,_ext);
  else MakeMesh::makeBox3D(mesh,_ext);
  if(_insideOut)mesh.insideOut();
}
BBox<scalar> BoxGeomCell::getBBInner() const
{
  Vec3 depth=Vec3::Zero();
  depth.block(0,0,_dim,1).setConstant(_depth);
  return BBox<scalar>(-_ext-depth,_ext+depth);
}
bool BoxGeomCell::distInner(const Vec3& pt,Vec3& n) const
{
  BBox<scalar> box(-_ext,_ext);
  if(box.contain(pt,_dim)^_insideOut) {
    closestInner(pt,n);
    return true;
  } else return false;
}
bool BoxGeomCell::closestInner(const Vec3& pt,Vec3& n,Vec3* normal) const
{
  BBox<scalar> box(-_ext,_ext);
  if(box.contain(pt,_dim)) {
    scalar minDist=ScalarUtil<scalar>::scalar_max,dist;
    for(sizeType d=0; d<_dim; d++) {
      dist=_ext[d]+pt[d];
      if(dist < minDist) {
        minDist=dist;
        n=-Vec3::Unit(d)*dist;
        if(normal)*normal=-Vec3::Unit(d);
      }
      dist=_ext[d]-pt[d];
      if(dist < minDist) {
        minDist=dist;
        n=Vec3::Unit(d)*dist;
        if(normal)*normal=Vec3::Unit(d);
      }
    }
    return true;
  } else {
    Vec3 cp=box.closestTo(pt,_dim);
    n=cp-pt;
    if(normal) {
      normal->setZero();
      for(sizeType d=0; d<_dim; d++)
        normal->coeffRef(d)=(cp[d] == -_ext[d]) ? -1 : (cp[d] == _ext[d]) ? 1 : 0;
      ASSERT_MSG(normal->norm() > 0,"Strange Geometry error!")
      normal->normalize();
    }
    return false;
  }
}
scalar BoxGeomCell::rayQueryInner(const Vec3& x0,const Vec3& dir) const
{
  scalar s,t;
  BBox<scalar> bb=getBBInner();
  if(bb.intersect(x0,x0+dir,s,t,_dim))
    return s;
  else return 1;
}

//SphereGeomCell
SphereGeomCell::SphereGeomCell():StaticGeomCell(typeid(SphereGeomCell).name()) {}
SphereGeomCell::SphereGeomCell(const Mat4& T,sizeType dim,scalar rad,scalar depth,bool insideOut)
  :StaticGeomCell(T,dim,typeid(SphereGeomCell).name()),_rad(rad),_depth(depth),_insideOut(insideOut)
{
  build();
}
bool SphereGeomCell::read(std::istream& is,IOData* dat)
{
  StaticGeomCell::read(is,dat);
  readBinaryData(_rad,is);
  readBinaryData(_depth,is);
  readBinaryData(_insideOut,is);
  return is.good();
}
bool SphereGeomCell::write(std::ostream& os,IOData* dat) const
{
  StaticGeomCell::write(os,dat);
  writeBinaryData(_rad,os);
  writeBinaryData(_depth,os);
  writeBinaryData(_insideOut,os);
  return os.good();
}
boost::shared_ptr<Serializable> SphereGeomCell::copy() const
{
  return boost::shared_ptr<Serializable>(new SphereGeomCell(*this));
}
scalar SphereGeomCell::getRad() const
{
  return _rad;
}
//helper
void SphereGeomCell::getMeshInner(ObjMesh& mesh) const
{
  if(_dim == 2)MakeMesh::makeSphere2D(mesh,_rad,16);
  else MakeMesh::makeSphere3D(mesh,_rad,16);
}
BBox<scalar> SphereGeomCell::getBBInner() const
{
  Vec3 rad=Vec3::Zero();
  rad.block(0,0,_dim,1).setConstant(_rad+_depth);
  return BBox<scalar>(-rad,rad);
}
bool SphereGeomCell::distInner(const Vec3& pt,Vec3& n) const
{
  scalar len=pt.norm();
  if((len <= _rad) ^ _insideOut) {
    closestInner(pt,n);
    return true;
  } else return false;
}
bool SphereGeomCell::closestInner(const Vec3& pt,Vec3& n,Vec3* normal) const
{
  scalar len=pt.norm();
  n=pt*(_rad-len)/std::max<scalar>(len,1E-6f);
  if(normal)*normal=pt/std::max<scalar>(len,1E-6f);
  return len < _rad;
}
scalar SphereGeomCell::rayQueryInner(const Vec3& x0,const Vec3& dir) const
{
  //solve ||x0+s*dir|| == _rad
  scalar a=dir.squaredNorm();
  scalar b=dir.dot(x0)*2;
  scalar c=x0.squaredNorm()-_rad*_rad;
  scalar delta=b*b-4*a*c;
  if(delta <= 0)
    return 1;
  else {
    scalar s0=(-b-sqrt(delta))/(2*a);
    scalar s1=(-b+sqrt(delta))/(2*a);
    if(s0 > 0 && s0 < 1)
      return s0;
    else if(s1 > 0 && s1 < 1)
      return s1;
    else return 1;
  }
}

//CylinderGeomCell
CylinderGeomCell::CylinderGeomCell():StaticGeomCell(typeid(CylinderGeomCell).name()) {}
CylinderGeomCell::CylinderGeomCell(const Mat4& T,sizeType dim,scalar rad,scalar y)
  :StaticGeomCell(T,dim,typeid(CylinderGeomCell).name()),_rad(rad),_y(y)
{
  build();
}
bool CylinderGeomCell::read(std::istream& is,IOData* dat)
{
  StaticGeomCell::read(is,dat);
  readBinaryData(_rad,is);
  readBinaryData(_y,is);
  return is.good();
}
bool CylinderGeomCell::write(std::ostream& os,IOData* dat) const
{
  StaticGeomCell::write(os,dat);
  writeBinaryData(_rad,os);
  writeBinaryData(_y,os);
  return os.good();
}
boost::shared_ptr<Serializable> CylinderGeomCell::copy() const
{
  return boost::shared_ptr<Serializable>(new CylinderGeomCell(*this));
}
//helper
CylinderGeomCell::CylinderGeomCell(const Mat4& T,sizeType dim,const string& type):StaticGeomCell(T,dim,type) {}
void CylinderGeomCell::getMeshInner(ObjMesh& mesh) const
{
  MakeMesh::makeCylinder3D(mesh,_rad,_y,16,1);
}
BBox<scalar> CylinderGeomCell::getBBInner() const
{
  Vec3 cor(_rad,_y,_rad);
  if(_dim == 2)cor[2]=0.0f;
  return BBox<scalar>(-cor,cor);
}
bool CylinderGeomCell::distInner(const Vec3& pt,Vec3& n) const
{
  scalar len=Vec3(pt[0],0.0f,pt[2]).norm();
  //boundary
  scalar dist=_rad-len;
  if(dist < 0.0f)return false;
  n=Vec3(pt[0],0.0f,pt[2])*dist/std::max<scalar>(len,1E-6f);
  //bottom
  scalar dist2=_y+pt[1];
  if(dist2 < 0.0f)return false;
  if(dist2 < dist) {
    dist=dist2;
    n=-Vec3::Unit(1)*dist2;
  }
  //top
  scalar dist3=_y-pt[1];
  if(dist3 < 0.0f)return false;
  if(dist3 < dist) {
    dist=dist3;
    n=Vec3::Unit(1)*dist3;
  }
  return true;
}
bool CylinderGeomCell::closestInner(const Vec3& pt,Vec3& n,Vec3* normal) const
{
  Vec3 ptN(pt[0],0.0f,pt[2]);
  scalar len=ptN.norm();
  if(len < _rad && abs(pt[1]) < _y) {
    scalar distLen=_rad-len;
    scalar distY1=_y-pt[1];
    scalar distY2=pt[1]+_y;
    if(distLen < distY1 && distLen < distY2) {
      ptN/=max<scalarD>(1E-5f,len);
      if(normal)*normal=ptN;
      ptN*=_rad;
      ptN[1]=pt[1];
    } else if(distY1 < distY2 && distY1 < distLen) {
      ptN[1]=_y;
      if(normal)*normal=Vec3::Unit(1);
    } else {
      ptN[1]=-_y;
      if(normal)*normal=-Vec3::Unit(1);
    }
    n=ptN-pt;
    return true;
  } else if(abs(pt[1]) < _y) {
    ptN/=max<scalarD>(1E-5f,len);
    if(normal)*normal=ptN;
    ptN*=_rad;
    ptN[1]=pt[1];
  } else if(len < _rad) {
    if(pt[1] >= _y) {
      ptN[1]=_y;
      if(normal)*normal=Vec3::Unit(1);
    } else {
      ptN[1]=-_y;
      if(normal)*normal=-Vec3::Unit(1);
    }
  } else {
    ptN*=_rad/max<scalarD>(1E-5f,len);
    if(pt[1] >= _y)
      ptN[1]=_y;
    else ptN[1]=-_y;
    if(normal) {
      *normal=pt-ptN;
      *normal/=max<scalarD>(1E-5f,normal->norm());
    }
  }
  n=ptN-pt;
  return false;
}
scalar CylinderGeomCell::rayQueryInner(const Vec3& x0,const Vec3& dir) const
{
  //solve ||x0+s*dir|| == _rad
  scalar a=Vec2(dir[0],dir[2]).squaredNorm();
  scalar b=Vec2(dir[0],dir[2]).dot(Vec2(x0[0],x0[2]))*2;
  scalar c=Vec2(x0[0],x0[2]).squaredNorm()-_rad*_rad;

  scalar s0=0,s1=1;
  scalar delta=b*b-4*a*c;
  if(a < EPS || delta <= 0) {
    if(c >= 0)
      return 1;
  } else {
    s0=max<scalar>((-b-sqrt(delta))/(2*a),0);
    s1=min<scalar>((-b+sqrt(delta))/(2*a),1);
    if(s0 >= s1)
      return 1;
  }

  scalar y0=x0[1]+dir[1]*s0;
  scalar y1=x0[1]+dir[1]*s1;
  if(y0 > _y) {
    if(y1 >= _y)
      return 1;
    return (_y-x0[1])/dir[1];
  } else if(y0 < -_y) {
    if(y1 <= -_y)
      return 1;
    return (-_y-x0[1])/dir[1];
  } else return s0;
}

//CapsuleGeomCell
CapsuleGeomCell::CapsuleGeomCell():CylinderGeomCell(Mat4::Identity(),3,typeid(CapsuleGeomCell).name()) {}
CapsuleGeomCell::CapsuleGeomCell(const Mat4& T,sizeType dim,scalar rad,scalar y)
  :CylinderGeomCell(T,dim,typeid(CapsuleGeomCell).name())
{
  _rad=rad;
  _y=y;
  build();
}
boost::shared_ptr<Serializable> CapsuleGeomCell::copy() const
{
  return boost::shared_ptr<Serializable>(new CapsuleGeomCell(*this));
}
//helper
void CapsuleGeomCell::getMeshInner(ObjMesh& mesh) const
{
  if(_dim == 3)
    MakeMesh::makeCapsule3D(mesh,_rad,_y,16);
  else MakeMesh::makeCapsule2D(mesh,_rad,_y,16);
}
BBox<scalar> CapsuleGeomCell::getBBInner() const
{
  Vec3 cor(_rad,_y+_rad,_rad);
  if(_dim == 2)cor[2]=0.0f;
  return BBox<scalar>(-cor,cor);
}
bool CapsuleGeomCell::distInner(const Vec3& pt,Vec3& n) const
{
  n=pt;
  if(!(n[1] >= _y || n[1] <= -_y))
    n[1]=0.0f;
  else if(n[1] >= _y)
    n-=Vec3::Unit(1)*_y;
  else n+=Vec3::Unit(1)*_y;

  scalar norm=std::max<scalar>(EPS,n.norm());
  if(norm > _rad)
    return false;
  n*=((_rad-norm)/norm);
  return true;
}
bool CapsuleGeomCell::closestInner(const Vec3& pt,Vec3& n,Vec3* normal) const
{
  if(pt[1] < -_y) {
    n=pt+Vec3::UnitY()*_y;
  } else if(pt[1] > _y) {
    n=pt-Vec3::UnitY()*_y;
  } else {
    n=pt;
    n[1]=0;
  }
  scalar len=n.norm();
  Vec3 nn=n/max<scalar>(len,EPS);
  if(normal)
    *normal=nn;
  n=nn*(_rad-len);
  return len < _rad;
}
scalar CapsuleGeomCell::rayQueryInner(const Vec3& x0,const Vec3& dir) const
{
  SphereGeomCell c(Mat4::Identity(),_dim,_rad);
  scalar s0=CylinderGeomCell::rayQueryInner(x0,dir);
  scalar s1=c.rayQueryInner(x0-Vec3::Unit(1)*_y,dir);
  scalar s2=c.rayQueryInner(x0+Vec3::Unit(1)*_y,dir);
  return std::min(s0,std::min(s1,s2));
}

//ObjMeshGeomCell
struct LineCallback {
  LineCallback(const std::vector<Vec3,Eigen::aligned_allocator<Vec3> >& vss,
               const std::vector<Vec3i,Eigen::aligned_allocator<Vec3i> >& iss,
               const LineSeg& l,sizeType dim):_vss(vss),_iss(iss),_l(l),_dim(dim),_s(1) {}
  bool validNode(const Node<sizeType>& node) {
    return node._bb.intersect(_l._x,_l._y,_dim);
  }
  void updateDist(const Node<sizeType>& node) {
    scalar s;
    const Vec3i& iss=_iss[node._cell];
    if(_dim == 3) {
      Triangle t(_vss[iss[0]],_vss[iss[1]],_vss[iss[2]]);
      if(t.intersect(_l,s) && s < _s)
        _s=s;
    } else {
      LineSeg l(_vss[iss[0]],_vss[iss[1]]);
      if(l.intersect(_l,s) && s < _s)
        _s=s;
    }
  }
  const std::vector<Vec3,Eigen::aligned_allocator<Vec3> >& _vss;
  const std::vector<Vec3i,Eigen::aligned_allocator<Vec3i> >& _iss;
  LineSeg _l;
  sizeType _dim;
  scalar _s;
};
ObjMeshGeomCell::ObjMeshGeomCell()
  :StaticGeomCell(typeid(ObjMeshGeomCell).name()),_insideOut(false),_depth(0.0f) {}
ObjMeshGeomCell::ObjMeshGeomCell(const Mat4& trans,const ObjMesh& mesh,scalar depth,bool insideOut)
  :StaticGeomCell(trans,mesh.getDim(),typeid(ObjMeshGeomCell).name()),_insideOut(insideOut),_depth(depth)
{
  _vss=mesh.getV();
  _iss=mesh.getI();
  build();
  // writeBVHByLevel<sizeType>(_bvh,-1);
  if(_depth == 0.0f)
    _depth=mesh.getBB().getExtent().maxCoeff()*0.1f;
}
bool ObjMeshGeomCell::read(std::istream& is,IOData* dat)
{
  StaticGeomCell::read(is,dat);
  _grid.read(is);
  readBinaryData(_depth,is);
  readBinaryData(_insideOut,is);
  return is.good();
}
bool ObjMeshGeomCell::write(std::ostream& os,IOData* dat) const
{
  StaticGeomCell::write(os,dat);
  _grid.write(os);
  writeBinaryData(_depth,os);
  writeBinaryData(_insideOut,os);
  return os.good();
}
boost::shared_ptr<Serializable> ObjMeshGeomCell::copy() const
{
  return boost::shared_ptr<Serializable>(new ObjMeshGeomCell(*this));
}
scalar ObjMeshGeomCell::depth() const
{
  return _depth;
}
scalar& ObjMeshGeomCell::depth()
{
  return _depth;
}
void ObjMeshGeomCell::calcMinDist2D(const Vec3i& I,const Vec3& pt,Vec3& cp,Vec3& n,scalar& dist,scalar* minDist,Mat3* hess) const
{
  Vec3 cpTmp,b;
  scalar distNew;
  LineSegTpl<scalar> l(_vss[I[0]],_vss[I[1]]);

  l.calcPointDist(pt,distNew,cpTmp,b);
  distNew=sqrt(distNew);
  *minDist=min(*minDist,distNew);
  if(distNew < dist && abs((pt-cpTmp).dot(l.normal())) > 1E-3f*distNew) {
    cp=cpTmp;
    dist=distNew;
    n=l.normal();
    if(hess) {
      if(b[0] > ScalarUtil<scalar>::scalar_eps && b[1] > ScalarUtil<scalar>::scalar_eps)
        *hess=n*n.transpose();
      else *hess=Mat3::Identity();
    }
  }
}
void ObjMeshGeomCell::calcMinDist3D(const Vec3i& I,const Vec3& pt,Vec3& cp,Vec3& n,scalar& dist,scalar* minDist,Mat3* hess) const
{
  Vec3 cpTmp,b;
  scalar distNew;
  Triangle t(_vss[I[0]],_vss[I[1]],_vss[I[2]]);

  t.calcPointDist(pt,distNew,cpTmp,b);
  distNew=sqrt(distNew);
  *minDist=min(*minDist,distNew);
  if(distNew < dist && abs((pt-cpTmp).dot(t.normal())) > 1E-3f*distNew) {
    cp=cpTmp;
    dist=distNew;
    n=t.normal();
    if(hess) {
      if(b[0] > ScalarUtil<scalar>::scalar_eps && b[1] > ScalarUtil<scalar>::scalar_eps && b[2] > ScalarUtil<scalar>::scalar_eps)
        *hess=n*n.transpose();
      else if(b[0] > ScalarUtil<scalar>::scalar_eps && b[1] > ScalarUtil<scalar>::scalar_eps) {
        cpTmp=(t._a-t._b).normalized();
        *hess=Mat3::Identity()-cpTmp*cpTmp.transpose();
      } else if(b[0] > ScalarUtil<scalar>::scalar_eps && b[2] > ScalarUtil<scalar>::scalar_eps) {
        cpTmp=(t._a-t._c).normalized();
        *hess=Mat3::Identity()-cpTmp*cpTmp.transpose();
      } else if(b[1] > ScalarUtil<scalar>::scalar_eps && b[2] > ScalarUtil<scalar>::scalar_eps) {
        cpTmp=(t._b-t._c).normalized();
        *hess=Mat3::Identity()-cpTmp*cpTmp.transpose();
      } else *hess=Mat3::Identity();
    }
  }
}
void ObjMeshGeomCell::buildLevelSet(scalar cellSz,scalar off,scalar eps)
{
  ImplicitFuncOffset offset;
  offset._off-=off;
  offset._inner.reset(new ImplicitFuncMeshRef(*this,eps));
  _grid=ImplicitFuncReinit(cellSz,offset,true)._ls;

  _grid.add(off);
  //GridOp<scalar,scalar>::write3DScalarGridVTK("./levelset.vtk",_grid);
}
scalar ObjMeshGeomCell::distLevelSet(const Vec3& pos) const
{
  return _grid.sampleSafe(transformHomo<scalar>(_invT,pos));
}
const ScalarField& ObjMeshGeomCell::getLevelSet() const
{
  return _grid;
}
//helper
class CallbackMesh
{
public:
  CallbackMesh(const ObjMeshGeomCell& cell,sizeType dim,Mat3* hess):_cell(cell),_iss(cell.iss()),_dim(dim),_hess(hess) {}
  void updateDist(const Node<sizeType>& node,const Vec3& pt,Vec3& cp,Vec3& n,scalar& dist,scalar* minDist) {
    if(_dim == 2)_cell.calcMinDist2D(_iss[node._cell],pt,cp,n,dist,minDist,_hess);
    else _cell.calcMinDist3D(_iss[node._cell],pt,cp,n,dist,minDist,_hess);
  }
  scalar depth() const {
    return _cell.depth();
  }
  const ObjMeshGeomCell& _cell;
  const vector<Vec3i,Eigen::aligned_allocator<Vec3i> >& _iss;
  const sizeType _dim;
  Mat3* _hess;
};
ObjMeshGeomCell::ObjMeshGeomCell(const string& name):StaticGeomCell(name) {}
void ObjMeshGeomCell::getMeshInner(ObjMesh& mesh) const
{
  mesh.getV()=_vss;
  mesh.getI()=_iss;
  mesh.setDim((int)_dim);
  mesh.smooth();
}
BBox<scalar> ObjMeshGeomCell::getBBInner() const
{
  if(!_grid.data().empty())
    return _grid.getBB();
  BBox<scalar> bb=_bvh.back()._bb;
  if(_insideOut)
    return bb.enlarge(_depth,_dim);
  else return bb;
}
bool ObjMeshGeomCell::distInner(const Vec3& pt,Vec3& n) const
{
  if(!_grid.data().empty()) {
    scalar phi=_grid.sampleSafe(pt);
    if(phi > 0)
      return false;
    else {
      n=-_grid.sampleSafeGrad(pt).normalized()*phi;
      return true;
    }
  }

  Vec3 cp;
  CallbackMesh cb(*this,_dim,NULL);
  scalar dist=ScalarUtil<scalar>::scalar_max,minDist=dist;
  BVHQuery<sizeType>(_bvh,_dim,-1).pointDistQuery(pt,cb,cp,n,dist,&minDist);
  cp-=pt;
  if(_insideOut)
    n*=-1.0f;
  if(cp.dot(n) < 1E-6f)
    return false;
  n=cp;
  return dist < ScalarUtil<scalar>::scalar_max;
}
bool ObjMeshGeomCell::closestInner(const Vec3& pt,Vec3& n,Vec3* normal) const
{
  return closestHessInner(pt,n,normal,NULL);
}
bool ObjMeshGeomCell::closestHessInner(const Vec3& pt,Vec3& n,Vec3* normal,Mat3* hess) const
{
  if(!_grid.data().empty()) {
    scalar phi=_grid.sampleSafe(pt);
    n=-_grid.sampleSafeGrad(pt).normalized()*phi;
    return phi < 0;
  }

  Vec3 cp,nor;
  scalar dist=ScalarUtil<scalar>::scalar_max,minDist=dist;
  cp.block(0,0,_dim,1).setConstant(dist);
  CallbackMesh cb(*this,_dim,hess);
  BVHQuery<sizeType>(_bvh,_dim,-1).pointDistQuery(pt,cb,cp,nor,dist,&minDist);
  if(normal)*normal=nor;
  n=cp-pt;

  if(dist < _depth) {
    n*=minDist/max<scalar>(n.norm(),EPS);
    return n.dot(nor) > 0.0f;
  } else {
    n.block(0,0,_dim,1).setConstant(_depth);
    if(minDist < _depth)
      n*=minDist/max<scalar>(n.norm(),EPS);
    return false;
  }
}
scalar ObjMeshGeomCell::rayQueryInner(const Vec3& x0,const Vec3& dir) const
{
  LineCallback cb(_vss,_iss,LineSeg(x0,x0+dir),_dim);
  BVHQuery<sizeType>(_bvh,_dim,-1).pointQuery(cb);
  return cb._s;
}

//HeightFieldGeomCell
//#define HEIGHT_FIELD_AS_MESH
#define GI(X,Y) Vec3i(id[0]+X,id[1]+Y,0).dot(stride)
HeightFieldGeomCell::HeightFieldGeomCell()
  :ObjMeshGeomCell(typeid(HeightFieldGeomCell).name()) {}
HeightFieldGeomCell::HeightFieldGeomCell(const Mat4& T,const ScalarField& h)
  :ObjMeshGeomCell(typeid(HeightFieldGeomCell).name()),_h(h)
{
  _T=T;
  _invT=_T.inverse();
  _dim=h.getDim()+1;
  _insideOut=false;
  ASSERT(!h.isCenter())

  build();
  //debugWrite();
}
HeightFieldGeomCell::HeightFieldGeomCell(sizeType dim,sizeType dimH,scalar h0,scalar hr,scalar sz,scalar cellSz)
  :ObjMeshGeomCell(typeid(HeightFieldGeomCell).name())
{
  _T=genT(dim-1,dimH);
  _invT=_T.inverse();
  _dim=dim;
  _insideOut=false;

  Vec3i id(0,0,0),nrCell(0,0,0);
  BBox<scalar> bb(Vec3::Zero(),Vec3::Zero());
  for(sizeType d=0; d<dim-1; d++) {
    bb._minC[d]=-sz;
    bb._maxC[d]=sz;
    nrCell[d]=(sizeType)(bb.getExtent()[d]/cellSz);
  }
  _h.reset(nrCell,bb,h0,false);
  for(id[0]=0; id[0]<_h.getNrPoint()[0]; id[0]++)
    for(id[1]=0; id[1]<_h.getNrPoint()[1]; id[1]++)
      _h.get(id)+=(RandEngine::randR01()*2-1)*hr;
  ASSERT(!_h.isCenter())
  build();
  //debugWrite();
}
bool HeightFieldGeomCell::read(std::istream& is,IOData* dat)
{
  ObjMeshGeomCell::read(is,dat);
  readBinaryData(_bb,is);
  _h.read(is);
  return is.good();
}
bool HeightFieldGeomCell::write(std::ostream& os,IOData* dat) const
{
  ObjMeshGeomCell::write(os,dat);
  writeBinaryData(_bb,os);
  _h.write(os);
  return os.good();
}
boost::shared_ptr<Serializable> HeightFieldGeomCell::copy() const
{
  return boost::shared_ptr<Serializable>(new HeightFieldGeomCell(*this));
}
//helper
Vec3i HeightFieldGeomCell::getStride(bool cell) const
{
  if(_dim == 2)
    return Vec3i(1,0,0);
  else return Vec3i(cell ? _h.getNrCell()[1] : _h.getNrPoint()[1],1,0);
}
void HeightFieldGeomCell::getMeshInner(ObjMesh& mesh) const
{
  Vec3i id(0,0,0),stride=getStride(false);
  Vec3 unit=Vec3::Unit(_dim-1);
  mesh.setDim((int)_dim);
  //get vertices
  mesh.getV().clear();
  for(id[0]=0; id[0]<_h.getNrPoint()[0]; id[0]++)
    for(id[1]=0; id[1]<_h.getNrPoint()[1]; id[1]++)
      mesh.getV().push_back(_h.getPt(id)+unit*_h.get(id));
  //get indices
  mesh.getI().clear();
  if(_dim == 2) {
    for(id[0]=0; id[0]<_h.getNrPoint()[0]-1; id[0]++)
      mesh.getI().push_back(Vec3i(GI(1,0),GI(0,0),0));
  } else {
    for(id[0]=0; id[0]<_h.getNrPoint()[0]-1; id[0]++)
      for(id[1]=0; id[1]<_h.getNrPoint()[1]-1; id[1]++) {
        mesh.getI().push_back(Vec3i(GI(0,0),GI(1,0),GI(1,1)));
        mesh.getI().push_back(Vec3i(GI(0,0),GI(1,1),GI(0,1)));
      }
  }
}
bool HeightFieldGeomCell::distInner(const Vec3& pt,Vec3& n) const
{
  if(!_bb.contain(pt,_dim))
    return false;
  Vec3 cp;
  scalar dist=ScalarUtil<scalar>::scalar_max,minDist=dist;
#ifdef HEIGHT_FIELD_AS_MESH
  BVHQuery<sizeType>(_bvh,_dim,-1).pointDistQuery(pt,*this,cp,n,dist,&minDist);
#else
  pointDistQuery(pt,cp,n,dist,minDist);
#endif
  cp-=pt;
  if(cp.dot(n) < 1E-6f)
    return false;
  n=cp;
  return dist < ScalarUtil<scalar>::scalar_max;
}
bool HeightFieldGeomCell::closestInner(const Vec3& pt,Vec3& n,Vec3* normal) const
{
  Vec3 cp,nor;
  scalar dist=ScalarUtil<scalar>::scalar_max,minDist=dist;
  cp.block(0,0,_dim,1).setConstant(dist);

#ifdef HEIGHT_FIELD_AS_MESH
  BVHQuery<sizeType>(_bvh,_dim,-1).pointDistQuery(pt,*this,cp,nor,dist,&minDist);
#else
  pointDistQuery(pt,cp,nor,dist,minDist);
#endif
  if(normal)*normal=nor;
  n=cp-pt;

  if(dist < _depth) {
    n*=minDist/max<scalar>(n.norm(),EPS);
    return n.dot(nor) > 0.0f;
  } else {
    n.block(0,0,_dim,1).setConstant(_depth);
    if(minDist < _depth)
      n*=minDist/max<scalar>(n.norm(),EPS);
    return false;
  }
}
BBox<scalar> HeightFieldGeomCell::getBBInner() const
{
  BBox<scalar> bb=_h.getBB();
  _h.minMax(bb._minC[_dim-1],bb._maxC[_dim-1]);
  return bb;
}
void HeightFieldGeomCell::build(bool buildBVHAsWell)
{
  StaticGeomCell::build(buildBVHAsWell);
  _bb=getBBInner();
  _depth=_bb.getExtent().maxCoeff();
  _bb._minC[_dim-1]-=_depth;
}
void HeightFieldGeomCell::debugWrite() const
{
  std::string path="./heightFieldVTK";
  if(boost::filesystem::exists(path))
    boost::filesystem::remove_all(path);
  boost::filesystem::create_directory(path);
  //debug mesh
  ObjMesh mesh;
  getMesh(mesh,true);
  mesh.writeVTK(path+"/mesh.vtk",true);
  //debug BVH
  writeBVHByLevel<sizeType,BBox<scalar> >(_bvh,-1,path);
  //debug ring
#define NR_TEST 1000
#define NR_RING 5
  for(sizeType i=0; i<NR_TEST; i++) {
    //find point for test
    Vec3 pos;
    while(true) {
      pos=Vec3::Random()*getBB().getExtent().maxCoeff();
      pos.segment(_dim,3-_dim).setZero();
      if(getBBInner().contain(pos,_dim))
        break;
    }
    //write bounding box
    stack<sizeType> ss;
    vector<scalar> css;
    vector<Vec3,Eigen::aligned_allocator<Vec3> > vss;
    sizeType nrCell=_h.getNrCell().segment(0,_dim-1).prod();
    for(sizeType j=0; j<NR_RING; j++) {
      addRing(ss,floorV(_h.getIndexFrac(pos)),j);
      while(!ss.empty()) {
        sizeType bvhId=ss.top();
        for(sizeType b=0; b<_dim-1; b++) {
          ASSERT(bvhId+b >= 0 && bvhId+b < nrCell*2)
          const BBox<scalar>& bb=_bvh[bvhId+b]._bb;
          vss.push_back(bb._minC);
          vss.push_back(bb._maxC);
          css.push_back((scalar)j);
        }
        ss.pop();
      }
    }
    VTKWriter<scalar> os("Ring",path+"/ring"+boost::lexical_cast<string>(i)+".vtk",true);
    os.appendVoxels(vss.begin(),vss.end(),true);
    os.appendCustomData("ring",css.begin(),css.end());
  }
#undef NR_RING
#undef NR_TEST
}
Mat4 HeightFieldGeomCell::genT(sizeType dim,sizeType dimH) const
{
  Mat4 ret=Mat4::Identity();
  typedef ScalarUtil<scalar>::ScalarQuat QUAT;
  ret.block<3,3>(0,0)=QUAT::FromTwoVectors(Vec3::Unit(dim),Vec3::Unit(dimH)).toRotationMatrix();
  return ret;
}
void HeightFieldGeomCell::pointDistQuery(Vec3 pt,Vec3& cp,Vec3& n,scalar& dist,scalar& minDist) const
{
  sizeType ring=0;
  std::stack<sizeType> ss;
  pt.segment(_dim,3-_dim).setZero();
  Vec3i id=floorV(_h.getIndexFrac(pt));
  addRing(ss,id,ring++);
  while(true) {
    //check current ring
    bool more=false;
    while(!ss.empty()) {
      sizeType bvhId=ss.top();
      ss.pop();

      const BBox<scalar>& bb=_bvh[bvhId]._bb;
      if(bb.distTo(pt,_dim-1) > std::min<scalar>(depth(),dist))
        continue;

      more=true;
      if(_dim == 3) {
        calcMinDist3D(_iss[bvhId+0],pt,cp,n,dist,&minDist,NULL);
        calcMinDist3D(_iss[bvhId+1],pt,cp,n,dist,&minDist,NULL);
      } else {
        calcMinDist2D(_iss[bvhId+0],pt,cp,n,dist,&minDist,NULL);
      }
    }
    if(!more)
      break;
    //add new ring
    addRing(ss,id,ring++);
  }
}
void HeightFieldGeomCell::addRing(stack<sizeType>& ss,const Vec3i& id,sizeType ring) const
{
  sizeType dim=_dim-1;
  Vec3i idr=id,nrC=_h.getNrCell(),stride=getStride(true);
  for(sizeType i=0; i<dim; i++)
    idr[i]=max<sizeType>(min<sizeType>(idr[i],nrC[i]-1),0);

  if(ring == 0)
    ss.push(stride.dot(idr)*dim);
  else if(_dim == 2) {
    //left
    idr[0]=id[0]-ring;
    if(idr[0] >= 0)
      ss.push(stride.dot(idr)*dim);
    //right
    idr[0]=id[0]+ring;
    if(idr[0] < nrC[0])
      ss.push(stride.dot(idr)*dim);
  } else {
    //left
    idr[0]=id[0]-ring;
    if(idr[0] >= 0)
      for(idr[1] =std::max<sizeType>(id[1]-ring,0);
          idr[1]<=std::min<sizeType>(id[1]+ring,nrC[1]-1); idr[1]++)
        ss.push(stride.dot(idr)*dim);
    //right
    idr[0]=id[0]+ring;
    if(idr[0] < nrC[0])
      for(idr[1] =std::max<sizeType>(id[1]-ring,0);
          idr[1]<=std::min<sizeType>(id[1]+ring,nrC[1]-1); idr[1]++)
        ss.push(stride.dot(idr)*dim);
    //bottom
    idr[1]=id[1]-ring;
    if(idr[1] >= 0)
      for(idr[0] =std::max<sizeType>(id[0]-ring+1,0);
          idr[0]<=std::min<sizeType>(id[0]+ring-1,nrC[0]-1); idr[0]++)
        ss.push(stride.dot(idr)*dim);
    //top
    idr[1]=id[1]+ring;
    if(idr[1] < nrC[1])
      for(idr[0] =std::max<sizeType>(id[0]-ring+1,0);
          idr[0]<=std::min<sizeType>(id[0]+ring-1,nrC[0]-1); idr[0]++)
        ss.push(stride.dot(idr)*dim);
  }
}
#undef GI

//CompositeGeomCell
CompositeGeomCell::CompositeGeomCell():StaticGeomCell(typeid(CompositeGeomCell).name()) {}
CompositeGeomCell::CompositeGeomCell(const Mat4& T,vector<boost::shared_ptr<StaticGeomCell> > children)
  :StaticGeomCell(T,children[0]->dim(),typeid(CompositeGeomCell).name()),_children(children)
{
  build();
}
bool CompositeGeomCell::read(std::istream& is,IOData* dat)
{
  StaticGeomCell::read(is,dat);
  readVector(_children,is,dat);
  return is.good();
}
bool CompositeGeomCell::write(std::ostream& os,IOData* dat) const
{
  StaticGeomCell::write(os,dat);
  writeVector(_children,os,dat);
  return os.good();
}
boost::shared_ptr<Serializable> CompositeGeomCell::copy() const
{
  return boost::shared_ptr<Serializable>(new CompositeGeomCell);
}
boost::shared_ptr<StaticGeomCell> CompositeGeomCell::getChild(sizeType i) const
{
  return _children[i];
}
sizeType CompositeGeomCell::nrChildren() const
{
  return (sizeType)_children.size();
}
void CompositeGeomCell::getMeshInner(ObjMesh& mesh) const
{
  ObjMesh m;
  mesh=ObjMesh();
  for(sizeType i=0; i<(sizeType)_children.size(); i++) {
    _children[i]->getMesh(m);
    mesh.addMesh(m,"c"+boost::lexical_cast<string>(i));
  }
}
BBox<scalar> CompositeGeomCell::getBBInner() const
{
  BBox<scalar> ret;
  for(sizeType i=0; i<(sizeType)_children.size(); i++)
    ret.setUnion(_children[i]->getBB(false));
  return ret;
}
bool CompositeGeomCell::distInner(const Vec3& pt,Vec3& n) const
{
  bool ret=false;
  Vec3 nTmp;
  n.setZero();
  n.segment(0,_dim).setConstant(ScalarUtil<scalar>::scalar_max);
  for(sizeType i=0; i<(sizeType)_children.size(); i++)
    if(_children[i]->dist(pt,nTmp)) {
      if(!ret || nTmp.norm() < n.norm())
        n=nTmp;
      ret=true;
    }
  return ret;
}
bool CompositeGeomCell::closestInner(const Vec3& pt,Vec3& n,Vec3* normal) const
{
  bool ret=false,retI;
  Vec3 nTmp,normalTmp;
  n.setZero();
  n.segment(0,_dim).setConstant(ScalarUtil<scalar>::scalar_max);
  for(sizeType i=0; i<(sizeType)_children.size(); i++) {
    retI=_children[i]->closest(pt,nTmp,normal ? &normalTmp : NULL);
    if(!ret) {
      if(retI || nTmp.norm() < n.norm()) {
        n=nTmp;
        if(normal)
          *normal=normalTmp;
      }
    } else if(retI && nTmp.norm() < n.norm()) {
      n=nTmp;
      if(normal)
        *normal=normalTmp;
    }
    ret=ret || retI;
  }
  return ret;
}
scalar CompositeGeomCell::rayQueryInner(const Vec3& x0,const Vec3& dir) const
{
  scalar s=ScalarUtil<scalar>::scalar_max,sc;
  for(sizeType i=0; i<(sizeType)_children.size(); i++) {
    sc=_children[i]->rayQuery(x0,dir);
    if(sc < s)
      s=sc;
  }
  return s;
}
