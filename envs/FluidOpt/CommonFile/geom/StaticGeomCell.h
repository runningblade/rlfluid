#ifndef STATIC_GEOM_CELL_H
#define STATIC_GEOM_CELL_H

#include "StaticGeom.h"
#include "../GridBasic.h"
#include <stack>

PRJ_BEGIN

template <typename T,typename TI,typename TG>struct Grid;
typedef Grid<scalar,scalar,vector<scalar,Eigen::aligned_allocator<scalar> > > ScalarField;

struct BoxGeomCell : public StaticGeomCell {
  BoxGeomCell();
  BoxGeomCell(const Mat4& T,sizeType dim,const Vec3& ext,scalar depth=0.0f,bool insideOut=false);
  virtual bool read(std::istream& is,IOData* dat);
  virtual bool write(std::ostream& os,IOData* dat) const;
  boost::shared_ptr<Serializable> copy() const;
  const Vec3& getExt() const;
protected:
  virtual void getMeshInner(ObjMesh& mesh) const;
  virtual BBox<scalar> getBBInner() const;
  virtual bool distInner(const Vec3& pt,Vec3& n) const;
  virtual bool closestInner(const Vec3& pt,Vec3& n,Vec3* normal=NULL) const;
  virtual scalar rayQueryInner(const Vec3& x0,const Vec3& dir) const;
  Vec3 _ext;
  scalar _depth;
  bool _insideOut;
};
struct SphereGeomCell : public StaticGeomCell {
  friend struct CapsuleGeomCell;
  SphereGeomCell();
  SphereGeomCell(const Mat4& T,sizeType dim,scalar rad,scalar depth=0.0f,bool insideOut=false);
  virtual bool read(std::istream& is,IOData* dat);
  virtual bool write(std::ostream& os,IOData* dat) const;
  boost::shared_ptr<Serializable> copy() const;
  scalar getRad() const;
protected:
  virtual void getMeshInner(ObjMesh& mesh) const;
  virtual BBox<scalar> getBBInner() const;
  virtual bool distInner(const Vec3& pt,Vec3& n) const;
  virtual bool closestInner(const Vec3& pt,Vec3& n,Vec3* normal=NULL) const;
  virtual scalar rayQueryInner(const Vec3& x0,const Vec3& dir) const;
  scalar _rad;
  scalar _depth;
  bool _insideOut;
};
struct CylinderGeomCell : public StaticGeomCell {
  CylinderGeomCell();
  CylinderGeomCell(const Mat4& T,sizeType dim,scalar rad,scalar y);
  virtual bool read(std::istream& is,IOData* dat);
  virtual bool write(std::ostream& os,IOData* dat) const;
  boost::shared_ptr<Serializable> copy() const;
protected:
  CylinderGeomCell(const Mat4& T,sizeType dim,const string& type);
  virtual void getMeshInner(ObjMesh& mesh) const;
  virtual BBox<scalar> getBBInner() const;
  virtual bool distInner(const Vec3& pt,Vec3& n) const;
  virtual bool closestInner(const Vec3& pt,Vec3& n,Vec3* normal) const;
  virtual scalar rayQueryInner(const Vec3& x0,const Vec3& dir) const;
  scalar _rad,_y;
};
struct CapsuleGeomCell : public CylinderGeomCell {
  CapsuleGeomCell();
  CapsuleGeomCell(const Mat4& T,sizeType dim,scalar rad,scalar y);
  boost::shared_ptr<Serializable> copy() const;
protected:
  virtual void getMeshInner(ObjMesh& mesh) const;
  virtual BBox<scalar> getBBInner() const;
  virtual bool distInner(const Vec3& pt,Vec3& n) const;
  virtual bool closestInner(const Vec3& pt,Vec3& n,Vec3* normal) const;
  virtual scalar rayQueryInner(const Vec3& x0,const Vec3& dir) const;
};
struct ObjMeshGeomCell : public StaticGeomCell {
  ObjMeshGeomCell();
  ObjMeshGeomCell(const Mat4& T,const ObjMesh& mesh,scalar depth,bool insideOut);
  bool read(std::istream& is,IOData* dat);
  bool write(std::ostream& os,IOData* dat) const;
  boost::shared_ptr<Serializable> copy() const;
  scalar depth() const;
  scalar& depth();
  void calcMinDist2D(const Vec3i& I,const Vec3& pt,Vec3& cp,Vec3& n,scalar& dist,scalar* minDist,Mat3* hess) const;
  void calcMinDist3D(const Vec3i& I,const Vec3& pt,Vec3& cp,Vec3& n,scalar& dist,scalar* minDist,Mat3* hess) const;
  void buildLevelSet(scalar cellSz,scalar off,scalar eps=0.0f);
  scalar distLevelSet(const Vec3& pos) const;
  const ScalarField& getLevelSet() const;
protected:
  ObjMeshGeomCell(const string& name);
  virtual void getMeshInner(ObjMesh& mesh) const;
  virtual BBox<scalar> getBBInner() const;
  virtual bool distInner(const Vec3& pt,Vec3& n) const;
  virtual bool closestInner(const Vec3& pt,Vec3& n,Vec3* normal=NULL) const;
  virtual bool closestHessInner(const Vec3& pt,Vec3& n,Vec3* normal=NULL,Mat3* hess=NULL) const;
  virtual scalar rayQueryInner(const Vec3& x0,const Vec3& dir) const;
  ScalarField _grid;
  char _insideOut;
  scalar _depth;
};
struct HeightFieldGeomCell : public ObjMeshGeomCell {
  HeightFieldGeomCell();
  HeightFieldGeomCell(const Mat4& T,const ScalarField& h);
  HeightFieldGeomCell(sizeType dim,sizeType dimH,scalar h0,scalar hr,scalar sz,scalar cellSz);
  bool read(std::istream& is,IOData* dat);
  bool write(std::ostream& os,IOData* dat) const;
  boost::shared_ptr<Serializable> copy() const;
protected:
  virtual Vec3i getStride(bool cell) const;
  virtual void getMeshInner(ObjMesh& mesh) const;
  virtual bool distInner(const Vec3& pt,Vec3& n) const;
  virtual bool closestInner(const Vec3& pt,Vec3& n,Vec3* normal) const;
  virtual BBox<scalar> getBBInner() const;
  virtual void build(bool buildBVHAsWell=true);
  virtual void debugWrite() const;
  Mat4 genT(sizeType dim,sizeType dimH) const;
  void pointDistQuery(Vec3 pt,Vec3& cp,Vec3& n,scalar& dist,scalar& minDist) const;
  void addRing(std::stack<sizeType>& ss,const Vec3i& id,sizeType ring) const;
  BBox<scalar> _bb;
  ScalarField _h;
};
struct CompositeGeomCell : public StaticGeomCell {
  CompositeGeomCell();
  CompositeGeomCell(const Mat4& T,vector<boost::shared_ptr<StaticGeomCell> > children);
  virtual bool read(std::istream& is,IOData* dat);
  virtual bool write(std::ostream& os,IOData* dat) const;
  boost::shared_ptr<Serializable> copy() const;
  boost::shared_ptr<StaticGeomCell> getChild(sizeType i) const;
  sizeType nrChildren() const;
protected:
  virtual void getMeshInner(ObjMesh& mesh) const;
  virtual BBox<scalar> getBBInner() const;
  virtual bool distInner(const Vec3& pt,Vec3& n) const;
  virtual bool closestInner(const Vec3& pt,Vec3& n,Vec3* normal=NULL) const;
  virtual scalar rayQueryInner(const Vec3& x0,const Vec3& dir) const;
  vector<boost::shared_ptr<StaticGeomCell> > _children;

};

PRJ_END

#endif
