import pyFluidOpt as fo
import numpy as np
import math,random,os,shutil

def setTrans(T, v):
    for i in range(3):
        T(i,3,v[i])
def setRot(T, r):
    for i in range(3):
        for j in range(3):
            T(i,j,r(i,j))
if __name__ == '__main__':
    #setup
    sz=64.0
    sol=fo.SolverSmoke2DConstant1()
    sol.initDomain(fo.BBox(fo.Vec3(0,0,0),fo.Vec3(3,4,0)),fo.Vec3(1.0/sz,1.0/sz,0),fo.Vec3i(1,0,0))
    #add solid
    rigid=fo.RigidSolver(2,1/sz)
    rigid.BB=fo.BBox(fo.Vec3(1/sz,1/sz,1/sz),fo.Vec3(3-1.0/sz,4+100.0/sz,0))
    rigid.addCross2(fo.Vec3(0.4,0.1,0),fo.Vec3(1.0,2.5,0),fo.Vec3(0,0,0))
    b=rigid.getBody(0)
    b.rho=2000.0
    sol.initSolid(rigid)
    #init
    sol.globalId=0
    sol.globalTime=0
    sol.setVelDamping(0)
    sol.setDamping(0)
    #source
    s=fo.RandomSource()
    sphere=fo.ImplicitSphere(fo.Vec3(1.1,2.0,0),0.1,2,1.0/sz,fo.Vec3(0,25,0),0,True,False)
    s.addSource(sphere)
    s.T=20
    s.rho=0.8
    sol.addSource(s)
    #simulate
    feature=fo.VectorField()
    sol.focusOn1(fo.BBox(fo.Vec3(0,0,0),fo.Vec3(1.5,2.0,0)))
    sol.setUseUnilateral(False)
    sol.setUseVariational(False)
    sol.setUseBOTE(True)
    if os.path.exists('rigidSmoke'):
        shutil.rmtree('rigidSmoke')
    os.mkdir('rigidSmoke')
    sol.getPSet().writeVTK('rigidSmoke/pSet.vtk')
    for i in range(500):
        print('Simulating frame: %d!'%i)
        sol.focusOn2(0,1)
        sol.advance(1e-2)
        sol.getFeature(feature,0)
        feature.writeVTK('rigidSmoke/feature'+str(i)+'.vtk')
        sol.getGrid().writeCellCenterVTK(sol.getRho(),'rigidSmoke/rho'+str(i)+'.vtk',False)
        sol.getRigid().writeMeshVTK('rigidSmoke/rigid'+str(i)+'.vtk')
