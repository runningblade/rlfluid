#include <Fluid/Utils.h>
#include <Fluid/ScriptedSource.h>
#include <Fluid/ShallowWaterSolver.h>
#include <Fluid/VariableSizedGridOp.h>
#include <FluidSurface/ParticleSurface.h>
#include <CommonFile/geom/StaticGeomCell.h>

USE_PRJ_NAMESPACE

void mainShallow(bool hasRigid,bool slope,bool source)
{
  //add fluid
  scalar sz=64.0f;
  Vec3i warpMode(1,0,0);
  ShallowWaterSolver<2,EncoderConstant<1> > sol;
  sol.initDomain(BBox<scalar>(Vec3(0.0f,0.0f,0.0f),Vec3(3.0f,3.0f,0.0f)),Vec3(1.0f/sz,1.0f/sz,0.0f),&warpMode);
  //add solid
  {
    boost::shared_ptr<RigidSolver> rigid(new RigidSolver(3,-1));
    rigid->setBB(BBox<scalar>(Vec3::Constant(0),Vec3::Constant(3.0f)));
    rigid->setGravity(-Vec3::UnitZ()*9.81f);
    {
      Mat4 T=Mat4::Identity();
      T.block<3,1>(0,3)=Vec3(1.5f,1.5f,-0.1f);
      rigid->addSolid(boost::shared_ptr<StaticGeomCell>(new BoxGeomCell(T,3,Vec3(2,2,0.1f))));
      if(slope) {
        T.block<3,1>(0,3)=Vec3(1.5f,1.5f,0.1f);
        T.block<3,3>(0,0)=makeRotation<scalar>(Vec3(M_PI/6.0f,0,0));
        rigid->addSolid(boost::shared_ptr<StaticGeomCell>(new BoxGeomCell(T,3,Vec3(2,2,0.1f))));
      }
    }
    if(hasRigid) {
      Vec3 rot=Vec3::Random();
      Vec3 ext(0.1f,0.1f,0.1f);
      Vec3 pos(1.5f,2.5f,2.5f);
      rigid->addBox(ext,pos,rot);
      rigid->getBody(0).rho()=500.0f;
      rigid->getBody(0).setP(Vec3(5.0f,0,0));
    }
    sol.initSolid(rigid);
    sol.initLiquidLevel(1.0f);
  }
  sol.setGlobalId(0);
  sol.setGlobalTime(0);
  if(source) {
    //source
    boost::shared_ptr<SourceRegion> s(new RandomSource(1.0f));
    boost::shared_ptr<SourceImplicitFunc> sf(new ImplicitRect(Vec3(2.4f,2.4f,0.0f),Vec3(2.6f,2.6f,1.25f),3,1.0f/sz));
    boost::dynamic_pointer_cast<ImplicitFuncSource>(s)->_sources.push_back(sf);
    sol.addSource(s);
    //sink
    boost::shared_ptr<SourceRegion> s2(new RandomSource(1.0f));
    boost::shared_ptr<SourceImplicitFunc> sf2(new ImplicitRect(Vec3(0.4f,0.4f,0.9f),Vec3(0.6f,0.6f,1.1f),3,1.0f/sz));
    boost::dynamic_pointer_cast<ImplicitFuncSource>(s2)->_sources.push_back(sf2);
    sf2->_source=false;
    sol.addSource(s2);
  }
  //save load
  {
    boost::filesystem::ofstream os("SWE.dat",ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    sol.write(os,dat.get());
  }
  {
    sol=ShallowWaterSolver<2,EncoderConstant<1> >();
    boost::filesystem::ifstream is("SWE.dat",ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    sol.read(is,dat.get());
  }
  //simulate
  sol.focusOn(BBox<scalar>(Vec3(0,0,0),Vec3(1.5f,2.0f,0)));
  recreate("SWE");
  VariableSizedGridOp<scalar,2,EncoderConstant<1> >::writeGridNodalVTK("SWE/B.vtk",sol.getGrid(),sol.getB(),true);
  VariableSizedGridOp<scalar,2,EncoderConstant<1> >::writeGridCellCenterVTK("SWE/W.vtk",sol.getGrid(),sol.getW(),true);
  sol.getRigid().writeMeshVTK("SWE/rigid.vtk");
  for(sizeType i=0; i<500; i++) {
    INFOV("Simulating frame: %ld!",i)
    sol.advance(1E-2f);
    if(hasRigid) {
      cout << "force: " << sol.getForceOnBody(0).transpose() << endl;
      cout << "torque: " << sol.getTorqueOnBody(0).transpose() << endl;
      sol.LiquidSolverPIC<2,EncoderConstant<1> >::focusOn(0);
    }
    {
      std::ostringstream oss;
      oss << "SWE/rigid" << i << ".vtk";
      sol.getRigid().writeMeshVTK(oss.str());
    }
    {
      std::ostringstream oss;
      oss << "SWE/W" << i << ".vtk";
      GridOp<scalar,scalar>::write2DScalarGridVTK(oss.str(),sol.getWS(),true);
      //VariableSizedGridOp<scalar,2,EncoderConstant<1> >::writeGridCellCenterVTK(oss.str(),sol.getGrid(),sol.getW(),true);
    }
    {
      std::ostringstream oss;
      oss << "SWE/B" << i << ".vtk";
      GridOp<scalar,scalar>::write2DScalarGridVTK(oss.str(),sol.getBS(),true);
      //VariableSizedGridOp<scalar,2,EncoderConstant<1> >::writeGridCellCenterVTK(oss.str(),sol.getGrid(),sol.getW(),true);
    }
  }
}
