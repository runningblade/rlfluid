#include <Fluid/Utils.h>
#include <Fluid/LiquidSolver.h>
#include <Fluid/ScriptedSource.h>
#include <Fluid/VariableSizedGridOp.h>
#include <CommonFile/geom/StaticGeomCell.h>

USE_PRJ_NAMESPACE

//#define TEST_WRITE
void mainRigidLiquid(bool BOTE,bool unilateral)
{
  //add fluid
  scalar sz=64.0f;
  Vec3i warpMode(1,0,0);
  LiquidSolverFLIP<2,EncoderConstant<1> > sol;
  sol.initDomain(BBox<scalar>(Vec3(0.0f,0.0f,0.0f),Vec3(3.0f,3.0f,0.0f)),Vec3(1.0f/sz,1.0f/sz,0.0f),&warpMode);
  //add solid
  {
    boost::shared_ptr<RigidSolver> rigid(new RigidSolver(2,1/sz));
    rigid->setBB(BBox<scalar>(Vec3::Constant(1/sz),Vec3::Constant(3.0f-1/sz)));
    rigid->addBall(0.2f,Vec3(1.5f,2.5f,0));
    rigid->getBody(0).setP(Vec3(1,0,0)*rigid->getBody(0).getM());
    rigid->getBody(0).rho()=1500.0f;
    sol.initSolid(rigid);
    sol.initLiquid(ImplicitRect(Vec3(-1.0f,0.0f,0.0f),Vec3(4.0f,1.5f,0.0f),2,sz));
  }
  sol.setGlobalId(0);
  sol.setGlobalTime(0);
  //save load
  {
    boost::filesystem::ofstream os("rigidLiquid.dat",ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    sol.write(os,dat.get());
  }
  {
    sol=LiquidSolverFLIP<2,EncoderConstant<1> >();
    boost::filesystem::ifstream is("rigidLiquid.dat",ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    sol.read(is,dat.get());
  }
  //simulate
  VectorField feature;
  sol.focusOn(BBox<scalar>(Vec3(0,0,0),Vec3(1.5f,2.0f,0)));
  sol.setUseUnilateral(unilateral);
  //sol.setUseVariational(false);
  sol.setUseBOTE(BOTE);
  recreate("rigidLiquid");
  sol.getPSet().writeVTK("rigidLiquid/pSet.vtk");
  VariableSizedGridOp<scalar,2,EncoderConstant<1> >::writeGridCellCenterVTK("rigidLiquid/phi.vtk",sol.getGrid(),sol.getPhi());
  for(sizeType i=0; i<500; i++) {
    INFOV("Simulating frame: %ld!",i)
    sol.LiquidSolverPIC<2,EncoderConstant<1> >::focusOn(0);
    sol.advance(1E-2f);
    sol.getFeature(feature,1);
    {
      std::ostringstream oss;
      oss << "rigidLiquid/pset" << i << ".vtk";
      sol.getPSet().writeVTK(oss.str());
    }
    {
      std::ostringstream oss;
      oss << "rigidLiquid/phi" << i << ".vtk";
      VariableSizedGridOp<scalar,2,EncoderConstant<1> >::writeGridCellCenterVTK(oss.str(),sol.getGrid(),sol.getPhi());
    }
    {
      std::ostringstream oss;
      oss << "rigidLiquid/rigid" << i << ".vtk";
      sol.getRigid().writeMeshVTK(oss.str());
    }
    {
      std::ostringstream oss;
      oss << "rigidLiquid/feature" << i << ".vtk";
      GridOp<scalar,scalar>::write2DVectorGridVTK(oss.str(),feature,0.01f);
    }
    {
      const ObjMesh& mesh=sol.getSurfaceMesh(true);
      std::ostringstream oss;
      oss << "rigidLiquid/surface" << i << ".vtk";
      mesh.writeVTK(oss.str(),true);
    }
  }
}
