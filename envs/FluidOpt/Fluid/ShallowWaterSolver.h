#ifndef SHALLOW_WATER_SOLVER_H
#define SHALLOW_WATER_SOLVER_H

#include "LiquidSolver.h"

PRJ_BEGIN

template <int DIM,typename Encoder>
class ShallowWaterSolver : public LiquidSolverPIC<DIM,Encoder>
{
public:
  typedef typename TypeTraits<scalar,DIM>::VecDIMd VecDIMd;
  typedef typename TypeTraits<scalar,DIM>::VecDIMi VecDIMi;
  typedef LiquidSolverPIC<DIM,Encoder> Parent;
  using Parent::_nodalSolidPhi;
  using Parent::_grd;
  using Parent::_grd0;
  using Parent::_rigid;
  using Parent::_sources;
  //param
  using Parent::_density;
  //timing
  using Parent::_globalTime;
  using Parent::_globalId;
  using Parent::_maxSubstep;
  using Parent::_maxCFL;
  ShallowWaterSolver();
  virtual ~ShallowWaterSolver();
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<Serializable> copy() const override;
  virtual scalar getSubstep(scalar& t,const scalar& dt,scalar limit) const;
  //setup
  virtual void initLiquidLevel(scalar l,const scalar& sparsity=1.0f);
  virtual void initSolid(boost::shared_ptr<RigidSolver> sol) override;
  virtual void initLiquid(const ImplicitFunc<scalar>& l,const scalar& sparsity=1.0f) override;
  virtual void initDomain(BBox<scalar> bb,const Vec3& cellSz,const Vec3i* warpMode=NULL) override;
  virtual void focusOn(const BBox<scalar>& region) override;
  //getter
  ScalarField getWS() const;
  ScalarField getBS() const;
  const CellCenterData<scalar>& getW() const;
  const NodalData<scalar>& getB() const;
  scalar getCDrag() const;
  void setCDrag(scalar c);
  scalar getCDragLiquid() const;
  void setCDragLiquid(scalar c);
  scalar getCLift() const;
  void setCLift(scalar c);
  scalar getTheta() const;
  void setTheta(scalar t);
  Vec3 getForceOnBody(sizeType bodyId) const;
  Vec3 getTorqueOnBody(sizeType bodyId) const;
  //solver
  virtual sizeType advance(const scalar& dt);
  void sampleSource();
protected:
  scalar _theta,_CDrag,_CDragLiquid,_CLift;
  NodalData<scalar> _B;
  CellCenterData<scalar> _U[DIM+1];
  //temporary data not involved in IO
  vector<Vec6,Eigen::aligned_allocator<Vec6> > _forceTorque;
};

PRJ_END

#endif
