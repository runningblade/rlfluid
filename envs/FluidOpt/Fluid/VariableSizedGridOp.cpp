#include "VariableSizedGridOp.h"
#include "VariableSizedGridOpProject.h"
#include "Utils.h"
#include <CommonFile/IO.h>
#include <CommonFile/GridOp.h>
#include <CommonFile/ParticleToGrid.h>
#include <boost/lexical_cast.hpp>

USE_PRJ_NAMESPACE

#define NR_EXTRAPOLATE_PASS 100
//VariableSizedGridOp
template <typename T,int DIM,typename Encoder>
template <typename TDATA>
void VariableSizedGridOp<T,DIM,Encoder>::writeGridNodalVTK(const string& path,const GridType& grd,const NodalData<TDATA>& data,bool extrude)
{
  VTKWriter<T> os("VariableSizedGrid",path,true);
  if(extrude) {
    vector<VecDIMPd,Eigen::aligned_allocator<VecDIMPd> > pss;
    for(NodalPointIt beg(grd,0),end(grd,-1); beg!=end; ++beg)
      pss.push_back(assignVariableTo<VecDIMPd,VecDIMd>(*beg)+VecDIMPd::Unit(DIM)*data._data[beg.off()]);
    os.appendPoints(pss.begin(),pss.end());
  } else os.appendPoints(NodalPointIt(grd,0),NodalPointIt(grd,-1));
  os.appendCustomPointData("data",data._data.data(),data._data.data()+data._data.size());
  os.appendCells(NodalCellIt(grd,0),NodalCellIt(grd,-1),DIM == 2 ? VTKWriter<T>::PIXEL : VTKWriter<T>::VOXEL);
}
template <typename T,int DIM,typename Encoder>
template <typename TDATA>
void VariableSizedGridOp<T,DIM,Encoder>::writeGridNodalPointVTK(const string& path,const GridType& grd,const NodalData<TDATA>& data,bool extrude)
{
  VTKWriter<T> os("VariableSizedGridPoint",path,true);
  if(extrude) {
    vector<VecDIMPd,Eigen::aligned_allocator<VecDIMPd> > pss;
    for(NodalPointIt beg(grd,0),end(grd,-1); beg!=end; ++beg)
      pss.push_back(assignVariableTo<VecDIMPd,VecDIMd>(*beg)+VecDIMPd::Unit(DIM)*data._data[beg.off()]);
    os.appendPoints(pss.begin(),pss.end());
  } else os.appendPoints(NodalPointIt(grd,0),NodalPointIt(grd,-1));
  os.appendCustomPointData("data",data._data.data(),data._data.data()+data._data.size());
  os.appendCells(NodalPointIndexIt(grd,0),NodalPointIndexIt(grd,-1),VTKWriter<T>::POINT);
}
template <typename T,int DIM,typename Encoder>
template <typename TDATA>
void VariableSizedGridOp<T,DIM,Encoder>::writeGridCellCenterVTK(const string& path,const GridType& grd,const CellCenterData<TDATA>& data,bool extrude)
{
  VTKWriter<T> os("VariableSizedGrid",path,true);
  if(extrude) {
    vector<VecDIMPd,Eigen::aligned_allocator<VecDIMPd> > pss;
    for(CellCenterPointIt beg(grd,0),end(grd,-1); beg!=end; ++beg)
      pss.push_back(assignVariableTo<VecDIMPd,VecDIMd>(*beg)+VecDIMPd::Unit(DIM)*data._data[beg.off()]);
    os.appendPoints(pss.begin(),pss.end());
  } else os.appendPoints(CellCenterPointIt(grd,0),CellCenterPointIt(grd,-1));
  os.appendCustomPointData("data",data._data.data(),data._data.data()+data._data.size());
  os.appendCells(CellCenterCellIt(grd,0),CellCenterCellIt(grd,-1),DIM == 2 ? VTKWriter<T>::PIXEL : VTKWriter<T>::VOXEL);
}
template <typename T,int DIM,typename Encoder>
template <typename TDATA>
void VariableSizedGridOp<T,DIM,Encoder>::writeGridCellCenterPointVTK(const string& path,const GridType& grd,const CellCenterData<TDATA>& data,bool extrude)
{
  VTKWriter<T> os("VariableSizedGridPoint",path,true);
  if(extrude) {
    vector<VecDIMPd,Eigen::aligned_allocator<VecDIMPd> > pss;
    for(CellCenterPointIt beg(grd,0),end(grd,-1); beg!=end; ++beg)
      pss.push_back(assignVariableTo<VecDIMPd,VecDIMd>(*beg)+VecDIMPd::Unit(DIM)*data._data[beg.off()]);
    os.appendPoints(pss.begin(),pss.end());
  } else os.appendPoints(CellCenterPointIt(grd,0),CellCenterPointIt(grd,-1));
  os.appendCustomPointData("data",data._data.data(),data._data.data()+data._data.size());
  os.appendCells(CellCenterPointIndexIt(grd,0),CellCenterPointIndexIt(grd,-1),VTKWriter<T>::POINT);
}
template <typename T,int DIM,typename Encoder>
template <typename TDATA>
void VariableSizedGridOp<T,DIM,Encoder>::writeGridMACVelVTK(const string& path,const GridType& grd,const MACVelData<DIM,TDATA>& data,T len)
{
  VecDIMi id;
  VecDIMd x,v;
  vector<VecDIMd,Eigen::aligned_allocator<VecDIMd> > vss;
  VTKWriter<T> os("VariableSizedGrid",path,true);
  for(int d=0; d<DIM; d++) {
    MACVelPointIt beg(d,grd,0);
    MACVelPointIt end(d,grd,-1);
    while(beg!=end) {
      x=*beg;
      grd.sampleMACVel(data,x,id,v);
      vss.push_back(*beg);
      vss.push_back(*beg+v*len);
      ++beg;
    }
  }
  os.appendPoints(vss.begin(),vss.end());
  os.appendCells(VTKWriter<scalar>::IteratorIndex<Vec3i>(0,2,0),
                 VTKWriter<scalar>::IteratorIndex<Vec3i>((sizeType)vss.size()/2,2,0),VTKWriter<scalar>::LINE);
}
template <typename T,int DIM,typename Encoder>
template <typename TDATA>
void VariableSizedGridOp<T,DIM,Encoder>::writeGridMACVelCompVTK(unsigned char D,const string& path,const GridType& grd,const MACVelData<DIM,TDATA>& data)
{
  VTKWriter<T> os("VariableSizedGrid",path,true);
  os.setRelativeIndex();
  os.appendPoints(MACVelPointIt(D,grd,0),MACVelPointIt(D,grd,-1));
  os.appendCustomPointData("data",data._data[D].data(),data._data[D].data()+data._data[D].size());
  os.appendCells(MACVelPointIndexIt(D,grd,0),MACVelPointIndexIt(D,grd,-1),VTKWriter<T>::POINT,true);
}
template <typename T,int DIM,typename Encoder>
template <typename TDATA>
void VariableSizedGridOp<T,DIM,Encoder>::writeGridMACVelPointVTK(const string& path,const GridType& grd,const MACVelData<DIM,TDATA>& data)
{
  VTKWriter<T> os("VariableSizedGridPoint",path,true);
  for(int d=0; d<DIM; d++) {
    os.setRelativeIndex();
    os.appendPoints(MACVelPointIt(d,grd,0),MACVelPointIt(d,grd,-1));
    os.appendCustomPointData("data",data._data[d].data(),data._data[d].data()+data._data[d].size());
    os.appendCells(MACVelPointIndexIt(d,grd,0),MACVelPointIndexIt(d,grd,-1),VTKWriter<T>::POINT,true);
  }
}
template <typename T,int DIM,typename Encoder>
template <typename TDATA>
void VariableSizedGridOp<T,DIM,Encoder>::copyImplicitFuncNodal(const GridType& grd,NodalData<TDATA>& data,const ImplicitFunc<TDATA>& func)
{
  Vec3 pos=Vec3::Zero();
  NodalPointIt beg(grd,0),end(grd,-1);
  while(beg!=end) {
    pos.template segment<DIM>(0)=(*beg).template cast<scalar>();
    data._data[beg.off()]=(T)func(pos);
    ++beg;
  }
}
template <typename T,int DIM,typename Encoder>
template <typename TDATA>
void VariableSizedGridOp<T,DIM,Encoder>::copyImplicitFuncCellCenter(const GridType& grd,CellCenterData<TDATA>& data,const ImplicitFunc<TDATA>& func)
{
  Vec3 pos=Vec3::Zero();
  CellCenterPointIt beg(grd,0),end(grd,-1);
  while(beg!=end) {
    pos.template segment<DIM>(0)=(*beg).template cast<scalar>();
    data._data[beg.off()]=(T)func(pos);
    ++beg;
  }
}
template <typename T,int DIM,typename Encoder>
template <typename TDATA>
void VariableSizedGridOp<T,DIM,Encoder>::copyImplicitFuncMACVel(const GridType& grd,MACVelData<DIM,TDATA>& data,const VelFunc<TDATA>& func)
{
  Vec3 pos=Vec3::Zero();
  for(sizeType d=0; d<DIM; d++) {
    MACVelPointIt beg(d,grd,0),end(d,grd,-1);
    while(beg!=end) {
      pos.template segment<DIM>(0)=(*beg).template cast<scalar>();
      data._data[d][beg.off()]=func(pos)[d];
      ++beg;
    }
  }
}
template <typename T,int DIM,typename Encoder>
template <typename TDATA>
void VariableSizedGridOp<T,DIM,Encoder>::copyFromGridNodal(const GridType& grd,NodalData<TDATA>& data,const ScalarField& func)
{
  Vec3 pos=Vec3::Zero();
  NodalPointIt beg(grd,0),end(grd,-1);
  DEFINE_GRID_WARP(func)
  while(beg!=end) {
    pos.template segment<DIM>(0)=(*beg).template cast<scalar>();
    data._data[beg.off()]=(T)func.sampleSafe(pos,&warpMode);
    ++beg;
  }
}
template <typename T,int DIM,typename Encoder>
template <typename TDATA>
void VariableSizedGridOp<T,DIM,Encoder>::copyFromGridCellCenter(const GridType& grd,CellCenterData<TDATA>& data,const ScalarField& func)
{
  Vec3 pos=Vec3::Zero();
  CellCenterPointIt beg(grd,0),end(grd,-1);
  DEFINE_GRID_WARP(func)
  while(beg!=end) {
    pos.template segment<DIM>(0)=(*beg).template cast<scalar>();
    data._data[beg.off()]=(T)func.sampleSafe(pos,&warpMode);
    ++beg;
  }
}
template <typename T,int DIM,typename Encoder>
template <typename TDATA>
void VariableSizedGridOp<T,DIM,Encoder>::copyToGridNodal(const GridType& grd0,const GridType& grd,const NodalData<TDATA>& data,ScalarField& func)
{
  BBox<T,DIM> bbTDim=grd0.getBBNodal();
  VecDIMi nrCellTDim=(bbTDim.getExtent().array()*grd0._invCellSz0.array()).matrix().template cast<sizeType>();
  BBox<scalar> bb=assignVariableTo<scalar,3,T,DIM>(bbTDim);
  Vec3i nrCell=assignVariableTo<Vec3i,VecDIMi>(nrCellTDim);
  func.reset(nrCell,bb,0,true);

  VecDIMd xGrd;
  VecDIMi idGrd;
  Vec3i id,nrP=func.getNrPoint();
  for(id[0]=0;id[0]<nrP[0];id[0]++)
    for(id[1]=0;id[1]<nrP[1];id[1]++)
      for(id[2]=0;id[2]<nrP[2];id[2]++) {
        xGrd=assignVariableTo<VecDIMd,Vec3>(func.getPt(id));
        func.get(id)=grd.template sampleNodal<TDATA>(data,xGrd,idGrd);
      }
}
template <typename T,int DIM,typename Encoder>
template <typename TDATA>
void VariableSizedGridOp<T,DIM,Encoder>::copyToGridCellCenter(const GridType& grd0,const GridType& grd,const CellCenterData<TDATA>& data,ScalarField& func)
{
  BBox<T,DIM> bbTDim=grd0.getBBCellCenter();
  VecDIMi nrCellTDim=(bbTDim.getExtent().array()*grd0._invCellSz0.array()).matrix().template cast<sizeType>();
  BBox<scalar> bb=assignVariableTo<scalar,3,T,DIM>(bbTDim);
  Vec3i nrCell=assignVariableTo<Vec3i,VecDIMi>(nrCellTDim);
  func.reset(nrCell,bb,0,true);

  VecDIMd xGrd;
  VecDIMi idGrd;
  Vec3i id,nrP=func.getNrPoint();
  for(id[0]=0;id[0]<nrP[0];id[0]++)
    for(id[1]=0;id[1]<nrP[1];id[1]++)
      for(id[2]=0;id[2]<nrP[2];id[2]++) {
        xGrd=assignVariableTo<VecDIMd,Vec3>(func.getPt(id));
        func.get(id)=grd.template sampleCellCenter<TDATA>(data,xGrd,idGrd);
      }
}
//check CFL
template <typename T,int DIM,typename Encoder>
T VariableSizedGridOp<T,DIM,Encoder>::checkCFL(const GridType& grd,const MACVelData<DIM,T>& vel)
{
  T ret=ScalarUtil<T>::scalar_max;
  for(sizeType D=0; D<DIM; D++)
    ret=min<T>(ret,grd._cellSz0[D]/vel._data[D].maxCoeff());
  return ret;
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOp<T,DIM,Encoder>::clamp(const GridType& grd,CellCenterData<T> data[DIM],T maxVel)
{
  VecDIMd vel;
  CellCenterPointIt beg(grd,0),end(grd,-1);
  while(beg!=end) {
    for(sizeType d=0; d<DIM; d++)
      vel[d]=data[d]._data[beg.off()];
    T len=vel.norm();
    if(len > maxVel) {
      vel*=maxVel/len;
      for(sizeType d=0; d<DIM; d++)
        data[d]._data[beg.off()]=vel[d];
    }
    ++beg;
  }
}
//compute level set from particle
template <typename T,int DIM,typename Encoder>
template <typename PS_TYPE>
void VariableSizedGridOp<T,DIM,Encoder>::computePhiCellCenter(const GridType& grd,CellCenterData<T>& phi,const PS_TYPE& pSet,T narrowBand,T radius)
{
  VecDIMb ob;
  VecDIMd cellSz;
  VecDIMi id,id0,id1,nrCell;
  phi._data.setConstant(narrowBand);
  nrCell=ceilV(VecDIMd(radius*2*grd._invCellSz0));
  const sizeType n=(sizeType)pSet.size();
  for(sizeType i=0; i<n; i++) {
    VecDIMd pos=pSet.get(i)._pos.template segment<DIM>(0);
    grd.indexSafeCellCenter(pos,id,ob);
    grd.getCellSzCellCenter(id,cellSz);
    //compute phi
    id0=id-nrCell;
    id1=id+nrCell+VecDIMi::Ones();
    CellCenterPointIt beg(grd,0,id0,grd._nrPointCellCenter-id1);
    CellCenterPointIt end(grd,-1,id0,grd._nrPointCellCenter-id1);
    for(; beg!=end; ++beg)
      ParticleToGrid<T,T,PS_TYPE>::updatePt(phi._data[beg.off()],(beg.pointWithWarp()-pos).norm(),radius);
  }
}
template <typename T,int DIM,typename Encoder>
template <typename PS_TYPE>
void VariableSizedGridOp<T,DIM,Encoder>::computePhiAdaptiveCellCenter(const GridType& grd,CellCenterData<T>& phi,const PS_TYPE& pSet,T narrowBand,T radius)
{
  T radiusP;
  VecDIMb ob;
  VecDIMd cellSz;
  VecDIMi id,id0,id1,nrCell;
  nrCell=ceilV(VecDIMd(radius*2*grd._invCellSz0));
  if(narrowBand > 0)
    //narrowBand <= 0 is a tag for not reseting
    phi._data.setConstant(narrowBand);
  const sizeType n=(sizeType)pSet.size();
  for(sizeType i=0; i<n; i++) {
    VecDIMd pos=pSet.get(i)._pos.template segment<DIM>(0);
    grd.indexSafeCellCenter(pos,id,ob);
    grd.getCellSzCellCenter(id,cellSz);
    //compute cell size
    radiusP=max<T>(radius,cellSz.maxCoeff()*1.01f);
    //compute phi
    id0=id-nrCell;
    id1=id+nrCell+VecDIMi::Ones();
    CellCenterPointIt beg(grd,0,id0,grd._nrPointCellCenter-id1);
    CellCenterPointIt end(grd,-1,id0,grd._nrPointCellCenter-id1);
    for(; beg!=end; ++beg)
      ParticleToGrid<T,T,PS_TYPE>::updatePt(phi._data[beg.off()],(beg.pointWithWarp()-pos).norm(),radiusP);
  }
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOp<T,DIM,Encoder>::extendPhiCellCenter(const GridType& grd,CellCenterData<T>& phi,const MACVelData<DIM,T>& weight,sizeType nrPass)
{
  T val;
  bool more=true;
  VecDIMd cellSz;
  sizeType LID,RID;
  for(sizeType i=0; i<nrPass && more; i++) {
    CellCenterData<T> tmp(phi);
    more=false;
    for(sizeType D=0; D<DIM; D++) {
      MACVelPointIt beg=MACVelPointIt::iterAllInternal(D,grd,0);
      MACVelPointIt end=MACVelPointIt::iterAllInternal(D,grd,-1);
      while(beg!=end) {
        LID=beg.offCellCenterLeft();
        RID=beg.offCellCenter();
        if(weight._data[D][beg.off()] < 1) {
          grd.getCellSzNodal(beg.id(),cellSz);
          val=-cellSz.sum()/DIM;
          T LPHI=phi._data[LID];
          T RPHI=phi._data[RID];
          if(LPHI < 0 && RPHI > 0) {
            tmp._data[RID]=val;
            more=true;
          } else if(LPHI > 0 && RPHI < 0) {
            tmp._data[LID]=val;
            more=true;
          }
        }
        ++beg;
      }
    }
    phi._data.swap(tmp._data);
  }
}
//compute velocity in solid
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOp<T,DIM,Encoder>::constraintMACVelDIM(unsigned char D,const GridType& grd,const Grid<T,T>& solid,MACVelData<DIM,T>& vel)
{
  VecDIMd ptL,ptR;
  MACVelPointIt beg(D,grd,0);
  MACVelPointIt end(D,grd,-1);
  DEFINE_GRID_WARP(solid)
  while(beg!=end) {
    if(!beg.hasCellCenter() || !beg.hasCellCenterLeft())
      vel._data[D][beg.off()]=0;
    else {
      grd.getPtCellCenter(beg.offCellCenter(),ptR);
      grd.getPtCellCenter(beg.offCellCenterLeft(),ptL);
      if(solid.sampleSafe(assignVariableTo<Vec3,VecDIMd>(ptL),&warpMode) < 0 ||
         solid.sampleSafe(assignVariableTo<Vec3,VecDIMd>(ptR),&warpMode) < 0)
        vel._data[D][beg.off()]=0;
    }
    ++beg;
  }
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOp<T,DIM,Encoder>::constraintMACVel(const GridType& grd,const Grid<T,T>& solid,MACVelData<DIM,T>& vel)
{
  for(sizeType D=0; D<DIM; D++)
    constraintMACVelDIM(D,grd,solid,vel);
}
template <typename T,int DIM,typename Encoder>
bool VariableSizedGridOp<T,DIM,Encoder>::extrapolateMACVelComp(unsigned char D,const GridType& grd,MACVelData<DIM,unsigned char>& valid,MACVelData<DIM,T>& vel,const VecDIMi& id,sizeType off,unsigned char flag)
{
  T sum=0.0f;
  T count=0.0f;
  VecSTENCILi neigh;
  int nr=grd.getNeighMACVel(D,id,off,neigh);
  for(int nid=0; nid<nr; nid++) {
    unsigned char TV=valid._data[D][neigh[nid]];
    if(TV > 0 && TV < flag) {
      sum+=vel._data[D][neigh[nid]];
      count+=1.0f;
    }
  }
  if(count > 0.0f) {
    vel._data[D][off]=sum/count;
    valid._data[D][off]=flag;
    return true;
  }
  return false;
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOp<T,DIM,Encoder>::extrapolateMACVelDIM(unsigned char D,const GridType& grd,MACVelData<DIM,unsigned char>& valid,MACVelData<DIM,T>& vel,sizeType nrPass)
{
  VecDIMi n;
  vector<VecDIMi> q,bq;
  VecSTENCILi neigh;
  nrPass=std::min<sizeType>(nrPass,200);
  {
    MACVelPointIt beg(D,grd,0);
    MACVelPointIt end(D,grd,-1);
    while(beg!=end) {
      if(valid._data[D][beg.off()] == 0 && extrapolateMACVelComp(D,grd,valid,vel,beg.id(),beg.off(),2))
        q.push_back(beg.id());
      ++beg;
    }
  }
  for(unsigned char i=1; i<nrPass; i++) {
    bq.clear();
    sizeType nrQ=(sizeType)q.size();
    for(sizeType j=0; j<nrQ; j++) {
      int nr=grd.getNeighMACVel(D,q[j],q[j].dot(grd._strideMACVel[D]),neigh);
      for(int nid=0; nid<nr; nid++) {
        grd.decodeMACVel(D,neigh[nid],n);
        if(valid._data[D][neigh[nid]]== 0 && extrapolateMACVelComp(D,grd,valid,vel,n,neigh[nid],i+2))
          bq.push_back(n);
      }
    }
    bq.swap(q);
  }
  {
    MACVelPointIt beg(D,grd,0);
    MACVelPointIt end(D,grd,-1);
    while(beg!=end) {
      unsigned char& TV=valid._data[D][beg.off()];
      if(TV > 0)
        TV=1;
      ++beg;
    }
  }
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOp<T,DIM,Encoder>::extrapolateMACVel(const GridType& grd,MACVelData<DIM,unsigned char>& valid,MACVelData<DIM,T>& vel,sizeType nrPass)
{
  for(sizeType D=0; D<DIM; D++)
    extrapolateMACVelDIM(D,grd,valid,vel,nrPass);
}
//compute velocity in solid
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOp<T,DIM,Encoder>::debugExtrapolate(const string& path,const Vec3i& warpMode)
{
  recreate(path);
  GridType testGrid=createTestGrid(warpMode);
  VelFuncRotate vel(VecDIMd::Constant(1),Vec3::Unit(2));
  //velocity
  MACVelData<DIM,T> velGrd(testGrid);
  copyImplicitFuncMACVel(testGrid,velGrd,vel);
  //nodalSolidPhi
  VecDIMd ctrGiven=testGrid.getBBNodal()._minC;
  ScalarField nodalSolidPhi=createTestSolidPhi(testGrid,&ctrGiven);
  writeGridMACVelVTK(path+"/vel.vtk",testGrid,velGrd,0.1f);
  if(DIM == 2)
    GridOp<scalar,scalar>::write2DScalarGridVTK(path+"/nodalSolidPhi.vtk",nodalSolidPhi,false);
  else if(DIM == 3)
    GridOp<scalar,scalar>::write3DScalarGridVTK(path+"/nodalSolidPhi.vtk",nodalSolidPhi);
  //constraint
  constraintMACVel(testGrid,nodalSolidPhi,velGrd);
  writeGridMACVelVTK(path+"/velConstraint.vtk",testGrid,velGrd,0.1f);
  //extrapolate
  MACVelData<DIM,unsigned char> valid(testGrid);
  for(sizeType d=0; d<DIM; d++)
    valid._data[d].array()=(velGrd._data[d].array()!=0).template cast<unsigned char>();
  extrapolateMACVel(testGrid,valid,velGrd,NR_EXTRAPOLATE_PASS);
  writeGridMACVelVTK(path+"/velExtrapolate.vtk",testGrid,velGrd,0.1f);
  //particle phi computation
  ParticleSetN pset;
  ParticleN<scalar> p;
  Vec3 dir=assignVariableTo<Vec3,VecDIMd>(VecDIMd::Ones().normalized());
  for(sizeType i=0; i<nodalSolidPhi.getNrPoint().prod(); i++) {
    p._pos=nodalSolidPhi.getPt(nodalSolidPhi.getIndex(i));
    if((p._pos-p._pos.dot(dir)*dir).norm() < 0.5f)
      pset.addParticle(p);
  }
  CellCenterData<T> phi(testGrid);
  computePhiAdaptiveCellCenter(testGrid,phi,pset,testGrid._cellSz0.maxCoeff(),nodalSolidPhi.getCellSize().maxCoeff());
  writeGridCellCenterVTK(path+"/phiAdaptive.vtk",testGrid,phi);
  computePhiCellCenter(testGrid,phi,pset,testGrid._cellSz0.maxCoeff(),nodalSolidPhi.getCellSize().maxCoeff());
  writeGridCellCenterVTK(path+"/phi.vtk",testGrid,phi);
  pset.writeVTK(path+"/Particle.vtk");
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOp<T,DIM,Encoder>::debugExtrapolateAllWarp()
{
  INFO("Debugging Extrapolate all warp cases!")
  debugExtrapolate("extrapolate"+GridType::printWarp(Vec3i::Zero()),Vec3i::Zero());
  if(Encoder::isConstantEncoder)
    for(sizeType D=0; D<DIM; D++)
      debugExtrapolate("extrapolate"+GridType::printWarp(Vec3i::Unit(D)),Vec3i::Unit(D));
}
//external force
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOp<T,DIM,Encoder>::addDamping(NodalData<T>& data,T alpha,T baseline,T dt)
{
  data._data.array()=(data._data.array()-baseline)*exp(-alpha*dt)+baseline;
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOp<T,DIM,Encoder>::addDamping(CellCenterData<T>& data,T alpha,T baseline,T dt)
{
  data._data.array()=(data._data.array()-baseline)*exp(-alpha*dt)+baseline;
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOp<T,DIM,Encoder>::addDamping(MACVelData<DIM,T>& data,T alpha,T dt)
{
  data.mul(exp(-alpha*dt));
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOp<T,DIM,Encoder>::addExternalForce(const GridType& grd,MACVelData<DIM,T>& data,const CellCenterData<T>* t,T alpha,T beta,T tAmb,T dt)
{
  alpha*=dt;
  beta*=dt;
  VecDIMd x;
  VecDIMi id;
  MACVelPointIt beg(1,grd,0);
  MACVelPointIt end(1,grd,-1);
  while(beg!=end) {
    T& val=data._data[1](beg.off());
    val-=alpha;
    if(t) {
      T temp=grd.sampleCellCenter(*t,x=*beg,id);
      val+=(temp-tAmb)*beta;
    }
    ++beg;
  }
}
//unconditionally stable integrator
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOp<T,DIM,Encoder>::advectSemiLagrangian(const GridType& grd,const CellCenterData<T>& from,CellCenterData<T>& to,const MACVelData<DIM,T>& vel,T dt)
{
  CellCenterPointIt beg(grd,0);
  CellCenterPointIt end(grd,-1);
  VecDIMd pos,pos1,v;
  VecDIMi id;
  while(beg!=end) {
    pos=*beg;
    grd.sampleMACVel(vel,pos1=pos,id,v);
    pos1=pos-v*dt;
    to._data[beg.off()]=grd.template sampleCellCenter<T>(from,pos1,id);
    ++beg;
  }
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOp<T,DIM,Encoder>::advectMacCormack(const GridType& grd,const CellCenterData<T>& from,CellCenterData<T>& to,const MACVelData<DIM,T>& vel,const CellCenterData<unsigned char>& type,T dt)
{
  CellCenterData<T> minV(from),maxV(from),tmp(to);
  VecSTENCILd weights;
  VecSTENCILi indices;
  VecDIMd pos,pos1,v;
  VecDIMb ob;
  VecDIMi id;
  bool valid;
  T val;
  sizeType nrP=indices.size();
  //predict
  {
    CellCenterPointIt beg(grd,0);
    CellCenterPointIt end(grd,-1);
    while(beg!=end) {
      pos=*beg;
      grd.sampleMACVel(vel,pos1=pos,id,v);
      pos1=pos-v*dt;
      T& minValue=minV._data[beg.off()];
      T& maxValue=maxV._data[beg.off()];
      T& data=tmp._data[beg.off()]=0;
      grd.sampleStencilCellCenter(pos1,id,weights,indices,ob);
      //find value
      val=from._data[indices[0]];
      data=weights[0]*val;
      valid=!ob.array().any() && type._data[indices[0]] == VariableSizedGridOpProject<T,DIM,Encoder>::FLUID;
      minValue=maxValue=val;
      for(sizeType p=1; p<nrP; p++) {
        val=from._data[indices[p]];
        data+=weights[p]*val;
        valid=valid && type._data[indices[p]] == VariableSizedGridOpProject<T,DIM,Encoder>::FLUID;
        minValue=min<T>(minValue,val);
        maxValue=max<T>(maxValue,val);
      }
      if(!valid)
        minValue=maxValue=numeric_limits<T>::quiet_NaN();
      ++beg;
    }
  }
  //correct
  {
    CellCenterPointIt beg(grd,0);
    CellCenterPointIt end(grd,-1);
    while(beg!=end) {
      T& minValue=minV._data[beg.off()];
      T& maxValue=maxV._data[beg.off()];
      T& data=to._data[beg.off()];
      if(std::isnan(minValue))
        data=tmp._data[beg.off()];
      else {
        pos=*beg;
        grd.sampleMACVel(vel,pos1=pos,id,v);
        pos1=pos+v*dt;
        val=grd.template sampleCellCenter<T>(tmp,pos1,id);
        data=tmp._data[beg.off()]+(from._data[beg.off()]-val)/2;
        if(data < minValue || data > maxValue)
          data=tmp._data[beg.off()];
      }
      ++beg;
    }
  }
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOp<T,DIM,Encoder>::advectSemiLagrangian(const GridType& grd,const MACVelData<DIM,T>& from,MACVelData<DIM,T>& to,const MACVelData<DIM,T>& vel,T dt)
{
  VecDIMd pos,pos1,v;
  VecDIMi id;
  for(sizeType D=0; D<DIM; D++) {
    MACVelPointIt beg(D,grd,0);
    MACVelPointIt end(D,grd,-1);
    while(beg!=end) {
      pos=*beg;
      grd.sampleMACVel(vel,pos1=pos,id,v);
      pos1=pos-v*dt;
      to._data[D][beg.off()]=grd.template sampleMACVel<T>(D,from,pos1,id);
      ++beg;
    }
  }
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOp<T,DIM,Encoder>::advectMacCormack(const GridType& grd,const MACVelData<DIM,T>& from,MACVelData<DIM,T>& to,const MACVelData<DIM,T>& vel,const MACVelData<DIM,unsigned char>& type,T dt)
{
  VecSTENCILd weights;
  VecSTENCILi indices;
  VecDIMd pos,pos1,v;
  VecDIMb ob;
  VecDIMi id;
  bool valid;
  T val;
  sizeType nrP=indices.size();
  MACVelData<DIM,T> minV(from),maxV(from),tmp(to);
  for(sizeType D=0; D<DIM; D++) {
    //predict
    {
      MACVelPointIt beg(D,grd,0);
      MACVelPointIt end(D,grd,-1);
      while(beg!=end) {
        pos=*beg;
        grd.sampleMACVel(vel,pos1=pos,id,v);
        pos1=pos-v*dt;
        T& minValue=minV._data[D][beg.off()];
        T& maxValue=maxV._data[D][beg.off()];
        T& data=tmp._data[D][beg.off()]=0;
        grd.sampleStencilMACVel(D,pos1,id,weights,indices,ob);
        //find value
        val=from._data[D][indices[0]];
        data=weights[0]*val;
        valid=!ob.array().any() && type._data[D][indices[0]] > 0;
        minValue=maxValue=val;
        for(sizeType p=1; p<nrP; p++) {
          val=from._data[D][indices[p]];
          data+=weights[p]*val;
          valid=valid && type._data[D][indices[p]] > 0;
          minValue=min<T>(minValue,val);
          maxValue=max<T>(maxValue,val);
        }
        if(!valid)
          minValue=maxValue=numeric_limits<T>::quiet_NaN();
        ++beg;
      }
    }
    //correct
    {
      MACVelPointIt beg(D,grd,0);
      MACVelPointIt end(D,grd,-1);
      while(beg!=end) {
        T& minValue=minV._data[D][beg.off()];
        T& maxValue=maxV._data[D][beg.off()];
        T& data=to._data[D][beg.off()];
        if(std::isnan(minValue))
          data=tmp._data[D][beg.off()];
        else {
          pos=*beg;
          grd.sampleMACVel(vel,pos1=pos,id,v);
          pos1=pos+v*dt;
          val=grd.template sampleMACVel<T>(D,tmp,pos1,id);
          data=tmp._data[D][beg.off()]+(from._data[D][beg.off()]-val)/2;
          if(data < minValue || data > maxValue)
            data=tmp._data[D][beg.off()];
        }
        ++beg;
      }
    }
  }
}
//exponential integrator
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOp<T,DIM,Encoder>::advectExponential(const GridType& grd,const CellCenterData<T>& from,CellCenterData<T>& to,const MACVelData<DIM,T>& vel,const CellCenterData<unsigned char>& type,T dt,T eps)
{
  to._data=from._data;
  CellCenterData<T> A=from,B=from;
  for(sizeType it=1;; it++) {
    A._data*=(1/(T)it);
    mulExponential(grd,A,B,vel,type,dt);
    to._data+=B._data;
    //INFOV("BNorm: %f!",B._data.cwiseAbs().maxCoeff())
    if(B._data.cwiseAbs().maxCoeff() < eps)
      break;
    B._data.swap(A._data);
  }
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOp<T,DIM,Encoder>::mulExponential(const GridType& grd,const CellCenterData<T>& from,CellCenterData<T>& to,const MACVelData<DIM,T>& vel,const CellCenterData<unsigned char>& type,T dt)
{
  to._data.setZero();
  sizeType LID,RID;
  VecDIMi lid,rid;
  for(sizeType D=0; D<DIM; D++) {
    MACVelPointIt beg=MACVelPointIt::iterAllInternal(D,grd,0);
    MACVelPointIt end=MACVelPointIt::iterAllInternal(D,grd,-1);
    while(beg!=end) {
      LID=beg.offCellCenterLeft(&lid);
      RID=beg.offCellCenter(&rid);
      T v=vel._data[D][beg.off()]*dt,den;
      T fluxL=v*grd._invCellSzNodals[D][lid[D]];
      T fluxR=v*grd._invCellSzNodals[D][rid[D]];
      unsigned char LT=type._data[LID],RT=type._data[RID];
      if(LT == VariableSizedGridOpProject<T,DIM,Encoder>::FLUID &&
         RT == VariableSizedGridOpProject<T,DIM,Encoder>::FLUID) {
        if(v > 0)
          den=from._data[LID];
        else den=from._data[RID];
        to._data[RID]+=fluxR*den;
        to._data[LID]-=fluxL*den;
      }
      ++beg;
    }
  }
}
//particle integrator
template <typename T,int DIM,typename Encoder>
template <typename PS_TYPE>
void VariableSizedGridOp<T,DIM,Encoder>::warpParticle(const GridType& grd,PS_TYPE& pset)
{
  sizeType n=pset.size();
  for(sizeType i=0; i<n; i++)
    pset[i]._pos.template segment<DIM>(0)=grd.handleWarp(pset[i]._pos.template segment<DIM>(0));
}
template <typename T,int DIM,typename Encoder>
template <typename PS_TYPE>
void VariableSizedGridOp<T,DIM,Encoder>::advectParticle(const GridType& grd,const MACVelData<DIM,T>& vel,PS_TYPE& pset,T dt,unsigned char order)
{
  VecDIMi id;
  VecDIMd pos,v;
  sizeType n=pset.size();
  if(order == 1) {
    for(sizeType i=0; i<n; i++) {
      pos=pset[i]._pos.template segment<DIM>(0);
      grd.sampleMACVel(vel,pos,id,v);
      pset[i]._pos.template segment<DIM>(0)+=v*dt;
    }
  } else if(order == 2) {
    for(sizeType i=0; i<n; i++) {
      pos=pset[i]._pos.template segment<DIM>(0);
      grd.sampleMACVel(vel,pos,id,v);
      pos=pset[i]._pos.template segment<DIM>(0)+v*dt/2;
      grd.sampleMACVel(vel,pos,id,v);
      pset[i]._pos.template segment<DIM>(0)+=v*dt;
    }
  }
}
//integrator
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOp<T,DIM,Encoder>::debugAdvect(const string& path,const Vec3i& warpMode,bool rotate)
{
  recreate(path);
  GridType testGrid=createTestGrid(warpMode);
  //ImplicitFuncCircle func(0.2f,VecDIMd::Constant(0.5f),20);
  ImplicitFuncBox func(VecDIMd::Constant(0.2f),VecDIMd::Constant(0.4f),DIM);
  boost::shared_ptr<VelFunc<scalar>> vel;
  if(rotate)
    vel.reset(new VelFuncRotate(VecDIMd::Constant(1),Vec3::Unit(2)));
  else vel.reset(new VelFuncConstant(VecDIMd::Constant(-1)));
  //Nodal
  NodalData<T> densityNodal(testGrid);
  copyImplicitFuncNodal(testGrid,densityNodal,func);
  writeGridNodalVTK(path+"/densityNodal.vtk",testGrid,densityNodal);
  writeGridNodalPointVTK(path+"/densityNodalPoint.vtk",testGrid,densityNodal);
  //CellCenter
  CellCenterData<T> densityCellCenter(testGrid),tmpDensityCellCenter(testGrid),tmpDensityCellCenter2(testGrid);
  CellCenterData<unsigned char> typeCellCenter(testGrid);
  typeCellCenter._data.setConstant(VariableSizedGridOpProject<T,DIM,Encoder>::FLUID);
  copyImplicitFuncCellCenter(testGrid,densityCellCenter,func);
  writeGridCellCenterVTK(path+"/densityCellCenter.vtk",testGrid,densityCellCenter);
  writeGridCellCenterPointVTK(path+"/densityCellCenterPoint.vtk",testGrid,densityCellCenter);
  //velocity
  MACVelData<DIM,T> velGrd(testGrid);
  copyImplicitFuncMACVel(testGrid,velGrd,*vel);
  writeGridMACVelVTK(path+"/rotateMACVel.vtk",testGrid,velGrd,1);
  writeGridMACVelPointVTK(path+"/rotateMACVelPoint.vtk",testGrid,velGrd);
  //particle
  BBox<T,DIM> bb;
  ParticleSetN pset;
  ParticleN<scalar> p;
  for(sizeType i=0; i<100; i++) {
    bb=testGrid.getBBCellCenter();
    p._pos.setZero();
    for(sizeType D=0; D<DIM; D++)
      p._pos[D]=RandEngine::randR(bb._minC[D],bb._maxC[D]);
    pset.addParticle(p);
  }
  //advect
#define NR_FRAME 100
#define DT 0.01f
  tmpDensityCellCenter2=densityCellCenter;
  for(sizeType frame=0; frame<NR_FRAME; frame++) {
    //INFO("Advect Exponential!")
    advectExponential(testGrid,densityCellCenter,tmpDensityCellCenter,velGrd,typeCellCenter,DT,1E-5f);
    writeGridCellCenterVTK(path+"/ExponentialFrm"+boost::lexical_cast<string>(frame)+".vtk",testGrid,tmpDensityCellCenter);
    densityCellCenter._data.swap(tmpDensityCellCenter._data);
  }
  densityCellCenter=tmpDensityCellCenter2;
  for(sizeType frame=0; frame<NR_FRAME; frame++) {
    //INFO("Advect SemiLagrangian!")
    advectSemiLagrangian(testGrid,densityCellCenter,tmpDensityCellCenter,velGrd,DT);
    writeGridCellCenterVTK(path+"/SemiLagrangianFrm"+boost::lexical_cast<string>(frame)+".vtk",testGrid,tmpDensityCellCenter);
    densityCellCenter._data.swap(tmpDensityCellCenter._data);
  }
  densityCellCenter=tmpDensityCellCenter2;
  for(sizeType frame=0; frame<NR_FRAME; frame++) {
    //INFO("Advect MacCormack!")
    advectMacCormack(testGrid,densityCellCenter,tmpDensityCellCenter,velGrd,typeCellCenter,DT);
    writeGridCellCenterVTK(path+"/MacCormackFrm"+boost::lexical_cast<string>(frame)+".vtk",testGrid,tmpDensityCellCenter);
    densityCellCenter._data.swap(tmpDensityCellCenter._data);
  }
  for(sizeType frame=0; frame<NR_FRAME; frame++) {
    //INFO("Advect Particle!")
    advectParticle(testGrid,velGrd,pset,DT);
    warpParticle(testGrid,pset);
    pset.writeVTK(path+"/ParticleFrm"+boost::lexical_cast<string>(frame)+".vtk");
  }
#undef DT
#undef NR_FRAME
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOp<T,DIM,Encoder>::debugAdvectAllWarp(bool rotate)
{
  INFO("Debugging Advect all warp cases!")
  string typeVel(rotate ? "Rotate" : "Constant");
  debugAdvect("advect"+typeVel+GridType::printWarp(Vec3i::Zero()),Vec3i::Zero(),rotate);
  if(Encoder::isConstantEncoder)
    for(sizeType D=0; D<DIM; D++)
      debugAdvect("advect"+typeVel+GridType::printWarp(Vec3i::Unit(D)),Vec3i::Unit(D),rotate);
}
//debug
template <typename T,int DIM,typename Encoder>
typename VariableSizedGridOp<T,DIM,Encoder>::GridType VariableSizedGridOp<T,DIM,Encoder>::createTestGrid(const Vec3i& warpMode)
{
  typedef VariableSizedGrid<T,DIM,Encoder> GridType;
  typedef typename GridType::VecDIMi VecDIMi;
  BBox<sizeType,DIM> extend;
  BBox<T,DIM> bbInterior;
  VecDIMi nrCellIterator;
  for(sizeType d=0; d<DIM; d++) {
    bbInterior._minC[d]=0;
    bbInterior._maxC[d]=0.5f*(d*0.25f+1);
    nrCellIterator[d]=100;
    extend._minC[d]=0;
    extend._maxC[d]=100;
  }
  //test grid
  GridType testGrid(bbInterior,nrCellIterator,extend,&warpMode);
  return testGrid;
}
template <typename T,int DIM,typename Encoder>
ScalarField VariableSizedGridOp<T,DIM,Encoder>::createTestSolidPhi(const VariableSizedGrid<T,DIM,Encoder>& testGrid,const VecDIMd* ctrGiven,const scalar* radCoef)
{
  typedef VariableSizedGrid<T,DIM,Encoder> GridType;
  typedef typename GridType::VecDIMi VecDIMi;
  ScalarField nodalSolidPhi;
  Vec3i nrCell=assignVariableTo<Vec3i,VecDIMi>(testGrid._nrPointNodal);
  BBox<T,DIM> bbDIM=testGrid.getBBNodal();
  BBox<scalar> bb=assignVariableTo<scalar,3,T,DIM>(bbDIM);
  nodalSolidPhi.reset(nrCell,bb,0,false,1);
  for(sizeType i=0; i<nodalSolidPhi.getNrPoint().prod(); i++) {
    VecDIMd pt=nodalSolidPhi.getPt(nodalSolidPhi.getIndex(i)).segment<DIM>(0);
    VecDIMd ctr=ctrGiven ? *ctrGiven : (bbDIM._minC+bbDIM._maxC)/2;
    scalar len=bbDIM.getExtent().norm()*(radCoef ? (*radCoef) : 0.5f);
    nodalSolidPhi.get(i)=((pt-ctr).norm() < len/2) ? 1 : -1;
  }
  return nodalSolidPhi;
}
//interp
#define INTERP_OP(DIM,TYPE,SPARSITY,TDATA)  \
template void VariableSizedGridOp<scalar,DIM,Encoder##TYPE<SPARSITY> >::writeGridNodalVTK<TDATA>(const string& path,const GridType& grd,const NodalData<TDATA>& data,bool extrude); \
template void VariableSizedGridOp<scalar,DIM,Encoder##TYPE<SPARSITY> >::writeGridNodalPointVTK<TDATA>(const string& path,const GridType& grd,const NodalData<TDATA>& data,bool extrude); \
template void VariableSizedGridOp<scalar,DIM,Encoder##TYPE<SPARSITY> >::writeGridCellCenterVTK<TDATA>(const string& path,const GridType& grd,const CellCenterData<TDATA>& data,bool extrude); \
template void VariableSizedGridOp<scalar,DIM,Encoder##TYPE<SPARSITY> >::writeGridCellCenterPointVTK<TDATA>(const string& path,const GridType& grd,const CellCenterData<TDATA>& data,bool extrude); \
template void VariableSizedGridOp<scalar,DIM,Encoder##TYPE<SPARSITY> >::writeGridMACVelVTK<TDATA>(const string& path,const GridType& grd,const MACVelData<DIM,TDATA>& data,scalar len); \
template void VariableSizedGridOp<scalar,DIM,Encoder##TYPE<SPARSITY> >::writeGridMACVelCompVTK<TDATA>(unsigned char D,const string& path,const GridType& grd,const MACVelData<DIM,TDATA>& data); \
template void VariableSizedGridOp<scalar,DIM,Encoder##TYPE<SPARSITY> >::writeGridMACVelPointVTK<TDATA>(const string& path,const GridType& grd,const MACVelData<DIM,TDATA>& data); \
//instance
#define INSTANCE(DIM,TYPE,SPARSITY) \
template class VariableSizedGridOp<scalar,DIM,Encoder##TYPE<SPARSITY> >;  \
INTERP_OP(DIM,TYPE,SPARSITY,scalarF)  \
INTERP_OP(DIM,TYPE,SPARSITY,scalarD)  \
INTERP_OP(DIM,TYPE,SPARSITY,sizeType)  \
INTERP_OP(DIM,TYPE,SPARSITY,unsigned char)  \
template void VariableSizedGridOp<scalar,DIM,Encoder##TYPE<SPARSITY> >::copyImplicitFuncNodal<scalar>(const GridType& grd,NodalData<scalar>& data,const ImplicitFunc<scalar>& func);  \
template void VariableSizedGridOp<scalar,DIM,Encoder##TYPE<SPARSITY> >::copyImplicitFuncCellCenter<scalar>(const GridType& grd,CellCenterData<scalar>& data,const ImplicitFunc<scalar>& func);  \
template void VariableSizedGridOp<scalar,DIM,Encoder##TYPE<SPARSITY> >::copyImplicitFuncMACVel<scalar>(const GridType& grd,MACVelData<DIM,scalar>& data,const VelFunc<scalar>& func); \
template void VariableSizedGridOp<scalar,DIM,Encoder##TYPE<SPARSITY> >::copyFromGridNodal<scalar>(const GridType& grd,NodalData<scalar>& data,const ScalarField& func);  \
template void VariableSizedGridOp<scalar,DIM,Encoder##TYPE<SPARSITY> >::copyFromGridCellCenter<scalar>(const GridType& grd,CellCenterData<scalar>& data,const ScalarField& func);  \
template void VariableSizedGridOp<scalar,DIM,Encoder##TYPE<SPARSITY> >::copyToGridNodal<scalar>(const GridType& grd0,const GridType& grd,const NodalData<scalar>& data,ScalarField& func);  \
template void VariableSizedGridOp<scalar,DIM,Encoder##TYPE<SPARSITY> >::copyToGridCellCenter<scalar>(const GridType& grd0,const GridType& grd,const CellCenterData<scalar>& data,ScalarField& func);  \
template void VariableSizedGridOp<scalar,DIM,Encoder##TYPE<SPARSITY> >::warpParticle<ParticleSetN>(const GridType& grd,ParticleSetN& pset); \
template void VariableSizedGridOp<scalar,DIM,Encoder##TYPE<SPARSITY> >::advectParticle<ParticleSetN>(const GridType& grd,const MACVelData<DIM,scalar>& vel,ParticleSetN& pset,scalar dt,unsigned char order);
INSTANCE_ALL
