#include "RigidSolver.h"
#include "RigidBodyMass.h"
#include "RigidContact.h"
#include <CommonFile/geom/BVHBuilder.h>
#include <CommonFile/RotationUtil.h>

USE_PRJ_NAMESPACE

//RigidBodyShapeInterface
void samplePSet(const StaticGeomCell& f,BBox<scalar> bb,const Vec3& cellSz,ParticleSetN& solidPSet)
{
  for(sizeType d=0; d<f.dim(); d++) {
    bb._minC[d]-=cellSz[d]*2;
    bb._maxC[d]+=cellSz[d]*2;
  }
  const Vec3 ext=bb.getExtent();
  const scalar thres=cellSz.maxCoeff()*0.2f;
  const sizeType MAX_ITER=10,nrParticlePerGrid=1<<f.dim();
  const Vec3i nrPoint=ceilV(Vec3(ext.x()/std::max(cellSz.x(),EPS),
                                 ext.y()/std::max(cellSz.y(),EPS),
                                 ext.z()/std::max(cellSz.z(),EPS)))+Vec3i::Ones();
  if(cellSz.minCoeff() < 0)
    return;
  //tmp
  ParticleN<scalar> p;
  scalar solidPhi;
  Vec3 n,grad;
  bool inside;
  //sample
  for(sizeType x=0; x<nrPoint.x(); x++)
    for(sizeType y=0; y<nrPoint.y(); y++)
      for(sizeType z=0; z<nrPoint.z(); z++) {
        Vec3 pt0=bb._minC+Vec3((scalar)x*cellSz.x(),(scalar)y*cellSz.y(),(scalar)z*cellSz.z());
        for(sizeType i=0; i<nrParticlePerGrid; i++) {
          //random adjustment
          Vec3& pt=p._pos=pt0;
          for(sizeType d=0; d<f.dim(); d++)
            pt[d]+=RandEngine::randR01()*cellSz[d];
          //adjust
          sizeType iter=0;
          inside=f.closest(pt,n,&grad);
          solidPhi=inside ? -n.norm() : n.norm();
          for(; solidPhi > -thres && iter < MAX_ITER; iter++) {
            pt+=grad*(-thres-solidPhi);
            inside=f.closest(pt,n,&grad);
            solidPhi=inside ? -n.norm() : n.norm();
          }
          if(iter == MAX_ITER)
            continue;
          inside=f.closest(pt,n,&grad);
          p._normal=p._vel=grad;
          solidPSet.addParticle(p);
        }
      }
}
RigidBodyShape::RigidBodyShape():Serializable(typeid(RigidBodyShape).name()) {}
RigidBodyShape::RigidBodyShape(boost::shared_ptr<StaticGeomCell> cell,scalar cellSz,bool doInit):Serializable(typeid(RigidBodyShape).name())
{
  _cell=cell;
  if(doInit)
    init(cellSz);
}
RigidBodyShape::RigidBodyShape(const scalar rad,sizeType dim,scalar cellSz,bool doInit):Serializable(typeid(RigidBodyShape).name())
{
  _cell.reset(new SphereGeomCell(Mat4::Identity(),dim,rad));
  if(doInit)
    init(cellSz);
}
RigidBodyShape::RigidBodyShape(const Vec3& ext,sizeType dim,scalar cellSz,bool doInit):Serializable(typeid(RigidBodyShape).name())
{
  _cell.reset(new BoxGeomCell(Mat4::Identity(),dim,ext));
  if(doInit)
    init(cellSz);
}
RigidBodyShape::RigidBodyShape(vector<boost::shared_ptr<StaticGeomCell> > children,scalar cellSz,bool doInit):Serializable(typeid(RigidBodyShape).name())
{
  _cell.reset(new CompositeGeomCell(Mat4::Identity(),children));
  if(doInit)
    init(cellSz);
}
void RigidBodyShape::init(scalar cellSz)
{
  //write mesh
  ObjMesh mesh;
  _cell->getMesh(mesh,true);
  RigidBodyMass mass(mesh);
  //mass
  _M=mass.getM();
  //inertia tensor
  _IB=mass.getMassCOM().block<3,3>(3,3);
  if(dim() == 2) {
    scalar tmp=_IB(2,2);
    _IB.setIdentity();
    _IB(2,2)=tmp;
  }
  _invIB=_IB.inverse();
  //sample pset
  BBox<scalar> bb=_cell->getBB();
  samplePSet(*_cell,bb,Vec3::Constant(cellSz),_solidPSet);
}
bool RigidBodyShape::read(std::istream& is,IOData* dat)
{
  StaticGeom::registerType(dat);
  readBinaryData(_M,is);
  readBinaryData(_IB,is);
  readBinaryData(_invIB,is);
  _solidPSet.read(is,NULL);
  readBinaryData(_cell,is,dat);
  return is.good();
}
bool RigidBodyShape::write(std::ostream& os,IOData* dat) const
{
  StaticGeom::registerType(dat);
  writeBinaryData(_M,os);
  writeBinaryData(_IB,os);
  writeBinaryData(_invIB,os);
  _solidPSet.write(os,NULL);
  writeBinaryData(_cell,os,dat);
  return os.good();
}
boost::shared_ptr<Serializable> RigidBodyShape::copy() const
{
  return boost::shared_ptr<Serializable>(new RigidBodyShape());
}
void RigidBodyShape::writeMeshVTK(const Mat3& rot,const Vec3& pos,VTKWriter<scalar>& os) const
{
  ObjMesh mesh;
  _cell->getMesh(mesh,true);
  mesh.getPos()=pos;
  mesh.getT()=rot;
  mesh.applyTrans(Vec3::Zero());
  mesh.writeVTK(os);
}
void RigidBodyShape::writePSetVTK(const Mat3& rot,const Vec3& pos,VTKWriter<scalar>& os) const
{
  _solidPSet.writeVTK(rot,pos,os);
}
sizeType RigidBodyShape::dim() const
{
  return _cell->dim();
}
void RigidBodyShape::getBB(BBox<scalar>& bb) const
{
  bb=_cell->getBB(true);
}
void RigidBodyShape::updateBB(const Mat3& rot,const Vec3& pos,BBox<scalar>& bb,bool fine) const
{
  Mat4 T=Mat4::Identity();
  T.block<3,3>(0,0)=rot;
  T.block<3,1>(0,3)=pos;
  _cell->setT(T);
  bb=_cell->getBB();
  _cell->setT(Mat4::Identity());
}
scalar RigidBodyShape::dist(const Vec3& pos) const
{
  Vec3 n,normal;
  bool inside=_cell->closest(pos,n,&normal);
  if(inside)
    return -n.norm();
  else return n.norm();
}
Vec3 RigidBodyShape::gradDist(const Vec3& pos) const
{
  Vec3 n,normal;
  _cell->closest(pos,n,&normal);
  return normal;
}
scalar RigidBodyShape::rayQuery(const Vec3& f,const Vec3& t) const
{
  return _cell->rayQuery(f,t-f);
}
sizeType RigidBodyShape::nrSolidPSet() const
{
  return _solidPSet.size();
}
const ParticleSetN& RigidBodyShape::getSolidPSet() const
{
  return _solidPSet;
}
const SphereGeomCell& RigidBodyShape::getCellBall() const
{
  return *boost::dynamic_pointer_cast<SphereGeomCell>(_cell);
}
const BoxGeomCell& RigidBodyShape::getCellBox() const
{
  return *boost::dynamic_pointer_cast<BoxGeomCell>(_cell);
}
const CompositeGeomCell& RigidBodyShape::getCellComposite() const
{
  return *boost::dynamic_pointer_cast<CompositeGeomCell>(_cell);
}
const StaticGeomCell& RigidBodyShape::getCell() const
{
  return *_cell;
}
StaticGeomCell& RigidBodyShape::getCell()
{
  return *_cell;
}
scalar RigidBodyShape::getM(const scalar& rho) const
{
  if(rho<=0.0f)
    return 0.0f;
  else return _M*rho;
}
Mat3 RigidBodyShape::getIB(const scalar& rho) const
{
  if(rho<=0.0f)
    return Mat3::Zero();
  else return _IB*rho;
}
Mat3 RigidBodyShape::getInvIB(const scalar& rho) const
{
  if(rho<=0.0f)
    return Mat3::Zero();
  else return _invIB/rho;
}
bool RigidBodyShape::isBall() const
{
  return boost::dynamic_pointer_cast<SphereGeomCell>(_cell) != NULL;
}
bool RigidBodyShape::isBox() const
{
  return boost::dynamic_pointer_cast<BoxGeomCell>(_cell) != NULL;
}
bool RigidBodyShape::isComposite() const
{
  return boost::dynamic_pointer_cast<CompositeGeomCell>(_cell) != NULL;
}
//RigidBodyState
RigidBodyState::RigidBodyState(sizeType id):Serializable(typeid(RigidBodyState).name())
{
  _x.setZero();
  _q.setIdentity();
  _p.setZero();
  _L.setZero();
  _force.setZero();
  _torque.setZero();
  _globalTime=0.0f;
}
RigidBodyState::RigidBodyState(const string& name,sizeType id):Serializable(name)
{
  _x.setZero();
  _q.setIdentity();
  _p.setZero();
  _L.setZero();
  _force.setZero();
  _torque.setZero();
  _globalTime=0.0f;
}
bool RigidBodyState::read(istream& is,IOData* dat)
{
  readBinaryData(_x,is);
  readBinaryData(_q,is);
  readBinaryData(_p,is);
  readBinaryData(_L,is);
  readBinaryData(_force,is);
  readBinaryData(_torque,is);
  readBinaryData(_globalTime,is);
  return is.good();
}
bool RigidBodyState::write(ostream& os,IOData* dat) const
{
  writeBinaryData(_x,os);
  writeBinaryData(_q,os);
  writeBinaryData(_p,os);
  writeBinaryData(_L,os);
  writeBinaryData(_force,os);
  writeBinaryData(_torque,os);
  writeBinaryData(_globalTime,os);
  return os.good();
}
boost::shared_ptr<Serializable> RigidBodyState::copy() const
{
  return boost::shared_ptr<Serializable>(new RigidBodyState());
}
bool RigidBodyState::operator<(const RigidBodyState& other) const
{
  return _globalTime < other._globalTime;
}
//RigidBodyTrajectory
RigidBodyTrajectory::RigidBodyTrajectory() :Serializable(typeid(RigidBodyTrajectory).name())
{
  _bodyId=-1;
}
void RigidBodyTrajectory::printInfo() const
{
  INFOV("Begin Trajectory with %d keyframes",_traj.size())
  for(sizeType i=0; i<(sizeType)_traj.size(); i++) {
    INFOV("\tKeyframe: Pos(%f %f %f), Quat(%f %f %f %f)",
          _traj[i]._x[0], _traj[i]._x[1], _traj[i]._x[2],
          _traj[i]._q.w(),_traj[i]._q.x(),_traj[i]._q.y(),_traj[i]._q.z())
  }
}
bool RigidBodyTrajectory::read(std::istream& is,IOData* dat)
{
  readBinaryData(_bodyId,is);
  readVector(_traj,is);
  return is.good();
}
bool RigidBodyTrajectory::write(std::ostream& os,IOData* dat) const
{
  writeBinaryData(_bodyId,os);
  writeVector(_traj,os);
  return os.good();
}
boost::shared_ptr<Serializable> RigidBodyTrajectory::copy() const
{
  return boost::shared_ptr<Serializable>(new RigidBodyTrajectory());
}
std::pair<Vec3,Vec3> RigidBodyTrajectory::MOnTraj(scalar time) const
{
  ASSERT(_traj.size() >= 1)
  if(_traj.size() == 1)
    return std::pair<Vec3,Vec3>(_traj[0]._p,_traj[0]._L);

  time=fmod(time,_traj.back()._globalTime);
  std::pair<Vec3,Vec3> ret;
  const sizeType nr=(sizeType)_traj.size();
  for(sizeType i=1; i<nr; i++) {
    if(_traj[i]._globalTime > time) {
      scalar t0=(time-_traj[i-1]._globalTime)/
                (_traj[i]._globalTime-_traj[i-1]._globalTime);
      ret.first=interp1D(_traj[i-1]._p,_traj[i]._p,t0);
      ret.second=interp1D(_traj[i-1]._L,_traj[i]._L,t0);
      return ret;
    }
  }
  ret.first=_traj.back()._p;
  ret.second=_traj.back()._L;
  return ret;
}
std::pair<Vec3,RigidBodyTrajectory::Quat> RigidBodyTrajectory::POnTraj(scalar time) const
{
  ASSERT(_traj.size() >= 1)
  if(_traj.size() == 1)
    return std::pair<Vec3,Quat>(_traj[0]._x,_traj[0]._q);

  time=fmod(time,_traj.back()._globalTime);
  std::pair<Vec3,Quat> ret;
  const sizeType nr=(sizeType)_traj.size();
  for(sizeType i=1; i<nr; i++) {
    if(_traj[i]._globalTime > time) {
      scalar t0=(time-_traj[i-1]._globalTime)/
                (_traj[i]._globalTime-_traj[i-1]._globalTime);
      ret.first=interp1D(_traj[i-1]._x,_traj[i]._x,t0);
      ret.second=_traj[i-1]._q.slerp(t0,_traj[i]._q);
      return ret;
    }
  }
  ret.first=_traj.back()._x;
  ret.second=_traj.back()._q;
  return ret;
}
std::pair<Vec3,Vec3> RigidBodyTrajectory::FOnTraj(scalar time) const
{
  ASSERT(_traj.size() >= 1)
  if(_traj.size() == 1)
    return std::pair<Vec3,Vec3>(_traj[0]._force,_traj[0]._torque);

  time=fmod(time,_traj.back()._globalTime);
  std::pair<Vec3,Vec3> ret;
  const sizeType nr=(sizeType)_traj.size();
  for(sizeType i=1; i<nr; i++) {
    if(_traj[i]._globalTime > time) {
      ret.first=_traj[i-1]._force;
      ret.second=_traj[i-1]._torque;
      return ret;
    }
  }
  ret.first=_traj.back()._force;
  ret.second=_traj.back()._torque;
  return ret;
}
void RigidBodyTrajectory::addKeyframe(const RigidBodyState& sKF,const scalar& dt)
{
  //insert the keyframe
  vector<RigidBodyState>::iterator iter=lower_bound(_traj.begin(),_traj.end(),sKF);
  if(iter == _traj.end())
    _traj.insert(iter,sKF);
  else if(iter->_globalTime == sKF._globalTime)
    *iter=sKF;
  else _traj.insert(iter,sKF);
}
//RigidBodyDefault
RigidBody::RigidBody()
  :RigidBodyState(typeid(RigidBody).name()) {}
RigidBody::RigidBody(boost::shared_ptr<RigidBodyShape> shape)
  :RigidBodyState(typeid(RigidBody).name()),_rho(1000.0f),_restitute(1)
{
  _shape=shape;
  init();
}
void RigidBody::init()
{
  _x.setZero();
  _q.setIdentity();
  _p.setZero();
  _L.setZero();
  _force.setZero();
  _torque.setZero();

  _worldBBValid=false;
  computePositionAux();
  computeVelocityAux();
}
void RigidBody::setShape(boost::shared_ptr<RigidBodyShape>  shape)
{
  _shape=shape;
}
void RigidBody::setGlobalTime(const scalar& globalTime)
{
  _globalTime=globalTime;
}
scalar RigidBody::getGlobalTime() const
{
  return _globalTime;
}
bool RigidBody::read(std::istream& is,IOData* dat)
{
  registerType<RigidBodyTrajectory>(dat);
  RigidBodyState::read(is,dat);
  //rigid body state
  readBinaryData(_traj,is,dat);
  readBinaryData(_shape,is,dat);
  //physics property
  readBinaryData(_rho,is);
  readBinaryData(_restitute,is);
  //aux property
  readBinaryData(_R,is);
  readBinaryData(_I,is);
  readBinaryData(_invI,is);
  readBinaryData(_v,is);
  readBinaryData(_w,is);
  _worldBBValid=false;
  return is.good();
}
bool RigidBody::write(std::ostream& os,IOData* dat) const
{
  registerType<RigidBodyTrajectory>(dat);
  RigidBodyState::write(os,dat);
  //rigid body state
  writeBinaryData(_traj,os,dat);
  writeBinaryData(_shape,os,dat);
  //physics property
  writeBinaryData(_rho,os);
  writeBinaryData(_restitute,os);
  //aux property
  writeBinaryData(_R,os);
  writeBinaryData(_I,os);
  writeBinaryData(_invI,os);
  writeBinaryData(_v,os);
  writeBinaryData(_w,os);
  return os.good();
}
boost::shared_ptr<Serializable> RigidBody::copy() const
{
  return boost::shared_ptr<Serializable>(new RigidBody());
}
void RigidBody::setPos(const Vec3& pos)
{
  if(!hasTraj()) {
    _x=pos;
    computePositionAux();
    _worldBBValid=false;
    //_sys.moveCollisionBody(*this);
  }
}
void RigidBody::setRot(const Quat& q)
{
  if(!hasTraj()) {
    _q=q;
    computePositionAux();
    _worldBBValid=false;
    //_sys.moveCollisionBody(*this);
  }
}
void RigidBody::setP(const Vec3& p)
{
  if(!hasTraj()) {
    _p=p;
    computeVelocityAux();
  }
}
void RigidBody::setL(const Vec3& L)
{
  if(!hasTraj()) {
    _L=L;
    computeVelocityAux();
  }
}
Vec3 RigidBody::pos() const
{
  return _x;
}
Mat3 RigidBody::rot() const
{
  return _R;
}
RigidBody::Quat RigidBody::q() const
{
  return _q;
}
Vec3 RigidBody::p() const
{
  return _p;
}
Vec3 RigidBody::L() const
{
  return _L;
}
Vec3 RigidBody::linSpd() const
{
  return _v;
}
Vec3 RigidBody::rotSpd() const
{
  return _w;
}
Mat3 RigidBody::I() const
{
  return _I;
}
Mat3 RigidBody::invI() const
{
  return _invI;
}
Vec3 RigidBody::vel(const Vec3& pos) const
{
  return _w.cross(pos-_x)+_v;
}
bool RigidBody::valid() const
{
  return true;
}
bool RigidBody::freeze() const
{
  return getM() <= 0.0f || hasTraj();
}
void RigidBody::applyTorque(const Vec3& t)
{
  if(!freeze())_torque+=t;
}
void RigidBody::applyForce(const Vec3& f)
{
  if(!freeze())_force+=f;
}
void RigidBody::applyAngularImpulse(const Vec3& AJ)
{
  if(!freeze())setL(L()+AJ);
}
void RigidBody::applyImpulse(const Vec3& J)
{
  if(!freeze())setP(p()+J);
}
void RigidBody::getWorldBB(BBox<scalar>& bb) const
{
  if(!_worldBBValid)
    const_cast<RigidBody&>(*this).updateBB();
  bb=_worldBB;
}
void RigidBody::updateBB(bool fine)
{
  _shape->updateBB(rot(),pos(),_worldBB,fine);
  _worldBBValid=true;
}
void RigidBody::applyWorldForce(const Vec3& f,const Vec3& p)
{
  applyForce(f);
  applyTorque((p-pos()).cross(f));
}
void RigidBody::applyWorldImpulse(const Vec3& J,const Vec3& p)
{
  applyImpulse(J);
  applyAngularImpulse((p-pos()).cross(J));
}
const RigidBodyShape& RigidBody::getShape() const
{
  return *_shape;
}
RigidBodyShape& RigidBody::getShape()
{
  return *_shape;
}
Vec3 RigidBody::toWorld(const Vec3& local) const
{
  return rot()*local+pos();
}
Vec3 RigidBody::toLocal(const Vec3& world) const
{
  return rot().transpose()*(world-pos());
}
scalar RigidBody::dist(const Vec3& world) const
{
  return _shape->dist(toLocal(world));
}
void RigidBody::getPSet(ParticleSetN& pset) const
{
  ParticleSetN::ParticleType p;
  const ParticleSetN& ps=_shape->getSolidPSet();
  for(sizeType i=0; i<ps.size(); i++) {
    p=ps[i];
    p._pos=_R*p._pos+_x;
    p._normal=_R*p._normal;
    p._vel=vel(p._pos);
    pset.addParticle(p);
  }
}
void RigidBody::writeMeshVTK(VTKWriter<scalar>& os) const
{
  getShape().writeMeshVTK(rot(),pos(),os);
}
void RigidBody::writeMeshVTK(const std::string& str) const
{
  VTKWriter<scalar> os("body",str,true);
  writeMeshVTK(os);
}
void RigidBody::writePSetVTK(VTKWriter<scalar>& os) const
{
  getShape().writePSetVTK(rot(),pos(),os);
}
void RigidBody::writePSetVTK(const std::string& str) const
{
  VTKWriter<scalar> os("body",str,true);
  writePSetVTK(os);
}
void RigidBody::applyFirstOrderCentralImpulse(const Vec3& J)
{
  if(!freeze()) {
    _x+=J/getM();
    //_sys.moveCollisionBody(*this);
  }
  //no auxiliary computation needed
}
scalar RigidBody::energy() const
{
  return (_w.transpose()*I()*_w+getM()*_v.squaredNorm())*0.5f;
}
scalar RigidBody::getM() const
{
  return getShape().getM(_rho);
}
Mat3 RigidBody::getIB() const
{
  return getShape().getIB(_rho);
}
Mat3 RigidBody::getInvIB() const
{
  return getShape().getInvIB(_rho);
}
const scalar& RigidBody::rho() const
{
  return _rho;
}
scalar& RigidBody::rho()
{
  return _rho;
}
const scalar& RigidBody::restitute() const
{
  return _restitute;
}
scalar& RigidBody::restitute()
{
  return _restitute;
}
void RigidBody::setTraj(boost::shared_ptr<RigidBodyTrajectory> traj)
{
  _traj=traj;
}
const RigidBodyTrajectory& RigidBody::traj() const
{
  return *_traj;
}
bool RigidBody::hasTraj() const
{
  return _traj != NULL;
}
std::pair<Vec3,Vec3> RigidBody::MOnTraj(const scalar& dt) const
{
  return _traj->MOnTraj(getGlobalTime()+dt);
}
std::pair<Vec3,RigidBody::Quat> RigidBody::POnTraj(const scalar& dt) const
{
  return _traj->POnTraj(getGlobalTime()+dt);
}
std::pair<Vec3,Vec3> RigidBody::FOnTraj(const scalar& dt) const
{
  return _traj->FOnTraj(getGlobalTime()+dt);
}
void RigidBody::getPosition(RigidBodyState& state)
{
  state._x=_x;
  state._q=_q;
}
void RigidBody::getVelocity(RigidBodyState& state)
{
  state._p=_p;
  state._L=_L;
}
void RigidBody::getForce(RigidBodyState& state)
{
  state._force=_force;
  state._torque=_torque;
}
void RigidBody::setPosition(const RigidBodyState& state)
{
  _x=state._x;
  _q=state._q;
  _worldBBValid=false;
  computePositionAux();
  //_sys.moveCollisionBody(*this);
}
void RigidBody::setVelocity(const RigidBodyState& state)
{
  _p=state._p;
  _L=state._L;
  computeVelocityAux();
}
void RigidBody::setForce(const RigidBodyState& state)
{
  if(!freeze()) {
    _force=state._force;
    _torque=state._torque;
  }
}
void RigidBody::getDerivVelocity(RigidBodyState& state)
{
  state._p=_force;
  state._L=_torque;
  if(rho() <= 0.0f) {
    state._p.setZero();
    state._L.setZero();
  }
}
void RigidBody::getDerivPosition(RigidBodyState& state)
{
  state._x=_v;
  state._q=(Quat(0.0f,_w.x()*0.5f,_w.y()*0.5f,_w.z()*0.5f)*_q);
}
void RigidBody::adjustVelocity(const RigidBodyState& last,scalar dt)
{
  Mat3 R=rot()*last._q.toRotationMatrix().transpose();
  Vec3 w=invExpW<scalar>(R);

  setP((pos()-last._x)/dt*getM());
  setL(I()*w/dt);
}
void RigidBody::debugAdjustVelocity()
{
#define NR_TEST 100
  boost::shared_ptr<RigidBodyShape> shape(new RigidBodyShape(0.1f,3,0.01f));
  RigidBodyState last,curr;
  RigidBody body(shape);
  body.setShape(shape);
  for(sizeType i=0; i<NR_TEST; i++) {
    Quat qLast(RandEngine::randR01(),RandEngine::randR01(),RandEngine::randR01(),RandEngine::randR01());
    Quat qCurr(RandEngine::randR01(),RandEngine::randR01(),RandEngine::randR01(),RandEngine::randR01());
    if(shape->dim() == 2)
      qLast.x()=qLast.y()=qCurr.x()=qCurr.y()=0;
    qLast.normalize();
    qCurr.normalize();
    //last
    body.setRot(qLast);
    body.setPos(Vec3::Random());
    body.getPosition(last);
    //current
    body.setRot(qCurr);
    body.setPos(Vec3::Random());
    body.getPosition(curr);
    //test
    scalar dt=0.01f;
    body.adjustVelocity(last,dt);
    body.setPosition(last);
    body.advancePosition(dt);
    INFOV("Pos: %f Err: %f",body._x.norm(),(body._x-curr._x).norm())
    INFOV("Rot: %f Err: %f",curr._q.toRotationMatrix().norm(),(body.rot()-curr._q.toRotationMatrix()).norm())
  }
#undef NR_TEST
}
void RigidBody::advanceVelocity(const scalar& dt)
{
  if(hasTraj()) {
    std::pair<Vec3,Vec3> P=_traj->MOnTraj(_globalTime);
    _p=P.first;
    _L=P.second;
  } else {
    RigidBodyState state;
    getDerivVelocity(state);
    _p+=state._p*dt;
    _L+=state._L*dt;
  }
  computeVelocityAux();
}
void RigidBody::advancePosition(const scalar& dt)
{
  if(hasTraj()) {
    std::pair<Vec3,Quat> P=_traj->POnTraj(_globalTime);
    _x=P.first;
    _q=P.second;
  } else {
    RigidBodyState state;
    getDerivPosition(state);
    _x+=state._x*dt;
    //use normalized finite difference
    /*_q.x()+=state._q.x()*dt;
    _q.y()+=state._q.y()*dt;
    _q.z()+=state._q.z()*dt;
    _q.w()+=state._q.w()*dt;
    _q.normalize();*/
    //use exponential map
    scalar wLen=std::max(_w.norm(),EPS);
    scalar theta=wLen*dt/2.0f;
    _q=Quat(cos(theta),
            (_w.x()/wLen)*sin(theta),
            (_w.y()/wLen)*sin(theta),
            (_w.z()/wLen)*sin(theta))*_q;
    _q.normalize();
  }
  //update bounding box
  updateBB();
  computePositionAux();
  //_sys.moveCollisionBody(*this);
}
void RigidBody::computePositionAux()
{
  //aux variable
  _R=_q.toRotationMatrix();
  _I=_R*getIB()*_R.transpose();
  _invI=_R*getInvIB()*_R.transpose();
}
void RigidBody::computeVelocityAux()
{
  if(rho() <= 0.0f) {
    _v=_p;
    _w=_L;
  } else {
    _v=_p/getM();
    _w=_invI*_L;
  }
}
void RigidBody::clearForceAndTorque()
{
  _force.setZero();
  _torque.setZero();
}
void RigidBody::clearContactFlag()
{
  _contactFlag=false;
}
void RigidBody::setContactFlag()
{
  _contactFlag=true;
}
bool RigidBody::hasContactFlag()
{
  return _contactFlag;
}
//RigidBodySolverInterface
#define NR_ITER 20
RigidSolver::RigidSolver()
  :Serializable(typeid(RigidSolver).name()) {}
RigidSolver::RigidSolver(sizeType dim,scalar cellSz)
  :Serializable(typeid(RigidSolver).name()),_dim(dim),_cellSz(cellSz)
{
  _bb._maxC.setConstant( ScalarUtil<scalar>::scalar_max);
  _bb._minC.setConstant(-ScalarUtil<scalar>::scalar_max);
  _g.setZero();
  _g[dim-1]=-9.81f;
  _warpMode.setZero();
}
bool RigidSolver::read(istream& is,IOData* dat)
{
  registerType<RigidBody>(dat);
  registerType<RigidBodyShape>(dat);
  StaticGeom::registerType(dat);
  readVector(_bodies,is,dat);
  readVector(_solids,is,dat);
  readBinaryData(_bb,is);
  readBinaryData(_dim,is);
  readBinaryData(_cellSz,is);
  readBinaryData(_warpMode,is);
  readBinaryData(_g,is);
  return is.good();
}
bool RigidSolver::write(ostream& os,IOData* dat) const
{
  registerType<RigidBody>(dat);
  registerType<RigidBodyShape>(dat);
  StaticGeom::registerType(dat);
  writeVector(_bodies,os,dat);
  writeVector(_solids,os,dat);
  writeBinaryData(_bb,os);
  writeBinaryData(_dim,os);
  writeBinaryData(_cellSz,os);
  writeBinaryData(_warpMode,os);
  writeBinaryData(_g,os);
  return os.good();
}
boost::shared_ptr<Serializable> RigidSolver::copy() const
{
  return boost::shared_ptr<Serializable>(new RigidSolver);
}
void RigidSolver::writeMeshVTK(VTKWriter<scalar>& os) const
{
  for(sizeType i=0; i<nrBody(); i++)
    _bodies[i]->writeMeshVTK(os);
  ObjMesh mesh;
  for(sizeType i=0; i<nrSolid(); i++) {
    _solids[i]->getMesh(mesh);
    mesh.writeVTK(os);
  }
}
void RigidSolver::writeMeshVTK(const string& path) const
{
  VTKWriter<scalar> os("rigidSolver",path,true);
  writeMeshVTK(os);
}
void RigidSolver::writePSetVTK(VTKWriter<scalar>& os) const
{
  for(sizeType i=0; i<nrBody(); i++)
    _bodies[i]->writePSetVTK(os);
}
void RigidSolver::writePSetVTK(const string& path) const
{
  VTKWriter<scalar> os("rigidSolver",path,true);
  writePSetVTK(os);
}
sizeType RigidSolver::nrBody() const
{
  return (sizeType)_bodies.size();
}
sizeType RigidSolver::nrSolid() const
{
  return (sizeType)_solids.size();
}
void RigidSolver::addBall(const scalar rad,const Vec3& pos)
{
  boost::shared_ptr<RigidBodyShape> shape(new RigidBodyShape(rad,_dim,_cellSz));
  boost::shared_ptr<RigidBody> body(new RigidBody(shape));
  body->setPos(pos);
  _bodies.push_back(body);
}
void RigidSolver::addBall(const Vec3& ext,const Vec3& pos,const Quat& rot)
{
  ASSERT(ext[0]==ext[1]);
  scalar rad=ext[0];
  boost::shared_ptr<RigidBodyShape> shape(new RigidBodyShape(rad,_dim,_cellSz));
  boost::shared_ptr<RigidBody> body(new RigidBody(shape));
  body->setPos(pos);
  body->setRot(rot);
  _bodies.push_back(body);
}
void RigidSolver::addBall(const Vec3& ext,const Vec3& pos,const Vec3& rot)
{
  Quat rotQ(makeRotation<scalar>(rot));
  addBall(ext,pos,rotQ);
}
void RigidSolver::addBox(const Vec3& ext,const Vec3& pos,const Quat& rot)
{
  boost::shared_ptr<RigidBodyShape> shape(new RigidBodyShape(ext,_dim,_cellSz));
  boost::shared_ptr<RigidBody> body(new RigidBody(shape));
  body->setPos(pos);
  body->setRot(rot);
  _bodies.push_back(body);
}
void RigidSolver::addBox(const Vec3& ext,const Vec3& pos,const Vec3& rot)
{
  Quat rotQ(makeRotation<scalar>(rot));
  addBox(ext,pos,rotQ);
}
void RigidSolver::addCross(const Vec3& ext,const Vec3& pos,const Quat& rot)
{
  vector<boost::shared_ptr<StaticGeomCell> > children;
  children.push_back(boost::shared_ptr<StaticGeomCell>(new BoxGeomCell(Mat4::Identity(),_dim,ext)));
  Mat4 R=Mat4::Identity();
  R.block<3,3>(0,0)=makeRotation<scalar>(Vec3::UnitZ()*M_PI/2);
  children.push_back(boost::shared_ptr<StaticGeomCell>(new BoxGeomCell(R,_dim,ext)));
  boost::shared_ptr<RigidBodyShape> shape(new RigidBodyShape(children,_cellSz));
  boost::shared_ptr<RigidBody> body(new RigidBody(shape));
  body->setPos(pos);
  body->setRot(rot);
  _bodies.push_back(body);
}
void RigidSolver::addCross(const Vec3& ext,const Vec3& pos,const Vec3& rot)
{
  Quat rotQ(makeRotation<scalar>(rot));
  addCross(ext,pos,rotQ);
}
void RigidSolver::addSolid(boost::shared_ptr<StaticGeomCell> cell)
{
  _solids.push_back(cell);
}
const RigidBody& RigidSolver::getBody(const sizeType& i) const
{
  return *_bodies[i];
}
RigidBody& RigidSolver::getBody(const sizeType& i)
{
  return *_bodies[i];
}
const StaticGeomCell& RigidSolver::getSolid(const sizeType& i) const
{
  return *_solids[i];
}
StaticGeomCell& RigidSolver::getSolid(const sizeType& i)
{
  return *_solids[i];
}
void RigidSolver::clearContactFlag()
{
  for(sizeType i=0; i<nrBody(); i++) {
    RigidBody& body=getBody(i);
    body.clearContactFlag();
  }
}
void RigidSolver::advanceVelocity(const scalar& dt,const scalar& globalTime)
{
  _poses.resize(nrBody());
  for(sizeType i=0; i<nrBody(); i++) {
    RigidBody& body=getBody(i);
    body.getPosition(_poses[i]); //save
    body.setGlobalTime(globalTime);
    body.applyForce(_g*body.getM());
    body.advanceVelocity(dt);
    body.advancePosition(dt);
  }
  //adjust
  adjustPosition(NR_ITER);
  for(sizeType i=0; i<nrBody(); i++) {
    RigidBody& body=getBody(i);
    body.adjustVelocity(_poses[i],dt);
    body.computeVelocityAux();
    body.clearForceAndTorque();
    body.setPosition(_poses[i]); //load
  }
}
void RigidSolver::advancePosition(const scalar& dt)
{
  for(sizeType i=0; i<nrBody(); i++) {
    RigidBody& body=getBody(i);
    body.getPosition(_poses[i]); //save
    body.advanceVelocity(dt);
    body.advancePosition(dt);
    body.clearForceAndTorque();
  }
  //adjust
  adjustPosition(NR_ITER);
  for(sizeType i=0; i<nrBody(); i++) {
    RigidBody& body=getBody(i);
    body.adjustVelocity(_poses[i],dt);
    body.computeVelocityAux();
  }
  //account for warping
  vector<Vec3,Eigen::aligned_allocator<Vec3> > warpAdjusts;
  handleWarps(warpAdjusts);
}
void RigidSolver::adjustPosition(sizeType nrIter)
{
  //account for bounding box warping
  BBox<scalar> bbWithWarp=_bb;
  for(sizeType d=0; d<_dim; d++)
    if(_warpMode[d] > 0) {
      bbWithWarp._minC[d]=-ScalarUtil<scalar>::scalar_max;
      bbWithWarp._maxC[d]= ScalarUtil<scalar>::scalar_max;
    }
  //account for warping
  vector<Vec3,Eigen::aligned_allocator<Vec3> > warpAdjusts;
  handleWarps(warpAdjusts);
  //build bvh
  vector<Node<sizeType> > bvh;
  sizeType nrLeaf=nrBody()+nrSolid();
  bvh.resize(nrLeaf);
  for(sizeType i=0; i<nrBody(); i++) {
    getBody(i).getWorldBB(bvh[i]._bb);
    bvh[i]._l=bvh[i]._r=-1;
    bvh[i]._nrCell=1;
    bvh[i]._cell=i;
  }
  for(sizeType i=nrBody(); i<nrLeaf; i++) {
    bvh[i]._bb=getSolid(i-nrBody()).getBB();
    bvh[i]._l=bvh[i]._r=-1;
    bvh[i]._nrCell=1;
    bvh[i]._cell=i;
  }
  //build hierarchy
  if(_dim == 2)
    BVHBuilder<Node<sizeType>,2>().buildBVH(bvh);
  else BVHBuilder<Node<sizeType>,3>().buildBVH(bvh);
  for(sizeType i=nrLeaf; i<(sizeType)bvh.size(); i++)
    bvh[i]._cell=-1;
  //update
  Contact c;
  BVHQuery<sizeType> query(bvh,_dim,-1);
  for(sizeType it=0; it<nrIter; it++) {
    //bounding box
    for(sizeType i=0; i<nrBody(); i++) {
      RigidBody& bodyI=getBody(i);
      if(bodyI.freeze())
        continue;
      getContact(c,bodyI,bbWithWarp);
      if(c._hasContact) {
        resolveContact(c,bodyI,bodyI.restitute());
        bodyI.setContactFlag();
      }
    }
    //interbody
    interBodyQueryWithWarp(query);
    //update
    for(sizeType i=0; i<nrBody(); i++)
      getBody(i).getWorldBB(bvh[i]._bb);
    for(sizeType i=nrBody(); i<nrLeaf; i++)
      bvh[i]._bb=getSolid(i-nrBody()).getBB();
    query.updateBVH();
  }
  //account for warping
  for(sizeType i=0; i<nrBody(); i++)
    getBody(i)._x-=warpAdjusts[i];
}
void RigidSolver::advance(const scalar& dt,const scalar& globalTime)
{
  //breaking the advance into two steps
  //is for two-way fluid rigid coupling
  advanceVelocity(dt,globalTime);
  advancePosition(dt);
}
void RigidSolver::setBB(const BBox<scalar>& bb)
{
  _bb=bb;
}
BBox<scalar> RigidSolver::getBB() const
{
  return _bb;
}
void RigidSolver::setGravity(const Vec3& g)
{
  _g=g;
}
Vec3 RigidSolver::getGravity() const
{
  return _g;
}
scalar RigidSolver::intersect(const Vec3& from,const Vec3& to,bool solidOnly,sizeType* bodyId,sizeType* solidId) const
{
  scalar ret=ScalarUtil<scalar>::scalar_max;
  if(!solidOnly)
    for(sizeType i=0; i<nrBody(); i++) {
      const RigidBody& body=getBody(i);
      Vec3 f=body.toLocal(from);
      Vec3 t=body.toLocal(to);
      scalar s=body.getShape().rayQuery(f,t);
      if(s < ret) {
        ret=s;
        if(bodyId)
          *bodyId=i;
      }
    }
  for(sizeType i=0; i<nrSolid(); i++) {
    const StaticGeomCell& cell=getSolid(i);
    scalar s=cell.rayQuery(from,to-from);
    if(s < ret) {
      ret=s;
      if(solidId)
        *solidId=i;
    }
  }
  return ret;
}
bool RigidSolver::intersect(const Vec3& pos,bool solidOnly) const
{
  Vec3 n;
  if(!_bb.contain(pos,_dim))
    return true;
  if(!solidOnly)
    for(sizeType i=0; i<nrBody(); i++) {
      const RigidBody& body=getBody(i);
      Vec3 L=body.toLocal(pos);
      if(body.getShape().dist(L) < 0)
        return true;
    }
  for(sizeType i=0; i<nrSolid(); i++) {
    const StaticGeomCell& cell=getSolid(i);
    if(cell.dist(pos,n))
      return true;
  }
  return false;
}
bool RigidSolver::intersect(scalar r,const Vec3& pos,bool solidOnly) const
{
  Vec3 n;
  if(!_bb.enlarge(-r,_dim).contain(pos,_dim))
    return true;
  if(!solidOnly)
    for(sizeType i=0; i<nrBody(); i++) {
      const RigidBody& body=getBody(i);
      Vec3 L=body.toLocal(pos);
      scalar dist=body.getShape().dist(L);
      if(dist < r)
        return true;
    }
  for(sizeType i=0; i<nrSolid(); i++) {
    const StaticGeomCell& cell=getSolid(i);
    if(cell.closest(pos,n) || n.norm() < r)
      return true;
  }
  return false;
}
void RigidSolver::getPSet(ParticleSetN& pset) const
{
  for(sizeType i=0; i<nrBody(); i++)
    getBody(i).getPSet(pset);
}
void RigidSolver::onCell(const Node<sizeType>& I,const Node<sizeType>& J,const vector<Vec3,Eigen::aligned_allocator<Vec3> >& offs)
{
  Contact c;
  if(I._cell >= nrBody() && J._cell >= nrBody())
    return;
  else if(I._cell == J._cell)
    return;
  else if(I._cell < nrBody() && J._cell < nrBody()) {
    RigidBody& b1=getBody(I._cell);
    RigidBody& b2=getBody(J._cell);
    if(b1.freeze() && b2.freeze())
      return;
    for(sizeType i=0,nrOff=(sizeType)offs.size(); i<nrOff; i++) {
      b1._x+=offs[i];
      getContact(c,b1,b2);
      if(!c._hasContact) {
        b1._x-=offs[i];
        continue;
      }
      resolveContact(c,b1,b2);
      b1.setContactFlag();
      b2.setContactFlag();
      b1._x-=offs[i];
      break;
    }
  } else if(I._cell < nrBody()) {
    RigidBody& b=getBody(I._cell);
    if(b.freeze())
      return;
    for(sizeType i=0,nrOff=(sizeType)offs.size(); i<nrOff; i++) {
      b._x+=offs[i];
      getContact(c,b,getSolid(J._cell-nrBody()));
      if(!c._hasContact) {
        b._x-=offs[i];
        continue;
      }
      resolveContact(c,b,b.restitute());
      b.setContactFlag();
      b._x-=offs[i];
      break;
    }
  } else if(J._cell < nrBody()) {
    RigidBody& b=getBody(J._cell);
    if(b.freeze())
      return;
    for(sizeType i=0,nrOff=(sizeType)offs.size(); i<nrOff; i++) {
      b._x+=offs[i];
      getContact(c,b,getSolid(I._cell-nrBody()));
      if(!c._hasContact) {
        b._x-=offs[i];
        continue;
      }
      resolveContact(c,b,b.restitute());
      b.setContactFlag();
      b._x-=offs[i];
      break;
    }
  }
}
//handle warping
void RigidSolver::handleWarps(vector<Vec3,Eigen::aligned_allocator<Vec3> >& warpAdjusts)
{
  warpAdjusts.resize(nrBody());
  for(sizeType i=0; i<nrBody(); i++) {
    Vec3& p=getBody(i)._x;
    warpAdjusts[i]=p;
    for(sizeType d=0; d<_dim; d++)
      if(_warpMode[d] > 0)
        handleScalarWarp(p[d],_bb._minC[d],_bb._maxC[d],NULL);
    warpAdjusts[i]=p-warpAdjusts[i];
  }
}
void RigidSolver::computeWarpOffsets(vector<Vec3,Eigen::aligned_allocator<Vec3> >& offs) const
{
  const Vec3 range=_bb.getExtent();
  offs.push_back(Vec3::Zero());
  for(sizeType d=0; d<_dim; d++)
    if(_warpMode[d] > 0)
      for(sizeType i=0,nrOff=(sizeType)offs.size(); i<nrOff; i++)
        offs.push_back(offs[i]+Vec3::Unit(d)*range[d]);
}
void RigidSolver::interBodyQueryWithWarp(const BVHQuery<sizeType,BBox<scalar> >& query)
{
  if(query._bvh.empty())
    return;
  sizeType verbose=-1;
  std::stack<Vec2i> ss;
  vector<Vec3,Eigen::aligned_allocator<Vec3> > offs;
  ss.push(Vec2i(query._bvh.size()-1,query._bvh.size()-1));
  computeWarpOffsets(offs);
  while(!ss.empty()) {
    sizeType rA=ss.top()[0];
    sizeType rB=ss.top()[1];
    ss.pop();
    //bb intersect with warp
    bool intersect=false;
    for(sizeType i=0,nrOff=(sizeType)offs.size(); i<nrOff && !intersect; i++) {
      const BBox<scalar>& bbA=query._bvh[rA]._bb;
      const BBox<scalar>& bbB=query._bvh[rB]._bb;
      intersect=BBox<scalar>(bbA._minC+offs[i],bbA._maxC+offs[i]).intersect(bbB,_dim);
    }
    if(!intersect)
      continue;
    //descend tree
    if(query._bvh[rA]._cell != verbose && query._bvh[rB]._cell != verbose) {
      onCell(query._bvh[rA],query._bvh[rB],offs);
    } else if(query._bvh[rA]._cell != verbose && query._bvh[rB]._cell == verbose) {
      ss.push(Vec2i(rA,query._bvh[rB]._l));
      ss.push(Vec2i(rA,query._bvh[rB]._r));
    } else if(query._bvh[rA]._cell == verbose && query._bvh[rB]._cell != verbose) {
      ss.push(Vec2i(query._bvh[rA]._l,rB));
      ss.push(Vec2i(query._bvh[rA]._r,rB));
    } else {
      ss.push(Vec2i(query._bvh[rA]._l,query._bvh[rB]._l));
      ss.push(Vec2i(query._bvh[rA]._l,query._bvh[rB]._r));
      ss.push(Vec2i(query._bvh[rA]._r,query._bvh[rB]._l));
      ss.push(Vec2i(query._bvh[rA]._r,query._bvh[rB]._r));
    }
  }
}
void RigidSolver::setWarpMode(const Vec3i& warpMode)
{
  _warpMode=warpMode.cwiseMax(Vec3i::Zero()).cwiseMin(Vec3i::Ones());
}
#undef NR_ITER
//RigidSolverImplicitFunc
RigidSolverImplicitFunc::RigidSolverImplicitFunc(const RigidSolver& sol,bool solidOnly):_sol(sol),_solidOnly(solidOnly) {}
scalar RigidSolverImplicitFunc::operator()(const Vec3& pos) const
{
  return _sol.intersect(pos,_solidOnly) ? -1 : 1;
}
