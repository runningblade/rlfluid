#include "VariableSizedGridOpShallowWater.h"
#include "VariableSizedGridOp.h"
#include "RigidSolver.h"
#include "Utils.h"

USE_PRJ_NAMESPACE

#define GETu(D) u[D]._data[beg.off()]
#define GETh h._data[beg.off()]

#define GETw U[0]._data[beg.off()]
#define GETdw(derivD) DU[0][derivD]._data[beg.off()]
#define GETwl(derivD) U[0]._data[beg.offCellCenterLeft(derivD)]
#define GETwr(derivD) U[0]._data[beg.offCellCenter(derivD)]

#define GEThu(D) U[D+1]._data[beg.off()]
#define GETdhu(D,derivD) DU[D+1][derivD]._data[beg.off()]
#define GEThul(D,derivD) U[D+1]._data[beg.offCellCenterLeft(derivD)]
#define GEThur(D,derivD) U[D+1]._data[beg.offCellCenter(derivD)]
#define MINMOD minmod(theta*(c-l)/dxl,(r-l)/(dxl+dxr),theta*(r-c)/dxr)
#define EPS_SWE 1E-6f
template <typename T,int DIM,typename Encoder>
typename VariableSizedGridOpShallowWater<T,DIM,Encoder>::VecDIMPd F(const typename VariableSizedGridOpShallowWater<T,DIM,Encoder>::VecDIMPd& U,T b,sizeType d,T g)
{
  typename VariableSizedGridOpShallowWater<T,DIM,Encoder>::VecDIMPd ret;
  ret.setZero();
  ret[0]=U[d+1];
  T hh=max<T>(U[0]-b,0);
  T coef=sqrt(2.0f)*hh/sqrt(hh*hh*hh*hh+max<T>(hh*hh*hh*hh,EPS_SWE));
  for(sizeType D=0; D<DIM; D++)
    ret[D+1]=U[D+1]*U[d+1]*coef;
  ret[d+1]+=0.5f*g*hh*hh;
  return ret;
}
template <typename T,int DIM,typename Encoder>
T A(const typename VariableSizedGridOpShallowWater<T,DIM,Encoder>::VecDIMPd& U,T b,sizeType d,T g,T sgn)
{
  T hh=max<T>(U[0]-b,0);
  T coef=sqrt(2.0f)*hh/sqrt(hh*hh*hh*hh+max<T>(hh*hh*hh*hh,EPS_SWE));
  return U[d+1]*coef+sqrt(g*hh)*sgn;
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOpShallowWater<T,DIM,Encoder>::computeU(const GridType& grd,const CellCenterData<T>& h,const CellCenterData<T> u[DIM],CellCenterData<T> U[DIM+1],const NodalData<T>& B)
{
  VecDIMd x;
  VecDIMi id;
  for(sizeType d=0; d<=DIM; d++)
    U[d].reset(grd);
  CellCenterPointIt beg(grd,0),end(grd,-1);
  while(beg!=end) {
    GETw=max<T>(GETh,0)+grd.sampleNodal(B,x=*beg,id);
    for(sizeType d=0; d<DIM; d++)
      GEThu(d)=GETh*GETu(d);
    ++beg;
  }
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOpShallowWater<T,DIM,Encoder>::computehu(const GridType& grd,CellCenterData<T>& h,CellCenterData<T> u[DIM],const CellCenterData<T> U[DIM+1],const NodalData<T>& B)
{
  T hh,coef;
  VecDIMd x;
  VecDIMi id;
  h.reset(grd);
  for(sizeType d=0; d<DIM; d++)
    u[d].reset(grd);
  CellCenterPointIt beg(grd,0),end(grd,-1);
  while(beg!=end) {
    hh=GETh=max<T>(GETw-grd.sampleNodal(B,x=*beg,id),0);
    coef=sqrt(2.0f)*hh/sqrt(hh*hh*hh*hh+max<T>(hh*hh*hh*hh,EPS_SWE));
    for(sizeType d=0; d<DIM; d++)
      GETu(d)=GEThu(d)*coef;
    ++beg;
  }
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOpShallowWater<T,DIM,Encoder>::computeDU(const GridType& grd,const CellCenterData<T> U[DIM+1],CellCenterData<T> DU[DIM+1][DIM],const NodalData<T>& B,T theta)
{
  T l,c,r;
  T lb,rb;
  T dxl,dx,dxr;
  VecDIMd x;
  VecDIMi id;
  for(sizeType d=0; d<=DIM; d++)
    for(sizeType D=0; D<DIM; D++)
      DU[d][D].reset(grd);
  CellCenterPointIt beg(grd,0),end(grd,-1);
  while(beg!=end) {
    for(sizeType derivD=0; derivD<DIM; derivD++) {
      grd.getCellSzNodal(beg.id(),x);
      dx=x[derivD];
      //w
      c=GETw;
      if(beg.hasCellCenterLeft(derivD)) {
        beg.offCellCenterLeft(derivD,&id);
        grd.getCellSzCellCenter(id,x);
        dxl=x[derivD];
        l=GETwl(derivD);
      } else {
        dxl=grd._cellSz0[derivD];
        l=c;
      }
      if(beg.hasCellCenter(derivD)) {
        beg.offCellCenter(derivD,&id);
        grd.getCellSzCellCenter(beg.id(),x);
        dxr=x[derivD];
        r=GETwr(derivD);
      } else {
        dxr=grd._cellSz0[derivD];
        r=c;
      }
      GETdw(derivD)=MINMOD;
      //fix positivity
      if(c-GETdw(derivD)*dx/2 < (lb=grd.sampleNodal(B,x=*beg-VecDIMd::Unit(derivD)*dx/2,id)))
        GETdw(derivD)=(c-lb)/(dx/2);
      if(c+GETdw(derivD)*dx/2 < (rb=grd.sampleNodal(B,x=*beg+VecDIMd::Unit(derivD)*dx/2,id)))
        GETdw(derivD)=(rb-c)/(dx/2);
      //hu
      for(sizeType d=0; d<DIM; d++) {
        c=GEThu(d);
        if(beg.hasCellCenterLeft(derivD))
          l=GEThul(d,derivD);
        else l=c;
        if(beg.hasCellCenter(derivD))
          r=GEThur(d,derivD);
        else r=c;
        GETdhu(d,derivD)=MINMOD;
      }
    }
    ++beg;
  }
}
template <typename T,int DIM,typename Encoder>
T VariableSizedGridOpShallowWater<T,DIM,Encoder>::computeRHS(const GridType& grd,const CellCenterData<T> U[DIM+1],const CellCenterData<T> DU[DIM+1][DIM],CellCenterData<T> RHS[DIM+1],const NodalData<T>& B,T g)
{
  T ap,am,b,db;
  VecDIMd limit=VecDIMd::Zero(),x,dx;
  VecDIMPd h,UP,UM,HP,HM;
  VecDIMi id;
  sizeType off;
  MACVelData<DIM,T> H[DIM+1];
  for(sizeType D=0; D<=DIM; D++) {
    H[D].reset(grd);
    H[D].setZero();
    RHS[D].reset(grd);
    RHS[D]._data.setZero();
  }
  //compute flux
  for(sizeType D=0; D<DIM; D++) {
    MACVelPointIt beg(D,grd,0),end(D,grd,-1);
    while(beg!=end) {
      b=grd.sampleNodal(B,x=*beg,id);
      if(beg.hasCellCenter()) {
        off=beg.offCellCenter(&id);
        grd.getCellSzNodal(id,dx);
        for(sizeType d=0; d<=DIM; d++)
          UP[d]=U[d]._data[off]-DU[d][D]._data[off]*dx[D]/2;
      }
      if(beg.hasCellCenterLeft()) {
        off=beg.offCellCenterLeft(&id);
        grd.getCellSzNodal(id,dx);
        for(sizeType d=0; d<=DIM; d++)
          UM[d]=U[d]._data[off]+DU[d][D]._data[off]*dx[D]/2;
      }
      //reflect boundary condition
      if(!beg.hasCellCenter()) {
        UP=-UM;
        UP[0]=UM[0];
      }
      if(!beg.hasCellCenterLeft()) {
        UM=-UP;
        UM[0]=UP[0];
      }
      ap=max<T>(max<T>(A<T,DIM,Encoder>(UM,b,D,g, 1),A<T,DIM,Encoder>(UP,b,D,g, 1)),0);
      am=min<T>(min<T>(A<T,DIM,Encoder>(UM,b,D,g,-1),A<T,DIM,Encoder>(UP,b,D,g,-1)),0);
      limit[D]=max<T>(limit[D],ap-am);
      //compute flux
      h=(ap*F<T,DIM,Encoder>(UM,b,D,g)-am*F<T,DIM,Encoder>(UP,b,D,g)+(ap*am)*(UP-UM))/max<T>(ap-am,EPS_SWE);
      if(!isFinite(UM) || !isFinite(UP) || !isFinite(h)) {
        WARNING("Infinite!")
        cout << UP.transpose() << endl;
        cout << UM.transpose() << endl;
        cout << h.transpose() << endl;
        exit(-1);
      }
      //assign
      for(sizeType d=0; d<=DIM; d++)
        H[d]._data[D][beg.off()]=h[d];
      ++beg;
    }
  }
  //compute RHS
  CellCenterPointIt beg(grd,0),end(grd,-1);
  while(beg!=end) {
    //flux
    grd.getCellSzNodal(beg.id(),dx);
    for(sizeType d=0; d<DIM; d++)
      for(sizeType D=0; D<=DIM; D++) {
        HP[D]=H[D]._data[d][beg.offFaceRight(d)];
        HM[D]=H[D]._data[d][beg.offFaceLeft(d)];
        RHS[D]._data[beg.off()]-=(HP[D]-HM[D])/dx[d];
      }
    //source
    T h=max<T>(GETw-grd.sampleNodal(B,x=*beg,id),0);
    for(sizeType d=0; d<DIM; d++) {
      db=grd.sampleNodal(B,x=*beg+VecDIMd::Unit(d)*dx[d]/2,id);
      db-=grd.sampleNodal(B,x=*beg-VecDIMd::Unit(d)*dx[d]/2,id);
      RHS[d+1]._data[beg.off()]-=g*h*db/dx[d];
    }
    ++beg;
  }
  //limit time
  T limitT=ScalarUtil<T>::scalar_max;
  for(sizeType d=0; d<DIM; d++)
    limitT=min<T>(limitT,grd._cellSz0[d]/max<T>(limit[d],EPS_SWE));
  return limitT;
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOpShallowWater<T,DIM,Encoder>::computeRigidForce(const GridType& grd,CellCenterData<T> U[DIM+1],const CellCenterData<T>& h,RigidSolver& rigid,T CDrag,T CDragLiquid,T CLift,T density,T g,T dt)
{
  VecDIMd cellSz;
  scalar omega=1;
  BBox<scalar> bodyBB,bb=rigid.getBB();
  for(sizeType i=0; i<rigid.nrBody(); i++) {
    RigidBody& b=rigid.getBody(i);
    b.getWorldBB(bodyBB);
    CellCenterPointIt beg(grd,0,assignVariableTo<T,DIM,scalar,3>(bodyBB),VecDIMi::Constant(3),VecDIMi::Constant(4));
    CellCenterPointIt end(grd,-1,assignVariableTo<T,DIM,scalar,3>(bodyBB),VecDIMi::Constant(3),VecDIMi::Constant(4));
    while(beg!=end) {
      Vec3 from=assignVariableTo<Vec3,VecDIMd>(beg.pointWithWarp()),to=from;
      from[DIM]=bb._minC[DIM];
      to[DIM]=bb._maxC[DIM];
      //intersect
      Vec3 f=b.toLocal(from),t=b.toLocal(to);
      scalar s=b.getShape().rayQuery(f,t);
      Vec3 p=interp1D(from,to,s);
      //vRel
      Vec3 vRel=b.vel(p);
      for(sizeType d=0; d<DIM; d++)
        vRel[d]-=GEThu(d)/max<T>(GETh,EPS_SWE);
      scalar vRelLen=max<scalar>(vRel.norm(),EPS_SWE);
      //nvRel
      Vec3 n=b.rot()*b.getShape().gradDist(b.toLocal(p));
      //check
      if(p[DIM] < GETw && n.dot(vRel) > 0) {
        //buoyancy force
        grd.getCellSzNodal(beg.id(),cellSz);
        T A=cellSz.prod(),depth=(GETw-p[DIM])*A*density*g;
        b.applyWorldForce(Vec3::Unit(DIM)*depth,p);
        //drag force
        T AEff=A*(omega*n.dot(vRel)/vRelLen+(1-omega));
        b.applyWorldForce(-CDrag*AEff/2*vRelLen*vRel,p);
        //force on fluid
        for(sizeType d=0; d<DIM; d++)
          GEThu(d)+=vRel[d]*min<T>(GETw-p[DIM],GETh)*min<T>(CDragLiquid*dt,1);
        //lift force
        Vec3 nvRel=n.cross(vRel);
        nvRel/=max<scalar>(nvRel.norm(),EPS_SWE);
        b.applyWorldForce(-CLift*AEff/2*vRelLen*(vRel.cross(nvRel)),p);
      }
      ++beg;
    }
  }
}
//debug shallow water equation
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOpShallowWater<T,DIM,Encoder>::debugSWE(const Vec3i& warpMode)
{
  GridType testGrid=VariableSizedGridOp<T,DIM,Encoder>::createTestGrid(warpMode);
  BBox<T,DIM> focus=testGrid.getBBNodal();
  focus.enlargedEps(-0.5f);
  testGrid.reset(testGrid,focus,2);
  //setup data structure
  CellCenterData<T> h(testGrid),h0(testGrid),hr;
  CellCenterData<T> u[DIM],ur[DIM],U[DIM+1],RHS[DIM+1],DU[DIM+1][DIM];
  NodalData<T> B(testGrid);
  for(sizeType D=0; D<DIM; D++) {
    u[D].reset(testGrid);
    u[D]._data.setRandom();
  }
  B._data.setRandom();
  h._data.setRandom();
  h0._data.setConstant(sqrt(sqrt(EPS_SWE))*2);
  h._data=h._data.cwiseMax(h0._data);
  VariableSizedGridOp<T,DIM,Encoder>::writeGridNodalVTK("bottom.vtk",testGrid,B);
  //debug U/hu conversion
  computeU(testGrid,h,u,U,B);
  computehu(testGrid,hr,ur,U,B);
  ASSERT((h._data-hr._data).cwiseAbs().maxCoeff() < 1E-6f)
  for(sizeType D=0; D<DIM; D++) {
    ASSERT((u[D]._data-ur[D]._data).cwiseAbs().maxCoeff() < 1E-6f)
  }
  //debug DU computation
  computeDU(testGrid,U,DU,B,1.0f);
  //test positivity of wE/W/N/S
  VecDIMd x;
  VecDIMi id;
  CellCenterPointIt beg(testGrid,0),end(testGrid,-1);
  while(beg!=end) {
    for(sizeType d=0; d<DIM; d++) {
      testGrid.getCellSzNodal(beg.id(),x);
      T dx=x[d];
      T wm=GETw-GETdw(d)*dx/2;
      T wp=GETw+GETdw(d)*dx/2;
      T bm=testGrid.sampleNodal(B,x=*beg-VecDIMd::Unit(d)*dx/2,id);
      T bp=testGrid.sampleNodal(B,x=*beg+VecDIMd::Unit(d)*dx/2,id);
      ASSERT(wm >= bm-1E-6f && wp >= bp-1E-6f)
    }
    ++beg;
  }
  //debug RHS computation
  computeRHS(testGrid,U,DU,RHS,B,9.81f);
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOpShallowWater<T,DIM,Encoder>::debugSWEAllWarp()
{
  INFO("Debugging SWE!")
  debugSWE(Vec3i::Zero());
  if(Encoder::isConstantEncoder)
    for(sizeType D=0; D<DIM; D++)
      debugSWE(Vec3i::Unit(D));
}
//instance
#define INSTANCE(DIM,TYPE,SPARSITY) \
template class VariableSizedGridOpShallowWater<scalar,DIM,Encoder##TYPE<SPARSITY> >;
INSTANCE_ALL
