﻿#ifndef VARIABLE_SIZED_GRID_OP_SHALLOW_WATER_H
#define VARIABLE_SIZED_GRID_OP_SHALLOW_WATER_H

#include "Utils.h"
#include "NodalData.h"
#include "MACVelData.h"
#include "CellCenterData.h"
#include "VariableSizedGrid.h"
#include <CommonFile/GridBasic.h>
#include <CommonFile/ParticleSet.h>
#include <CommonFile/ImplicitFuncInterface.h>

PRJ_BEGIN

//VariableSizedGridOpShallowWater
DECL_FORWARD_LINEAR_SOLVER
template <typename T,int DIM,typename Encoder>
struct VariableSizedGridOpShallowWater : public TypeTraits<T,DIM> {
  DECL_TYPES
  //shallow water op
  static void computeU(const GridType& grd,const CellCenterData<T>& h,const CellCenterData<T> u[DIM],CellCenterData<T> U[DIM+1],const NodalData<T>& B);
  static void computehu(const GridType& grd,CellCenterData<T>& h,CellCenterData<T> u[DIM],const CellCenterData<T> U[DIM+1],const NodalData<T>& B);
  static void computeDU(const GridType& grd,const CellCenterData<T> U[DIM+1],CellCenterData<T> DU[DIM+1][DIM],const NodalData<T>& B,T theta);
  static T computeRHS(const GridType& grd,const CellCenterData<T> U[DIM+1],const CellCenterData<T> DU[DIM+1][DIM],CellCenterData<T> RHS[DIM+1],const NodalData<T>& B,T g);
  static void computeRigidForce(const GridType& grd,CellCenterData<T> U[DIM+1],const CellCenterData<T>& h,RigidSolver& rigid,T CDrag,T CDragLiquid,T CLift,T density,T g,T dt);
  //debug
  static void debugSWE(const Vec3i& warpMode);
  static void debugSWEAllWarp();
};

PRJ_END

#endif
