#include "VariableSizedGridOpProject.h"
#include "VariableSizedGridOp.h"
#include "RigidSolver.h"
#include "RigidMatrix.h"
#include "Utils.h"
#include <CommonFile/ComputeWeightInterface.h>
#include <CommonFile/solvers/PMinresQLP.h>
#include <CommonFile/solvers/QPSolver.h>
#include <boost/lexical_cast.hpp>

USE_PRJ_NAMESPACE

//VariableSizedGridOpProject
//project
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOpProject<T,DIM,Encoder>::sampleWeightMACVel(const GridType& grd,MACVelData<DIM,T>& weight,const ScalarField& nodalSolidPhi)
{
  weight.setZero();
  DEFINE_GRID_WARP(nodalSolidPhi)
  for(sizeType D=0; D<DIM; D++) {
    MACVelPointIt beg(D,grd,0);
    MACVelPointIt end(D,grd,-1);
    T v0,v1,v2,v3,v4,v5,v6,v7;
    VecDIMi lastId;
    BBox<T,DIM> bb;
    lastId.setConstant(-1);
    if(DIM == 2)
      while(beg!=end) {
        bb=grd.getCellMACVel(D,beg.id());
        if(beg.id() == lastId+VecDIMi::Unit(DIM-1)) {
          v0=v2;
          v1=v3;
          v2=nodalSolidPhi.sampleSafe2D(Vec3(bb._minC[0],bb._maxC[1],0),&warpMode);
          v3=nodalSolidPhi.sampleSafe2D(Vec3(bb._maxC[0],bb._maxC[1],0),&warpMode);
        } else {
          v0=nodalSolidPhi.sampleSafe2D(Vec3(bb._minC[0],bb._minC[1],0),&warpMode);
          v1=nodalSolidPhi.sampleSafe2D(Vec3(bb._maxC[0],bb._minC[1],0),&warpMode);
          v2=nodalSolidPhi.sampleSafe2D(Vec3(bb._minC[0],bb._maxC[1],0),&warpMode);
          v3=nodalSolidPhi.sampleSafe2D(Vec3(bb._maxC[0],bb._maxC[1],0),&warpMode);
        }
        weight._data[D][beg.off()]=ComputeWeightInterface::fractionCell2D(v0,v1,v2,v3);
        lastId=beg.id();
        ++beg;
      }
    else {
      while(beg!=end) {
        bb=grd.getCellMACVel(D,beg.id());
        if(beg.id() == lastId+VecDIMi::Unit(0)) {
          v0=v4;
          v1=v5;
          v2=v6;
          v3=v7;
          v4=nodalSolidPhi.sampleSafe3D(Vec3(bb._minC[0],bb._minC[1],bb._maxC[2]),&warpMode);
          v5=nodalSolidPhi.sampleSafe3D(Vec3(bb._maxC[0],bb._minC[1],bb._maxC[2]),&warpMode);
          v6=nodalSolidPhi.sampleSafe3D(Vec3(bb._minC[0],bb._maxC[1],bb._maxC[2]),&warpMode);
          v7=nodalSolidPhi.sampleSafe3D(Vec3(bb._maxC[0],bb._maxC[1],bb._maxC[2]),&warpMode);
        } else {
          v0=nodalSolidPhi.sampleSafe3D(Vec3(bb._minC[0],bb._minC[1],bb._minC[2]),&warpMode);
          v1=nodalSolidPhi.sampleSafe3D(Vec3(bb._maxC[0],bb._minC[1],bb._minC[2]),&warpMode);
          v2=nodalSolidPhi.sampleSafe3D(Vec3(bb._minC[0],bb._maxC[1],bb._minC[2]),&warpMode);
          v3=nodalSolidPhi.sampleSafe3D(Vec3(bb._maxC[0],bb._maxC[1],bb._minC[2]),&warpMode);
          v4=nodalSolidPhi.sampleSafe3D(Vec3(bb._minC[0],bb._minC[1],bb._maxC[2]),&warpMode);
          v5=nodalSolidPhi.sampleSafe3D(Vec3(bb._maxC[0],bb._minC[1],bb._maxC[2]),&warpMode);
          v6=nodalSolidPhi.sampleSafe3D(Vec3(bb._minC[0],bb._maxC[1],bb._maxC[2]),&warpMode);
          v7=nodalSolidPhi.sampleSafe3D(Vec3(bb._maxC[0],bb._maxC[1],bb._maxC[2]),&warpMode);
        }
        weight._data[D][beg.off()]=ComputeWeightInterface::fractionCell3D(v0,v1,v2,v3,v4,v5,v6,v7);
        lastId=beg.id();
        ++beg;
      }
    }
    weight._data[D].array()=1-weight._data[D].array();
  }
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOpProject<T,DIM,Encoder>::assembleMatrix(const GridType& grd,SMat& LHS,const SparseSTENCILd& LHSd,const SparseSTENCILi& LHSi)
{
#define RIGHT_NEIGH NR_NEIGH-D
#define LEFT_NEIGH D
#define NR_NEIGH (DIM-1)
  sizeType nrP=grd._nrPointCellCenter.prod();
  LHS.resize(nrP);
  SMat::ROW& value=LHS.getValue();
  vector<sizeType>& rowOff=LHS.getRowOffset();
  value.resize((LHSd.array()!=0).count());
  CellCenterPointIt beg(grd,0);
  CellCenterPointIt end(grd,-1);
  rowOff[0]=0;
  while(beg!=end) {
    const scalarD* coef=&(LHSd(0,beg.off()));
    const sizeType* coefi=&(LHSi(0,beg.off()));
    sizeType& rowOffCurr=rowOff[beg.off()+1]=rowOff[beg.off()];
    if(coef[DIM] > 0) {
      //LEFT
      for(sizeType D=0; D<DIM; D++,coef++,coefi++)
        if(*coef != 0) {
          value[rowOffCurr]=make_pair(*coef,*coefi);
          ++rowOffCurr;
        }
      //CENTER
      value[rowOffCurr]=make_pair(*coef,*coefi);
      ++rowOffCurr;
      coef++;
      coefi++;
      //RIGHT
      for(sizeType D=0; D<DIM; D++,coef++,coefi++)
        if(*coef != 0) {
          value[rowOffCurr]=make_pair(*coef,*coefi);
          ++rowOffCurr;
        }
    }
    ++beg;
  }
#ifdef DEBUG_MATRIX
  LHS.sortRows();
  ASSERT(LHS.isSymmetric())
#endif
#undef RIGHT_NEIGH
#undef LEFT_NEIGH
#undef NR_NEIGH
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOpProject<T,DIM,Encoder>::computeTag(const GridType& grd,CellCenterData<unsigned char>& tag,const CellCenterData<T>& phi,const ScalarField& nodalSolidPhi)
{
  Vec3 pos=Vec3::Zero();
  CellCenterPointIt beg(grd,0);
  CellCenterPointIt end(grd,-1);
  DEFINE_GRID_WARP(nodalSolidPhi)
  while(beg!=end) {
    pos.segment<DIM>(0)=*beg;
    if(nodalSolidPhi.sampleSafe(pos,&warpMode) < 0)
      tag._data[beg.off()]=SOLID;
    else if(phi._data[beg.off()] < 0)
      tag._data[beg.off()]=FLUID;
    else tag._data[beg.off()]=AIR;
    ++beg;
  }
}
template <typename T,int DIM,typename Encoder>
Cold VariableSizedGridOpProject<T,DIM,Encoder>::buildMatrix(const GridType& grd,const CellCenterData<unsigned char>& tag,const CellCenterData<T>& phi,MACVelData<DIM,T>& vel,SMat& LHS,Cold& RHS,T dt,T density,Cold* lowerBound)
{
#define CTR_NEIGH DIM
#define RIGHT_NEIGH NR_NEIGH-D
#define LEFT_NEIGH D
#define NR_NEIGH (DIM<<1)
  //fill
  sizeType nrP=grd._nrPointCellCenter.prod();
  SparseSTENCILd LHSd(NR_NEIGH+1,nrP);
  SparseSTENCILi LHSi(NR_NEIGH+1,nrP);
  LHSd.setZero();
  RHS.setZero(nrP);
  VecDIMd cellSz;
  T mass,theta,coefLHS,coefRHS,LF,RF;
  unsigned char LT,RT;
  sizeType LID=-1,RID=-1;
  for(sizeType D=0; D<DIM; D++) {
    MACVelPointIt beg(D,grd,0);
    MACVelPointIt end(D,grd,-1);
    while(beg!=end) {
      if(!beg.hasCellCenterLeft()) {
        RID=beg.offCellCenter();
        LT=AIR;
        RT=tag._data[RID];
        RF=phi._data[RID];
        LF=abs(RF);
      } else if(!beg.hasCellCenter()) {
        LID=beg.offCellCenterLeft();
        LT=tag._data[LID];
        RT=AIR;
        LF=phi._data[LID];
        RF=abs(LF);
      } else {
        LID=beg.offCellCenterLeft();
        RID=beg.offCellCenter();
        LT=tag._data[LID];
        RT=tag._data[RID];
        LF=phi._data[LID];
        RF=phi._data[RID];
      }
      if(LT != SOLID && RT != SOLID) {
        if(LT == FLUID || RT == FLUID) {
          grd.getCellSzCtrMACVel(D,beg.id(),cellSz);
#ifdef DEBUG_CELLSZ
          VecDIMd cellSzL=VecDIMd::Zero(),cellSzR=VecDIMd::Zero();
          VecDIMi lid,rid;
          if(beg.hasCellCenterLeft()) {
            grd.decodeCellCenter(LID,lid);
            grd.getCellSzNodal(lid,cellSzL);
          }
          if(beg.hasCellCenter()) {
            grd.decodeCellCenter(RID,rid);
            grd.getCellSzNodal(rid,cellSzR);
          }
          T groundTruth=(cellSzL.prod()+cellSzR.prod())/2;
          ASSERT(abs(groundTruth-cellSz.prod()) < 1E-6f)
#endif
          mass=cellSz.prod()*density;
          theta=1.0f;
          if(LT == AIR || RT == AIR)
            theta=ComputeWeightInterface::fractionCell1D(LF,RF);
          if(theta < 0.01f)
            theta=0.01f;
          mass*=theta;
          theta=1/(theta*cellSz[D]*density);
          coefRHS=theta*mass;
          coefLHS=dt*theta*coefRHS;
          if(LT == FLUID) {
            LHSd(CTR_NEIGH,LID)+=coefLHS;
            LHSi(CTR_NEIGH,LID)=LID;
            RHS[LID]-=vel._data[D][beg.off()]*coefRHS;
          }
          if(RT == FLUID) {
            LHSd(CTR_NEIGH,RID)+=coefLHS;
            LHSi(CTR_NEIGH,RID)=RID;
            RHS[RID]+=vel._data[D][beg.off()]*coefRHS;
          }
          if(LT == RT) {
            LHSd(RIGHT_NEIGH,LID)-=coefLHS;
            LHSi(RIGHT_NEIGH,LID)=RID;
            LHSd(LEFT_NEIGH,RID)-=coefLHS;
            LHSi(LEFT_NEIGH,RID)=LID;
          }
        }
      } else {
        if(lowerBound) {
          if(LID >= 0)
            (*lowerBound)[LID]=0;
          if(RID >= 0)
            (*lowerBound)[RID]=0;
        }
        vel._data[D][beg.off()]=0;
      }
      ++beg;
    }
  }
#undef CTR_NEIGH
#undef RIGHT_NEIGH
#undef LEFT_NEIGH
#undef NR_NEIGH
  assembleMatrix(grd,LHS,LHSd,LHSi);
  return LHSd.row(DIM);
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOpProject<T,DIM,Encoder>::projectOut(const GridType& grd,const CellCenterData<scalarD>& pressure,const CellCenterData<unsigned char>& tag,const CellCenterData<T>& phi,const ScalarField& nodalSolidPhi,MACVelData<DIM,T>& vel,MACVelData<DIM,unsigned char>& valid,T dt,T density)
{
  MACVelData<DIM,T> velTmp(vel);
  T theta=0,LF,RF,PL,PR;
  unsigned char LT,RT;
  sizeType LID=-1,RID=-1;
  valid.setZero();
  vel.setZero();
  VecDIMd cellSz;
  for(sizeType D=0; D<DIM; D++) {
    MACVelPointIt beg(D,grd,0);
    MACVelPointIt end(D,grd,-1);
    while(beg!=end) {
      if(!beg.hasCellCenterLeft()) {
        RID=beg.offCellCenter();
        LT=AIR;
        RT=tag._data[RID];
        LF=abs(RF);
        RF=phi._data[RID];
        PL=0;
        PR=pressure._data[RID];
      } else if(!beg.hasCellCenter()) {
        LID=beg.offCellCenterLeft();
        LT=tag._data[LID];
        RT=AIR;
        LF=phi._data[LID];
        RF=abs(LF);
        PL=pressure._data[LID];
        PR=0;
      } else {
        LID=beg.offCellCenterLeft();
        RID=beg.offCellCenter();
        LT=tag._data[LID];
        RT=tag._data[RID];
        LF=phi._data[LID];
        RF=phi._data[RID];
        PL=pressure._data[LID];
        PR=pressure._data[RID];
      }
      if(LT == FLUID || RT == FLUID) {
        grd.getCellSzCtrMACVel(D,beg.id(),cellSz);
        vel._data[D][beg.off()]=velTmp._data[D][beg.off()];
        if(LT != SOLID && RT != SOLID) {
          theta=1.0f;
          if(LT == AIR || RT == AIR)
            theta=ComputeWeightInterface::fractionCell1D(LF,RF);
          if(theta < 0.01f)
            theta=0.01f;
          theta*=cellSz[D]*density;
          vel._data[D][beg.off()]-=(T)(PR-PL)*dt/theta;
        }
        valid._data[D][beg.off()]=1;
      }
      ++beg;
    }
  }
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOpProject<T,DIM,Encoder>::project(const GridType& grd,CellCenterData<scalarD>& pressure,const CellCenterData<T>& phi,const ScalarField& nodalSolidPhi,MACVelData<DIM,T>& vel,MACVelData<DIM,unsigned char>& valid,CellCenterData<unsigned char>& tag,T dt,T density)
{
  Cold RHS;
  PCGSolver<scalarD> solver;
  FixedSparseMatrix<scalarD,Kernel<scalarD> > LHS;
  computeTag(grd,tag,phi,nodalSolidPhi);
  buildMatrix(grd,tag,phi,vel,LHS,RHS,dt,density);
  solver.setMatrix(LHS,true);
  solver.setSolverParameters(SOLVER_EPS,1E6);
  //solver.setCallback(boost::shared_ptr<Callback<scalarD,Kernel<scalarD> > >(new Callback<scalarD,Kernel<scalarD> >()));
  ASSERT(solver.solve(RHS,pressure._data) == PCGSolver<scalarD>::SUCCESSFUL);
  projectOut(grd,pressure,tag,phi,nodalSolidPhi,vel,valid,dt,density);
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOpProject<T,DIM,Encoder>::projectUnilateral(const GridType& grd,CellCenterData<scalarD>& pressure,const CellCenterData<T>& phi,const ScalarField& nodalSolidPhi,MACVelData<DIM,T>& vel,MACVelData<DIM,unsigned char>& valid,CellCenterData<unsigned char>& tag,T dt,T density)
{
  typedef MPRGPQPSolver<scalarD,Kernel<scalarD> > QPSolver;
  Cold RHS,L,H;
  QPSolver solver;
  FixedSparseMatrix<scalarD,Kernel<scalarD> > LHS;
  L.setConstant(grd._nrPointCellCenter.prod(),-ScalarUtil<scalarD>::scalar_max);
  H.setConstant(grd._nrPointCellCenter.prod(), ScalarUtil<scalarD>::scalar_max);
  computeTag(grd,tag,phi,nodalSolidPhi);
  buildMatrix(grd,tag,phi,vel,LHS,RHS,dt,density,&L);
  solver.reset(L,H,true);
  solver.setMatrix(LHS,true);
  solver.setSolverParameters(SOLVER_EPS,1E6);
  solver.setPre(boost::shared_ptr<DiagonalInFacePreconSolver<scalarD> >(new DiagonalInFacePreconSolver<scalarD>(LHS)));
  //solver.setCallback(boost::shared_ptr<Callback<scalarD,Kernel<scalarD> > >(new Callback<scalarD,Kernel<scalarD> >()));
  ASSERT(solver.solve(RHS,pressure._data) == QPSolver::SUCCESSFUL);
  projectOut(grd,pressure,tag,phi,nodalSolidPhi,vel,valid,dt,density);
}
//project variational
template <typename T,int DIM,typename Encoder>
bool VariableSizedGridOpProject<T,DIM,Encoder>::checkFaceValid(const MACVelPointIt& beg,T weight,const CellCenterData<T>& phi,sizeType& LID,sizeType& RID,T& LF,T& RF,T& theta)
{
  LF=1;
  RF=1;
  LID=-1;
  RID=-1;
  if(!beg.hasCellCenterLeft()) {
    RID=beg.offCellCenter();
    RF=phi._data[RID];
    LF=abs(RF);
  } else if(!beg.hasCellCenter()) {
    LID=beg.offCellCenterLeft();
    LF=phi._data[LID];
    RF=abs(LF);
  } else {
    LID=beg.offCellCenterLeft();
    RID=beg.offCellCenter();
    LF=phi._data[LID];
    RF=phi._data[RID];
  }
  theta=1.0f;
  if(weight == 1 && (LF >= 0 || RF >= 0))
    theta=ComputeWeightInterface::fractionCell1D(LF,RF);
  if(theta < 0.01f)
    theta=0.01f;
  return LF < 0 || RF < 0;
}
template <typename T,int DIM,typename Encoder>
Cold VariableSizedGridOpProject<T,DIM,Encoder>::buildMatrixVariational(const GridType& grd,const CellCenterData<T>& phi,const MACVelData<DIM,T>& weight,MACVelData<DIM,T>& vel,FixedSparseMatrix<scalarD,Kernel<scalarD> >& LHS,Cold& RHS,T dt,T density,Cold* lowerBound)
{
#define CTR_NEIGH DIM
#define RIGHT_NEIGH NR_NEIGH-D
#define LEFT_NEIGH D
#define NR_NEIGH (DIM<<1)
  //fill
  sizeType nrP=grd._nrPointCellCenter.prod();
  SparseSTENCILd LHSd(NR_NEIGH+1,nrP);
  SparseSTENCILi LHSi(NR_NEIGH+1,nrP);
  LHSd.setZero();
  RHS.setZero(nrP);
  VecDIMd cellSz;
  T mass,theta,coefLHS,coefRHS,w,LF,RF;
  sizeType LID=-1,RID=-1;
  for(sizeType D=0; D<DIM; D++) {
    MACVelPointIt beg(D,grd,0);
    MACVelPointIt end(D,grd,-1);
    while(beg!=end) {
      w=weight._data[D][beg.off()];
      if(w > 0 && checkFaceValid(beg,w,phi,LID,RID,LF,RF,theta)) {
        grd.getCellSzCtrMACVel(D,beg.id(),cellSz);
#ifdef DEBUG_CELLSZ
        VecDIMd cellSzL=VecDIMd::Zero(),cellSzR=VecDIMd::Zero();
        VecDIMi lid,rid;
        if(beg.hasCellCenterLeft()) {
          grd.decodeCellCenter(LID,lid);
          grd.getCellSzNodal(lid,cellSzL);
        }
        if(beg.hasCellCenter()) {
          grd.decodeCellCenter(RID,rid);
          grd.getCellSzNodal(rid,cellSzR);
        }
        T groundTruth=(cellSzL.prod()+cellSzR.prod())/2;
        ASSERT(abs(groundTruth-cellSz.prod()) < 1E-6f)
#endif
        mass=cellSz.prod()*density*w*theta;
        theta=1/(theta*cellSz[D]*density);
        coefRHS=theta*mass;
        coefLHS=dt*theta*coefRHS;
        if(LF < 0) {
          LHSd(CTR_NEIGH,LID)+=coefLHS;
          LHSi(CTR_NEIGH,LID)=LID;
          RHS[LID]-=vel._data[D][beg.off()]*coefRHS;
        }
        if(RF < 0) {
          LHSd(CTR_NEIGH,RID)+=coefLHS;
          LHSi(CTR_NEIGH,RID)=RID;
          RHS[RID]+=vel._data[D][beg.off()]*coefRHS;
        }
        if(LF < 0 && RF < 0) {
          LHSd(RIGHT_NEIGH,LID)-=coefLHS;
          LHSi(RIGHT_NEIGH,LID)=RID;
          LHSd(LEFT_NEIGH,RID)-=coefLHS;
          LHSi(LEFT_NEIGH,RID)=LID;
        }
      } else {
        if(lowerBound) {
          if(LID >= 0)
            (*lowerBound)[LID]=0;
          if(RID >= 0)
            (*lowerBound)[RID]=0;
        }
        vel._data[D][beg.off()]=0;
      }
      ++beg;
    }
  }
#undef CTR_NEIGH
#undef RIGHT_NEIGH
#undef LEFT_NEIGH
#undef NR_NEIGH
  assembleMatrix(grd,LHS,LHSd,LHSi);
  return LHSd.row(DIM);
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOpProject<T,DIM,Encoder>::projectOutVariational(const GridType& grd,const CellCenterData<scalarD>& pressure,const CellCenterData<T>& phi,const MACVelData<DIM,T>& weight,MACVelData<DIM,T>& vel,MACVelData<DIM,unsigned char>& valid,T dt,T density)
{
  MACVelData<DIM,T> velTmp(vel);
  T theta=0,w,LF,RF,PL,PR;
  valid.setZero();
  vel.setZero();
  VecDIMd cellSz;
  sizeType LID=-1,RID=-1;
  for(sizeType D=0; D<DIM; D++) {
    MACVelPointIt beg(D,grd,0);
    MACVelPointIt end(D,grd,-1);
    while(beg!=end) {
      w=weight._data[D][beg.off()];
      if(w > 0 && checkFaceValid(beg,w,phi,LID,RID,LF,RF,theta)) {
        PL=LF < 0 ? pressure._data[LID] : 0;
        PR=RF < 0 ? pressure._data[RID] : 0;
        grd.getCellSzCtrMACVel(D,beg.id(),cellSz);
        vel._data[D][beg.off()]=velTmp._data[D][beg.off()];
        theta*=cellSz[D]*density;
        vel._data[D][beg.off()]-=(T)(PR-PL)*dt/theta;
        valid._data[D][beg.off()]=1;
      }
      ++beg;
    }
  }
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOpProject<T,DIM,Encoder>::projectVariational(const GridType& grd,CellCenterData<scalarD>& pressure,const CellCenterData<T>& phi,const MACVelData<DIM,T>& weight,MACVelData<DIM,T>& vel,MACVelData<DIM,unsigned char>& valid,T dt,T density)
{
  Cold RHS;
  PCGSolver<scalarD> solver;
  FixedSparseMatrix<scalarD,Kernel<scalarD> > LHS;
  buildMatrixVariational(grd,phi,weight,vel,LHS,RHS,dt,density);
  solver.setMatrix(LHS,true);
  solver.setSolverParameters(SOLVER_EPS,1E6);
  //solver.setCallback(boost::shared_ptr<Callback<scalarD,Kernel<scalarD> > >(new Callback<scalarD,Kernel<scalarD> >()));
  ASSERT(solver.solve(RHS,pressure._data) == PCGSolver<scalarD>::SUCCESSFUL);
  projectOutVariational(grd,pressure,phi,weight,vel,valid,dt,density);
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOpProject<T,DIM,Encoder>::projectVariationalUnilateral(const GridType& grd,CellCenterData<scalarD>& pressure,const CellCenterData<T>& phi,const MACVelData<DIM,T>& weight,MACVelData<DIM,T>& vel,MACVelData<DIM,unsigned char>& valid,T dt,T density)
{
  typedef MPRGPQPSolver<scalarD,Kernel<scalarD> > QPSolver;
  Cold RHS,L,H;
  QPSolver solver;
  FixedSparseMatrix<scalarD,Kernel<scalarD> > LHS;
  L.setConstant(grd._nrPointCellCenter.prod(),-ScalarUtil<scalarD>::scalar_max);
  H.setConstant(grd._nrPointCellCenter.prod(), ScalarUtil<scalarD>::scalar_max);
  buildMatrixVariational(grd,phi,weight,vel,LHS,RHS,dt,density,&L);
  solver.reset(L,H,true);
  solver.setMatrix(LHS,true);
  solver.setSolverParameters(SOLVER_EPS,1E6);
  solver.setPre(boost::shared_ptr<DiagonalInFacePreconSolver<scalarD> >(new DiagonalInFacePreconSolver<scalarD>(LHS)));
  //solver.setCallback(boost::shared_ptr<Callback<scalarD,Kernel<scalarD> > >(new Callback<scalarD,Kernel<scalarD> >()));
  ASSERT(solver.solve(RHS,pressure._data) == QPSolver::SUCCESSFUL);
  projectOutVariational(grd,pressure,phi,weight,vel,valid,dt,density);
}
//project with rigid body, implementing: [A fast variational framework for accurate solid-fluid coupling]
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOpProject<T,DIM,Encoder>::computeRigidBodyWeightMACVel(const GridType& grd,MACVelData<DIM,T>* weightRigid,MACVelData<DIM,T>* weightWithRigid,MACVelData<DIM,sizeType>* rigidId,const RigidBody& body,sizeType RID)
{
#define UPDATE_WEIGHT \
if(weightRigid) \
  weightRigid->_data[D][beg.off()]=w; \
else if(weightWithRigid && rigidId) {  \
  T& wCurr=weightWithRigid->_data[D][beg.off()];  \
  w=min<T>(max<T>(1-w,0),1);  \
  if(w < wCurr) { \
    rigidId->_data[D][beg.off()]=RID; \
    wCurr=w;  \
  } \
}
  BBox<scalar> bbb;
  body.getWorldBB(bbb);
  //main loop
  VecDIMi lastId;
  if(weightRigid)
    weightRigid->setZero();
  T v0,v1,v2,v3,v4,v5,v6,v7;
  for(sizeType D=0; D<DIM; D++) {
    BBox<T,DIM> bb=assignVariableTo<T,DIM,scalar,3>(bbb);
    MACVelPointIt beg(D,grd,0,bb,VecDIMi::Constant(3),VecDIMi::Constant(4));
    MACVelPointIt end(D,grd,-1,bb,VecDIMi::Constant(3),VecDIMi::Constant(4));
    lastId.setConstant(-1);
    if(DIM == 2) {
      while(beg!=end) {
        bb=grd.getCellMACVel(D,beg.id());
        bb._minC+=beg.warpCompensate();
        bb._maxC+=beg.warpCompensate();
        if(beg.id() == lastId+VecDIMi::Unit(DIM-1)) {
          v0=v2;
          v1=v3;
          v2=body.dist(Vec3(bb._minC[0],bb._maxC[1],0));
          v3=body.dist(Vec3(bb._maxC[0],bb._maxC[1],0));
        } else {
          v0=body.dist(Vec3(bb._minC[0],bb._minC[1],0));
          v1=body.dist(Vec3(bb._maxC[0],bb._minC[1],0));
          v2=body.dist(Vec3(bb._minC[0],bb._maxC[1],0));
          v3=body.dist(Vec3(bb._maxC[0],bb._maxC[1],0));
        }
        T w=ComputeWeightInterface::fractionCell2D(v0,v1,v2,v3);
        UPDATE_WEIGHT
        lastId=beg.id();
        ++beg;
      }
    } else if(DIM == 3) {
      while(beg!=end) {
        bb=grd.getCellMACVel(D,beg.id());
        if(beg.id() == lastId+VecDIMi::Unit(0)) {
          v0=v4;
          v1=v5;
          v2=v6;
          v3=v7;
          v4=body.dist(Vec3(bb._minC[0],bb._minC[1],bb._maxC[2]));
          v5=body.dist(Vec3(bb._maxC[0],bb._minC[1],bb._maxC[2]));
          v6=body.dist(Vec3(bb._minC[0],bb._maxC[1],bb._maxC[2]));
          v7=body.dist(Vec3(bb._maxC[0],bb._maxC[1],bb._maxC[2]));
        } else {
          v0=body.dist(Vec3(bb._minC[0],bb._minC[1],bb._minC[2]));
          v1=body.dist(Vec3(bb._maxC[0],bb._minC[1],bb._minC[2]));
          v2=body.dist(Vec3(bb._minC[0],bb._maxC[1],bb._minC[2]));
          v3=body.dist(Vec3(bb._maxC[0],bb._maxC[1],bb._minC[2]));
          v4=body.dist(Vec3(bb._minC[0],bb._minC[1],bb._maxC[2]));
          v5=body.dist(Vec3(bb._maxC[0],bb._minC[1],bb._maxC[2]));
          v6=body.dist(Vec3(bb._minC[0],bb._maxC[1],bb._maxC[2]));
          v7=body.dist(Vec3(bb._maxC[0],bb._maxC[1],bb._maxC[2]));
        }
        T w=ComputeWeightInterface::fractionCell3D(v0,v1,v2,v3,v4,v5,v6,v7);
        UPDATE_WEIGHT
        lastId=beg.id();
        ++beg;
      }
    }
    else {
      ASSERT(false)
    }
  }
#undef UPDATE_WEIGHT
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOpProject<T,DIM,Encoder>::buildForceMatrix(const Cold& diag,const GridType& grd,const CellCenterData<T>& phi,const MACVelData<DIM,T>& weightRigid,const RigidBody& body,T dt,sizeType i,MJ& mj,bool changeRHS,Cold* lowerBound)
{
  BBox<scalar> bbb;
  body.getWorldBB(bbb);
  BBox<T,DIM> bb=assignVariableTo<T,DIM,scalar,3>(bbb);
  CellCenterPointIt beg(grd,0,bb,VecDIMi::Constant(3),VecDIMi::Constant(4));
  CellCenterPointIt end(grd,-1,bb,VecDIMi::Constant(3),VecDIMi::Constant(4));
  //build force matrix
  Vec6d e;
  Vec3 rel;
  T liquidPhi;
  Vec6 weights;
  std::vector<sizeType>& col=mj._col;
  std::vector<Vec6d,Eigen::aligned_allocator<Vec6d> >& colData=mj._colData;
  while(beg!=end) {
    //the second criterion is very important, especially when rigid body is crossing the boundary
    liquidPhi=phi._data[beg.off()];
    if(liquidPhi >= 0 || diag[beg.off()] <= 0) {
      ++beg;
      continue;
    }
    //the order is NX PX NY PY NZ PZ
    weights.setZero();
    for(sizeType D=0; D<DIM; D++) {
      weights[D*2+0]=weightRigid._data[D][beg.offFaceLeft(D)];
      weights[D*2+0]*=grd.getFaceAreaMACVel(D,beg.id());
      weights[D*2+1]=weightRigid._data[D][beg.offFaceRight(D)];
      weights[D*2+1]*=grd.getFaceAreaMACVel(D,beg.id());
    }
    if(weights.isZero()) {
      ++beg;
      //test if we need further computation
      continue;
    }
    //build force
    e.setZero();
    e[0]=(weights[1]-weights[0]);
    e[1]=(weights[3]-weights[2]);
    if(DIM == 3)
      e[2]=(weights[5]-weights[4]);
    rel=assignVariableTo<Vec3,VecDIMd>(beg.pointWithWarp())-body.pos();
    if(DIM == 3)
      e[3]=(weights[2]-weights[3])*rel.z()-(weights[4]-weights[5])*rel.y();
    if(DIM == 3)
      e[4]=(weights[4]-weights[5])*rel.x()-(weights[0]-weights[1])*rel.z();
    e[5]=(weights[0]-weights[1])*rel.y()-(weights[2]-weights[3])*rel.x();
    if(!e.isZero()) {
      if(lowerBound)
        (*lowerBound)[beg.off()]=0;
      col.push_back(beg.off());
      colData.push_back(e);
    }
    ++beg;
  }
  Mat3 invI=body.invI();
  scalar M=body.getM();
  if(changeRHS) {
    mj._invMDt=dt/body.getM();
    mj._invIDt=invI.cast<scalarD>()*dt;
  } else {
    mj._invMDt=-1/(M*dt);
    mj._invIDt=-invI.cast<scalarD>()/dt;
    for(sizeType j=0; j<(sizeType)colData.size(); j++) {
      colData[j].segment<3>(0)/=M;
      colData[j].segment<3>(3)=(invI.cast<scalarD>()*colData[j].segment<3>(3)).eval();
    }
  }
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOpProject<T,DIM,Encoder>::buildRigidBodyMatrix(const Cold& diag,vector<MJ>& MJs,vector<sizeType>& MJIDs,Cold& RHS,const GridType& grd,const CellCenterData<T>& phi,const vector<MACVelData<DIM,T> >& weightRigids,const RigidSolver& rigid,T dt,bool changeRHS,Cold* lowerBound)
{
  Vec6d V;
  MJs.clear();
  MJIDs.clear();
  //assemble all the rigid body velocity components
  const sizeType nrBody=(sizeType)rigid.nrBody();
  for(sizeType i=0; i<nrBody; i++) {
    const RigidBody body=rigid.getBody(i);
    if(!body.valid())
      continue;

    MJs.push_back(MJ());
    MJIDs.push_back(i);
    buildForceMatrix(diag,grd,phi,weightRigids[i],body,dt,i,MJs.back(),changeRHS,lowerBound);
    if(MJs.back()._col.size() == 0) {
      MJs.pop_back();
      MJIDs.pop_back();
      continue;
    }

    //contribute right hand side
    V.segment<3>(0)=body.linSpd().cast<scalarD>();
    V.segment<3>(3)=body.rotSpd().cast<scalarD>();
    if(changeRHS || body.freeze())
      MJs.back().multiplyJTSubtract(V,RHS);
    else RHS=concat(RHS,-V/dt);

    //contribute left hand side
    if(body.freeze()) {
      MJs.pop_back();
      MJIDs.pop_back();
    }
  }
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOpProject<T,DIM,Encoder>::rigidProjectVariational(const GridType& grd,CellCenterData<scalarD>& pressure,const CellCenterData<T>& phi,const MACVelData<DIM,T>& weight,const vector<MACVelData<DIM,T> >& weightRigids,RigidSolver& rigid,MACVelData<DIM,T>& vel,MACVelData<DIM,unsigned char>& valid,T dt,T density)
{
  Cold RHS;
  PCGSolver<scalarD> solver;
  FixedSparseMatrix<scalarD,Kernel<scalarD> > LHS,LHSWithRigid;
  Cold diag=buildMatrixVariational(grd,phi,weight,vel,LHS,RHS,dt,density);
  //writeGridCellCenterVTK("phiPrj.vtk",grd,phi);
  //writeGridCellCenterVTK("weightPrj.vtk",grd,weight);
  //exit(-1);

  vector<MJ> MJs;
  vector<sizeType> MJIDs;
  buildRigidBodyMatrix(diag,MJs,MJIDs,RHS,grd,phi,weightRigids,rigid,dt,true);

  boost::shared_ptr<RigidBodyKrylovMatrix<MJ> > krylovMatrix;
  krylovMatrix.reset(new RigidBodyKrylovMatrix<MJ>(LHS,MJs,MJIDs));
#ifdef PRECOMPUTE_RIGID_MATRIX
  krylovMatrix->toFixed(LHSWithRigid);
  solver.setMatrix(LHSWithRigid,true);
#else
  solver.getPre()->setMatrix(LHS,false);
  solver.setKrylovMatrix(krylovMatrix);
#endif
  solver.setSolverParameters(SOLVER_EPS,1E6);
  //solver.setCallback(boost::shared_ptr<Callback<scalarD,Kernel<scalarD> > >(new Callback<scalarD,Kernel<scalarD> >()));
  ASSERT(solver.solve(RHS,pressure._data) == PCGSolver<scalarD>::SUCCESSFUL);
  projectOutVariational(grd,pressure,phi,weight,vel,valid,dt,density);
  krylovMatrix->applyForce(pressure,rigid,dt);
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOpProject<T,DIM,Encoder>::rigidProjectVariationalMINRES(const GridType& grd,CellCenterData<scalarD>& pressure,const CellCenterData<T>& phi,const MACVelData<DIM,T>& weight,const vector<MACVelData<DIM,T> >& weightRigids,RigidSolver& rigid,MACVelData<DIM,T>& vel,MACVelData<DIM,unsigned char>& valid,T dt,T density)
{
  Cold RHS;
  //PMINRESSolverQLP<scalarD,Kernel<scalarD> > solver;
  PMINRESSolverQLP<scalarD,Kernel<scalarD>,RigidFluidPrecondition> solver;
  FixedSparseMatrix<scalarD,Kernel<scalarD> > LHS;
  Cold diag=buildMatrixVariational(grd,phi,weight,vel,LHS,RHS,dt,density);
  //writeGridCellCenterVTK("phiPrj.vtk",grd,phi);
  //writeGridCellCenterVTK("weightPrj.vtk",grd,weight);
  //exit(-1);

  vector<MJ> MJs;
  vector<sizeType> MJIDs;
  buildRigidBodyMatrix(diag,MJs,MJIDs,RHS,grd,phi,weightRigids,rigid,dt,false);
  sizeType sSize=LHS.rows();

  boost::shared_ptr<RigidBodyKrylovMatrixSparse<MJ> > krylovMatrix;
  krylovMatrix.reset(new RigidBodyKrylovMatrixSparse<MJ>(LHS,MJs,MJIDs));
  if(solver.getPre()) {
    RigidFluidPrecondition* pre=dynamic_cast<RigidFluidPrecondition*>(solver.getPre());
    if(pre) {
      pre->reset(MJs,sSize);
      pre->setMatrix(LHS,false);
    }
  }
  pressure._data.resize(RHS.size());
  solver.setKrylovMatrix(krylovMatrix);
  solver.setSolverParameters(SOLVER_EPS,1E6);
  //solver.setCallback(boost::shared_ptr<Callback<scalarD,Kernel<scalarD> > >(new Callback<scalarD,Kernel<scalarD> >()));
  ASSERT(solver.solve(RHS,pressure._data) == PMINRESSolverQLP<scalarD>::SUCCESSFUL);
  projectOutVariational(grd,pressure,phi,weight,vel,valid,dt,density);
  krylovMatrix->applyForceMINRES(pressure,rigid,dt);
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOpProject<T,DIM,Encoder>::rigidProjectVariationalUnilateral(const GridType& grd,CellCenterData<scalarD>& pressure,const CellCenterData<T>& phi,const MACVelData<DIM,T>& weight,const vector<MACVelData<DIM,T> >& weightRigids,RigidSolver& rigid,MACVelData<DIM,T>& vel,MACVelData<DIM,unsigned char>& valid,T dt,T density)
{
  typedef MPRGPQPSolver<scalarD,Kernel<scalarD> > QPSolver;
  Cold RHS,L,H;
  QPSolver solver;
  FixedSparseMatrix<scalarD,Kernel<scalarD> > LHS,LHSWithRigid;
  L.setConstant(grd._nrPointCellCenter.prod(),-ScalarUtil<scalarD>::scalar_max);
  H.setConstant(grd._nrPointCellCenter.prod(), ScalarUtil<scalarD>::scalar_max);
  Cold diag=buildMatrixVariational(grd,phi,weight,vel,LHS,RHS,dt,density,&L);
  //writeGridCellCenterVTK("phiPrj.vtk",grd,phi);
  //writeGridCellCenterVTK("weightPrj.vtk",grd,weight);
  //exit(-1);

  vector<MJ> MJs;
  vector<sizeType> MJIDs;
  buildRigidBodyMatrix(diag,MJs,MJIDs,RHS,grd,phi,weightRigids,rigid,dt,true,&L);

  RigidBodyKrylovMatrix<MJ> krylovMatrix(LHS,MJs,MJIDs);
  krylovMatrix.toFixed(LHSWithRigid);
  solver.reset(L,H,true);
  solver.setMatrix(LHSWithRigid,true);
  solver.setSolverParameters(SOLVER_EPS,1E6);
  solver.setPre(boost::shared_ptr<DiagonalInFacePreconSolver<scalarD> >(new DiagonalInFacePreconSolver<scalarD>(LHSWithRigid)));
  //solver.setCallback(boost::shared_ptr<Callback<scalarD,Kernel<scalarD> > >(new Callback<scalarD,Kernel<scalarD> >()));
  ASSERT(solver.solve(RHS,pressure._data) == QPSolver::SUCCESSFUL);
  projectOutVariational(grd,pressure,phi,weight,vel,valid,dt,density);
  krylovMatrix.applyForce(pressure,rigid,dt);
}
//project
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOpProject<T,DIM,Encoder>::debugRigidProject(const string& path,const Vec3i& warpMode)
{
  recreate(path);
  GridType testGrid=VariableSizedGridOp<T,DIM,Encoder>::createTestGrid(warpMode);
  //debug rigid weight: add rigid
  scalar cellSz=testGrid._cellSz0.maxCoeff();
  RigidSolver sol(DIM,cellSz);
  Vec3 ext=Vec3::Zero();
  ext[0]=cellSz*10;
  ext[1]=cellSz*40;
  if(DIM == 3)
    ext[2]=cellSz*10;
  RigidSolver::Quat q;
  q.setFromTwoVectors(Vec3(1,0,0),Vec3(1,1,0));
  Vec3 pos=assignVariableTo<Vec3,VecDIMd>(testGrid.getBBNodal()._minC);
  pos.segment<DIM>(0).array()+=cellSz*5;
  sol.addCross(ext,pos,q);
  sol.getBody(0).setP(Vec3(0,1,0)*sol.getBody(0).getM());
  //compute rigid weights
  vector<MACVelData<DIM,T> > weights(1);
  weights[0].reset(testGrid);
  computeRigidBodyWeightMACVel(testGrid,&weights[0],NULL,NULL,sol.getBody(0),0);
  for(sizeType D=0; D<DIM; D++)
    VariableSizedGridOp<T,DIM,Encoder>::writeGridMACVelCompVTK(D,path+"/weightComp"+boost::lexical_cast<string>((int)D)+".vtk",testGrid,weights[0]);
  //compute weight
  MACVelData<DIM,T> weight(testGrid);
  weight.clamp(1,1);
  //phi
  CellCenterData<T> phi(testGrid);
  {
    BBox<T,DIM> bbCellCenter=testGrid.getBBNodal();
    VecDIMd ctr=(bbCellCenter._minC+bbCellCenter._maxC)/2;
    T rad=bbCellCenter.getExtent().maxCoeff()/4;
    CellCenterPointIt beg(testGrid,0);
    CellCenterPointIt end(testGrid,-1);
    while(beg!=end) {
      if((*beg-ctr).norm() > rad)
        phi._data[beg.off()]=-1;
      else phi._data[beg.off()]=1;
      ++beg;
    }
    VariableSizedGridOp<T,DIM,Encoder>::writeGridCellCenterVTK(path+"/phi.vtk",testGrid,phi);
  }
  //create zero velocity
  MACVelData<DIM,T> vel(testGrid);
  vel.setZero();
  //project
  MACVelData<DIM,unsigned char> valid(testGrid);
  CellCenterData<scalarD> pressure(testGrid);
  rigidProjectVariational(testGrid,pressure,phi,weight,weights,sol,vel,valid,0.01f,1000.0f);
  VariableSizedGridOp<T,DIM,Encoder>::writeGridCellCenterVTK(path+"/pressure.vtk",testGrid,pressure);
  VariableSizedGridOp<T,DIM,Encoder>::writeGridMACVelVTK(path+"/velAfter.vtk",testGrid,vel,0.1f);
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOpProject<T,DIM,Encoder>::debugRigidProjectAllWarp()
{
  INFO("Debugging RigidProject all warp cases!")
  debugRigidProject("rigidProject"+GridType::printWarp(Vec3i::Zero()),Vec3i::Zero());
  if(Encoder::isConstantEncoder) {
    for(sizeType D=0; D<DIM; D++)
      debugRigidProject("rigidProject"+GridType::printWarp(Vec3i::Unit(D)),Vec3i::Unit(D));
    debugRigidProject("rigidProject"+GridType::printWarp(Vec3i::Ones()),Vec3i::Ones());
  }
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOpProject<T,DIM,Encoder>::debugProject(const string& path,const Vec3i& warpMode)
{
  recreate(path);
  GridType testGrid=VariableSizedGridOp<T,DIM,Encoder>::createTestGrid(warpMode);
  //nodalSolidPhi
  scalar radCoefGiven=0.3333333f;
  VecDIMd ctrGiven=testGrid.getBBNodal()._minC;
  BBox<T,DIM> bbCellCenter=testGrid.getBBCellCenter();
  ScalarField nodalSolidPhi=VariableSizedGridOp<T,DIM,Encoder>::createTestSolidPhi(testGrid,&ctrGiven,&radCoefGiven);
  nodalSolidPhi.mul(-1);
  CellCenterData<T> phi(testGrid);
  {
    CellCenterPointIt beg(testGrid,0);
    CellCenterPointIt end(testGrid,-1);
    while(beg!=end) {
      if((*beg)[1] < (bbCellCenter._minC[1]+bbCellCenter._maxC[1])/2 &&
          (*beg)[0] < (bbCellCenter._minC[0]+bbCellCenter._maxC[0])/2)
        phi._data[beg.off()]=-1;
      else phi._data[beg.off()]=1;
      ++beg;
    }
  }
  MACVelData<DIM,T> vel(testGrid);
  for(sizeType D=0; D<DIM; D++)
    if(D != 1)
      vel._data[D].setZero();
    else {
      MACVelPointIt beg(D,testGrid,0);
      MACVelPointIt end(D,testGrid,-1);
      while(beg!=end) {
        if((*beg)[1] < (bbCellCenter._minC[1]+bbCellCenter._maxC[1])/2 &&
            (*beg)[0] < (bbCellCenter._minC[0]+bbCellCenter._maxC[0])/2)
          vel._data[D][beg.off()]=-1;
        else vel._data[D][beg.off()]=0;
        ++beg;
      }
    }
  if(DIM == 2)
    GridOp<scalar,scalar>::write2DScalarGridVTK(path+"/nodalSolidPhi.vtk",nodalSolidPhi,false);
  else if(DIM == 3)
    GridOp<scalar,scalar>::write3DScalarGridVTK(path+"/nodalSolidPhi.vtk",nodalSolidPhi);
  VariableSizedGridOp<T,DIM,Encoder>::writeGridCellCenterVTK(path+"/phi.vtk",testGrid,phi);
  VariableSizedGridOp<T,DIM,Encoder>::writeGridMACVelVTK(path+"/velBefore.vtk",testGrid,vel,0.1f);
  VariableSizedGridOp<T,DIM,Encoder>::constraintMACVel(testGrid,nodalSolidPhi,vel);
  VariableSizedGridOp<T,DIM,Encoder>::writeGridMACVelVTK(path+"/velConstraint.vtk",testGrid,vel,0.1f);
  MACVelData<DIM,T> tmp=vel;
  {
    MACVelData<DIM,unsigned char> valid(testGrid);
    CellCenterData<unsigned char> tag(testGrid);
    CellCenterData<scalarD> pressure(testGrid);
    project(testGrid,pressure,phi,nodalSolidPhi,vel,valid,tag,0.01f,1000.0f);
    VariableSizedGridOp<T,DIM,Encoder>::writeGridCellCenterVTK(path+"/pressure.vtk",testGrid,pressure);
    VariableSizedGridOp<T,DIM,Encoder>::writeGridMACVelVTK(path+"/velAfter.vtk",testGrid,vel,0.1f);
  }
  {
    vel=tmp;
    MACVelData<DIM,unsigned char> valid(testGrid);
    MACVelData<DIM,T> weight(testGrid);
    CellCenterData<scalarD> pressure(testGrid);
    sampleWeightMACVel(testGrid,weight,nodalSolidPhi);
    projectVariational(testGrid,pressure,phi,weight,vel,valid,0.01f,1000.0f);
    VariableSizedGridOp<T,DIM,Encoder>::writeGridCellCenterVTK(path+"/pressureVariational.vtk",testGrid,pressure);
    VariableSizedGridOp<T,DIM,Encoder>::writeGridMACVelVTK(path+"/velAfterVariational.vtk",testGrid,vel,0.1f);
    for(sizeType D=0; D<DIM; D++)
      VariableSizedGridOp<T,DIM,Encoder>::writeGridMACVelCompVTK(D,path+"/weightComp"+boost::lexical_cast<string>((int)D)+".vtk",testGrid,weight);
  }
  INFOV("VelNormAfter: %f!",vel.maxAbs())
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOpProject<T,DIM,Encoder>::debugProjectAllWarp()
{
  INFO("Debugging Project all warp cases!")
  debugProject("project"+GridType::printWarp(Vec3i::Zero()),Vec3i::Zero());
  if(Encoder::isConstantEncoder)
    for(sizeType D=0; D<DIM; D++)
      debugProject("project"+GridType::printWarp(Vec3i::Unit(D)),Vec3i::Unit(D));
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOpProject<T,DIM,Encoder>::debugWeight(const string& path,const Vec3i& warpMode)
{
  recreate(path);
  GridType testGrid=VariableSizedGridOp<T,DIM,Encoder>::createTestGrid(warpMode);
  //nodalSolidPhi
  VecDIMd ctrGiven=testGrid.getBBNodal()._minC;
  ScalarField nodalSolidPhi=VariableSizedGridOp<T,DIM,Encoder>::createTestSolidPhi(testGrid,&ctrGiven);
  if(DIM == 2)
    GridOp<scalar,scalar>::write2DScalarGridVTK(path+"/nodalSolidPhi.vtk",nodalSolidPhi,false);
  else GridOp<scalar,scalar>::write3DScalarGridVTK(path+"/nodalSolidPhi.vtk",nodalSolidPhi);
  //compute weight
  MACVelData<DIM,T> weight(testGrid);
  sampleWeightMACVel(testGrid,weight,nodalSolidPhi);
  for(sizeType D=0; D<DIM; D++)
    VariableSizedGridOp<T,DIM,Encoder>::writeGridMACVelCompVTK(D,path+"/weightComp"+boost::lexical_cast<string>((int)D)+".vtk",testGrid,weight);
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOpProject<T,DIM,Encoder>::debugWeightAllWarp()
{
  INFO("Debugging Weight all warp cases!")
  debugWeight("weight"+GridType::printWarp(Vec3i::Zero()),Vec3i::Zero());
  if(Encoder::isConstantEncoder)
    for(sizeType D=0; D<DIM; D++)
      debugWeight("weight"+GridType::printWarp(Vec3i::Unit(D)),Vec3i::Unit(D));
}
#include "VariableSizedGridOpProjectBOTE.cpp"
//instance
#define INSTANCE(DIM,TYPE,SPARSITY) \
template class VariableSizedGridOpProject<scalar,DIM,Encoder##TYPE<SPARSITY> >;  \
template <> const scalar VariableSizedGridOpProject<scalar,DIM,Encoder##TYPE<SPARSITY> >::SOLVER_EPS=1E-10f;
INSTANCE_ALL
