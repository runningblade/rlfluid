#include "VariableSizedGrid.h"
#include "CellCenterData.h"
#include "NodalData.h"
#include "MACVelData.h"
#include "Utils.h"
#include <boost/lexical_cast.hpp>

USE_PRJ_NAMESPACE

template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::genIteratorTestRange(const VecDIMi& nrPoint,const VecDIMi& warpMode,vector<pair<VecDIMi,VecDIMi> >& ranges)
{
  ranges.clear();
  vector<pair<VecDIMi,VecDIMi> > ranges2;
  ranges.push_back(pair<VecDIMi,VecDIMi>());
  for(sizeType D=0; D<DIM; D++) {
    if(warpMode[D] > 0) {
      for(sizeType r=0; r<(sizeType)ranges.size(); r++) {
        pair<VecDIMi,VecDIMi>& range=ranges[r];
        //A
        range.first[D]=0;
        range.second[D]=nrPoint[D];
        ranges2.push_back(range);
        //B
        range.first[D]=1;
        range.second[D]=nrPoint[D]-1;
        ranges2.push_back(range);
        //C
        range.first[D]=-nrPoint[D]*1.2f;
        range.second[D]=nrPoint[D]*1.2f;
        ranges2.push_back(range);
        //D
        range.first[D]=nrPoint[D]*2;
        range.second[D]=nrPoint[D]*3.2f;
        ranges2.push_back(range);
        //E
        range.first[D]=-nrPoint[D]*3.2f;
        range.second[D]=-nrPoint[D]*2;
        ranges2.push_back(range);
      }
    } else {
      for(sizeType r=0; r<(sizeType)ranges.size(); r++) {
        pair<VecDIMi,VecDIMi>& range=ranges[r];
        //A
        range.first[D]=0;
        range.second[D]=nrPoint[D];
        ranges2.push_back(range);
        //B
        range.first[D]=1;
        range.second[D]=nrPoint[D];
        ranges2.push_back(range);
        //C
        range.first[D]=0;
        range.second[D]=nrPoint[D]-1;
        ranges2.push_back(range);
        //D
        range.first[D]=1;
        range.second[D]=nrPoint[D]-1;
        ranges2.push_back(range);
      }
    }
    ranges2.swap(ranges);
    ranges2.clear();
  }
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::debugIteratorNodal(const Vec3i& warpMode)
{
  VecDIMi warpCompensate;
  Grid testGrid=createTestGrid(warpMode);
  VecDIMd warpRange=testGrid.getBBNodal().getExtent();
  vector<pair<VecDIMi,VecDIMi> > ranges;
  genIteratorTestRange(testGrid._nrPointNodal,testGrid._warpMode,ranges);
  for(sizeType i=0; i<(sizeType)ranges.size(); i++) {
    VecDIMi id0=ranges[i].first;
    VecDIMi id1=ranges[i].second;
    VecDIMi idRef=ranges[i].first;
    NodalIteratorPoint<T,DIM,Encoder> beg(testGrid,0,id0,testGrid._nrPointNodal-id1);
    NodalIteratorPoint<T,DIM,Encoder> end(testGrid,-1,id0,testGrid._nrPointNodal-id1);
    //reference
    sizeType nr=0;
    while(beg!=end) {
      ASSERT(testGrid.handleWarp(idRef,testGrid._nrPointNodal,&warpCompensate) == beg.id())
      VecDIMd warpCompensateT=(warpCompensate.template cast<T>().array()*warpRange.array()).matrix();
      ASSERT((warpCompensateT-beg.warpCompensate()).norm() < 1E-5f)
      //advance
      for(sizeType D=DIM-1; D>=0; D--) {
        idRef[D]++;
        if(idRef[D] == id1[D])
          idRef[D]=id0[D];
        else break;
      }
      ++beg;
      nr++;
    }
    ASSERT(nr == (id1-id0).prod())
  }
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::debugIteratorCellCenter(const Vec3i& warpMode)
{
  VecDIMi warpCompensate;
  Grid testGrid=createTestGrid(warpMode);
  VecDIMd warpRange=testGrid.getBBNodal().getExtent();
  vector<pair<VecDIMi,VecDIMi> > ranges;
  genIteratorTestRange(testGrid._nrPointCellCenter,testGrid._warpMode,ranges);
  for(sizeType i=0; i<(sizeType)ranges.size(); i++) {
    VecDIMi id0=ranges[i].first;
    VecDIMi id1=ranges[i].second;
    VecDIMi idRef=ranges[i].first;
    CellCenterIteratorPoint<T,DIM,Encoder> beg(testGrid,0,id0,testGrid._nrPointCellCenter-id1);
    CellCenterIteratorPoint<T,DIM,Encoder> end(testGrid,-1,id0,testGrid._nrPointCellCenter-id1);
    //reference
    sizeType nr=0;
    while(beg!=end) {
      ASSERT(testGrid.handleWarp(idRef,testGrid._nrPointCellCenter,&warpCompensate) == beg.id())
      VecDIMd warpCompensateT=(warpCompensate.template cast<T>().array()*warpRange.array()).matrix();
      ASSERT((warpCompensateT-beg.warpCompensate()).norm() < 1E-5f)
      //advance
      for(sizeType D=DIM-1; D>=0; D--) {
        idRef[D]++;
        if(idRef[D] == ranges[i].second[D])
          idRef[D]=ranges[i].first[D];
        else break;
      }
      ++beg;
      nr++;
    }
    ASSERT(nr == (id1-id0).prod())
  }
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::debugIteratorMACVel(const Vec3i& warpMode)
{
  VecDIMi warpCompensate;
  Grid testGrid=createTestGrid(warpMode);
  VecDIMd warpRange=testGrid.getBBNodal().getExtent();
  vector<pair<VecDIMi,VecDIMi> > ranges;
  for(sizeType DV=0; DV<DIM; DV++) {
    genIteratorTestRange(testGrid._nrPointMACVel[DV],testGrid._warpMode,ranges);
    for(sizeType i=0; i<(sizeType)ranges.size(); i++) {
      VecDIMi id0=ranges[i].first;
      VecDIMi id1=ranges[i].second;
      VecDIMi idRef=ranges[i].first;
      MACVelIteratorPoint<T,DIM,Encoder> beg(DV,testGrid,0,id0,testGrid._nrPointMACVel[DV]-id1);
      MACVelIteratorPoint<T,DIM,Encoder> end(DV,testGrid,-1,id0,testGrid._nrPointMACVel[DV]-id1);
      //reference
      sizeType nr=0;
      while(beg!=end) {
        ASSERT(testGrid.handleWarp(idRef,testGrid._nrPointMACVel[DV],&warpCompensate) == beg.id())
        VecDIMd warpCompensateT=(warpCompensate.template cast<T>().array()*warpRange.array()).matrix();
        ASSERT((warpCompensateT-beg.warpCompensate()).norm() < 1E-5f)
        //advance
        for(sizeType D=DIM-1; D>=0; D--) {
          idRef[D]++;
          if(idRef[D] == ranges[i].second[D])
            idRef[D]=ranges[i].first[D];
          else break;
        }
        ++beg;
        nr++;
      }
      ASSERT(nr == (id1-id0).prod())
    }
  }
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::debugIteratorAllType(const Vec3i& warpMode)
{
  debugIteratorNodal(warpMode);
  debugIteratorCellCenter(warpMode);
  debugIteratorMACVel(warpMode);
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::debugIteratorAllWarp()
{
  INFO("Debugging Iterator all warp!")
  debugIteratorAllType(Vec3i::Zero());
  if(Encoder::isConstantEncoder)
    for(sizeType D=0; D<DIM; D++) {
      debugIteratorAllType(Vec3i::Unit(D));
      debugIteratorAllType(Vec3i::Ones()-Vec3i::Unit(D));
    }
}
//debug focus
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::debugFocus(const Vec3i& warpMode)
{
  //create grid
  Grid testGridFocus;
  Grid testGrid=createTestGrid(warpMode);
  NodalData<T> dataNodal(testGrid);
  CellCenterData<T> dataCellCenter(testGrid);
  MACVelData<DIM,T> dataMACVel(testGrid);
  {
    dataNodal._data.setRandom();
    dataCellCenter._data.setRandom();
    dataMACVel.setRandom();
  }
  BBox<T,DIM> bb=testGrid.getBBNodal(),bbCellMACVel,bbNodal,bbNodalByMACVel;
  BBox<T,DIM> bbFocus(VecDIMd::Constant(bb._minC.minCoeff()),VecDIMd::Constant(bb._maxC.maxCoeff()));
  bbFocus.enlargedEps(-0.5f);
  {
    VecDIMd focus=interp1D(bb._minC,bb._maxC,(T)RandEngine::randR(0,100.0f)/100.0f)-(bbFocus._minC+bbFocus._maxC)/2;
    bbFocus._minC+=focus;
    bbFocus._maxC+=focus;
    testGridFocus.reset(testGrid,bbFocus,2);
    {
      bbNodal=testGridFocus.getBBNodal();
      VecDIMd cellSz;
      T volNodal=0;
      {
        NodalIteratorPoint<T,DIM,Encoder> beg(testGridFocus,0,VecDIMi::Zero(),VecDIMi::Ones()-testGridFocus._warpMode);
        NodalIteratorPoint<T,DIM,Encoder> end(testGridFocus,-1,VecDIMi::Zero(),VecDIMi::Ones()-testGridFocus._warpMode);
        while(beg!=end) {
          testGridFocus.getCellSzNodal(beg.id(),cellSz);
          volNodal+=cellSz.prod();
          ++beg;
        }
      }
      ASSERT(abs(volNodal-bbNodal.getExtent().prod()) < 1E-5f)
      for(sizeType D=0; D<DIM; D++) {
        T volMACVel=0;
        bbNodalByMACVel.reset();
        MACVelIteratorPoint<T,DIM,Encoder> beg(D,testGridFocus,0);
        MACVelIteratorPoint<T,DIM,Encoder> end(D,testGridFocus,-1);
        while(beg!=end) {
          //test left-right cell
          VecDIMi id=beg.id(),id2;
          if(testGridFocus._warpMode[D] > 0) {
            ASSERT(beg.hasCellCenter() && beg.hasCellCenterLeft())
            id[D]=beg.id()[D]%testGridFocus._nrPointCellCenter[D];
            ASSERT(beg.offCellCenter(&id2) == id.dot(testGridFocus._strideCellCenter) && id2 == id)
            id[D]=(beg.id()[D]+testGridFocus._nrPointCellCenter[D]-1)%testGridFocus._nrPointCellCenter[D];
            ASSERT(beg.offCellCenterLeft(&id2) == id.dot(testGridFocus._strideCellCenter) && id2 == id)
          } else {
            if(compGE(id,VecDIMi::Zero()) && compL(id,testGridFocus._nrPointCellCenter)) {
              ASSERT(beg.hasCellCenter() && beg.offCellCenter(&id2) == id.dot(testGridFocus._strideCellCenter) && id2 == id)
            } else {
              ASSERT(!beg.hasCellCenter())
            }
            id=beg.id()-VecDIMi::Unit(D);
            if(compGE(id,VecDIMi::Zero()) && compL(id,testGridFocus._nrPointCellCenter)) {
              ASSERT(beg.hasCellCenterLeft() && beg.offCellCenterLeft(&id2) == id.dot(testGridFocus._strideCellCenter) && id2 == id)
            } else {
              ASSERT(!beg.hasCellCenterLeft())
            }
          }
          //test cell center iterator
          if(beg.hasCellCenter()) {
            beg.offCellCenter(&id);
            CellCenterIteratorPoint<T,DIM,Encoder> iterC(testGridFocus,0,id,id+VecDIMi::Ones());
            if(beg.hasCellCenterLeft()) {
              ASSERT(iterC.hasCellCenterLeft(D) && iterC.offCellCenterLeft(D,&id2) == beg.offCellCenterLeft(&id) && id2 == id)
            } else {
              ASSERT(!iterC.hasCellCenterLeft(D))
            }
          }
          if(beg.hasCellCenterLeft()) {
            beg.offCellCenterLeft(&id);
            CellCenterIteratorPoint<T,DIM,Encoder> iterC(testGridFocus,0,id,id+VecDIMi::Ones());
            if(beg.hasCellCenter()) {
              ASSERT(iterC.hasCellCenter(D) && iterC.offCellCenter(D,&id2) == beg.offCellCenter(&id) && id2 == id)
            } else {
              ASSERT(!iterC.hasCellCenter(D))
            }
          }
          //test volume
          testGridFocus.getCellSzCtrMACVel(D,beg.id(),cellSz);
          bbCellMACVel=testGridFocus.getCellMACVel(D,beg.id());
          bbNodalByMACVel.setUnion(bbCellMACVel);
          ASSERT((bbCellMACVel.getExtent()-cellSz).norm() < 1E-6f)
          volMACVel+=cellSz.prod();
          ++beg;
        }
        ASSERT(abs(volMACVel-bbNodalByMACVel.getExtent().prod()) < 1E-5f)
        ASSERT(abs(volNodal-volMACVel) < 1E-5f)
      }
    }
    {
      NodalData<T> dataNodalInterp(testGridFocus),dataNodalInterpBF=dataNodalInterp;
      testGridFocus.sampleNodal(dataNodalInterp,testGrid,dataNodal);
      testGridFocus.sampleNodalBF(dataNodalInterpBF,testGrid,dataNodal);
      ASSERT((dataNodalInterp._data-dataNodalInterpBF._data).norm() < 1E-3f)

      CellCenterData<T> dataCellCenterInterp(testGridFocus),dataCellCenterInterpBF=dataCellCenterInterp;
      testGridFocus.sampleCellCenter(dataCellCenterInterp,testGrid,dataCellCenter);
      testGridFocus.sampleCellCenterBF(dataCellCenterInterpBF,testGrid,dataCellCenter);
      ASSERT((dataCellCenterInterp._data-dataCellCenterInterpBF._data).norm() < 1E-3f)

      MACVelData<DIM,T> dataMACVelInterp(testGridFocus),dataMACVelInterpBF=dataMACVelInterp;
      testGridFocus.sampleMACVel(dataMACVelInterp,testGrid,dataMACVel);
      testGridFocus.sampleMACVelBF(dataMACVelInterpBF,testGrid,dataMACVel);
      for(sizeType D=0; D<DIM; D++) {
        ASSERT((dataMACVelInterp._data[D]-dataMACVelInterpBF._data[D]).norm() < 1E-3f)
      }
    }
  }
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::debugFocusAllWarp()
{
  INFO("Debugging Focus all warp cases!")
  debugFocus(Vec3i::Zero());
  if(Encoder::isConstantEncoder)
    for(sizeType D=0; D<DIM; D++)
      debugFocus(Vec3i::Unit(D));
}
//Nodal operator
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::debugNodal(const Vec3i& warpMode)
{
  //test encoder
  //INFO("Using encoding sequence:");
  //cout << "\t";
  //for(sizeType d=0; d<30; d++)
  //  cout << Encoder().decodeDeltaDIM(d) << " ";
  //cout << endl;
  //create grid
  Grid testGrid=createTestGrid(warpMode);
  //set random value
  NodalData<T> data(testGrid);
  data._data.setRandom();
  //nr point
  VecDIMb ob;
  VecSTENCILi si;
  VecSTENCILd s,sv;
  T minV,maxV,val;
  VecDIMi id,id2,id3,id4,decoded;
  VecDIMd tmp,pt,pt2,pt3,pt4,pt4modify,x0,cellSz,invCellSz;
  //test indexing/interpolation
  sizeType nrP=testGrid._nrPointNodal.prod();
  for(sizeType p=0; p<nrP; p++) {
    //test basic indexing
    testGrid.decodeNodal(p,decoded);
    testGrid.getPtNodal(decoded,pt);
    testGrid.indexNodal(tmp=pt+VecDIMd::Constant(1E-5f),id);
    testGrid.getCellSzNodal(id,cellSz);
    testGrid.getInvCellSzNodal(id,invCellSz);
    testGrid.indexNodal(tmp=pt+cellSz*0.99f,id2);
    testGrid.getPtNodal(id,x0);
    ASSERT_MSG(decoded == id && decoded == id2,"Integer point decoding error!")
    ASSERT_MSG(pt == x0,"Float point consistency error!")
    for(sizeType d=0; d<DIM; d++)
      if(id[d]+1 < testGrid._nrPointNodal[d]) {
        pt3[d]=pt[d]+RandEngine::randR(0.25f,0.75f)/invCellSz[d];
        pt4[d]=pt[d]+RandEngine::randR(-0.75f,0.75f)/invCellSz[d];
        pt2=pt+VecDIMd::Unit(d)/invCellSz[d];
        testGrid.indexNodal(tmp=pt2+VecDIMd::Constant(1E-5f),id2);
        ASSERT_MSG(id2-id == VecDIMi::Unit(d),"CellSz consistency error!")
      }
    //test interpolation
    val=testGrid.sampleNodal(data,pt3,id3,minV,maxV,ob);
    testGrid.sampleStencilNodal(pt3,id3,s,si,ob);
    for(sizeType d=0; d<(1<<DIM); d++)
      sv[d]=data._data[si[d]];
    ASSERT_MSG(minV == sv.minCoeff() && maxV == sv.maxCoeff(),"Min/Max stencil error!")
    ASSERT_MSG(abs(sv.dot(s)-val) < 1E-5f,"Interpolation stencil error!")
    ASSERT_MSG((ob.array() == false).all(),"Out-of-boundary condition error!")
    //test interpolation that many be out-of-boundary
    val=testGrid.sampleNodal(data,pt4modify=pt4,id4,minV,maxV,ob);
    testGrid.sampleStencilNodal(pt4modify=pt4,id4,s,si,ob);
    for(sizeType d=0; d<DIM; d++)
      if(pt4modify[d] == pt4[d]) {
        ASSERT_MSG(!ob[d],"Out-of-boundary condition error!")
      } else if(warpMode[d] > 0) {
        T modifyDist=pt4modify[d]-pt4[d];
        T range=testGrid.getBBNodal().getExtent()[d];
        ASSERT(fabs(modifyDist-range) < 1E-5f || fabs(modifyDist) < 1E-5f)
      } else {
        ASSERT_MSG(ob[d],"Out-of-boundary condition error!")
      }
    ASSERT_MSG(s.minCoeff() >= 0,"Out-of-boundary interpolation weight error!")
  }
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::debugNodalAllWarp()
{
  INFO("Debugging Nodal all warp cases!")
  debugNodal(Vec3i::Zero());
  if(Encoder::isConstantEncoder)
    for(sizeType D=0; D<DIM; D++)
      debugNodal(Vec3i::Unit(D));
}
//CellCenter
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::debugCellCenter(const Vec3i& warpMode)
{
  //create grid
  Grid testGrid=createTestGrid(warpMode);
  //set random value
  CellCenterData<T> data(testGrid);
  data._data.setRandom();
  //nr point
  VecDIMb ob;
  VecSTENCILi si;
  VecSTENCILd s,sv;
  T minV,maxV,val;
  VecDIMi id,id2,id3,id4,decoded;
  VecDIMd tmp,pt,pt2,pt3,pt4,pt4modify,x0,cellSz,invCellSz;
  //test indexing/interpolation
  sizeType nrP=testGrid._nrPointCellCenter.prod();
  for(sizeType p=0; p<nrP; p++) {
    //test basic indexing
    testGrid.decodeCellCenter(p,decoded);
    testGrid.getPtCellCenter(decoded,pt);
    testGrid.indexCellCenter(tmp=pt+VecDIMd::Constant(1E-5f),id);
    testGrid.getCellSzCellCenter(id,cellSz);
    testGrid.getInvCellSzCellCenter(id,invCellSz);
    testGrid.indexCellCenter(tmp=pt+cellSz*0.99f,id2);
    testGrid.getPtCellCenter(id,x0);
    ASSERT_MSG(decoded == id && decoded == id2,"Integer point decoding error!")
    ASSERT_MSG(pt == x0,"Float point consistency error!")
    for(sizeType d=0; d<DIM; d++)
      if(id[d]+1 < testGrid._nrPointCellCenter[d]) {
        pt3[d]=pt[d]+RandEngine::randR(0.25f,0.75f)/invCellSz[d];
        pt4[d]=pt[d]+RandEngine::randR(-0.75f,0.75f)/invCellSz[d];
        pt2=pt+VecDIMd::Unit(d)/invCellSz[d];
        testGrid.indexCellCenter(tmp=pt2+VecDIMd::Constant(1E-5f),id2);
        ASSERT_MSG(id2-id == VecDIMi::Unit(d),"CellSz consistency error!")
      }
    //test interpolation
    val=testGrid.sampleCellCenter(data,pt3,id3,minV,maxV,ob);
    testGrid.sampleStencilCellCenter(pt3,id3,s,si,ob);
    for(sizeType d=0; d<(1<<DIM); d++)
      sv[d]=data._data[si[d]];
    ASSERT_MSG(minV == sv.minCoeff() && maxV == sv.maxCoeff(),"Min/Max stencil error!")
    ASSERT_MSG(abs(sv.dot(s)-val) < 1E-5f,"Interpolation stencil error!")
    ASSERT_MSG((ob.array() == false).all(),"Out-of-boundary condition error!")
    //test interpolation that many be out-of-boundary
    val=testGrid.sampleCellCenter(data,pt4modify=pt4,id4,minV,maxV,ob);
    testGrid.sampleStencilCellCenter(pt4modify=pt4,id4,s,si,ob);
    for(sizeType d=0; d<DIM; d++)
      if(pt4modify[d] == pt4[d]) {
        ASSERT_MSG(!ob[d],"Out-of-boundary condition error!")
      } else if(warpMode[d] > 0) {
        T modifyDist=pt4modify[d]-pt4[d];
        T range=testGrid.getBBCellCenter().getExtent()[d];
        if(fabs(modifyDist-range) > 1E-5f && fabs(modifyDist) > 1E-5f)
          testGrid.sampleStencilCellCenter(pt4modify=pt4,id4,s,si,ob);
        ASSERT(fabs(modifyDist-range) < 1E-5f || fabs(modifyDist) < 1E-5f)
      } else {
        ASSERT_MSG(ob[d],"Out-of-boundary condition error!")
      }
    ASSERT_MSG(s.minCoeff() >= 0,"Out-of-boundary interpolation weight error!")
  }
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::debugCellCenterAllWarp()
{
  INFO("Debugging CellCenter all warp cases!")
  debugCellCenter(Vec3i::Zero());
  if(Encoder::isConstantEncoder)
    for(sizeType D=0; D<DIM; D++)
      debugCellCenter(Vec3i::Unit(D));
}
//MACVel operator
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::debugMACVel(const Vec3i& warpMode)
{
  //create grid
  Grid testGrid=createTestGrid(warpMode);
  //set random value
  MACVelData<DIM,T> data(testGrid);
  data.setRandom();
  //nr point
  VecDIMb ob;
  VecSTENCILi si;
  VecSTENCILd s,sv;
  T minV,maxV,val;
  VecDIMi id,id2,id3,id4,decoded;
  VecDIMd tmp,pt,pt2,pt3,pt4,pt4modify,x0,cellSz,invCellSz;
  for(unsigned char DV=0; DV<DIM; DV++) {
    //test indexing/interpolation
    sizeType nrP=testGrid._nrPointMACVel[DV].prod();
    for(sizeType p=0; p<nrP; p++) {
      //test basic indexing
      testGrid.decodeMACVel(DV,p,decoded);
      testGrid.getPtMACVel(DV,decoded,pt);
      testGrid.indexMACVel(DV,tmp=pt+VecDIMd::Constant(1E-5f),id);
      testGrid.getCellSzMACVel(DV,id,cellSz);
      testGrid.getInvCellSzMACVel(DV,id,invCellSz);
      testGrid.indexMACVel(DV,tmp=pt+cellSz*0.99f,id2);
      testGrid.getPtMACVel(DV,id,x0);
      ASSERT_MSG(decoded == id && decoded == id2,"Integer point decoding error!")
      ASSERT_MSG(pt == x0,"Float point consistency error!")
      for(sizeType d=0; d<DIM; d++)
        if(id[d]+1 < testGrid._nrPointMACVel[DV][d]) {
          pt3[d]=pt[d]+RandEngine::randR(0.25f,0.75f)/invCellSz[d];
          pt4[d]=pt[d]+RandEngine::randR(-0.75f,0.75f)/invCellSz[d];
          pt2=pt+VecDIMd::Unit(d)/invCellSz[d];
          testGrid.indexMACVel(DV,tmp=pt2+VecDIMd::Constant(1E-5f),id2);
          ASSERT_MSG(id2-id == VecDIMi::Unit(d),"CellSz consistency error!")
        }
      //test interpolation
      val=testGrid.sampleMACVel(DV,data,pt3,id3,minV,maxV,ob);
      testGrid.sampleStencilMACVel(DV,pt3,id3,s,si,ob);
      for(sizeType d=0; d<(1<<DIM); d++)
        sv[d]=data._data[DV][si[d]];
      ASSERT_MSG(minV == sv.minCoeff() && maxV == sv.maxCoeff(),"Min/Max stencil error!")
      ASSERT_MSG(abs(sv.dot(s)-val) < 1E-5f,"Interpolation stencil error!")
      ASSERT_MSG((ob.array() == false).all(),"Out-of-boundary condition error!")
      //test interpolation that many be out-of-boundary
      val=testGrid.sampleMACVel(DV,data,pt4modify=pt4,id4,minV,maxV,ob);
      testGrid.sampleStencilMACVel(DV,pt4modify=pt4,id4,s,si,ob);
      for(sizeType d=0; d<DIM; d++)
        if(pt4modify[d] == pt4[d]) {
          ASSERT_MSG(!ob[d],"Out-of-boundary condition error!")
        } else if(warpMode[d] > 0) {
          T modifyDist=pt4modify[d]-pt4[d];
          T range=testGrid.getBBMACVel(DV).getExtent()[d];
          ASSERT(fabs(modifyDist-range) < 1E-5f || fabs(modifyDist) < 1E-5f)
        } else {
          ASSERT_MSG(ob[d],"Out-of-boundary condition error!")
        }
      ASSERT_MSG(s.minCoeff() >= 0,"Out-of-boundary interpolation weight error!")
    }
  }
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::debugMACVelAllWarp()
{
  INFO("Debugging MACVel all warp cases!")
  debugMACVel(Vec3i::Zero());
  if(Encoder::isConstantEncoder)
    for(sizeType D=0; D<DIM; D++)
      debugMACVel(Vec3i::Unit(D));
}
//debug
template <typename T,int DIM,typename Encoder>
typename VariableSizedGrid<T,DIM,Encoder>::Grid VariableSizedGrid<T,DIM,Encoder>::createTestGrid(const Vec3i& warpMode)
{
  typedef VariableSizedGrid<T,DIM,Encoder> GridType;
  typedef typename GridType::VecDIMi VecDIMi;
  BBox<T,DIM> bbInterior;
  VecDIMi nrCellInterior;
  BBox<sizeType,DIM> extend;
  for(sizeType d=0,d1=1; d<DIM; d++,d1++) {
    bbInterior._minC[d]=1;
    bbInterior._maxC[d]=1+1/(T)d1;
    nrCellInterior[d]=RandEngine::randI(1,10)*Encoder().decodeDeltaDIM(1);
    extend._minC[d]=d1*RandEngine::randI(0,10);
    extend._maxC[d]=d1*RandEngine::randI(0,10);
  }
  GridType testGrid(bbInterior,nrCellInterior,extend,&warpMode);
  return testGrid;
}

