#ifndef RIGID_BODY_MASS_H
#define RIGID_BODY_MASS_H

#include <CommonFile/ObjMesh.h>

PRJ_BEGIN

template <typename T>
static typename ScalarUtil<T>::ScalarMat3 cross(const typename ScalarUtil<T>::ScalarVec3& v)
{
  typename ScalarUtil<T>::ScalarMat3 ret;
  ret.setZero();
  ret(0,1)=-v[2];
  ret(0,2)=v[1];
  ret(1,2)=-v[0];
  ret(1,0)=v[2];
  ret(2,0)=-v[1];
  ret(2,1)=v[0];
  return ret;
}
template <typename T>
static typename ScalarUtil<T>::ScalarVec3 invCross(const typename ScalarUtil<T>::ScalarMat3& wCross)
{
  return typename ScalarUtil<T>::ScalarVec3(wCross(2,1),wCross(0,2),wCross(1,0));
}
struct RigidBodyMass {
  RigidBodyMass(const ObjMesh& mesh);
  Vec3 getCtr() const;
  Mat6 getMass() const;
  Mat6 getMassCOM() const;
  scalar getM() const;
  Vec3 getMC() const;
  Mat3 getMCCT() const;
private:
  Mat6 _mat,_matCOM;
  Vec3 _ctr;
  Mat3 _MCCT;
};

PRJ_END

#endif
