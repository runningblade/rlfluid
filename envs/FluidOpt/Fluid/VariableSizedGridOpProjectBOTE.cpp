#include "VariableSizedGridOpProject.h"
#include "VariableSizedGridOp.h"
#include "RigidBodyMass.h"
#include "RigidMatrix.h"
#include "RigidSolver.h"
#include "Utils.h"
#include <CommonFile/ComputeWeightInterface.h>
#include <CommonFile/solvers/PMinresQLP.h>
#include <CommonFile/solvers/QPSolver.h>
#include <boost/lexical_cast.hpp>

USE_PRJ_NAMESPACE

//projectc using back-of-the-envelop computation
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOpProject<T,DIM,Encoder>::buildForceMatrixBOTE(const Cold& diag,const GridType& grd,const CellCenterData<T>& phi,const MACVelData<DIM,T>& weight,const MACVelData<DIM,sizeType>& rigidId,const RigidBody& body,const MACVelData<DIM,T>& vel,T dt,sizeType i,GeneralizedMJ& mj,T density,Cold* lowerBound,bool volWeight)
{
  BBox<scalar> bbb;
  BBox<T,DIM> bb;
  Vec6d I;
  Vec3 rel;
  VecDIMd cellSz;
  //lump with fluid
  T mFluid,w,theta,area,LF,RF;
  sizeType LID=-1,RID=-1;
  //compute bb
  body.getWorldBB(bbb);
  bb=assignVariableTo<T,DIM,scalar,3>(bbb);
  //init RHS
  mj._RHS.segment<3>(0)=body.p().cast<scalarD>();
  mj._RHS.segment<3>(3)=body.L().cast<scalarD>();
  //init mass
  mj.clear();
  mj._invMDt.setZero();
  mj._invMDt.block<3,3>(0,0)=body.getM()*Mat3d::Identity();
  mj._invMDt.block<3,3>(3,3)=body.I().cast<scalarD>();
  //lump
  for(sizeType D=0; D<DIM; D++) {
    MACVelPointIt beg(D,grd,0,bb,VecDIMi::Constant(3),VecDIMi::Constant(4));
    MACVelPointIt end(D,grd,-1,bb,VecDIMi::Constant(3),VecDIMi::Constant(4));
    while(beg!=end) {
      w=weight._data[D][beg.off()];
      if(rigidId._data[D][beg.off()] == i && checkFaceValid(beg,w,phi,LID,RID,LF,RF,theta)) {
        mFluid=grd.getCellMACVel(D,beg.id()).getExtent().prod()*density*w;
        rel=assignVariableTo<Vec3,VecDIMd>(beg.pointWithWarp())-body.pos();
        grd.getCellSzCtrMACVel(D,beg.id(),cellSz);
        //update mass matrix
        I.setZero();
        I[D]=1;
        I.segment<3>(3)=cross<scalar>(rel).col(D).cast<scalarD>();
        if(DIM == 2)
          I[3]=I[4]=0;
        //update pressure operator
        area=grd.getFaceAreaMACVel(D,beg.id());
        theta=dt*area/theta;
        if(volWeight)
          theta*=w;
        bool valid=false;
        if(RF < 0 && diag[RID] > 0) {
          if(lowerBound)
            (*lowerBound)[RID]=0;
          mj.insert(RID,I*theta);
          valid=true;
        }
        if(LF < 0 && diag[LID] > 0) {
          if(lowerBound)
            (*lowerBound)[LID]=0;
          mj.insert(LID,-I*theta);
          valid=true;
        }
        if(valid) {
          mj._invMDt+=I*I.transpose()*mFluid;
          mj._RHS+=I*vel._data[D][beg.off()]*mFluid;
        }
      }
      ++beg;
    }
  }
  //compact terms
  //NOTE: the equations are scaled by -1/dt
  mj._RHS*=-1/dt;
  mj._invMDt*=-1/dt;
  for(sizeType i=0; i<(sizeType)mj._colData.size(); i++)
    mj._colData[i]*=-1/dt;
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOpProject<T,DIM,Encoder>::buildRigidBodyMatrixBOTE(const Cold& diag,vector<GeneralizedMJ>& MJs,vector<sizeType>& MJIDs,Cold& RHS,const GridType& grd,const CellCenterData<T>& phi,const MACVelData<DIM,T>& weight,const MACVelData<DIM,sizeType>& rigidId,const RigidSolver& rigid,const MACVelData<DIM,T>& vel,T dt,T density,bool changeRHS,Cold* lowerBound,bool volWeight)
{
  Vec6d V;
  MJs.clear();
  MJIDs.clear();
  //assemble all the rigid body velocity components
  const sizeType nrBody=(sizeType)rigid.nrBody();
  for(sizeType i=0; i<nrBody; i++) {
    const RigidBody body=rigid.getBody(i);
    if(!body.valid())
      continue;

    MJs.push_back(GeneralizedMJ());
    MJIDs.push_back(i);
    buildForceMatrixBOTE(diag,grd,phi,weight,rigidId,body,vel,dt,i,MJs.back(),density,lowerBound,volWeight);
    if(MJs.back()._col.size() == 0) {
      MJs.pop_back();
      MJIDs.pop_back();
      continue;
    }

    //contribute right hand side
    V.segment<3>(0)=body.linSpd().cast<scalarD>();
    V.segment<3>(3)=body.rotSpd().cast<scalarD>();
    if(body.freeze()) {
      MJs.back().multiplyJTSubtract(V,RHS);
      MJs.pop_back();
      MJIDs.pop_back();
    } else if(changeRHS) {
      GeneralizedMJ& mj=MJs.back();
      mj._invMDt=-mj._invMDt.inverse();
      mj.multiplyJTAdd(mj._invMDt*mj._RHS,RHS);
    } else {
      GeneralizedMJ& mj=MJs.back();
      RHS=concat(RHS,mj._RHS);
    }
  }
}
template <typename T,int DIM,typename Encoder>
Cold VariableSizedGridOpProject<T,DIM,Encoder>::buildMatrixBOTE(const GridType& grd,const CellCenterData<T>& phi,const MACVelData<DIM,T>& weight,const MACVelData<DIM,sizeType>& rigidId,MACVelData<DIM,T>& vel,SMat& LHS,Cold& RHS,T dt,T density,Cold* lowerBound)
{
#define CTR_NEIGH DIM
#define RIGHT_NEIGH NR_NEIGH-D
#define LEFT_NEIGH D
#define NR_NEIGH (DIM<<1)
  //fill
  sizeType nrP=grd._nrPointCellCenter.prod();
  SparseSTENCILd LHSd(NR_NEIGH+1,nrP);
  SparseSTENCILi LHSi(NR_NEIGH+1,nrP);
  LHSd.setZero();
  RHS.setZero(nrP);
  VecDIMd cellSz;
  T theta=0,w=0,area,LF,RF;
  sizeType LID=-1,RID=-1;
  for(sizeType D=0; D<DIM; D++) {
    MACVelPointIt beg(D,grd,0);
    MACVelPointIt end(D,grd,-1);
    while(beg!=end) {
      w=weight._data[D][beg.off()];
      if(rigidId._data[D][beg.off()] == -1 && w == 1 && checkFaceValid(beg,w,phi,LID,RID,LF,RF,theta)) {
        area=grd.getFaceAreaMACVel(D,beg.id());
        grd.getCellSzCtrMACVel(D,beg.id(),cellSz);
        theta=area*dt/(theta*cellSz[D]*density);
        if(LF < 0) {
          LHSd(CTR_NEIGH,LID)+=theta;
          LHSi(CTR_NEIGH,LID)=LID;
          RHS[LID]-=vel._data[D][beg.off()]*area;
        }
        if(RF < 0) {
          LHSd(CTR_NEIGH,RID)+=theta;
          LHSi(CTR_NEIGH,RID)=RID;
          RHS[RID]+=vel._data[D][beg.off()]*area;
        }
        if(LF < 0 && RF < 0) {
          LHSd(RIGHT_NEIGH,LID)-=theta;
          LHSi(RIGHT_NEIGH,LID)=RID;
          LHSd(LEFT_NEIGH,RID)-=theta;
          LHSi(LEFT_NEIGH,RID)=LID;
        }
      } else {
        if(lowerBound) {
          if(LID >= 0)
            (*lowerBound)[LID]=0;
          if(RID >= 0)
            (*lowerBound)[RID]=0;
        }
        vel._data[D][beg.off()]=0;
      }
      ++beg;
    }
  }
#undef CTR_NEIGH
#undef RIGHT_NEIGH
#undef LEFT_NEIGH
#undef NR_NEIGH
  assembleMatrix(grd,LHS,LHSd,LHSi);
  return LHSd.row(DIM);
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOpProject<T,DIM,Encoder>::projectOutBOTE(const GridType& grd,const CellCenterData<scalarD>& pressure,const CellCenterData<T>& phi,const MACVelData<DIM,T>& weight,const MACVelData<DIM,sizeType>& rigidId,RigidSolver& rigid,const vector<GeneralizedMJ>& MJs,const vector<sizeType>& MJIDs,MACVelData<DIM,T>& vel,MACVelData<DIM,unsigned char>& valid,T dt,T density,bool changeRHS)
{
  Vec6d VW;
  for(sizeType i=0; i<(sizeType)MJIDs.size(); i++) {
    const GeneralizedMJ& mj=MJs[i];
    RigidBody& body=rigid.getBody(MJIDs[i]);
    if(changeRHS) {
      mj.multiplyJ(pressure._data,VW);
      VW=mj._invMDt*(VW-mj._RHS);
    } else VW=Eigen::Map<const Vec6d>(pressure._data.data()+grd._nrPointCellCenter.prod()+i*6);
    body.setP(body.getM()*VW.segment<3>(0).cast<scalar>());
    body.setL(body.I()*VW.segment<3>(3).cast<scalar>());
  }
  MACVelData<DIM,T> velTmp(vel);
  T theta=0,w=0,LF,RF,PL,PR;
  valid.setZero();
  vel.setZero();
  VecDIMd rel,cellSz;
  sizeType LID=-1,RID=-1,rID;
  for(sizeType D=0; D<DIM; D++) {
    MACVelPointIt beg(D,grd,0);
    MACVelPointIt end(D,grd,-1);
    while(beg!=end) {
      w=weight._data[D][beg.off()];
      if(checkFaceValid(beg,w,phi,LID,RID,LF,RF,theta)) {
        rID=rigidId._data[D][beg.off()];
        if(rID >= 0) {
          const RigidBody& body=rigid.getBody(rID);
          rel=grd.handleWarpDiff(*beg-assignVariableTo<VecDIMd,Vec3>(body.pos()));
          vel._data[D][beg.off()]=body.vel(assignVariableTo<Vec3,VecDIMd>(rel))[D];
        } else if(w == 1) {
          PL=LF < 0 ? pressure._data[LID] : 0;
          PR=RF < 0 ? pressure._data[RID] : 0;
          grd.getCellSzCtrMACVel(D,beg.id(),cellSz);
          vel._data[D][beg.off()]=velTmp._data[D][beg.off()];
          theta*=cellSz[D]*density;
          vel._data[D][beg.off()]-=(T)(PR-PL)*dt/theta;
        }
        valid._data[D][beg.off()]=1;
      }
      ++beg;
    }
  }
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOpProject<T,DIM,Encoder>::rigidProjectBOTE(const GridType& grd,CellCenterData<scalarD>& pressure,const CellCenterData<T>& phi,const MACVelData<DIM,T>& weight,const MACVelData<DIM,sizeType>& rigidId,RigidSolver& rigid,MACVelData<DIM,T>& vel,MACVelData<DIM,unsigned char>& valid,T dt,T density)
{
  Cold RHS;
  PCGSolver<scalarD> solver;
  FixedSparseMatrix<scalarD,Kernel<scalarD> > LHS,LHSWithRigid;
  Cold diag=buildMatrixBOTE(grd,phi,weight,rigidId,vel,LHS,RHS,dt,density);

  vector<GeneralizedMJ> MJs;
  vector<sizeType> MJIDs;
  buildRigidBodyMatrixBOTE(diag,MJs,MJIDs,RHS,grd,phi,weight,rigidId,rigid,vel,dt,density,true,NULL);

  boost::shared_ptr<RigidBodyKrylovMatrix<GeneralizedMJ> > krylovMatrix;
  krylovMatrix.reset(new RigidBodyKrylovMatrix<GeneralizedMJ>(LHS,MJs,MJIDs));
#ifdef PRECOMPUTE_RIGID_MATRIX
  krylovMatrix->toFixed(LHSWithRigid);
  solver.setMatrix(LHSWithRigid,true);
#else
  solver.getPre()->setMatrix(LHS,false);
  solver.setKrylovMatrix(krylovMatrix);
#endif
  solver.setSolverParameters(SOLVER_EPS,1E6);
  //solver.setCallback(boost::shared_ptr<Callback<scalarD,Kernel<scalarD> > >(new Callback<scalarD,Kernel<scalarD> >()));
  ASSERT(solver.solve(RHS,pressure._data) == PCGSolver<scalarD>::SUCCESSFUL);
  projectOutBOTE(grd,pressure,phi,weight,rigidId,rigid,MJs,MJIDs,vel,valid,dt,density,true);
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOpProject<T,DIM,Encoder>::rigidProjectBOTEMINRES(const GridType& grd,CellCenterData<scalarD>& pressure,const CellCenterData<T>& phi,const MACVelData<DIM,T>& weight,const MACVelData<DIM,sizeType>& rigidId,RigidSolver& rigid,MACVelData<DIM,T>& vel,MACVelData<DIM,unsigned char>& valid,T dt,T density)
{
  Cold RHS;
  //PMINRESSolverQLP<scalarD,Kernel<scalarD> > solver;
  PMINRESSolverQLP<scalarD,Kernel<scalarD>,RigidFluidPrecondition> solver;
  FixedSparseMatrix<scalarD,Kernel<scalarD> > LHS;
  Cold diag=buildMatrixBOTE(grd,phi,weight,rigidId,vel,LHS,RHS,dt,density);

  vector<GeneralizedMJ> MJs;
  vector<sizeType> MJIDs;
  buildRigidBodyMatrixBOTE(diag,MJs,MJIDs,RHS,grd,phi,weight,rigidId,rigid,vel,dt,density,false,NULL);
  sizeType sSize=LHS.rows();

  boost::shared_ptr<RigidBodyKrylovMatrixSparse<GeneralizedMJ> > krylovMatrix;
  krylovMatrix.reset(new RigidBodyKrylovMatrixSparse<GeneralizedMJ>(LHS,MJs,MJIDs));
  if(solver.getPre()) {
    RigidFluidPrecondition* pre=dynamic_cast<RigidFluidPrecondition*>(solver.getPre());
    if(pre) {
      pre->reset(MJs,sSize);
      pre->setMatrix(LHS,false);
    }
  }
  pressure._data.resize(RHS.size());
  solver.setKrylovMatrix(krylovMatrix);
  solver.setSolverParameters(SOLVER_EPS,1E6);
  //solver.setCallback(boost::shared_ptr<Callback<scalarD,Kernel<scalarD> > >(new Callback<scalarD,Kernel<scalarD> >()));
  ASSERT(solver.solve(RHS,pressure._data) == PMINRESSolverQLP<scalarD>::SUCCESSFUL);
  projectOutBOTE(grd,pressure,phi,weight,rigidId,rigid,MJs,MJIDs,vel,valid,dt,density,false);
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOpProject<T,DIM,Encoder>::rigidProjectBOTEUnilateral(const GridType& grd,CellCenterData<scalarD>& pressure,const CellCenterData<T>& phi,const MACVelData<DIM,T>& weight,const MACVelData<DIM,sizeType>& rigidId,RigidSolver& rigid,MACVelData<DIM,T>& vel,MACVelData<DIM,unsigned char>& valid,T dt,T density)
{
  typedef MPRGPQPSolver<scalarD,Kernel<scalarD> > QPSolver;
  Cold RHS,L,H;
  QPSolver solver;
  FixedSparseMatrix<scalarD,Kernel<scalarD> > LHS,LHSWithRigid;
  L.setConstant(grd._nrPointCellCenter.prod(),-ScalarUtil<scalarD>::scalar_max);
  H.setConstant(grd._nrPointCellCenter.prod(), ScalarUtil<scalarD>::scalar_max);
  Cold diag=buildMatrixBOTE(grd,phi,weight,rigidId,vel,LHS,RHS,dt,density,&L);

  vector<GeneralizedMJ> MJs;
  vector<sizeType> MJIDs;
  buildRigidBodyMatrixBOTE(diag,MJs,MJIDs,RHS,grd,phi,weight,rigidId,rigid,vel,dt,density,true,&L);

  RigidBodyKrylovMatrix<GeneralizedMJ> krylovMatrix(LHS,MJs,MJIDs);
  krylovMatrix.toFixed(LHSWithRigid);
  solver.reset(L,H,true);
  solver.setMatrix(LHSWithRigid,true);
  solver.setSolverParameters(SOLVER_EPS,1E6);
  //solver.setCallback(boost::shared_ptr<Callback<scalarD,Kernel<scalarD> > >(new Callback<scalarD,Kernel<scalarD> >()));
  ASSERT(solver.solve(RHS,pressure._data) == QPSolver::SUCCESSFUL);
  projectOutBOTE(grd,pressure,phi,weight,rigidId,rigid,MJs,MJIDs,vel,valid,dt,density,true);
}
//project BOTE
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOpProject<T,DIM,Encoder>::debugRigidProjectBOTELump(const string& path,const Vec3i& warpMode)
{
  recreate(path);
  //create grid
  BBox<scalar> bbb;
  BBox<T,DIM> bb;
  VecDIMi nrCell;
  BBox<sizeType,DIM> extend;
  for(sizeType D=0; D<DIM; D++) {
    bb._minC[D]=RandEngine::randR(0,0.05f);
    bb._maxC[D]=1+bb._minC[D];
    nrCell[D]=RandEngine::randI(32,64)*4;
    extend._minC[D]=RandEngine::randI(0,10);
    extend._maxC[D]=RandEngine::randI(0,10);
  }
  VariableSizedGrid<T,DIM,Encoder> grd;
  grd.reset(bb,nrCell,extend,&warpMode);
  //create body
  RigidSolver sol(DIM,0.01);
  sol.addBox(assignVariableTo<Vec3,VecDIMd>(VecDIMd::Constant(0.1f)),
             assignVariableTo<Vec3,VecDIMd>(bb._minC+VecDIMd::Constant(0.01f)),
             DIM == 2 ? Vec3(Vec3::Unit(2)*RandEngine::randR01()) : Vec3(Vec3::Random()));
  sol.addBox(assignVariableTo<Vec3,VecDIMd>(VecDIMd::Constant(0.1f)),
             assignVariableTo<Vec3,VecDIMd>(bb._minC+VecDIMd::Constant(0.01f+0.05f)),
             DIM == 2 ? Vec3(Vec3::Unit(2)*RandEngine::randR01()) : Vec3(Vec3::Random()));
  sol.getBody(sol.nrBody()-1).rho()=-1;
  sol.addBox(assignVariableTo<Vec3,VecDIMd>(VecDIMd::Constant(0.1f)),
             assignVariableTo<Vec3,VecDIMd>(bb._minC+VecDIMd::Constant(0.01f+0.1f)),
             DIM == 2 ? Vec3(Vec3::Unit(2)*RandEngine::randR01()) : Vec3(Vec3::Random()));
  sol.addBox(assignVariableTo<Vec3,VecDIMd>(VecDIMd::Constant(0.1f)),
             assignVariableTo<Vec3,VecDIMd>(bb._minC+VecDIMd::Constant(0.01f+0.15f)),
             DIM == 2 ? Vec3(Vec3::Unit(2)*RandEngine::randR01()) : Vec3(Vec3::Random()));
  sol.getBody(sol.nrBody()-1).rho()=-1;
  //create valid/weight/rigidId
  MACVelData<DIM,sizeType> rigidId(grd);
  MACVelData<DIM,T> weightWithRigid(grd);
  rigidId.clamp(-1,-1);
  weightWithRigid.clamp(1,1);
  for(sizeType i=0; i<sol.nrBody(); i++) {
    RigidBody& body=sol.getBody(i);
    computeRigidBodyWeightMACVel(grd,NULL,&weightWithRigid,&rigidId,body,i);
    body.setP(body.getM()*assignVariableTo<Vec3,VecDIMd>(VecDIMd::Random()));
    body.setL(body.I()*(DIM == 2 ? Vec3(Vec3::Unit(2)*RandEngine::randR01()) : Vec3(Vec3::Random())));
  }
  //lump
  Cold RHS,diag;
  T RDensity=500.0f,dt=0.01f;
  MACVelData<DIM,T> vel(grd);
  CellCenterData<T> phi(grd);
  CellCenterData<scalarD> pre(grd);
  vector<GeneralizedMJ> lumpedRigids;
  vector<sizeType> MJIDs;
  phi._data.setRandom();
  pre._data.setRandom();
  pre._data*=100;
  diag.setOnes(grd._nrPointCellCenter.prod());
  buildRigidBodyMatrixBOTE(diag,lumpedRigids,MJIDs,RHS,grd,phi,weightWithRigid,rigidId,sol,vel,dt,RDensity,false,NULL,true);
  //write everything
  sol.writeMeshVTK(path+"/solid.vtk");
  for(sizeType D=0; D<DIM; D++) {
    VariableSizedGridOp<T,DIM,Encoder>::writeGridMACVelCompVTK(D,path+"/weight"+boost::lexical_cast<string>(D)+".vtk",grd,weightWithRigid);
    VariableSizedGridOp<T,DIM,Encoder>::writeGridMACVelCompVTK(D,path+"/rigidId"+boost::lexical_cast<string>(D)+".vtk",grd,rigidId);
  }
  //compute rigid's kinetic energy
  for(sizeType off=0; off<(sizeType)MJIDs.size(); off++) {
    RigidBody& body=sol.getBody(MJIDs[off]);
    sizeType i=MJIDs[off];
    body.getWorldBB(bbb);
    bb=assignVariableTo<T,DIM,scalar,3>(bbb);
    //compute using lumped info
    Vec6d VW=concatRow(body.linSpd(),body.rotSpd()).cast<scalarD>();
    GeneralizedMJ& LR=lumpedRigids[off];
    LR._invMDt*=-dt;
    for(sizeType i=0; i<(sizeType)LR._colData.size(); i++)
      LR._colData[i]*=-dt;
    scalar KERef=VW.dot(LR._invMDt*VW)/2;
    Vec3d PRef=(LR._invMDt*VW).segment<3>(0);
    Vec3d LRef=(LR._invMDt*VW).segment<3>(3);
    //compute using brute force
    scalar KE=body.linSpd().dot(body.p())/2;
    KE+=body.rotSpd().dot(body.L())/2;
    scalar KE2=KE;
    Vec3d P=body.p().cast<scalarD>(),P2=P;
    Vec3d L=body.L().cast<scalarD>(),L2=L;
    T mFluid,theta,LF,RF;
    sizeType LID=-1,RID=-1;
    for(sizeType D=0; D<DIM; D++) {
      MACVelPointIt beg(D,grd,0,bb,VecDIMi::Constant(3),VecDIMi::Constant(4));
      MACVelPointIt end(D,grd,-1,bb,VecDIMi::Constant(3),VecDIMi::Constant(4));
      while(beg!=end) {
        mFluid=weightWithRigid._data[D][beg.off()];
        if(rigidId._data[D][beg.off()] == i && checkFaceValid(beg,mFluid,phi,LID,RID,LF,RF,theta)) {
          Vec3 rel=assignVariableTo<Vec3,VecDIMd>(beg.pointWithWarp())-body.pos();
          scalar VComp=body.vel(assignVariableTo<Vec3,VecDIMd>(beg.pointWithWarp()))[D];
          mFluid=grd.getCellMACVel(D,beg.id()).getExtent().prod()*RDensity*mFluid;
          KE+=VComp*VComp*mFluid/2;
          P[D]+=VComp*mFluid;
          L+=rel.cross(Vec3::Unit(D)*VComp).cast<scalarD>()*mFluid;
          //modify using pressure
          VecDIMd cellSz;
          grd.getCellSzCtrMACVel(D,beg.id(),cellSz);
          T PR=RF < 0 ? pre._data[RID] : 0;
          T PL=LF < 0 ? pre._data[LID] : 0;
          scalar VComp2=VComp-dt*(PR-PL)/(theta*cellSz[D]*RDensity);
          P2[D]+=VComp2*mFluid;
          L2+=rel.cross(Vec3::Unit(D)*VComp2).cast<scalarD>()*mFluid;
        }
        ++beg;
      }
    }
    //compute change using LR
    Vec6d prePL;
    LR.multiplyJ(pre._data,prePL);
    prePL*=-1;
    //compare
    INFOV("KE: %f KERef: %f, Err: %f, FluidKE: %f!",KE,KERef,KE-KERef,KE-KE2)
    INFOV("P: %f PRef: %f, Err: %f!",P.norm(),PRef.norm(),(P-PRef).norm())
    INFOV("L: %f LRef: %f, Err: %f!",L.norm(),LRef.norm(),(L-LRef).norm())
    //NOTE: this is only true when volWeight=true, which is not physically correct
    INFOV("P2Diff: %f, Err: %f!",(P2-P).norm(),(P2-P-prePL.segment<3>(0)).norm())
    INFOV("L2Diff: %f, Err: %f!",(L2-L).norm(),(L2-L-prePL.segment<3>(3)).norm())
  }
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGridOpProject<T,DIM,Encoder>::debugRigidProjectBOTELumpAllWarp()
{
  INFO("Debugging BOTELump all warp cases!")
  debugRigidProjectBOTELump("weight"+GridType::printWarp(Vec3i::Zero()),Vec3i::Zero());
  if(Encoder::isConstantEncoder)
    for(sizeType D=0; D<DIM; D++)
      debugRigidProjectBOTELump("weight"+GridType::printWarp(Vec3i::Unit(D)),Vec3i::Unit(D));
}
