#include "RigidContact.h"
#include <CommonFile/RotationUtil.h>

PRJ_BEGIN

//bounding box
void getContactBall(Contact& c,const SphereGeomCell& cell,const BBox<scalar>& bb)
{
  c._depth=0;
  scalar depth;
  scalar r=cell.getRad();
  Vec3 ctr=cell.getT().block<3,1>(0,3);
  unsigned char dim=cell.dim();
  for(sizeType d=0; d<dim; d++) {
    depth=bb._minC[d]+r-ctr[d];
    if(depth > c._depth) {
      c._depth=depth;
      c._pI=ctr-Vec3::Unit(d)*r;
      c._pJ=ctr;
      c._pJ[d]=bb._minC[d];
      c._nIJ=Vec3::Unit(d);
      c._hasContact=true;
    }
    depth=ctr[d]+r-bb._maxC[d];
    if(depth > c._depth) {
      c._depth=depth;
      c._pI=ctr+Vec3::Unit(d)*r;
      c._pJ=ctr;
      c._pJ[d]=bb._maxC[d];
      c._nIJ=-Vec3::Unit(d);
      c._hasContact=true;
    }
  }
}
void getContactBox(Contact& c,const BoxGeomCell& cell,const BBox<scalar>& bb)
{
  c._depth=0;
  scalar depth;
  Vec3 ext=cell.getExt();
  unsigned char dim=cell.dim(),nrP=1<<dim;
  Vec3 pos=Vec3::Zero();
  for(sizeType p=0; p<nrP; p++) {
    for(sizeType d=0; d<dim; d++)
      pos[d]=(p&(1<<d)) ? -ext[d] : ext[d];
    pos=cell.getT().block<3,3>(0,0)*pos+cell.getT().block<3,1>(0,3);
    for(sizeType d=0; d<dim; d++) {
      depth=bb._minC[d]-pos[d];
      if(depth > c._depth) {
        c._depth=depth;
        c._pI=pos;
        c._pJ=pos;
        c._pJ[d]=bb._minC[d];
        c._nIJ=Vec3::Unit(d);
        c._hasContact=true;
      }
      depth=pos[d]-bb._maxC[d];
      if(depth > c._depth) {
        c._depth=depth;
        c._pI=pos;
        c._pJ=pos;
        c._pJ[d]=bb._maxC[d];
        c._nIJ=-Vec3::Unit(d);
        c._hasContact=true;
      }
    }
  }
}
void getContact(Contact& c,StaticGeomCell& cell,const BBox<scalar>& bb)
{
  Mat4 tmpT;
  Contact c2;
  c._hasContact=false;
  //ball
  try {
    getContactBall(c,dynamic_cast<SphereGeomCell&>(cell),bb);
    return;
  } catch(...) {}
  //box
  try {
    getContactBox(c,dynamic_cast<BoxGeomCell&>(cell),bb);
    return;
  } catch(...) {}
  //composite
  try {
    c._depth=0;
    CompositeGeomCell& comp=dynamic_cast<CompositeGeomCell&>(cell);
    for(sizeType i=0; i<comp.nrChildren(); i++) {
      StaticGeomCell& ci=*(comp.getChild(i));
      tmpT=ci.getT();
      ci.setT(cell.getT()*ci.getT());
      getContact(c2,ci,bb);
      ci.setT(tmpT);
      if(c2._depth > c._depth)
        c=c2;
    }
    return;
  } catch(...) {}
}
void getContact(Contact& c,RigidBody& body,const BBox<scalar>& bb)
{
  StaticGeomCell& cell=body.getShape().getCell();
  Mat4 T=Mat4::Identity();
  T.block<3,3>(0,0)=body.rot();
  T.block<3,1>(0,3)=body.pos();
  cell.setT(T);
  getContact(c,cell,bb);
}
//interbody
void getContactBall(Contact& c,const SphereGeomCell& bodyI,const SphereGeomCell& bodyJ)
{
  scalar rI=bodyI.getRad();
  scalar rJ=bodyJ.getRad();
  Vec3 ctrI=bodyI.getT().block<3,1>(0,3);
  Vec3 ctrJ=bodyJ.getT().block<3,1>(0,3);
  Vec3 ij=ctrI-ctrJ;
  scalar dist=ij.norm();
  ij/=max<scalar>(dist,EPS);
  if(dist < rI+rJ) {
    c._depth=rI+rJ-dist;
    c._pI=ctrI-ij*rI;
    c._pJ=ctrJ+ij*rJ;
    c._nIJ=ij;
    c._hasContact=true;
  }
}
void getContactBoxVert(Contact& c,const BoxGeomCell& bodyI,const BoxGeomCell& bodyJ)
{
  c._depth=0;
  Contact c2;
  scalar depth;
  Vec3 extI=bodyI.getExt();
  BBox<scalar> bbJ=bodyJ.getBB(true);
  unsigned char dim=bodyI.dim(),nrP=1<<dim;
  Vec3 posI=Vec3::Zero(),posIJ;
  for(sizeType p=0; p<nrP; p++) {
    for(sizeType d=0; d<dim; d++)
      posI[d]=(p&(1<<d)) ? -extI[d] : extI[d];
    posI=bodyI.getT().block<3,3>(0,0)*posI+bodyI.getT().block<3,1>(0,3);
    posIJ=bodyJ.getT().block<3,3>(0,0).transpose()*(posI-bodyJ.getT().block<3,1>(0,3));
    if(bbJ.contain(posIJ,(sizeType)dim)) {
      c2._hasContact=true;
      c2._depth=ScalarUtil<scalar>::scalar_max;
      for(sizeType d=0; d<dim; d++) {
        depth=posIJ[d]-bbJ._minC[d];
        if(depth < c2._depth) {
          c2._pI=posI;
          c2._nIJ=bodyJ.getT().block<3,3>(0,0).col(d);
          c2._pJ=posI-c2._nIJ*depth;
          c2._depth=depth;
        }
        depth=bbJ._maxC[d]-posIJ[d];
        if(depth < c2._depth) {
          c2._pI=posI;
          c2._nIJ=-bodyJ.getT().block<3,3>(0,0).col(d);
          c2._pJ=posI-c2._nIJ*depth;
          c2._depth=depth;
        }
      }
      if(c2._depth > c._depth)
        c=c2;
    }
  }
}
void getContactBoxEdge(Contact& c,const BoxGeomCell& bodyI,const BoxGeomCell& bodyJ)
{
  c._depth=0;
  Contact c2;
  scalar depth,s,t;
  Vec3 extI=bodyI.getExt();
  BBox<scalar> bbJ=bodyJ.getBB(true);
  Vec3 posI0=Vec3::Zero(),posIJ0;
  Vec3 posI1=Vec3::Zero(),posIJ1;
  for(sizeType d=0; d<3; d++) {
    sizeType d1=(d+1)%3,d2=(d+2)%3;
    for(sizeType s1=-1; s1<=1; s1+=2)
      for(sizeType s2=-1; s2<=1; s2+=2) {
        //I0
        posI0[d1]=extI[d1]*s1;
        posI0[d2]=extI[d2]*s2;
        posI0[d]=-extI[d];
        posI0=bodyI.getT().block<3,3>(0,0)*posI0+bodyI.getT().block<3,1>(0,3);
        posIJ0=bodyJ.getT().block<3,3>(0,0).transpose()*(posI0-bodyJ.getT().block<3,1>(0,3));
        //I1
        posI1[d1]=extI[d1]*s1;
        posI1[d2]=extI[d2]*s2;
        posI1[d]=extI[d];
        posI1=bodyI.getT().block<3,3>(0,0)*posI1+bodyI.getT().block<3,1>(0,3);
        posIJ1=bodyJ.getT().block<3,3>(0,0).transpose()*(posI1-bodyJ.getT().block<3,1>(0,3));
        //test
        if(bbJ.intersect(posIJ0,posIJ1,s,t,3)) {
          s=(s+t)/2;
          c2._hasContact=true;
          c2._depth=ScalarUtil<scalar>::scalar_max;
          posI0=posI0*(1-s)+posI1*s;
          posIJ0=posIJ0*(1-s)+posIJ1*s;
          for(sizeType d=0; d<3; d++) {
            depth=posIJ0[d]-bbJ._minC[d];
            if(depth < c2._depth) {
              c2._pI=posI0;
              c2._nIJ=bodyJ.getT().block<3,3>(0,0).col(d);
              c2._pJ=posI0-c2._nIJ*depth;
              c2._depth=depth;
            }
            depth=bbJ._maxC[d]-posIJ0[d];
            if(depth < c2._depth) {
              c2._pI=posI0;
              c2._nIJ=-bodyJ.getT().block<3,3>(0,0).col(d);
              c2._pJ=posI0-c2._nIJ*depth;
              c2._depth=depth;
            }
          }
          if(c2._depth > c._depth)
            c=c2;
        }
      }
  }
}
void getContactBoxBall(Contact& c,const BoxGeomCell& bodyI,const SphereGeomCell& bodyJ)
{
  c._depth=0;
  Contact c2;
  scalar depth;
  scalar r=bodyJ.getRad();
  Vec3 extI=bodyI.getExt();
  Vec3 ctr=bodyJ.getT().block<3,1>(0,3);
  BBox<scalar> bbI=bodyI.getBB(true);
  unsigned char dim=bodyI.dim(),nrP=1<<dim;
  //ball to box
  Vec3 posJI=bodyI.getT().block<3,3>(0,0).transpose()*(ctr-bodyI.getT().block<3,1>(0,3));
  if(!bbI.contain(posJI,(sizeType)dim)) {
    Vec3 cptJI=bbI.closestTo(posJI,(sizeType)dim);
    depth=r-(cptJI-posJI).norm();
    if(depth > c._depth) {
      c._pI=bodyI.getT().block<3,3>(0,0)*cptJI+bodyI.getT().block<3,1>(0,3);
      c._pJ=ctr;
      c._nIJ=(c._pI-c._pJ)/max<scalar>((c._pI-c._pJ).norm(),EPS);
      if(c._nIJ.norm() > 0.99f) {
        c._pJ=ctr+c._nIJ*r;
        c._depth=depth;
        c._hasContact=true;
      }
    }
  } else {
    c2._hasContact=true;
    c2._depth=ScalarUtil<scalar>::scalar_max;
    for(sizeType d=0; d<dim; d++) {
      depth=r+posJI[d]-bbI._minC[d];
      if(depth < c2._depth) {
        c2._nIJ=-bodyI.getT().block<3,3>(0,0).col(d);
        c2._pJ=ctr-c2._nIJ*r;
        c2._pI=ctr+c2._nIJ*(depth-r);
        c2._depth=depth;
      }
      depth=r+bbI._maxC[d]-posJI[d];
      if(depth < c2._depth) {
        c2._nIJ=bodyI.getT().block<3,3>(0,0).col(d);
        c2._pJ=ctr-c2._nIJ*r;
        c2._pI=ctr+c2._nIJ*(depth-r);
        c2._depth=depth;
      }
    }
    if(c2._depth > c._depth)
      c=c2;
  }
  //box to ball
  for(sizeType p=0; p<nrP; p++) {
    Vec3 posI=Vec3::Zero();
    for(sizeType d=0; d<dim; d++)
      posI[d]=(p&(1<<d)) ? -extI[d] : extI[d];
    posI=bodyI.getT().block<3,3>(0,0)*posI+bodyI.getT().block<3,1>(0,3);
    Vec3 posIJ=posI-ctr;
    scalar distJI=posIJ.norm();
    depth=r-distJI;
    if(depth > c._depth) {
      c._pI=posI;
      c._nIJ=posIJ/max<scalar>(posIJ.norm(),EPS);
      c._pJ=ctr+c._nIJ*r;
      c._depth=depth;
      c._hasContact=true;
    }
  }
}
void getContact(Contact& c,StaticGeomCell& bodyI,StaticGeomCell& bodyJ)
{
  Mat4 tmpT;
  Contact c2;
  c._hasContact=false;
  c._depth=0;
  //sphere/sphere
  try {
    getContactBall(c,dynamic_cast<SphereGeomCell&>(bodyI),dynamic_cast<SphereGeomCell&>(bodyJ));
    return;
  } catch(...) {}
  //box/box
  try {
    getContactBoxVert(c,dynamic_cast<BoxGeomCell&>(bodyI),dynamic_cast<BoxGeomCell&>(bodyJ));
    getContactBoxVert(c2,dynamic_cast<BoxGeomCell&>(bodyJ),dynamic_cast<BoxGeomCell&>(bodyI));
    if(c2._depth > c._depth) {
      c=c2;
      swap(c._pI,c._pJ);
      c._nIJ*=-1;
    }
    if(bodyI.dim() == 3) {
      getContactBoxEdge(c2,dynamic_cast<BoxGeomCell&>(bodyI),dynamic_cast<BoxGeomCell&>(bodyJ));
      if(c2._depth > c._depth)
        c=c2;
      getContactBoxEdge(c2,dynamic_cast<BoxGeomCell&>(bodyJ),dynamic_cast<BoxGeomCell&>(bodyI));
      if(c2._depth > c._depth) {
        c=c2;
        swap(c._pI,c._pJ);
        c._nIJ*=-1;
      }
    }
    return;
  } catch(...) {}
  //box/ball
  try {
    getContactBoxBall(c,dynamic_cast<BoxGeomCell&>(bodyI),dynamic_cast<SphereGeomCell&>(bodyJ));
    return;
  } catch(...) {}
  try {
    getContactBoxBall(c,dynamic_cast<BoxGeomCell&>(bodyJ),dynamic_cast<SphereGeomCell&>(bodyI));
    swap(c._pI,c._pJ);
    c._nIJ*=-1;
    return;
  } catch(...) {}
  //composite/any
  try {
    c._depth=0;
    CompositeGeomCell& comp=dynamic_cast<CompositeGeomCell&>(bodyI);
    for(sizeType i=0; i<comp.nrChildren(); i++) {
      StaticGeomCell& ci=*(comp.getChild(i));
      tmpT=ci.getT();
      ci.setT(bodyI.getT()*ci.getT());
      getContact(c2,ci,bodyJ);
      ci.setT(tmpT);
      if(c2._depth > c._depth)
        c=c2;
    }
    return;
  } catch(...) {}
  //any/composite
  try {
    c._depth=0;
    CompositeGeomCell& comp=dynamic_cast<CompositeGeomCell&>(bodyJ);
    for(sizeType i=0; i<comp.nrChildren(); i++) {
      StaticGeomCell& ci=*(comp.getChild(i));
      tmpT=ci.getT();
      ci.setT(bodyJ.getT()*ci.getT());
      getContact(c2,bodyI,ci);
      ci.setT(tmpT);
      if(c2._depth > c._depth)
        c=c2;
    }
    return;
  } catch(...) {}
}
void getContact(Contact& c,RigidBody& bodyI,RigidBody& bodyJ)
{
  Mat4 T=Mat4::Identity();
  StaticGeomCell& cellI=bodyI.getShape().getCell();
  StaticGeomCell& cellJ=bodyJ.getShape().getCell();
  T.block<3,3>(0,0)=bodyI.rot();
  T.block<3,1>(0,3)=bodyI.pos();
  cellI.setT(T);
  T.block<3,3>(0,0)=bodyJ.rot();
  T.block<3,1>(0,3)=bodyJ.pos();
  cellJ.setT(T);
  getContact(c,cellI,cellJ);
}
//solid
void getContact(Contact& c,RigidBody& body,StaticGeomCell& solid)
{
  Mat4 T=Mat4::Identity();
  StaticGeomCell& cellI=body.getShape().getCell();
  T.block<3,3>(0,0)=body.rot();
  T.block<3,1>(0,3)=body.pos();
  cellI.setT(T);
  getContact(c,cellI,solid);
}
//resolve
void resolveContact(const Contact& c,RigidBody& body,scalar restitute)
{
  if(body.freeze())
    return;
  Mat3 crossI=cross<scalar>(c._pI-body.pos());
  Mat3 vI=Mat3::Identity()/body.getM()-crossI*(body.invI()*crossI);

  scalar denom=c._nIJ.dot(vI*c._nIJ);
  if(abs(denom) < EPS)
    return;
  scalar alpha=c._nIJ.dot(c._pJ-c._pI)*(restitute+1)/denom;
  body.clearForceAndTorque();
  body.applyWorldForce(c._nIJ*alpha,c._pI);
  body.setP(Vec3::Zero());
  body.setL(Vec3::Zero());
  body.advanceVelocity(1);
  body.advancePosition(1);
}
void resolveContact(const Contact& c,RigidBody& bodyI,RigidBody& bodyJ)
{
  if(bodyI.freeze() && bodyJ.freeze())
    return;
  Mat3 crossI=cross<scalar>(c._pI-bodyI.pos());
  Mat3 vI=Mat3::Identity()/bodyI.getM()-crossI*(bodyI.invI()*crossI);
  if(bodyI.freeze())
    vI.setZero();
  Mat3 crossJ=cross<scalar>(c._pJ-bodyJ.pos());
  Mat3 vJ=Mat3::Identity()/bodyJ.getM()-crossJ*(bodyJ.invI()*crossJ);
  if(bodyJ.freeze())
    vJ.setZero();

  scalar denom=(c._nIJ.dot(vI*c._nIJ)+c._nIJ.dot(vJ*c._nIJ));
  if(abs(denom) < EPS)
    return;
  scalar alpha=c._nIJ.dot(c._pJ-c._pI)/denom;
  if(!bodyI.freeze()) {
    bodyI.clearForceAndTorque();
    bodyI.applyWorldForce(c._nIJ*alpha,c._pI);
    bodyI.setP(Vec3::Zero());
    bodyI.setL(Vec3::Zero());
    bodyI.advanceVelocity(1);
    bodyI.advancePosition(1);
  }
  if(!bodyJ.freeze()) {
    bodyJ.clearForceAndTorque();
    bodyJ.applyWorldForce(-c._nIJ*alpha,c._pJ);
    bodyJ.setP(Vec3::Zero());
    bodyJ.setL(Vec3::Zero());
    bodyJ.advanceVelocity(1);
    bodyJ.advancePosition(1);
  }
}

PRJ_END
