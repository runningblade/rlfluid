#ifndef SCRIPTED_SOURCE_H
#define SCRIPTED_SOURCE_H

#include "SourceRegion.h"
#include <CommonFile/ImplicitFuncInterface.h>

PRJ_BEGIN

class SourceImplicitFunc : public ImplicitFunc<scalar>, public Serializable
{
public:
  SourceImplicitFunc();
  scalar lastFor() const;
  virtual void onActivate();
  virtual void onFrame();
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual boost::shared_ptr<Serializable> copy() const=0;
  //data
  BBox<scalar> _bb;
  Vec3 _vel;
  sizeType _dim;
  scalar _res;
  scalar _lastFor;
  bool _source;
  bool _instant;
};
class ImplicitRect : public SourceImplicitFunc
{
public:
  ImplicitRect();
  ImplicitRect(const Vec3& minC,const Vec3& maxC,const sizeType& dim,const scalar& res,const Vec3& vel=Vec3::Zero(),const scalar& lastFor=0.0f,bool source=true,bool instant=false);
  void reset(const Vec3& minC,const Vec3& maxC,const sizeType& dim,const scalar& res,const Vec3& vel=Vec3::Zero(),const scalar& lastFor=0.0f,bool source=true,bool instant=false);
  virtual scalar operator()(const Vec3& pos) const;
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<Serializable> copy() const override;
  BBox<scalar> _rectBB;
};
class ImplicitRectNeg : public ImplicitRect
{
public:
  ImplicitRectNeg();
  ImplicitRectNeg(const Vec3& minC,const Vec3& maxC,const sizeType& dim,const scalar& res,const Vec3& vel=Vec3::Zero(),const scalar& lastFor=0.0f,bool source=true,bool instant=false);
  virtual scalar operator()(const Vec3& pos) const;
  virtual boost::shared_ptr<Serializable> copy() const override;
};
class ImplicitCylinder : public SourceImplicitFunc
{
public:
  ImplicitCylinder();
  ImplicitCylinder(const Vec3& ctr,const Vec3& dir,const scalar& rad,const scalar& thickness,const sizeType& dim,const scalar& res,const Vec3& vel=Vec3::Zero(),const scalar& lastFor=0.0f,bool source=true,bool instant=false);
  void reset(const Vec3& ctr,const Vec3& dir,const scalar& rad,const scalar& thickness,const sizeType& dim,const scalar& res,const Vec3& vel=Vec3::Zero(),const scalar& lastFor=0.0f,bool source=true,bool instant=false);
  virtual scalar operator()(const Vec3& pos) const;
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<Serializable> copy() const override;
  Vec3 _ctr;
  Vec3 _dir;
  scalar _rad;
  scalar _thickness;
};
class ImplicitSphere : public SourceImplicitFunc
{
public:
  ImplicitSphere();
  ImplicitSphere(const Vec3& ctr,const scalar& rad,const sizeType& dim,const scalar& res,const Vec3& vel=Vec3::Zero(),const scalar& lastFor=0.0f,bool source=true,bool instant=false);
  void reset(const Vec3& ctr,const scalar& rad,const sizeType& dim,const scalar& res,const Vec3& vel=Vec3::Zero(),const scalar& lastFor=0.0f,bool source=true,bool instant=false);
  scalar operator()(const Vec3& pos) const;
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<Serializable> copy() const override;
  //data
  Vec3 _ctr;
  scalar _rad;
};

class ImplicitFuncSource : public SourceRegion
{
public:
  typedef SourceRegion PARENT;
  using PARENT::_region;
  using PARENT::_vel;
  ImplicitFuncSource();
  virtual bool findSource(const scalar& globalTime) =0;
  virtual bool filterSource(const scalar& globalTime) override;
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  //data
  vector<boost::shared_ptr<SourceImplicitFunc> > _sources;
  boost::shared_ptr<SourceImplicitFunc> _source;
};
class RandomSource : public ImplicitFuncSource
{
public:
  using ImplicitFuncSource::_sources;
  using ImplicitFuncSource::_source;
  RandomSource();
  RandomSource(const scalar& period);
  virtual void setPeriod(const scalar& period);
  virtual void addSource(boost::shared_ptr<SourceImplicitFunc> source);
  virtual bool findSource(const scalar& globalTime) override;
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<Serializable> copy() const override;
  //data
  scalar _period;
  sizeType _lastId;
};
class TokenRingSource : public ImplicitFuncSource
{
public:
  using ImplicitFuncSource::_sources;
  using ImplicitFuncSource::_source;
  TokenRingSource();
  TokenRingSource(const scalar& period);
  virtual void setPeriod(const scalar& period);
  virtual void addSource(boost::shared_ptr<SourceImplicitFunc> source);
  virtual bool findSource(const scalar& globalTime) override;
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<Serializable> copy() const override;
  //data
  scalar _period;
  scalar _lastTime;
  sizeType _lastSource;
};
class VariableTimeTokenRingSource : public ImplicitFuncSource
{
public:
  using ImplicitFuncSource::_sources;
  using ImplicitFuncSource::_source;
  VariableTimeTokenRingSource();
  virtual void addSource(boost::shared_ptr<SourceImplicitFunc> source);
  virtual bool findSource(const scalar& globalTime) override;
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<Serializable> copy() const override;
  //data
  scalar _sourceStartTime;
  sizeType _currentSource;
};

PRJ_END

#endif
