#ifndef SPRING_FORCE_H
#define SPRING_FORCE_H

#include <CommonFile/ParticleSet.h>
#include <CommonFile/ParticleCD.h>

PRJ_BEGIN

class SpringForce : public Serializable
{
public:
  SpringForce();
  virtual ~SpringForce();
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<Serializable> copy() const override;
  virtual void resetSolid(const ScalarField& solid,scalar radius,sizeType nrParticlePerGrid);
  virtual void sampleSolidPSet(const ScalarField& solid);
  virtual bool correct(ParticleSetN& pSet,scalar dt,const ParticleSetN* additionalSolidPSet,const Vec3i* warpMode=NULL);
  virtual bool pushParticle(ParticleSetN& pSet,scalar dt);
  virtual bool adjustPosition(scalar dt);
  const ParticleSetN& getSolidPSet() const;
  ParticleSetN& getSolidPSet();
  const CollisionInterface<scalar,ParticleSetN>& getPSetCD() const;
  CollisionInterface<scalar,ParticleSetN>& getPSetCD();
protected:
  sizeType _dim;
  scalar _radius;
  scalar _spring;
  sizeType _maxIter;
  sizeType _prepareInterval;
  sizeType _nrParticlePerGrid;
  scalar _thresInvalid;
  CollisionInterface<scalar,ParticleSetN> _pSetCD;
  CollisionInterface<scalar,ParticleSetN> _solidPSetCD;
  ParticleSetN _newPSet;
  ParticleSetN _solidPSet;
};

PRJ_END

#endif
