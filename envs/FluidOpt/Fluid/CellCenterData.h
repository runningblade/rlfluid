#ifndef CELL_CENTER_DATA_H
#define CELL_CENTER_DATA_H

#include "VariableSizedGrid.h"

PRJ_BEGIN

//Iterator
template <typename TDATA>
struct CellCenterData : public Serializable {
  CellCenterData():Serializable(typeid(CellCenterData<TDATA>).name()) {}
  template <typename T,int DIM,typename Encoder>
  CellCenterData(const VariableSizedGrid<T,DIM,Encoder>& grid)
    :Serializable(typeid(CellCenterData<TDATA>).name()) {
    reset(grid);
  }
  template <typename T,int DIM,typename Encoder>
  void reset(const VariableSizedGrid<T,DIM,Encoder>& grid) {
    _data.setZero(grid._nrPointCellCenter.prod());
  }
  virtual bool read(istream& is,IOData* dat) override {
    readBinaryData(_data,is);
    return is.good();
  }
  virtual bool write(ostream& os,IOData* dat) const override {
    writeBinaryData(_data,os);
    return os.good();
  }
  virtual boost::shared_ptr<Serializable> copy() const override {
    return boost::shared_ptr<Serializable>(new CellCenterData<TDATA>);
  }
  Eigen::Matrix<TDATA,-1,1> _data;
};
template <typename T,int DIM,typename Encoder>
struct CellCenterIteratorPoint : public TypeTraits<T,DIM> {
  using typename TypeTraits<T,DIM>::VecDIMd;
  using typename TypeTraits<T,DIM>::VecDIMb;
  using typename TypeTraits<T,DIM>::VecDIMi;
  typedef VariableSizedGrid<T,DIM,Encoder> Grid;
  typedef VecDIMd value_type;
  CellCenterIteratorPoint(const Grid& grd,int verbose,const VecDIMi& id0=VecDIMi::Zero(),const VecDIMi& id1=VecDIMi::Zero(),
                          const VecDIMi& extendMin=VecDIMi::Zero(),const VecDIMi& extendMax=VecDIMi::Zero());
  CellCenterIteratorPoint(const Grid& grd,int verbose,const BBox<T,DIM>& bb,
                          const VecDIMi& extendMin=VecDIMi::Zero(),const VecDIMi& extendMax=VecDIMi::Zero());
  virtual ~CellCenterIteratorPoint() {}
  bool operator!=(const CellCenterIteratorPoint<T,DIM,Encoder>& other) const;
  value_type operator*() const;
  value_type pointWithWarp() const;
  const value_type& warpCompensate() const;
  void operator++();
  sizeType off() const;
  const VecDIMi& id() const;
  sizeType offFaceLeft(sizeType D) const;
  sizeType offFaceRight(sizeType D) const;
  bool hasCellCenter(sizeType D) const;
  bool hasCellCenterLeft(sizeType D) const;
  sizeType offCellCenter(sizeType D,VecDIMi* id=NULL) const;
  sizeType offCellCenterLeft(sizeType D,VecDIMi* id=NULL) const;
protected:
  const Grid& _grd;
  VecDIMi _id0,_id1,_id,_warpCount,_warpCountMax;
  VecDIMd _warpCompensate,_warpCompensate0,_warpRange;
  sizeType _off;
};
template <typename T,int DIM,typename Encoder>
struct CellCenterIteratorPointIndex : public CellCenterIteratorPoint<T,DIM,Encoder> {
  using typename CellCenterIteratorPoint<T,DIM,Encoder>::VecDIMd;
  using typename CellCenterIteratorPoint<T,DIM,Encoder>::VecDIMi;
  using typename CellCenterIteratorPoint<T,DIM,Encoder>::Grid;
  typedef VecDIMi value_type;
  using CellCenterIteratorPoint<T,DIM,Encoder>::_off;
  CellCenterIteratorPointIndex(const Grid& grd,int verbose,const VecDIMi& id0=VecDIMi::Zero(),const VecDIMi& id1=VecDIMi::Zero());
  value_type operator*() const;
};
template <typename T,int DIM,typename Encoder>
struct CellCenterIteratorCell : public CellCenterIteratorPoint<T,DIM,Encoder>  {
  using typename CellCenterIteratorPoint<T,DIM,Encoder>::VecDIMd;
  using typename CellCenterIteratorPoint<T,DIM,Encoder>::VecDIMi;
  using typename CellCenterIteratorPoint<T,DIM,Encoder>::Grid;
  using typename TypeTraits<T,DIM>::VecSTENCILi;
  typedef VecSTENCILi value_type;
  using CellCenterIteratorPoint<T,DIM,Encoder>::_grd;
  using CellCenterIteratorPoint<T,DIM,Encoder>::_id;
  CellCenterIteratorCell(const Grid& grd,int verbose,const VecDIMi& id0=VecDIMi::Zero(),const VecDIMi& id1=VecDIMi::Zero());
  value_type operator*() const;
};

PRJ_END

#endif
