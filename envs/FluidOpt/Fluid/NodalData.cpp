#include "NodalData.h"
#include "Utils.h"
#include <functional>

USE_PRJ_NAMESPACE

//Iterator: Point
template <typename T,int DIM,typename Encoder>
NodalIteratorPoint<T,DIM,Encoder>::NodalIteratorPoint(const Grid& grd,int verbose,const VecDIMi& id0,const VecDIMi& id1,const VecDIMi& extendMin,const VecDIMi& extendMax)
  :_grd(grd),_id0(id0),_id1(grd._nrPointNodal-id1),_warpCount(VecDIMi::Zero())
{
  _warpRange=_grd.getBBNodal().getExtent();
  for(int D=0; D<DIM; D++) {
    IteratorUtil<T,DIM>::makeIteratorRangeSafe(_id0[D],_id1[D],_grd._nrPointNodal[D],_grd._warpMode[D],_warpCountMax[D],_warpRange[D],_warpCompensate0[D]);
    IteratorUtil<T,DIM>::extendIterator(_id0[D],_id1[D],_grd._nrPointNodal[D],_grd._warpMode[D],_warpCountMax[D],extendMin[D],extendMax[D],_warpRange[D],_warpCompensate0[D]);
  }
  IteratorUtil<T,DIM>::initIterator(_id,_id0,_id1,_warpCount,_warpCountMax,verbose);
  _warpCompensate=_warpCompensate0;
  _off=_id.dot(_grd._strideNodal);
}
template <typename T,int DIM,typename Encoder>
NodalIteratorPoint<T,DIM,Encoder>::NodalIteratorPoint(const Grid& grd,int verbose,const BBox<T,DIM>& bb,const VecDIMi& extendMin,const VecDIMi& extendMax)
  :_grd(grd),_warpCount(VecDIMi::Zero())
{
  _warpRange=_grd.getBBNodal().getExtent();
  using namespace std::placeholders;
  IteratorUtil<T,DIM>::computeIdBB(_grd,_id0,_id1,bb,_warpCountMax,_warpRange,_warpCompensate0,std::bind(&Grid::indexSafeNodal,_grd,_1,_2,_3));
  for(sizeType D=0; D<DIM; D++)
    IteratorUtil<T,DIM>::extendIterator(_id0[D],_id1[D],_grd._nrPointNodal[D],_grd._warpMode[D],_warpCountMax[D],extendMin[D],extendMax[D],_warpRange[D],_warpCompensate0[D]);
  IteratorUtil<T,DIM>::initIterator(_id,_id0,_id1,_warpCount,_warpCountMax,verbose);
  _warpCompensate=_warpCompensate0;
  _off=_id.dot(_grd._strideNodal);
}
template <typename T,int DIM,typename Encoder>
bool NodalIteratorPoint<T,DIM,Encoder>::operator!=(const NodalIteratorPoint<T,DIM,Encoder>& other) const
{
  return _id != other._id || _warpCount != other._warpCount;
}
template <typename T,int DIM,typename Encoder>
typename NodalIteratorPoint<T,DIM,Encoder>::value_type NodalIteratorPoint<T,DIM,Encoder>::operator*() const
{
  VecDIMd pt;
  _grd.getPtNodal(_id,pt);
  return pt;
}
template <typename T,int DIM,typename Encoder>
typename NodalIteratorPoint<T,DIM,Encoder>::value_type NodalIteratorPoint<T,DIM,Encoder>::pointWithWarp() const
{
  return operator*()+_warpCompensate;
}
template <typename T,int DIM,typename Encoder>
const typename NodalIteratorPoint<T,DIM,Encoder>::value_type& NodalIteratorPoint<T,DIM,Encoder>::warpCompensate() const
{
  return _warpCompensate;
}
template <typename T,int DIM,typename Encoder>
void NodalIteratorPoint<T,DIM,Encoder>::operator++()
{
  OperatorTraits<T,DIM,Encoder,DIM-1>::advanceIterator
  (_grd._strideNodal,_grd._nrPointNodal,_warpCountMax,
   _warpRange,_warpCompensate0,_warpCompensate,
   _id0,_id1,_id,_warpCount,_off);
}
template <typename T,int DIM,typename Encoder>
sizeType NodalIteratorPoint<T,DIM,Encoder>::off() const
{
  return _off;
}
template <typename T,int DIM,typename Encoder>
const typename NodalIteratorPoint<T,DIM,Encoder>::VecDIMi& NodalIteratorPoint<T,DIM,Encoder>::id() const
{
  return _id;
}
//Iterator: PointIndex
template <typename T,int DIM,typename Encoder>
NodalIteratorPointIndex<T,DIM,Encoder>::NodalIteratorPointIndex(const Grid& grd,int verbose,const VecDIMi& id0,const VecDIMi& id1)
  :NodalIteratorPoint<T,DIM,Encoder>(grd,verbose,id0,id1) {}
template <typename T,int DIM,typename Encoder>
typename NodalIteratorPointIndex<T,DIM,Encoder>::value_type NodalIteratorPointIndex<T,DIM,Encoder>::operator*() const
{
  return VecDIMi::Constant(_off);
}
//Iterator: Cell
template <typename T,int DIM,typename Encoder>
NodalIteratorCell<T,DIM,Encoder>::NodalIteratorCell(const Grid& grd,int verbose,const VecDIMi& id0,const VecDIMi& id1)
  :NodalIteratorPoint<T,DIM,Encoder>(grd,verbose,id0,id1+VecDIMi::Ones()) {}
template <typename T,int DIM,typename Encoder>
typename NodalIteratorCell<T,DIM,Encoder>::value_type NodalIteratorCell<T,DIM,Encoder>::operator*() const
{
  VecSTENCILi stencil;
  OperatorTraits<T,DIM,Encoder,DIM-1>::getStencil(_grd._strideNodal,_id.dot(_grd._strideNodal),stencil,0);
  return stencil;
}
//instance
#define INSTANCE(DIM,TYPE,SPARSITY) \
template class NodalIteratorPoint<scalar,DIM,Encoder##TYPE<SPARSITY> >;  \
template class NodalIteratorPointIndex<scalar,DIM,Encoder##TYPE<SPARSITY> >;  \
template class NodalIteratorCell<scalar,DIM,Encoder##TYPE<SPARSITY> >;
INSTANCE_ALL
