#ifndef RIGID_CONTACT_H
#define RIGID_CONTACT_H

#include "RigidSolver.h"

PRJ_BEGIN

//contact
struct Contact {
  Contact():_pI(Vec3::Zero()),_pJ(Vec3::Zero()),_nIJ(Vec3::Zero()),_hasContact(false),_depth(0) {}
  Vec3 _pI,_pJ,_nIJ;
  bool _hasContact;
  scalar _depth;
};
void getContact(Contact& c,RigidBody& body,const BBox<scalar>& bb);
void getContact(Contact& c,RigidBody& bodyI,RigidBody& bodyJ);
void getContact(Contact& c,RigidBody& body,StaticGeomCell& solid);
//resolve
void resolveContact(const Contact& c,RigidBody& body,scalar restitute);
void resolveContact(const Contact& c,RigidBody& bodyI,RigidBody& bodyJ);

PRJ_END

#endif
