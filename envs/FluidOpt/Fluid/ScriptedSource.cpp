#include "ScriptedSource.h"
#include <CommonFile/GridOp.h>

USE_PRJ_NAMESPACE

//SourceImplicitFunc
SourceImplicitFunc::SourceImplicitFunc()
  :Serializable(typeid(SourceImplicitFunc).name()),_source(true),_instant(false) {}
scalar SourceImplicitFunc::lastFor() const
{
  return _lastFor;
}
void SourceImplicitFunc::onActivate() {}
void SourceImplicitFunc::onFrame() {}
bool SourceImplicitFunc::read(istream& is,IOData* dat)
{
  readBinaryData(_bb,is);
  readBinaryData(_vel,is);
  readBinaryData(_dim,is);
  readBinaryData(_res,is);
  readBinaryData(_lastFor,is);
  readBinaryData(_source,is);
  readBinaryData(_instant,is);
  return is.good();
}
bool SourceImplicitFunc::write(ostream& os,IOData* dat) const
{
  writeBinaryData(_bb,os);
  writeBinaryData(_vel,os);
  writeBinaryData(_dim,os);
  writeBinaryData(_res,os);
  writeBinaryData(_lastFor,os);
  writeBinaryData(_source,os);
  writeBinaryData(_instant,os);
  return os.good();
}
//ImplicitRect
ImplicitRect::ImplicitRect()
{
  setType(typeid(ImplicitRect).name());
}
ImplicitRect::ImplicitRect(const Vec3& minC,const Vec3& maxC,const sizeType& dim,const scalar& res,const Vec3& vel,const scalar& lastFor,bool source,bool instant)
{
  setType(typeid(ImplicitRect).name());
  reset(minC,maxC,dim,res,vel,lastFor,source,instant);
}
void ImplicitRect::reset(const Vec3& minC,const Vec3& maxC,const sizeType& dim,const scalar& res,const Vec3& vel,const scalar& lastFor,bool source,bool instant)
{
  _rectBB._minC=minC;
  _rectBB._maxC=maxC;
  _bb=_rectBB;
  _bb.enlarged(res*2.0f);
  if(dim == 2)
    _bb._maxC.z()=_bb._minC.z()=0.0f;
  _vel=vel;
  _dim=dim;
  _res=res;
  _lastFor=lastFor;
  _source=source;
  _instant=instant;
}
scalar ImplicitRect::operator()(const Vec3& pos) const
{
  scalar box;
  if(_dim == 2)
    box=std::max(std::max(_rectBB._minC.x()-pos.x(),pos.x()-_rectBB._maxC.x()),
                 std::max(_rectBB._minC.y()-pos.y(),pos.y()-_rectBB._maxC.y()));
  else
    box=std::max(std::max(std::max(_rectBB._minC.x()-pos.x(),pos.x()-_rectBB._maxC.x()),
                          std::max(_rectBB._minC.y()-pos.y(),pos.y()-_rectBB._maxC.y())),
                 std::max(_rectBB._minC.z()-pos.z(),pos.z()-_rectBB._maxC.z()));
  return box;
}
bool ImplicitRect::read(istream& is,IOData* dat)
{
  SourceImplicitFunc::read(is,dat);
  readBinaryData(_rectBB,is);
  return is.good();
}
bool ImplicitRect::write(ostream& os,IOData* dat) const
{
  SourceImplicitFunc::write(os,dat);
  writeBinaryData(_rectBB,os);
  return os.good();
}
boost::shared_ptr<Serializable> ImplicitRect::copy() const
{
  return boost::shared_ptr<Serializable>(new ImplicitRect);
}
//ImplicitRectNeg
ImplicitRectNeg::ImplicitRectNeg()
{
  setType(typeid(ImplicitRectNeg).name());
}
ImplicitRectNeg::ImplicitRectNeg(const Vec3& minC,const Vec3& maxC,const sizeType& dim,const scalar& res,const Vec3& vel,const scalar& lastFor,bool source,bool instant)
  :ImplicitRect(minC,maxC,dim,res,vel,lastFor,source,instant)
{
  setType(typeid(ImplicitRectNeg).name());
}
scalar ImplicitRectNeg::operator()(const Vec3& pos) const
{
  return -ImplicitRect::operator()(pos);
}
boost::shared_ptr<Serializable> ImplicitRectNeg::copy() const
{
  return boost::shared_ptr<Serializable>(new ImplicitRectNeg);
}
//ImplicitCylinder
ImplicitCylinder::ImplicitCylinder()
{
  setType(typeid(ImplicitCylinder).name());
}
ImplicitCylinder::ImplicitCylinder(const Vec3& ctr,const Vec3& dir,const scalar& rad,const scalar& thickness,const sizeType& dim,const scalar& res,const Vec3& vel,const scalar& lastFor,bool source,bool instant)
{
  setType(typeid(ImplicitCylinder).name());
  reset(ctr,dir,rad,thickness,dim,res,vel,lastFor,source,instant);
}
void ImplicitCylinder::reset(const Vec3& ctr,const Vec3& dir,const scalar& rad,const scalar& thickness,const sizeType& dim,const scalar& res,const Vec3& vel,const scalar& lastFor,bool source,bool instant)
{
  _ctr=ctr;
  _dir=dir;
  _rad=rad;
  _thickness=thickness;
  _bb._minC=_bb._maxC=ctr;
  _bb.setUnion(ctr-thickness*dir-Vec3::Constant(rad));
  _bb.setUnion(ctr-thickness*dir+Vec3::Constant(rad));
  _bb.setUnion(ctr+thickness*dir-Vec3::Constant(rad));
  _bb.setUnion(ctr+thickness*dir+Vec3::Constant(rad));
  _bb.enlarged(res*2.0f);
  if(dim == 2)
    _bb._maxC.z()=_bb._minC.z()=0.0f;
  _vel=vel;
  _dim=dim;
  _res=res;
  _lastFor=lastFor;
  _source=source;
  _instant=instant;
}
scalar ImplicitCylinder::operator()(const Vec3& pos) const
{
  Vec3 rel=pos-_ctr;
  return std::max<scalar>((rel-rel.dot(_dir)*_dir).norm()-_rad,
                          abs(rel.dot(_dir))-_thickness);
}
bool ImplicitCylinder::read(istream& is,IOData* dat)
{
  SourceImplicitFunc::read(is,dat);
  readBinaryData(_ctr,is);
  readBinaryData(_dir,is);
  readBinaryData(_rad,is);
  readBinaryData(_thickness,is);
  return is.good();
}
bool ImplicitCylinder::write(ostream& os,IOData* dat) const
{
  SourceImplicitFunc::write(os,dat);
  writeBinaryData(_ctr,os);
  writeBinaryData(_dir,os);
  writeBinaryData(_rad,os);
  writeBinaryData(_thickness,os);
  return os.good();
}
boost::shared_ptr<Serializable> ImplicitCylinder::copy() const
{
  return boost::shared_ptr<Serializable>(new ImplicitCylinder);
}
//ImplicitSphere
ImplicitSphere::ImplicitSphere()
{
  setType(typeid(ImplicitSphere).name());
}
ImplicitSphere::ImplicitSphere(const Vec3& ctr,const scalar& rad,const sizeType& dim,const scalar& res,const Vec3& vel,const scalar& lastFor,bool source,bool instant)
{
  setType(typeid(ImplicitSphere).name());
  reset(ctr,rad,dim,res,vel,lastFor,source,instant);
}
void ImplicitSphere::reset(const Vec3& ctr,const scalar& rad,const sizeType& dim,const scalar& res,const Vec3& vel,const scalar& lastFor,bool source,bool instant)
{
  _ctr=ctr;
  _rad=rad;
  _bb._maxC=ctr+Vec3::Constant(rad);
  _bb._minC=ctr-Vec3::Constant(rad);
  _bb.enlarged(res*2.0f);
  if(dim == 2)
    _bb._maxC.z()=_bb._minC.z()=0.0f;
  _vel=vel;
  _dim=dim;
  _res=res;
  _lastFor=lastFor;
  _source=source;
  _instant=instant;
}
scalar ImplicitSphere::operator()(const Vec3& pos) const
{
  if(_dim == 2)
    return (pos-Vec3(_ctr.x(),_ctr.y(),0.0f)).norm()-_rad;
  else
    return (pos-_ctr).norm()-_rad;
}
bool ImplicitSphere::read(istream& is,IOData* dat)
{
  SourceImplicitFunc::read(is,dat);
  readBinaryData(_ctr,is);
  readBinaryData(_rad,is);
  return is.good();
}
bool ImplicitSphere::write(ostream& os,IOData* dat) const
{
  SourceImplicitFunc::write(os,dat);
  writeBinaryData(_ctr,os);
  writeBinaryData(_rad,os);
  return os.good();
}
boost::shared_ptr<Serializable> ImplicitSphere::copy() const
{
  return boost::shared_ptr<Serializable>(new ImplicitSphere);
}
//ImplicitFuncSource
ImplicitFuncSource::ImplicitFuncSource():_source((SourceImplicitFunc*)NULL) {}
bool ImplicitFuncSource::filterSource(const scalar& globalTime)
{
  if(findSource(globalTime) && _source) {
    SourceImplicitFunc& s=*_source;
    if(s._dim == 0)
      return false;

    BBox<scalar> bb=s._bb;
    if(s._dim == 2)
      bb._minC.z()=bb._maxC.z()=0.0f;

    //region
    const Vec3i nrCell=ceilV((Vec3)(s._bb.getExtent()/s._res));
    _region.reset(nrCell,bb,0.0f);
    GridOp<scalar,scalar>::copyFromImplictFunc(_region,s);

    //velocity
    _vel.reset(_region);
    _vel.init(s._vel);

    //source or sink
    PARENT::setSource(s._source);
    if(_source->_instant)
      _source.reset((SourceImplicitFunc*)NULL);

    s.onFrame();
    return true;
  }
  return false;
}
bool ImplicitFuncSource::read(istream& is,IOData* dat)
{
  SourceRegion::read(is,dat);
  readVector(_sources,is,dat);
  readBinaryData(_source,is,dat);
  return is.good();
}
bool ImplicitFuncSource::write(ostream& os,IOData* dat) const
{
  SourceRegion::write(os,dat);
  writeVector(_sources,os,dat);
  writeBinaryData(_source,os,dat);
  return os.good();
}
//RandomSource
RandomSource::RandomSource()
{
  setType(typeid(RandomSource).name());
}
RandomSource::RandomSource(const scalar& period)
  :_period(period),_lastId(-1)
{
  setType(typeid(RandomSource).name());
}
void RandomSource::setPeriod(const scalar& period)
{
  _period=period;
  _lastId=-1;
}
void RandomSource::addSource(boost::shared_ptr<SourceImplicitFunc> source)
{
  _sources.push_back(source);
}
bool RandomSource::findSource(const scalar& globalTime)
{
  if(_sources.empty())
    return false;

  sizeType id=(sizeType)std::floor(globalTime/_period);
  if(id != _lastId) {
    _source=_sources[rand()%(sizeType)(_sources.size())];
    _source->onActivate();
    _lastId=id;
    return true;
  }
  return true;
}
bool RandomSource::read(istream& is,IOData* dat)
{
  ImplicitFuncSource::read(is,dat);
  readBinaryData(_period,is);
  readBinaryData(_lastId,is);
  return is.good();
}
bool RandomSource::write(ostream& os,IOData* dat) const
{
  ImplicitFuncSource::write(os,dat);
  writeBinaryData(_period,os);
  writeBinaryData(_lastId,os);
  return os.good();
}
boost::shared_ptr<Serializable> RandomSource::copy() const
{
  return boost::shared_ptr<Serializable>(new RandomSource);
}
//TokenRingSource
TokenRingSource::TokenRingSource()
{
  setType(typeid(TokenRingSource).name());
}
TokenRingSource::TokenRingSource(const scalar& period)
  :_period(period),_lastTime(-1.0f),_lastSource(-1)
{
  setType(typeid(TokenRingSource).name());
}
void TokenRingSource::setPeriod(const scalar& period)
{
  _period=period;
}
void TokenRingSource::addSource(boost::shared_ptr<SourceImplicitFunc> source)
{
  _sources.push_back(source);
}
bool TokenRingSource::findSource(const scalar& globalTime)
{
  if(_sources.empty())
    return false;

  INFOV("Test For Source: LastTime: %f, Period: %f, GlobalTime: %f",_lastTime,_period,globalTime)
  if(_lastTime+_period < globalTime) {
    INFO("Set Source")
    _lastSource=(_lastSource+1)%(sizeType)(_sources.size());
    _source=_sources[_lastSource];
    _source->onActivate();
    _lastTime=globalTime;
  }
  return true;
}
bool TokenRingSource::read(istream& is,IOData* dat)
{
  ImplicitFuncSource::read(is,dat);
  readBinaryData(_period,is);
  readBinaryData(_lastTime,is);
  readBinaryData(_lastSource,is);
  return is.good();
}
bool TokenRingSource::write(ostream& os,IOData* dat) const
{
  ImplicitFuncSource::write(os,dat);
  writeBinaryData(_period,os);
  writeBinaryData(_lastTime,os);
  writeBinaryData(_lastSource,os);
  return os.good();
}
boost::shared_ptr<Serializable> TokenRingSource::copy() const
{
  return boost::shared_ptr<Serializable>(new TokenRingSource);
}
//VariableTimeTokenRingSource
VariableTimeTokenRingSource::VariableTimeTokenRingSource()
  :_sourceStartTime(-1.0f),_currentSource(-1)
{
  setType(typeid(VariableTimeTokenRingSource).name());
}
void VariableTimeTokenRingSource::addSource(boost::shared_ptr<SourceImplicitFunc> source)
{
  _sources.push_back(source);
}
bool VariableTimeTokenRingSource::findSource(const scalar& globalTime)
{
  scalar totalLast=0.0f;
  for(sizeType i=0; i<(sizeType)_sources.size(); i++)
    totalLast+=_sources[i]->lastFor();
  if(totalLast == 0.0f)
    return false;

  if(_sourceStartTime == -1.0f) {
    _sourceStartTime=globalTime;
    _currentSource=0;
    _source=_sources[0];
    _source->onActivate();
  }
  while(_source->lastFor() == 0.0f || _sourceStartTime+_source->lastFor() < globalTime) {
    _sourceStartTime=globalTime;
    _currentSource=(_currentSource+1)%(sizeType)(_sources.size());
    _source=_sources[_currentSource];
    _source->onActivate();
  }
  return true;
}
bool VariableTimeTokenRingSource::read(istream& is,IOData* dat)
{
  ImplicitFuncSource::read(is,dat);
  readBinaryData(_sourceStartTime,is);
  readBinaryData(_currentSource,is);
  return is.good();
}
bool VariableTimeTokenRingSource::write(ostream& os,IOData* dat) const
{
  ImplicitFuncSource::write(os,dat);
  writeBinaryData(_sourceStartTime,os);
  writeBinaryData(_currentSource,os);
  return os.good();
}
boost::shared_ptr<Serializable> VariableTimeTokenRingSource::copy() const
{
  return boost::shared_ptr<Serializable>(new VariableTimeTokenRingSource);
}
