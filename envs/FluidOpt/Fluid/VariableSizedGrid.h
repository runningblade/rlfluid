#ifndef VARIABLE_SIZED_GRID_H
#define VARIABLE_SIZED_GRID_H

#include <CommonFile/IOBasic.h>

PRJ_BEGIN

template <typename T,int DIM,typename Encoder>
struct VariableSizedGrid;
template <typename TDATA>
struct NodalData;
template <typename TDATA>
struct CellCenterData;
template <int DIM,typename TDATA>
struct MACVelData;

//TypeTraits
template <typename T,int DIM>
struct TypeTraits {
  typedef Eigen::Matrix<T,DIM,DIM> MatDIMd;
  typedef Eigen::Matrix<T,DIM,1> VecDIMd;
  typedef Eigen::Matrix<T,DIM+1,1> VecDIMPd;
  typedef Eigen::Matrix<unsigned char,DIM,1> VecDIMb;
  typedef Eigen::Matrix<sizeType,DIM,1> VecDIMi;
  typedef Eigen::Matrix<T,1<<DIM,1> VecSTENCILd;
  typedef Eigen::Matrix<sizeType,1<<DIM,1> VecSTENCILi;
};
//Encoder
template <int sparsity>
struct EncoderOctree {
  static const int blockSz=1<<sparsity;
  static const bool isConstantEncoder=false;
  //given index measured by cellSz, recover storage index
  int indexDeltaDIM(int input) const;
  //given storage index, reocver index measured cellSz
  int decodeDeltaDIM(int input) const;
};
template <int sparsity>
struct EncoderConstant {
  static const bool isConstantEncoder=true;
  int indexDeltaDIM(int input) const;
  int decodeDeltaDIM(int input) const;
};
//OperatorTraits
template <typename T,int DIM,typename Encoder,int DIM2>
struct OperatorTraits : public TypeTraits<T,DIM> {
  using typename TypeTraits<T,DIM>::VecDIMd;
  using typename TypeTraits<T,DIM>::VecDIMb;
  using typename TypeTraits<T,DIM>::VecDIMi;
  using typename TypeTraits<T,DIM>::VecSTENCILd;
  using typename TypeTraits<T,DIM>::VecSTENCILi;
  typedef VariableSizedGrid<T,DIM,Encoder> Grid;
  static const int CO_DIM2=DIM-1-DIM2;
  static void sampleStencil(const VecDIMd& x,const VecDIMd& x0,const VecDIMi& stride,const VecDIMd& invCellSz,sizeType off,VecSTENCILd& weights,VecSTENCILi& indices,T w0,char offStencil);
  static void computeStrideWithWarp(const VecDIMi& id,const VecDIMi& nrPoint,const VecDIMi& stride,const VecDIMi& warpMode,VecDIMi& strideWithWarp);
  template <typename DATA>
  static typename DATA::Scalar sample(const DATA& data,const VecDIMd& x,const VecDIMd& x0,const VecDIMi& stride,const VecDIMd& invCellSz,sizeType off,T& minV,T& maxV);
  template <typename DATA>
  static typename DATA::Scalar sample(const DATA& data,const VecDIMd& x,const VecDIMd& x0,const VecDIMi& stride,const VecDIMd& invCellSz,sizeType off);
  template <typename DATA>
  static void sampleVel(const Grid& grd,const DATA& data,VecDIMd& x,VecDIMi& id,VecDIMd& vel);
  static void getNeigh(const VecDIMi& nrPoint,const VecDIMi& stride,const VecDIMi& warpMode,const VecDIMi& id,sizeType off,VecSTENCILi& neigh,int& nr);
  static void getVal(const vector<T> vals[DIM],const VecDIMi& id,VecDIMd& val);
  static void getValBut(unsigned char D,const vector<T> valButs[DIM],const vector<T> vals[DIM],const VecDIMi& id,VecDIMd& val);
  static void stride(VecDIMi& stride,const VecDIMi& nrPoint);
  static void decode(sizeType id,const VecDIMi& stride,VecDIMi& decoded);
  static void getStencil(const VecDIMi& stride,sizeType off,VecSTENCILi& indices,unsigned char offStencil);
  //iterator
  static void advanceIterator(const VecDIMi& stride,const VecDIMi& nrPoint,const VecDIMi& warpCountMax,
                              const VecDIMd& warpRange,const VecDIMd& warpCompensate0,VecDIMd& warpCompensate,
                              const VecDIMi& id0,const VecDIMi& id1,VecDIMi& id,VecDIMi& warpCount,sizeType& off);
  static void advanceIteratorFaceCell(const VecDIMi& strideF,const VecDIMi& nrPointF,
                                      const VecDIMi& strideC,const VecDIMi& nrPointC,const VecDIMi& warpCountMax,
                                      const VecDIMd& warpRange,const VecDIMd& warpCompensate0,VecDIMd& warpCompensate,
                                      const VecDIMi& id0,const VecDIMi& id1,VecDIMi& id,VecDIMi& warpCount,sizeType& off,sizeType& offC);
};
template <typename T,int DIM,typename Encoder>
struct OperatorTraits<T,DIM,Encoder,0> : public TypeTraits<T,DIM> {
  using typename TypeTraits<T,DIM>::VecDIMd;
  using typename TypeTraits<T,DIM>::VecDIMb;
  using typename TypeTraits<T,DIM>::VecDIMi;
  using typename TypeTraits<T,DIM>::VecSTENCILd;
  using typename TypeTraits<T,DIM>::VecSTENCILi;
  typedef VariableSizedGrid<T,DIM,Encoder> Grid;
  static const int CO_DIM2=DIM-1;
  static void sampleStencil(const VecDIMd& x,const VecDIMd& x0,const VecDIMi& stride,const VecDIMd& invCellSz,sizeType off,VecSTENCILd& weights,VecSTENCILi& indices,T w0,char offStencil);
  static void computeStrideWithWarp(const VecDIMi& id,const VecDIMi& nrPoint,const VecDIMi& stride,const VecDIMi& warpMode,VecDIMi& strideWithWarp);
  template <typename DATA>
  static typename DATA::Scalar sample(const DATA& data,const VecDIMd& x,const VecDIMd& x0,const VecDIMi& stride,const VecDIMd& invCellSz,sizeType off,T& minV,T& maxV);
  template <typename DATA>
  static typename DATA::Scalar sample(const DATA& data,const VecDIMd& x,const VecDIMd& x0,const VecDIMi& stride,const VecDIMd& invCellSz,sizeType off);
  template <typename DATA>
  static void sampleVel(const Grid& grd,const DATA& data,VecDIMd& x,VecDIMi& id,VecDIMd& vel);
  static void getNeigh(const VecDIMi& nrPoint,const VecDIMi& stride,const VecDIMi& warpMode,const VecDIMi& id,sizeType off,VecSTENCILi& neigh,int& nr);
  static void getVal(const vector<T> vals[DIM],const VecDIMi& id,VecDIMd& val);
  static void getValBut(unsigned char D,const vector<T> valButs[DIM],const vector<T> vals[DIM],const VecDIMi& id,VecDIMd& val);
  static void stride(VecDIMi& stride,const VecDIMi& nrPoint);
  static void decode(sizeType id,const VecDIMi& stride,VecDIMi& decoded);
  static void getStencil(const VecDIMi& stride,sizeType off,VecSTENCILi& indices,unsigned char offStencil);
  //iterator
  static void advanceIterator(const VecDIMi& stride,const VecDIMi& nrPoint,const VecDIMi& warpCountMax,
                              const VecDIMd& warpRange,const VecDIMd& warpCompensate0,VecDIMd& warpCompensate,
                              const VecDIMi& id0,const VecDIMi& id1,VecDIMi& id,VecDIMi& warpCount,sizeType& off);
  static void advanceIteratorFaceCell(const VecDIMi& strideF,const VecDIMi& nrPointF,
                                      const VecDIMi& strideC,const VecDIMi& nrPointC,const VecDIMi& warpCountMax,
                                      const VecDIMd& warpRange,const VecDIMd& warpCompensate0,VecDIMd& warpCompensate,
                                      const VecDIMi& id0,const VecDIMi& id1,VecDIMi& id,VecDIMi& warpCount,sizeType& off,sizeType& offC);
};
//VariableSizedGrid
template <typename T,int DIM,typename Encoder>
struct VariableSizedGrid : public Encoder, public TypeTraits<T,DIM>, public Serializable {
  using typename TypeTraits<T,DIM>::VecDIMd;
  using typename TypeTraits<T,DIM>::VecDIMb;
  using typename TypeTraits<T,DIM>::VecDIMi;
  using typename TypeTraits<T,DIM>::VecSTENCILd;
  using typename TypeTraits<T,DIM>::VecSTENCILi;
  typedef VariableSizedGrid<T,DIM,Encoder> Grid;
  VariableSizedGrid();
  VariableSizedGrid(const BBox<T>& bbInterior,const Vec3i& nrCellInterior,const BBox<sizeType>& extend,int verbose,const Vec3i* wrapMode=NULL);
  VariableSizedGrid(const BBox<T,DIM>& bbInterior,const VecDIMi& nrCellInterior,const BBox<sizeType,DIM>& extend,const Vec3i* wrapMode=NULL);
  void reset(const BBox<T>& bbInterior,const Vec3i& nrCellInterior,const BBox<sizeType>& extend,int verbose,const Vec3i* wrapMode=NULL);
  void reset(const BBox<T,DIM>& bbInterior,const VecDIMi& nrCellInterior,const BBox<sizeType,DIM>& extend,const Vec3i* wrapMode=NULL);
  void reset(const VariableSizedGrid<T,DIM,Encoder>& refGrid,BBox<T,DIM> focus,int extendBD);
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<Serializable> copy() const override;
  //debug
  static void genIteratorTestRange(const VecDIMi& nrPoint,const VecDIMi& warpMode,vector<pair<VecDIMi,VecDIMi> >& ranges);
  static void debugIteratorNodal(const Vec3i& warpMode);
  static void debugIteratorCellCenter(const Vec3i& warpMode);
  static void debugIteratorMACVel(const Vec3i& warpMode);
  static void debugIteratorAllType(const Vec3i& warpMode);
  static void debugIteratorAllWarp();
  //debug focus
  static void debugFocus(const Vec3i& warpFocus);
  static void debugFocusAllWarp();
  //handle warp related function
  static string printWarp(const Vec3i& warpMode);
  T handleWarp(T& x,unsigned char D) const;
  VecDIMd handleWarp(VecDIMd x) const;
  void handleWarpDiff(T& x,unsigned char D) const;
  VecDIMd handleWarpDiff(VecDIMd x) const;
  VecDIMi handleWarp(const VecDIMi& id,const VecDIMi& nrPoint,VecDIMi* warpCompensate=NULL) const;
  //Nodal operator
  BBox<T,DIM> getBBNodal() const;
  void sampleStencilNodal(VecDIMd& x,VecDIMi& id,VecSTENCILd& weight,VecSTENCILi& indices,VecDIMb& ob) const;
  template <typename TDATA>
  TDATA sampleNodal(const NodalData<TDATA>& data,VecDIMd& x,VecDIMi& id,T& minV,T& maxV,VecDIMb& ob) const;
  template <typename TDATA>
  TDATA sampleNodal(const NodalData<TDATA>& data,VecDIMd& x,VecDIMi& id) const;
  template <typename TDATA>
  void sampleNodal(unsigned char D,sizeType off,Eigen::Matrix<TDATA,-1,1>& data,const VariableSizedGrid<T,DIM,Encoder>& refGrid,const Eigen::Matrix<TDATA,-1,1>& refData) const;
  template <typename TDATA>
  void sampleNodal(NodalData<TDATA>& data,const VariableSizedGrid<T,DIM,Encoder>& refGrid,const NodalData<TDATA>& refData) const;
  template <typename TDATA>
  void sampleNodalBF(NodalData<TDATA>& data,const VariableSizedGrid<T,DIM,Encoder>& refGrid,const NodalData<TDATA>& refData) const;
  void indexSafeNodal(VecDIMd& x,VecDIMi& id,VecDIMb& ob) const;
  void indexNodal(VecDIMd& x,VecDIMi& id) const;
  void getInvCellSzNodal(const VecDIMi& id,VecDIMd& invCellSz) const;
  void getCellSzNodal(const VecDIMi& id,VecDIMd& cellSz) const;
  void getPtNodal(const VecDIMi& id,VecDIMd& pt) const;
  void getPtNodal(sizeType off,VecDIMd& pt) const;
  void decodeNodal(sizeType id,VecDIMi& decoded) const;
  int indexSafeDIMNodal(T& x,unsigned char D,unsigned char& ob) const;
  int indexDIMNodal(T& x,unsigned char D) const;
  //debug
  static void debugNodal(const Vec3i& warpMode);
  static void debugNodalAllWarp();
  //CellCenter operator
  BBox<T,DIM> getBBCellCenter() const;
  void sampleStencilCellCenter(VecDIMd& x,VecDIMi& id,VecSTENCILd& weight,VecSTENCILi& indices,VecDIMb& ob) const;
  template <typename TDATA>
  TDATA sampleCellCenter(const CellCenterData<TDATA>& data,VecDIMd& x,VecDIMi& id,T& minV,T& maxV,VecDIMb& ob) const;
  template <typename TDATA>
  TDATA sampleCellCenter(const CellCenterData<TDATA>& data,VecDIMd& x,VecDIMi& id) const;
  template <typename TDATA>
  void sampleCellCenter(unsigned char D,sizeType off,Eigen::Matrix<TDATA,-1,1>& data,const VariableSizedGrid<T,DIM,Encoder>& refGrid,const Eigen::Matrix<TDATA,-1,1>& refData) const;
  template <typename TDATA>
  void sampleCellCenter(CellCenterData<TDATA>& data,const VariableSizedGrid<T,DIM,Encoder>& refGrid,const CellCenterData<TDATA>& refData) const;
  template <typename TDATA>
  void sampleCellCenterBF(CellCenterData<TDATA>& data,const VariableSizedGrid<T,DIM,Encoder>& refGrid,const CellCenterData<TDATA>& refData) const;
  void indexSafeCellCenter(VecDIMd& x,VecDIMi& id,VecDIMb& ob) const;
  void indexCellCenter(VecDIMd& x,VecDIMi& id) const;
  void getInvCellSzCellCenter(const VecDIMi& id,VecDIMd& invCellSz) const;
  void getCellSzCellCenter(const VecDIMi& id,VecDIMd& cellSz) const;
  void getPtCellCenter(const VecDIMi& id,VecDIMd& pt) const;
  void getPtCellCenter(sizeType off,VecDIMd& pt) const;
  void decodeCellCenter(sizeType id,VecDIMi& decoded) const;
  int indexSafeDIMCellCenter(T& x,unsigned char D,unsigned char& ob) const;
  int indexDIMCellCenter(T& x,unsigned char D) const;
  //debug
  static void debugCellCenter(const Vec3i& warpMode);
  static void debugCellCenterAllWarp();
  //MACVel operator
  BBox<T,DIM> getBBMACVel(unsigned char D) const;
  BBox<T,DIM> getCellMACVel(unsigned char D,const VecDIMi& id) const;
  void sampleStencilMACVel(unsigned char D,VecDIMd& x,VecDIMi& id,VecSTENCILd& weight,VecSTENCILi& indices,VecDIMb& ob) const;
  template <typename TDATA>
  TDATA sampleMACVel(unsigned char D,const MACVelData<DIM,TDATA>& data,VecDIMd& x,VecDIMi& id,T& minV,T& maxV,VecDIMb& ob) const;
  template <typename TDATA>
  TDATA sampleMACVel(unsigned char D,const MACVelData<DIM,TDATA>& data,VecDIMd& x,VecDIMi& id) const;
  template <typename TDATA>
  void sampleMACVel(unsigned char D,unsigned char D2,sizeType off,Eigen::Matrix<TDATA,-1,1>& data,const VariableSizedGrid<T,DIM,Encoder>& refGrid,const Eigen::Matrix<TDATA,-1,1>& refData) const;
  template <typename TDATA>
  void sampleMACVel(unsigned char D,MACVelData<DIM,TDATA>& data,const VariableSizedGrid<T,DIM,Encoder>& refGrid,const MACVelData<DIM,TDATA>& refData) const;
  template <typename TDATA>
  void sampleMACVel(MACVelData<DIM,TDATA>& data,const VariableSizedGrid<T,DIM,Encoder>& refGrid,const MACVelData<DIM,TDATA>& refData) const;
  template <typename TDATA>
  void sampleMACVelBF(MACVelData<DIM,TDATA>& data,const VariableSizedGrid<T,DIM,Encoder>& refGrid,const MACVelData<DIM,TDATA>& refData) const;
  template <typename TDATA>
  void sampleMACVel(const MACVelData<DIM,TDATA>& data,VecDIMd& x,VecDIMi& id,VecDIMd& vel) const;
  void indexSafeMACVel(unsigned char D,VecDIMd& x,VecDIMi& id,VecDIMb& ob) const;
  void indexMACVel(unsigned char D,VecDIMd& x,VecDIMi& id) const;
  void getInvCellSzMACVel(unsigned char D,const VecDIMi& id,VecDIMd& invCellSz) const;
  void getCellSzMACVel(unsigned char D,const VecDIMi& id,VecDIMd& cellSz) const;
  void getCellSzCtrMACVel(unsigned char D,const VecDIMi& id,VecDIMd& cellSz) const;
  T getFaceAreaMACVel(unsigned char D,const VecDIMi& id) const;
  void getPtMACVel(unsigned char D,const VecDIMi& id,VecDIMd& pt) const;
  void getPtMACVel(unsigned char D,sizeType off,VecDIMd& pt) const;
  void decodeMACVel(unsigned char D,sizeType id,VecDIMi& decoded) const;
  int indexSafeDIMMACVel(unsigned char D,T& x,unsigned char D0,unsigned char& ob) const;
  int indexDIMMACVel(unsigned char D,T& x,unsigned char D0) const;
  int getNeighMACVel(unsigned char D,const VecDIMi& id,sizeType off,VecSTENCILi& neigh) const;
  //debug
  static void debugMACVel(const Vec3i& warpMode);
  static void debugMACVelAllWarp();
  //data: Nodal
  VecDIMi _nrPointNodalWithWarp,_nrPointNodal,_strideNodal;
  vector<T> _x0Nodals[DIM],_cellSzNodals[DIM],_invCellSzNodals[DIM];
  //data: CellCenter
  VecDIMi _nrPointCellCenterWithWarp,_nrPointCellCenter,_strideCellCenter;
  vector<T> _x0CellCenters[DIM],_cellSzCellCenters[DIM],_invCellSzCellCenters[DIM];
  //data: MACVel
  VecDIMi _nrPointMACVelWithWarp[DIM],_nrPointMACVel[DIM],_strideMACVel[DIM];
  //data
  VecDIMd _cellSz0,_invCellSz0;
  BBox<sizeType,DIM> _offset;
  VecDIMi _warpMode;
private:
  //debug
  static Grid createTestGrid(const Vec3i& warpMode);
};

PRJ_END

#endif
