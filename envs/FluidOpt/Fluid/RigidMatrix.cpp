#include "RigidMatrix.h"

USE_PRJ_NAMESPACE

void MJAbstract::clear()
{
  _col.clear();
  _colData.clear();
  _index.clear();
}
void MJAbstract::insert(sizeType col,const Vec6d& data)
{
  if(_index.find(col) == _index.end()) {
    sizeType id=(sizeType)_col.size();
    _index[col]=id;
    _col.push_back(col);
    _colData.push_back(data);
  } else _colData[_index[col]]+=data;
}
void MJAbstract::toMatrix(FixedSparseMatrix<scalarD,Kernel<scalarD> >& m) const
{
  Mat6d InvMDt;
  getInvMDt(InvMDt);
  vector<Eigen::Triplet<scalarD,sizeType> > trips;
  for(sizeType i=0; i<(sizeType)_colData.size(); i++)
    for(sizeType j=0; j<(sizeType)_colData.size(); j++) {
      scalarD val=_colData[i].transpose()*InvMDt*_colData[j];
      trips.push_back(Eigen::Triplet<scalarD,sizeType>(_col[i],_col[j],val));
    }
  m.buildFromTriplets(trips.begin(),trips.end());
}
void MJAbstract::multiplyJ(const Vec& b,Vec6d& out) const
{
  out.setZero();
  multiplyJAdd(b,out);
}
void MJAbstract::multiplyJSubtract(const Vec& b,Vec6d& out) const
{
  for(sizeType i=0; i<(sizeType)_col.size(); i++)
    out-=_colData[i]*b[_col[i]];
}
void MJAbstract::multiplyJAdd(const Vec& b,Vec6d& out) const
{
  for(sizeType i=0; i<(sizeType)_col.size(); i++)
    out+=_colData[i]*b[_col[i]];
}
void MJAbstract::multiplyJT(const Vec6d& b,Vec& out) const
{
  out.setZero();
  multiplyJTAdd(b,out);
}
void MJAbstract::multiplyJTSubtract(const Vec6d& b,Vec& out) const
{
  for(sizeType i=0; i<(sizeType)_col.size(); i++)
    out[_col[i]]-=_colData[i].dot(b);
}
void MJAbstract::multiplyJTAdd(const Vec6d& b,Vec& out) const
{
  for(sizeType i=0; i<(sizeType)_col.size(); i++)
    out[_col[i]]+=_colData[i].dot(b);
}
void MJ::multiplyInvMDt(const Vec6d& b,Vec6d& out) const
{
  out.block<3,1>(0,0)=b.block<3,1>(0,0)*_invMDt;
  out.block<3,1>(3,0)=_invIDt*b.block<3,1>(3,0);
}
void MJ::getInvMDt(Mat6d& InvMDt) const
{
  InvMDt.setIdentity();
  InvMDt(0,0)=InvMDt(1,1)=InvMDt(2,2)=_invMDt;
  InvMDt.block<3,3>(3,3)=_invIDt;
}
void GeneralizedMJ::multiplyInvMDt(const Vec6d& b,Vec6d& out) const
{
  out=_invMDt*b;
}
void GeneralizedMJ::getInvMDt(Mat6d& InvMDt) const
{
  InvMDt=_invMDt;
}
//RigidBodyKrylovMatrix
template <typename MJ_TYPE>
RigidBodyKrylovMatrix<MJ_TYPE>::RigidBodyKrylovMatrix(const FixedSparseMatrix<scalarD,Kernel<scalarD> > &matrix,const vector<MJ_TYPE>& MJs,const vector<sizeType>& MJIDs)
  :DefaultKrylovMatrix<scalarD,Kernel<scalarD> >(matrix),_MJs(MJs),_MJIDs(MJIDs) {}
template <typename MJ_TYPE>
void RigidBodyKrylovMatrix<MJ_TYPE>::multiply(const Vec& b,Vec& out) const
{
  Vec6d Jb,MJb;
  _fixedMatrix.multiply(b,out);
  for(sizeType i=0; i<(sizeType)_MJs.size(); i++) {
    const MJ_TYPE& body=_MJs[i];
    body.multiplyJ(b,Jb);
    body.multiplyInvMDt(Jb,MJb);
    body.multiplyJTAdd(MJb,out);
  }
}
template <typename MJ_TYPE>
void RigidBodyKrylovMatrix<MJ_TYPE>::applyForce(const CellCenterData<scalarD>& pressure,RigidSolver& solver,scalar dt)
{
  Vec6d ft;
  _forces.assign(solver.nrBody(),make_pair(Vec3::Zero(),Vec3::Zero()));
  for(sizeType i=0; i<(sizeType)_MJIDs.size(); i++) {
    const MJ_TYPE& mj=_MJs[i];
    mj.multiplyJ(pressure._data,ft);

    RigidBody& body=solver.getBody(_MJIDs[i]);
    //you have to directly change momentum and angular-momentum
    Vec3 force((scalar)ft[0],(scalar)ft[1],(scalar)ft[2]);
    Vec3 torque((scalar)ft[3],(scalar)ft[4],(scalar)ft[5]);
    body.setP(body.p()+force*dt);
    body.setL(body.L()+torque*dt);
    //record the force for later use
    _forces[_MJIDs[i]]=make_pair(force,torque);
  }
}
template <typename MJ_TYPE>
void RigidBodyKrylovMatrix<MJ_TYPE>::applyForceMINRES(const CellCenterData<scalarD>& pressure,RigidSolver& solver,scalar dt)
{
  Vec6d ft;
  _forces.assign(solver.nrBody(),make_pair(Vec3::Zero(),Vec3::Zero()));
  for(sizeType i=0; i<(sizeType)_MJIDs.size(); i++) {
    const MJ_TYPE& mj=_MJs[i];
    mj.multiplyJ(pressure._data,ft);

    RigidBody& body=solver.getBody(_MJIDs[i]);
    //you have to directly change momentum and angular-momentum
    Vec3 force((scalar)ft[0],(scalar)ft[1],(scalar)ft[2]);
    Vec3 torque((scalar)ft[3],(scalar)ft[4],(scalar)ft[5]);
    body.setP(body.p()+body.getM()*force*dt);
    body.setL(body.L()+body.I()*torque*dt);
    //record the force for later use
    _forces[_MJIDs[i]]=make_pair(force,torque);
  }
}
template <typename MJ_TYPE>
void RigidBodyKrylovMatrix<MJ_TYPE>::toFixed(FixedSparseMatrix<scalarD,Kernel<scalarD> > &matrix)
{
  Vec6d out;
  scalarD val=0;
  sizeType nrMJ=(sizeType)_MJs.size();
  vector<Eigen::Triplet<scalarD,sizeType> > trips;
  trips.reserve(nrMJ*nrMJ);
  for(sizeType b=0; b<nrMJ; b++) {
    const MJ_TYPE& mj=_MJs[b];
    for(sizeType i=0; i<nrMJ; i++)
      for(sizeType j=0; j<=i; j++) {
        mj.multiplyInvMDt(mj._colData[i],out);
        val=out.dot(mj._colData[j]);
        trips.push_back(Eigen::Triplet<scalarD,sizeType>(Eigen::Triplet<scalarD,sizeType>(mj._col[i],mj._col[j],val)));
        if(i != j)
          trips.push_back(Eigen::Triplet<scalarD,sizeType>(Eigen::Triplet<scalarD,sizeType>(mj._col[j],mj._col[i],val)));
      }
  }
  matrix.resize(_fixedMatrix.rows());
  matrix.buildFromTriplets(trips.begin(),trips.end());
  matrix.add(_fixedMatrix);
}
//RigidBodyKrylovMatrixSparse
template <typename MJ_TYPE>
RigidBodyKrylovMatrixSparse<MJ_TYPE>::RigidBodyKrylovMatrixSparse(const FixedSparseMatrix<scalarD,Kernel<scalarD> > &matrix,const vector<MJ_TYPE>& MJs,const vector<sizeType>& MJIDs)
  :RigidBodyKrylovMatrix<MJ_TYPE>(matrix,MJs,MJIDs) {}
template <typename MJ_TYPE>
void RigidBodyKrylovMatrixSparse<MJ_TYPE>::multiply(const Vec& b,Vec& out) const
{
  Vec6d Jb;
  out.setZero();
  _fixedMatrix.multiply(b,out);
  sizeType sSize=_fixedMatrix.rows();
  for(sizeType i=0; i<(sizeType)_MJs.size(); i++) {
    const MJ_TYPE& body=_MJs[i];
    body.multiplyInvMDt(b.template segment<6>(sSize+i*6),Jb);
    body.multiplyJAdd(b,Jb);
    out.template segment<6>(sSize+i*6)=Jb;
    body.multiplyJTAdd(b.template segment<6>(sSize+i*6),out);
  }
}
template <typename MJ_TYPE>
sizeType RigidBodyKrylovMatrixSparse<MJ_TYPE>::n() const
{
  return _fixedMatrix.rows()+(sizeType)_MJs.size()*6;
}
//RigidFluidFilter
RigidFluidFilter::RigidFluidFilter(sizeType sSize):_sSize(sSize) {}
bool RigidFluidFilter::operator()(sizeType r,sizeType c) const
{
  return (r<_sSize && c<_sSize) ? false : true;
}
void RigidFluidPrecondition::reset(const vector<MJ>& MJs,sizeType sSize)
{
  _sSize=sSize;
  _bodies.resize(MJs.size());
  for(sizeType i=0; i<(sizeType)MJs.size(); i++) {
    _bodies[i].setIdentity();
    _bodies[i].block<3,3>(0,0)=MJs[i]._invMDt*Mat3d::Identity();
    _bodies[i].block<3,3>(3,3)=MJs[i]._invIDt;
    _bodies[i]=-_bodies[i].inverse().eval();
  }
  setFilter(boost::shared_ptr<MIC0Solver<scalarD>::MatrixFilter>(new RigidFluidFilter(sSize)));
}
void RigidFluidPrecondition::reset(const vector<GeneralizedMJ>& MJs,sizeType sSize)
{
  _sSize=sSize;
  _bodies.resize(MJs.size());
  for(sizeType i=0; i<(sizeType)MJs.size(); i++)
    _bodies[i]=-MJs[i]._invMDt.inverse().eval();
  setFilter(boost::shared_ptr<MIC0Solver<scalarD>::MatrixFilter>(new RigidFluidFilter(sSize)));
}
Solver<scalarD,Kernel<scalarD> >::SOLVER_RESULT RigidFluidPrecondition::solve(const Vec& rhs,Vec& result)
{
  Solver<scalarD,Kernel<scalarD> >::SOLVER_RESULT ret=MIC0Solver<scalarD>::solve(rhs,result);
  for(sizeType i=_sSize,j=0; j<(sizeType)_bodies.size(); i+=6,j++)
    result.segment<6>(i)=_bodies[j]*rhs.segment<6>(i);
  return ret;
}
//instance
template class RigidBodyKrylovMatrix<MJ>;
template class RigidBodyKrylovMatrix<GeneralizedMJ>;
template class RigidBodyKrylovMatrixSparse<MJ>;
template class RigidBodyKrylovMatrixSparse<GeneralizedMJ>;
