#include "SmokeSolver.h"
#include "VariableSizedGridOp.h"
#include "VariableSizedGridOpProject.h"

USE_PRJ_NAMESPACE

template <int DIM,typename Encoder>
SmokeSolver<DIM,Encoder>::SmokeSolver()
{
  Serializable::setType(typeid(SmokeSolver<DIM,Encoder>).name());
}
template <int DIM,typename Encoder>
bool SmokeSolver<DIM,Encoder>::read(istream& is,IOData* dat)
{
  LiquidSolverFLIP<DIM,Encoder>::read(is,dat);
  _T.read(is,dat);
  _rho.read(is,dat);
  readBinaryData(_damping,is);
  readBinaryData(_velDamping,is);
  readBinaryData(_TAmb,is);
  readBinaryData(_beta,is);
  readBinaryData(_order,is);
  return is.good();
}
template <int DIM,typename Encoder>
bool SmokeSolver<DIM,Encoder>::write(ostream& os,IOData* dat) const
{
  LiquidSolverFLIP<DIM,Encoder>::write(os,dat);
  _T.write(os,dat);
  _rho.write(os,dat);
  writeBinaryData(_damping,os);
  writeBinaryData(_velDamping,os);
  writeBinaryData(_TAmb,os);
  writeBinaryData(_beta,os);
  writeBinaryData(_order,os);
  return os.good();
}
template <int DIM,typename Encoder>
boost::shared_ptr<Serializable> SmokeSolver<DIM,Encoder>::copy() const
{
  return boost::shared_ptr<Serializable>(new SmokeSolver<DIM,Encoder>);
}
//setup
template <int DIM,typename Encoder>
void SmokeSolver<DIM,Encoder>::initDomain(BBox<scalar> bb,const Vec3& cellSz,const Vec3i* warpMode)
{
  Parent::initDomain(bb,cellSz,warpMode);
  _damping=1.0f;
  _velDamping=1.0f;
  _TAmb=20.0f;
  _beta=0.01f;
  _order=2;

  _T.reset(_grd);
  _rho.reset(_grd);

  _T._data.setConstant(_TAmb);
  _rho._data.setConstant(0);
  _phi._data.setConstant(-1);
}
template <int DIM,typename Encoder>
void SmokeSolver<DIM,Encoder>::focusOn(const BBox<scalar>& region)
{
  BBox<scalar,DIM> regionDIM;
  regionDIM._minC=assignVariableTo<VecDIMd,Vec3>(region._minC);
  regionDIM._maxC=assignVariableTo<VecDIMd,Vec3>(region._maxC);
  VariableSizedGrid<scalar,DIM,Encoder> grd;
  grd.reset(_grd0,regionDIM,2);
  if((grd._offset.getExtent().array() > 2).all()) {
    //phi
    CellCenterData<scalar> phiNew(grd);
    grd.sampleCellCenter(phiNew,_grd,_phi);
    _phi=phiNew;
    //weight
    _weight.reset(grd);
    VariableSizedGridOpProject<scalar,DIM,Encoder>::sampleWeightMACVel(grd,_weight,_nodalSolidPhi);
    //vel
    MACVelData<DIM,scalar> velNew(grd);
    grd.sampleMACVel(velNew,_grd,_vel);
    _vel=velNew;
    //T
    CellCenterData<scalar> TNew(grd);
    grd.sampleCellCenter(TNew,_grd,_T);
    _T=TNew;
    //rho
    CellCenterData<scalar> rhoNew(grd);
    grd.sampleCellCenter(rhoNew,_grd,_rho);
    _rho=rhoNew;
    //grd
    _grd=grd;
  } else {
    WARNING("Focus region too small!")
  }
}
template <int DIM,typename Encoder>
void SmokeSolver<DIM,Encoder>::getFeature(VectorField& field,scalar sz)
{
  if(_rigid->nrBody() < 1) {
    INFO("Cannot call getFeature without RigidBodies!")
    exit(EXIT_FAILURE);
  }

  Vec3i idf;
  VecDIMi id;
  VecDIMd vel,pt;
  BBox<scalar> bb;
  BBox<scalar,DIM> bbGrd;

  if(sz >= 0) {
    _rigid->getBody(0).getWorldBB(bb);
    bb.enlargedEps(sz);
  } else {
    bb=_nodalSolidPhi.getBB();
  }
  VecDIMi nrCell=ceilV(VecDIMd((bb.getExtent().template segment<DIM>(0).array()/_grd._cellSz0.array()).matrix()));
  field.reset(assignVariableTo<Vec3i,VecDIMi>(nrCell),bb,Vec3::Zero(),true,1);

  bbGrd=_grd.getBBNodal();
  for(sizeType i=0; i<field.getNrPoint().prod(); i++) {
    idf=field.getIndex(i);
    pt=field.getPt(idf).template segment<DIM>(0);
    if(bbGrd.contain(pt,(sizeType)DIM)) {
      _grd.sampleMACVel(_vel,pt,id,vel);
      field[i].template segment<DIM>(0)=vel;
    }
  }
}
//getter
template <int DIM,typename Encoder>
const CellCenterData<scalar>& SmokeSolver<DIM,Encoder>::getT() const
{
  return _T;
}
template <int DIM,typename Encoder>
const CellCenterData<scalar>& SmokeSolver<DIM,Encoder>::getRho() const
{
  return _rho;
}
template <int DIM,typename Encoder>
void SmokeSolver<DIM,Encoder>::setVelDamping(scalar velDamping)
{
  _velDamping=velDamping;
}
template <int DIM,typename Encoder>
void SmokeSolver<DIM,Encoder>::setDamping(scalar damping)
{
  _damping=damping;
}
//solver
template <int DIM,typename Encoder>
sizeType SmokeSolver<DIM,Encoder>::advance(const scalar& dt)
{
  _rigid->clearContactFlag();
  _forceTorque.clear();
  CellCenterData<scalar> tmp(_grd);
  MACVelData<DIM,scalar> velTmp(_grd);
  CellCenterData<unsigned char> tag(_grd);
  MACVelData<DIM,unsigned char> valid(_grd);
  sizeType index=0;
  scalar t=0;
  while(t < dt) {
    //compute timestep
    scalar substep=Parent::getSubstep(t,dt);
    if(_rigid && _rigid->nrBody() > 0)
      _rigid->advanceVelocity(substep,Parent::getGlobalTime()+t);
    Parent::computePSet();
    //add external force
    scalar g=-_rigid->getGravity().dot(Vec3::UnitY());
    VariableSizedGridOp<scalar,DIM,Encoder>::addExternalForce(_grd,_vel,&_T,g,_beta,_TAmb,substep);
    VariableSizedGridOp<scalar,DIM,Encoder>::addDamping(_rho,_damping,0,substep);
    VariableSizedGridOp<scalar,DIM,Encoder>::addDamping(_T,_damping,_TAmb,substep);
    VariableSizedGridOp<scalar,DIM,Encoder>::addDamping(_vel,_velDamping,substep);
    //add source region
    sampleSmokeSource(Parent::getGlobalTime()+t);
    //project
    Parent::makeDivergenceFree(substep,tag,valid);
    _vel.clamp(Parent::getMaxVelocity());
    //advect particle
    if(_rigid && _rigid->nrBody() > 0)
      _rigid->advancePosition(substep);
    if(_order == 1) {
      VariableSizedGridOp<scalar,DIM,Encoder>::advectSemiLagrangian(_grd,_rho,tmp,_vel,substep);
      _rho=tmp;
      VariableSizedGridOp<scalar,DIM,Encoder>::advectSemiLagrangian(_grd,_T,tmp,_vel,substep);
      _T=tmp;
      VariableSizedGridOp<scalar,DIM,Encoder>::advectSemiLagrangian(_grd,_vel,velTmp,_vel,substep);
      _vel=velTmp;
    } else {
      VariableSizedGridOp<scalar,DIM,Encoder>::advectMacCormack(_grd,_rho,tmp,_vel,tag,substep);
      _rho=tmp;
      VariableSizedGridOp<scalar,DIM,Encoder>::advectMacCormack(_grd,_T,tmp,_vel,tag,substep);
      _T=tmp;
      VariableSizedGridOp<scalar,DIM,Encoder>::advectMacCormack(_grd,_vel,velTmp,_vel,valid,substep);
      _vel=velTmp;
    }
    index++;
  }
  Parent::advanceTime(dt);
  return index;
}
template <int DIM,typename Encoder>
void SmokeSolver<DIM,Encoder>::sampleSmokeSource(const scalar& globalTime)
{
  typedef vector<boost::shared_ptr<SourceRegion> >::iterator iter;
  for(iter beg=_sources.begin(),end=_sources.end(); beg!=end; beg++) {
    (*beg)->setCellSz(_nodalSolidPhi.getCellSize());
    (*beg)->sampleRegion<DIM,Encoder>(_grd,&_rho,&_T,_vel,globalTime);
  }
}
//instance
#define INSTANCE(DIM,TYPE,SPARSITY) \
template class SmokeSolver<DIM,Encoder##TYPE<SPARSITY> >;
INSTANCE_ALL
