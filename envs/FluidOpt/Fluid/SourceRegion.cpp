#include "SourceRegion.h"

USE_PRJ_NAMESPACE

//Counter
class Counter : public CollisionFunc<ParticleN<scalar> >
{
public:
  Counter(sizeType& count,const Vec3& pos,const Vec3& cellSzHF)
    :_count(count),_pos(pos),_cellSzHF(cellSzHF) {}
  void operator()(const ParticleN<scalar>& p,const sizeType& id) {
    const Vec3 d=p._pos-_pos;
    scalar total=0.0f;
    for(sizeType i=0; i<3; i++)
      total+=abs(d[i]);
    if(total < _cellSzHF.sum())
      _count++;
  }
protected:
  sizeType& _count;
  Vec3 _pos;
  Vec3 _cellSzHF;
};
//SourceRegionFilter
SourceRegion::SourceRegionFilter::SourceRegionFilter():Serializable(typeid(SourceRegionFilter).name()) {}
void SourceRegion::SourceRegionFilter::resetFilter(const scalar& globalTime) {}
void SourceRegion::SourceRegionFilter::filter(ParticleSetN::ParticleType& p,const scalar& globalTime) {}
bool SourceRegion::SourceRegionFilter::read(istream& is,IOData* dat)
{
  return is.good();
}
bool SourceRegion::SourceRegionFilter::write(ostream& os,IOData* dat) const
{
  return os.good();
}
boost::shared_ptr<Serializable> SourceRegion::SourceRegionFilter::copy() const
{
  return boost::shared_ptr<Serializable>(new SourceRegionFilter);
}
//SourceRegion
SourceRegion::SourceRegion()
  :Serializable(typeid(SourceRegion).name()),_rho(1.0f),_T(0.0f)
{
  setSource(true);
  setAlterVel(true);
  setAlterProp(true);
}
SourceRegion::SourceRegion(const scalar& rho,const scalar& T)
  :Serializable(typeid(SourceRegion).name()),_rho(rho),_T(T)
{
  setSource(true);
  setAlterVel(true);
  setAlterProp(true);
}
bool SourceRegion::read(istream& is,IOData* dat)
{
  registerType<SourceRegionFilter>(dat);
  readBinaryData(_filter,is,dat);
  _region.read(is,dat);
  _vel.read(is,dat);
  readVector(_valid,is);
  readBinaryData(_isSource,is);
  readBinaryData(_alterProp,is);
  readBinaryData(_alterVel,is);
  readBinaryData(_nrParticlePerGrid,is);
  readBinaryData(_cellSz,is);
  readBinaryData(_rho,is);
  readBinaryData(_T,is);
  return is.good();
}
bool SourceRegion::write(ostream& os,IOData* dat) const
{
  registerType<SourceRegionFilter>(dat);
  writeBinaryData(_filter,os,dat);
  _region.write(os,dat);
  _vel.write(os,dat);
  writeVector(_valid,os);
  writeBinaryData(_isSource,os);
  writeBinaryData(_alterProp,os);
  writeBinaryData(_alterVel,os);
  writeBinaryData(_nrParticlePerGrid,os);
  writeBinaryData(_cellSz,os);
  writeBinaryData(_rho,os);
  writeBinaryData(_T,os);
  return os.good();
}
boost::shared_ptr<Serializable> SourceRegion::copy() const
{
  return boost::shared_ptr<Serializable>(new SourceRegion);
}
ScalarField& SourceRegion::getRegion()
{
  return _region;
}
const ScalarField& SourceRegion::getRegion() const
{
  return _region;
}
MACVelocityField& SourceRegion::getVel()
{
  return _vel;
}
const MACVelocityField& SourceRegion::getVel() const
{
  return _vel;
}
void SourceRegion::setSource(bool isSource)
{
  _isSource=isSource;
}
void SourceRegion::sampleRegion(const ScalarField& solid,const CollisionInterface<scalar,ParticleSetN>& cd,const scalar& rad,ParticleSetN& pSet,const scalar& globalTime)
{
  if(!filterSource(globalTime))
    return;
  if(_isSource)
    sampleSource(solid,cd,rad,pSet,globalTime);
  else sampleSink(pSet,cd);
}
void SourceRegion::sampleSource(const ScalarField& solid,const CollisionInterface<scalar,ParticleSetN>& cd,const scalar& rad,ParticleSetN& pSet,const scalar& globalTime)
{
  static sizeType seed=0;
  const Vec3 cellSz=_cellSz;
  Vec3 cellSzHF=cellSz*0.5f;
  if(cd.getDim() == 2)
    cellSzHF.z()=0.0f;

  if(_filter)
    _filter->resetFilter(globalTime);

  ParticleSetN tmp;
  if(_region.getDim() == 2) {
    for(scalar x=_region.getBB()._minC.x()+cellSzHF.x(); x<=_region.getBB()._maxC.x()-cellSzHF.x(); x+=cellSz.x())
      for(scalar y=_region.getBB()._minC.y()+cellSzHF.y(); y<=_region.getBB()._maxC.y()-cellSzHF.y(); y+=cellSz.y()) {
        Vec3 pos(x,y,0.0f);
        if(_region.sampleSafe2D(pos) > 0.0f)
          continue;

        sizeType count=0;
        Counter cc(count,pos,cellSzHF);
        cd.fill2D(pSet,pos,cellSzHF.squaredNorm(),cc);
        while(count < _nrParticlePerGrid) {
          ParticleSetN::ParticleType p;
          p._pos=pos+Vec3(cellSzHF.x()*RandEngine::randSR(seed++,-1.0f,1.0f),
                          cellSzHF.y()*RandEngine::randSR(seed++,-1.0f,1.0f),0.0f);
          if(solid.sampleSafe(p._pos) > 0.0f) {
            p._vel=_vel.sampleSafe(p._pos);
            if(_filter)_filter->filter(p,globalTime);
            tmp.addParticle(p);
          }
          count++;
        }
      }
  } else {
    for(scalar x=_region.getBB()._minC.x()+cellSzHF.x(); x<=_region.getBB()._maxC.x()-cellSzHF.x(); x+=cellSz.x())
      for(scalar y=_region.getBB()._minC.y()+cellSzHF.y(); y<=_region.getBB()._maxC.y()-cellSzHF.y(); y+=cellSz.y())
        for(scalar z=_region.getBB()._minC.z()+cellSzHF.z(); z<=_region.getBB()._maxC.z()-cellSzHF.z(); z+=cellSz.z()) {
          Vec3 pos(x,y,z);
          if(_region.sampleSafe3D(pos) > 0.0f)
            continue;

          sizeType count=0;
          Counter cc(count,pos,cellSzHF);
          cd.fill3D(pSet,pos,cellSzHF.squaredNorm(),cc);
          while(count < _nrParticlePerGrid) {
            ParticleSetN::ParticleType p;
            p._pos=pos+Vec3(cellSzHF.x()*RandEngine::randSR(seed++,-1.0f,1.0f),
                            cellSzHF.y()*RandEngine::randSR(seed++,-1.0f,1.0f),
                            cellSzHF.z()*RandEngine::randSR(seed++,-1.0f,1.0f));
            if(solid.sampleSafe(p._pos) > 0.0f) {
              p._vel=_vel.sampleSafe(p._pos);
              if(_filter)_filter->filter(p,globalTime);
              tmp.addParticle(p);
            }
            count++;
          }
        }
  }

  pSet.append(tmp);
}
void SourceRegion::sampleSink(ParticleSetN& pSet,const CollisionInterface<scalar,ParticleSetN>& cd)
{
  const sizeType nrP=pSet.size();
  _valid.assign(pSet.size(),true);
  BBox<scalar> bbRegion=_region.getBB();
  BBox<scalar> bbWarp=cd.getBB();
  sizeType DIM=cd.getDim();
  const Vec3i& warpMode=cd.getWarp();
  const Vec3 warpRange=bbWarp.getExtent();
  for(sizeType d=0; d<DIM; d++)
    if(warpMode[d] > 0) {
      bbRegion._minC[d]-=warpRange[d]/2;
      bbRegion._maxC[d]+=warpRange[d]/2;
    }
  for(sizeType i=0; i<nrP; i++) {
    Vec3 pos=pSet.get(i)._pos;
    for(sizeType d=0; d<DIM; d++)
      if(warpMode[d] > 0) {
        while(pos[d] < bbRegion._minC[d])
          pos[d]+=warpRange[d];
        while(pos[d] > bbRegion._maxC[d])
          pos[d]-=warpRange[d];
      }
    _valid[i]=_region.sampleSafe(pos) > 0.0f;
  }
  compact(pSet);
}
void SourceRegion::compact(ParticleSetN& pSet)
{
  const sizeType nrP=pSet.size();
  ASSERT((sizeType)_valid.size() == nrP)

  sizeType j=0;
  for(sizeType i=0; i<nrP; i++) {
    if(_valid[i]) {
      pSet.get(j)=pSet.get(i);
      j++;
    }
  }
  pSet.resize(j);
}
void SourceRegion::setFilter(boost::shared_ptr<SourceRegionFilter> filter)
{
  _filter=filter;
}
void SourceRegion::setNrParticlePerGrid(const sizeType& nrParticlePerGrid)
{
  _nrParticlePerGrid=nrParticlePerGrid;
}
void SourceRegion::setCellSz(const Vec3& cellSz)
{
  _cellSz=cellSz;
}
//smoke
void SourceRegion::setRho(const scalar& rho)
{
  _rho=rho;
}
scalar SourceRegion::getRho() const
{
  return _rho;
}
void SourceRegion::setT(const scalar& T)
{
  _T=T;
}
scalar SourceRegion::getT() const
{
  return _T;
}
void SourceRegion::setAlterVel(bool alterVel)
{
  _alterVel=alterVel;
}
void SourceRegion::setAlterProp(bool alterProp)
{
  _alterProp=alterProp;
}
bool SourceRegion::filterSource(const scalar& globalTime)
{
  return true;
}
