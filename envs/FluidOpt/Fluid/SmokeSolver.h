#ifndef SMOKE_SOLVER_H
#define SMOKE_SOLVER_H

#include "LiquidSolver.h"

PRJ_BEGIN

template <int DIM,typename Encoder>
class SmokeSolver : public LiquidSolverFLIP<DIM,Encoder>
{
public:
  typedef typename TypeTraits<scalar,DIM>::VecDIMd VecDIMd;
  typedef typename TypeTraits<scalar,DIM>::VecDIMi VecDIMi;
  typedef LiquidSolverFLIP<DIM,Encoder> Parent;
  using Parent::_nodalSolidPhi;
  using Parent::_grd;
  using Parent::_grd0;
  using Parent::_phi;
  using Parent::_weight;
  using Parent::_vel;
  using Parent::_rigid;
  using Parent::_pSet;
  using Parent::_sources;
  using Parent::_spring;
  //param
  using Parent::_density;
  using Parent::_narrowBand;
  using Parent::_radius;
  using Parent::_springRad;
  using Parent::_nrPass;
  using Parent::_nrParticlePerGrid;
  //timing
  using Parent::_globalTime;
  using Parent::_globalId;
  using Parent::_maxSubstep;
  using Parent::_maxCFL;
  //temporary data not involved in IO
  using Parent::_pSetAll;
  using Parent::_pSetRigid;
  //forcetorque feedback
  using Parent::_forceTorque;
  SmokeSolver();
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<Serializable> copy() const override;
  //setup
  virtual void initDomain(BBox<scalar> bb,const Vec3& cellSz,const Vec3i* warpMode=NULL) override;
  virtual void focusOn(const BBox<scalar>& region) override;
  virtual void getFeature(VectorField& field,scalar sz) override;
  //getter
  virtual const CellCenterData<scalar>& getT() const;
  virtual const CellCenterData<scalar>& getRho() const;
  virtual void setVelDamping(scalar velDamping);
  virtual void setDamping(scalar damping);
  //solver
  virtual sizeType advance(const scalar& dt) override;
  virtual void sampleSmokeSource(const scalar& globalTime);
protected:
  CellCenterData<scalar> _T,_rho;
  scalar _damping,_velDamping;
  scalar _TAmb,_beta;
  sizeType _order;
};

PRJ_END

#endif
