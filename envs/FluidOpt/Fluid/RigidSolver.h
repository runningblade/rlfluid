#ifndef RIGID_SOLVER_H
#define RIGID_SOLVER_H

#include <CommonFile/ParticleSet.h>
#include <CommonFile/geom/StaticGeomCell.h>
#include <CommonFile/ImplicitFunc.h>

PRJ_BEGIN

//RigidBodyShape
class RigidBodyShape : public Serializable
{
public:
  RigidBodyShape();
  RigidBodyShape(boost::shared_ptr<StaticGeomCell> cell,scalar cellSz,bool doInit=true);
  RigidBodyShape(const scalar rad,sizeType dim,scalar cellSz,bool doInit=true);
  RigidBodyShape(const Vec3& ext,sizeType dim,scalar cellSz,bool doInit=true);
  RigidBodyShape(vector<boost::shared_ptr<StaticGeomCell> > children,scalar cellSz,bool doInit=true);
  virtual void init(scalar cellSz);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<Serializable> copy() const override;
  virtual void writeMeshVTK(const Mat3& rot,const Vec3& pos,VTKWriter<scalar>& os) const;
  virtual void writePSetVTK(const Mat3& rot,const Vec3& pos,VTKWriter<scalar>& os) const;
  virtual sizeType dim() const;
  virtual void getBB(BBox<scalar>& bb) const;
  virtual void updateBB(const Mat3& rot,const Vec3& pos,BBox<scalar>& bb,bool fine=true) const;
  virtual scalar dist(const Vec3& pos) const;
  virtual Vec3 gradDist(const Vec3& pos) const;
  virtual scalar rayQuery(const Vec3& f,const Vec3& t) const;
  virtual sizeType nrSolidPSet() const;
  const ParticleSetN& getSolidPSet() const;
  const SphereGeomCell& getCellBall() const;
  const BoxGeomCell& getCellBox() const;
  const CompositeGeomCell& getCellComposite() const;
  const StaticGeomCell& getCell() const;
  StaticGeomCell& getCell();
  //physics
  scalar getM(const scalar& rho) const;
  Mat3 getIB(const scalar& rho) const;
  Mat3 getInvIB(const scalar& rho) const;
  bool isBall() const;
  bool isBox() const;
  bool isComposite() const;
protected:
  //constant
  scalar _M;
  Mat3 _IB;
  Mat3 _invIB;
  ParticleSetN _solidPSet;
  boost::shared_ptr<StaticGeomCell> _cell;
};
//RigidBodyState
class RigidBodyState : public Serializable
{
public:
  typedef ScalarUtil<scalar>::ScalarQuat Quat;
  RigidBodyState(sizeType id=-1);
  RigidBodyState(const string& name,sizeType id=-1);
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<Serializable> copy() const override;
  virtual bool operator<(const RigidBodyState& other) const;
  //state variable
  Vec3 _x;
  Quat _q;
  Vec3 _p;
  Vec3 _L;
  Vec3 _force;
  Vec3 _torque;
  scalar _globalTime;
};
//RigidBodyTrajectory
struct RigidBodyTrajectory : public Serializable {
  typedef ScalarUtil<scalar>::ScalarQuat Quat;
  RigidBodyTrajectory();
  void printInfo() const;
  bool read(std::istream& is,IOData* dat) override;
  bool write(std::ostream& os,IOData* dat) const override;
  boost::shared_ptr<Serializable> copy() const override;
  pair<Vec3,Vec3> MOnTraj(scalar time) const;
  pair<Vec3,Quat> POnTraj(scalar time) const;
  pair<Vec3,Vec3> FOnTraj(scalar time) const;
  void addKeyframe(const RigidBodyState& sKF,const scalar& dt);
  //control terms
  sizeType _bodyId;   //which body we want to apply control
  //frame data
  vector<RigidBodyState> _traj;  //optimized trajectory
};
//RigidBody
class RigidBody : public RigidBodyState
{
public:
  typedef ScalarUtil<scalar>::ScalarQuat Quat;
  RigidBody();
  RigidBody(boost::shared_ptr<RigidBodyShape> shape);
  void init();
  void setShape(boost::shared_ptr<RigidBodyShape>  shape);
  void setGlobalTime(const scalar& globalTime);
  scalar getGlobalTime() const;
  //create a rigid body with voxels
  bool read(std::istream& is,IOData* dat) override;
  bool write(std::ostream& os,IOData* dat) const override;
  boost::shared_ptr<Serializable> copy() const override;
  void setPos(const Vec3& pos);
  void setRot(const Quat& q);
  void setP(const Vec3& p);
  void setL(const Vec3& L);
  Vec3 pos() const;
  Mat3 rot() const;
  Quat q() const;
  Vec3 p() const;
  Vec3 L() const;
  Vec3 linSpd() const;
  Vec3 rotSpd() const;
  Mat3 I() const;
  Mat3 invI() const;
  Vec3 vel(const Vec3& pos) const;
  bool valid() const;
  bool freeze() const;
  void applyTorque(const Vec3& t);
  void applyForce(const Vec3& f);
  void applyAngularImpulse(const Vec3& AJ);
  void applyImpulse(const Vec3& J);
  void getWorldBB(BBox<scalar>& bb) const;
  void updateBB(bool fine=true);
  void applyWorldForce(const Vec3& f,const Vec3& p);
  void applyWorldImpulse(const Vec3& J,const Vec3& p);
  const RigidBodyShape& getShape() const;
  RigidBodyShape& getShape();
  Vec3 toWorld(const Vec3& local) const;
  Vec3 toLocal(const Vec3& world) const;
  scalar dist(const Vec3& world) const;
  void getPSet(ParticleSetN& pset) const;
  void writeMeshVTK(VTKWriter<scalar>& os) const;
  void writeMeshVTK(const std::string& str) const;
  void writePSetVTK(VTKWriter<scalar>& os) const;
  void writePSetVTK(const std::string& str) const;
  void applyFirstOrderCentralImpulse(const Vec3& J);
  scalar energy() const;
  //physics property
  scalar getM() const;
  Mat3 getIB() const;
  Mat3 getInvIB() const;
  const scalar& rho() const;
  scalar& rho();
  const scalar& restitute() const;
  scalar& restitute();
  //for scripted motion
  void setTraj(boost::shared_ptr<RigidBodyTrajectory> traj);
  const RigidBodyTrajectory& traj() const;
  bool hasTraj() const;
  std::pair<Vec3,Vec3> MOnTraj(const scalar& dt) const;
  std::pair<Vec3,Quat> POnTraj(const scalar& dt) const;
  std::pair<Vec3,Vec3> FOnTraj(const scalar& dt) const;
public:
  //get state
  void getPosition(RigidBodyState& state);
  void getVelocity(RigidBodyState& state);
  void getForce(RigidBodyState& state);
  //set state
  void setPosition(const RigidBodyState& state);
  void setVelocity(const RigidBodyState& state);
  void setForce(const RigidBodyState& state);
  //advance
  void getDerivVelocity(RigidBodyState& state);
  void getDerivPosition(RigidBodyState& state);
  void adjustVelocity(const RigidBodyState& last,scalar dt);
  static void debugAdjustVelocity();
  void advanceVelocity(const scalar& dt);
  void advancePosition(const scalar& dt);
  void computePositionAux();
  void computeVelocityAux();
  //clear
  void clearForceAndTorque();
  //collision/contact flag
  void clearContactFlag();
  void setContactFlag();
  bool hasContactFlag();
protected:
  //scripted motion
  boost::shared_ptr<RigidBodyTrajectory> _traj;
  boost::shared_ptr<RigidBodyShape> _shape;
  //physics property
  scalar _rho,_restitute;
  //aux variable
  Mat3 _R;
  Mat3 _I;
  Mat3 _invI;
  Vec3 _v;
  Vec3 _w;
  //bounding box
  BBox<scalar> _worldBB;
  bool _worldBBValid;
  //temporary data not written
  bool _contactFlag;
};
//RigidSolver
template <typename T,typename BBOX>
class BVHQuery;
class RigidSolver : public Serializable
{
public:
  typedef ScalarUtil<scalar>::ScalarQuat Quat;
  RigidSolver();
  RigidSolver(sizeType dim,scalar cellSz);
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<Serializable> copy() const override;
  virtual void writeMeshVTK(VTKWriter<scalar>& os) const;
  virtual void writeMeshVTK(const string& path) const;
  virtual void writePSetVTK(VTKWriter<scalar>& os) const;
  virtual void writePSetVTK(const string& path) const;
  virtual sizeType nrBody() const;
  virtual sizeType nrSolid() const;
  virtual void addBall(const scalar rad,const Vec3& pos);
  virtual void addBall(const Vec3& ext,const Vec3& pos,const Quat& rot);
  virtual void addBall(const Vec3& ext,const Vec3& pos,const Vec3& rot);
  virtual void addBox(const Vec3& ext,const Vec3& pos,const Quat& rot);
  virtual void addBox(const Vec3& ext,const Vec3& pos,const Vec3& rot);
  virtual void addCross(const Vec3& ext,const Vec3& pos,const Quat& rot);
  virtual void addCross(const Vec3& ext,const Vec3& pos,const Vec3& rot);
  virtual void addSolid(boost::shared_ptr<StaticGeomCell> cell);
  virtual const RigidBody& getBody(const sizeType& i) const;
  virtual RigidBody& getBody(const sizeType& i);
  virtual const StaticGeomCell& getSolid(const sizeType& i) const;
  virtual StaticGeomCell& getSolid(const sizeType& i);
  virtual void clearContactFlag();
  virtual void advanceVelocity(const scalar& dt,const scalar& globalTime);
  virtual void advancePosition(const scalar& dt);
  virtual void adjustPosition(sizeType nrIter);
  virtual void advance(const scalar& dt,const scalar& globalTime);
  void setBB(const BBox<scalar>& bb);
  BBox<scalar> getBB() const;
  void setGravity(const Vec3& g);
  Vec3 getGravity() const;
  scalar intersect(const Vec3& from,const Vec3& to,bool solidOnly,sizeType* bodyId=NULL,sizeType* solidId=NULL) const;
  bool intersect(const Vec3& pos,bool solidOnly) const;
  bool intersect(scalar r,const Vec3& pos,bool solidOnly) const;
  void getPSet(ParticleSetN& pset) const;
  void onCell(const Node<sizeType>& I,const Node<sizeType>& J,const vector<Vec3,Eigen::aligned_allocator<Vec3> >& offs);
  //handle warping
  void handleWarps(vector<Vec3,Eigen::aligned_allocator<Vec3> >& warpAdjusts);
  void computeWarpOffsets(vector<Vec3,Eigen::aligned_allocator<Vec3> >& offs) const;
  void interBodyQueryWithWarp(const BVHQuery<sizeType,BBox<scalar> >& query);
  void setWarpMode(const Vec3i& warpMode);
protected:
  vector<boost::shared_ptr<RigidBody> > _bodies;
  vector<boost::shared_ptr<StaticGeomCell> > _solids;
  vector<RigidBodyState> _poses;
  BBox<scalar> _bb;
  sizeType _dim;
  scalar _cellSz;
  Vec3i _warpMode;
  Vec3 _g;
};
//RigidSolverImplicitFunc
class RigidSolverImplicitFunc : public ImplicitFunc<scalar>
{
public:
  RigidSolverImplicitFunc(const RigidSolver& sol,bool solidOnly);
  scalar operator()(const Vec3& pos) const override;
protected:
  const RigidSolver& _sol;
  bool _solidOnly;
};

PRJ_END

#endif
