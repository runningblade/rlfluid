﻿#ifndef VARIABLE_SIZED_GRID_OP_H
#define VARIABLE_SIZED_GRID_OP_H

#include "Utils.h"
#include "NodalData.h"
#include "MACVelData.h"
#include "CellCenterData.h"
#include "VariableSizedGrid.h"
#include <CommonFile/GridBasic.h>
#include <CommonFile/ParticleSet.h>
#include <CommonFile/ImplicitFuncInterface.h>

PRJ_BEGIN

//VariableSizedGridOp
DECL_FORWARD_LINEAR_SOLVER
template <typename T,int DIM,typename Encoder>
struct VariableSizedGridOp : public TypeTraits<T,DIM> {
  DECL_TYPES
  //basics
  template <typename TDATA>
  static void writeGridNodalVTK(const string& path,const GridType& grd,const NodalData<TDATA>& data,bool extrude=false);
  template <typename TDATA>
  static void writeGridNodalPointVTK(const string& path,const GridType& grd,const NodalData<TDATA>& data,bool extrude=false);
  template <typename TDATA>
  static void writeGridCellCenterVTK(const string& path,const GridType& grd,const CellCenterData<TDATA>& data,bool extrude=false);
  template <typename TDATA>
  static void writeGridCellCenterPointVTK(const string& path,const GridType& grd,const CellCenterData<TDATA>& data,bool extrude=false);
  template <typename TDATA>
  static void writeGridMACVelVTK(const string& path,const GridType& grd,const MACVelData<DIM,TDATA>& data,T len);
  template <typename TDATA>
  static void writeGridMACVelCompVTK(unsigned char D,const string& path,const GridType& grd,const MACVelData<DIM,TDATA>& data);
  template <typename TDATA>
  static void writeGridMACVelPointVTK(const string& path,const GridType& grd,const MACVelData<DIM,TDATA>& data);
  template <typename TDATA>
  static void copyImplicitFuncNodal(const GridType& grd,NodalData<TDATA>& data,const ImplicitFunc<TDATA>& func);
  template <typename TDATA>
  static void copyImplicitFuncCellCenter(const GridType& grd,CellCenterData<TDATA>& data,const ImplicitFunc<TDATA>& func);
  template <typename TDATA>
  static void copyImplicitFuncMACVel(const GridType& grd,MACVelData<DIM,TDATA>& data,const VelFunc<TDATA>& func);
  template <typename TDATA>
  static void copyFromGridNodal(const GridType& grd,NodalData<TDATA>& data,const ScalarField& func);
  template <typename TDATA>
  static void copyFromGridCellCenter(const GridType& grd,CellCenterData<TDATA>& data,const ScalarField& func);
  template <typename TDATA>
  static void copyToGridNodal(const GridType& grd0,const GridType& grd,const NodalData<TDATA>& data,ScalarField& func);
  template <typename TDATA>
  static void copyToGridCellCenter(const GridType& grd0,const GridType& grd,const CellCenterData<TDATA>& data,ScalarField& func);
  //CFL
  static T checkCFL(const GridType& grd,const MACVelData<DIM,T>& vel);
  static void clamp(const GridType& grd,CellCenterData<T> data[DIM],T maxVel);
  //compute level set from particle
  template <typename PS_TYPE>
  static void computePhiCellCenter(const GridType& grd,CellCenterData<T>& phi,const PS_TYPE& pSet,T narrowBand,T radius);
  template <typename PS_TYPE>
  static void computePhiAdaptiveCellCenter(const GridType& grd,CellCenterData<T>& phi,const PS_TYPE& pSet,T narrowBand,T radius);
  static void extendPhiCellCenter(const GridType& grd,CellCenterData<T>& phi,const MACVelData<DIM,T>& weight,sizeType nrPass);
  //compute velocity in solid
  static void constraintMACVelDIM(unsigned char D,const GridType& grd,const Grid<T,T>& solid,MACVelData<DIM,T>& vel);
  static void constraintMACVel(const GridType& grd,const Grid<T,T>& solid,MACVelData<DIM,T>& vel);
  static bool extrapolateMACVelComp(unsigned char D,const GridType& grd,MACVelData<DIM,unsigned char>& valid,MACVelData<DIM,T>& vel,const VecDIMi& id,sizeType off,unsigned char flag);
  static void extrapolateMACVelDIM(unsigned char D,const GridType& grd,MACVelData<DIM,unsigned char>& valid,MACVelData<DIM,T>& vel,sizeType nrPass);
  static void extrapolateMACVel(const GridType& grd,MACVelData<DIM,unsigned char>& valid,MACVelData<DIM,T>& vel,sizeType nrPass);
  //debug
  static void debugExtrapolate(const string& path,const Vec3i& warpMode);
  static void debugExtrapolateAllWarp();
  //external force
  static void addDamping(NodalData<T>& data,T alpha,T baseline,T dt);
  static void addDamping(CellCenterData<T>& data,T alpha,T baseline,T dt);
  static void addDamping(MACVelData<DIM,T>& data,T alpha,T dt);
  static void addExternalForce(const GridType& grd,MACVelData<DIM,T>& data,const CellCenterData<T>* t,T alpha,T beta,T tAmb,T dt);
  //unconditionally stable integrator
  static void advectSemiLagrangian(const GridType& grd,const CellCenterData<T>& from,CellCenterData<T>& to,const MACVelData<DIM,T>& vel,T dt);
  static void advectMacCormack(const GridType& grd,const CellCenterData<T>& from,CellCenterData<T>& to,const MACVelData<DIM,T>& vel,const CellCenterData<unsigned char>& type,T dt);
  static void advectSemiLagrangian(const GridType& grd,const MACVelData<DIM,T>& from,MACVelData<DIM,T>& to,const MACVelData<DIM,T>& vel,T dt);
  static void advectMacCormack(const GridType& grd,const MACVelData<DIM,T>& from,MACVelData<DIM,T>& to,const MACVelData<DIM,T>& vel,const MACVelData<DIM,unsigned char>& type,T dt);
  //exponential integrator
  static void advectExponential(const GridType& grd,const CellCenterData<T>& from,CellCenterData<T>& to,const MACVelData<DIM,T>& vel,const CellCenterData<unsigned char>& type,T dt,T eps);
  static void mulExponential(const GridType& grd,const CellCenterData<T>& from,CellCenterData<T>& to,const MACVelData<DIM,T>& vel,const CellCenterData<unsigned char>& type,T dt);
  //particle integrator
  template <typename PS_TYPE>
  static void warpParticle(const GridType& grd,PS_TYPE& pset);
  template <typename PS_TYPE>
  static void advectParticle(const GridType& grd,const MACVelData<DIM,T>& vel,PS_TYPE& pset,T dt,unsigned char order=2);
  //debug
  static void debugAdvect(const string& path,const Vec3i& warpMode,bool rotate);
  static void debugAdvectAllWarp(bool rotate);
  static GridType createTestGrid(const Vec3i& warpMode);
  static ScalarField createTestSolidPhi(const VariableSizedGrid<T,DIM,Encoder>& testGrid,const VecDIMd* ctrGiven=NULL,const scalar* radCoef=NULL);
};

PRJ_END

#endif
