#include <Fluid/Utils.h>
#include <Fluid/RigidSolver.h>
#include <CommonFile/geom/StaticGeomCell.h>

USE_PRJ_NAMESPACE

#define NR_RIGID 1000
int main()
{
  //add rigid
  Vec3i warpMode(1,0,0);
  BBox<scalar> bb(Vec3(0,0,0),Vec3(2,1,0));
  RigidSolver sol(2,1/32.0f);
  sol.setBB(bb);
  RandEngine::seed(100);
  RandEngine::useDeterministic();
  //add solid
  Mat4 T=Mat4::Identity();
  T.block<3,1>(0,3)=Vec3(1,0.5f,0);
  T.block<3,3>(0,0)=makeRotation<scalar>(Vec3(0,0,M_PI/4));
  boost::shared_ptr<StaticGeomCell> cell(new BoxGeomCell(T,2,Vec3(0.5f,0.1f,0)));
  sol.addSolid(cell);
  //add body
  for(sizeType i=0; i<NR_RIGID; i++) {
    scalar rad=RandEngine::randR(0.1f,0.3f);
    Vec3 pos=Vec3(RandEngine::randR(bb._minC[0]+rad,bb._maxC[0]-rad),
                  RandEngine::randR(bb._minC[1]+rad,bb._maxC[1]-rad),0.0f);
    if(!sol.intersect(rad,pos,false)) {
      if(i < NR_RIGID/2)
        sol.addCross(Vec3(rad/2,rad/4,0),pos,Vec3(0,0,RandEngine::randR01()));
      else sol.addBall(rad,pos);
    }
  }
  {
    sol.addBox(Vec3(0.5f,0.1f,0),Vec3(1.5f,0.5f,0),Vec3::Zero());
    RigidBody& body=sol.getBody(sol.nrBody()-1);
    boost::shared_ptr<RigidBodyTrajectory> traj(new RigidBodyTrajectory);
    scalar V=-1,W=2;
    RigidBodyState state;
    state._x=Vec3(2,0.5f,0);
    state._q=RigidBody::Quat(Eigen::AngleAxis<scalar>(M_PI/2,Vec3::UnitZ()));
    state._p=body.getM()*Vec3(V,0,0);
    state._L=body.I()*Vec3::UnitZ()*W;
    //keyframe
    for(scalar i=0; i<100; i+=0.1f) {
      state._globalTime=i;
      state._x=Vec3(2+V*state._globalTime,0.5f,0);
      state._q=RigidBody::Quat(Eigen::AngleAxis<scalar>(M_PI/2+W*state._globalTime,Vec3::UnitZ()));
      traj->addKeyframe(state,0);
    }
    body.setTraj(traj);
  }
  sol.setWarpMode(warpMode);
  //save load
  {
    boost::filesystem::ofstream os("rigid.dat",ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    sol.write(os,dat.get());
  }
  {
    sol=RigidSolver();
    boost::filesystem::ifstream is("rigid.dat",ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    sol.read(is,dat.get());
  }
  //simulate
  recreate("rigid");
  scalar gTime=0,dt=1E-3f;
  sol.writeMeshVTK("rigid/mesh.vtk");
  sol.writePSetVTK("rigid/pset.vtk");
  for(sizeType i=0,I=10; i<5000; i++) {
    INFOV("Simulating frame: %ld!",i)
    sol.advance(dt,gTime);
    if(i%I == 0) {
      std::ostringstream oss;
      oss << "rigid/pset" << i/I << ".vtk";
      sol.writePSetVTK(oss.str());
    }
    if(i%I == 0) {
      std::ostringstream oss;
      oss << "rigid/mesh" << i/I << ".vtk";
      sol.writeMeshVTK(oss.str());
    }
    gTime+=dt;
  }
}
