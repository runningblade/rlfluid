from rendering import *
from gym.envs.classic_control.rendering import _add_attrs
import ctypes as ct
import numpy as np
from PIL import Image, ImageOps
import os
from sceneSetup import LiquidSol, SmokeSol

class Renderer():

    def __init__(self, option, sol, w, h, res, bg_obj, rigid_obj, sh_obj, png_path=None):
        self.option = option
        self.sol = sol
        self.w = w
        self.h = h
        self.res = res
        self.bg_obj = bg_obj
        self.rigid_obj = rigid_obj
        self.sh_obj = sh_obj
        self.bg_render_by_option = {'g': self.bg_obj.good_render, 'p': self.bg_obj.plain_render}
        self.fg_render_by_option = {'g': self.good_render, 'p': self.plain_render}
        self.step_cnt = 0
        self.png_path = png_path
        if self.png_path is not None:
            if self.png_path[-1] != '/':
                self.png_path += '/'
            os.makedirs(self.png_path, exist_ok=True)
            for opt in self.bg_render_by_option:
                if not os.path.exists(self.png_path + opt):
                    os.makedir(self.png_path + opt)
        
    def render(self, viewers, mode, close, sh_poss, sh_thetas, mesh_v_size=0, mesh_i_size=0):
        if close:
            for i in range(len(viewers)):
                if viewers[i] is not None:
                    viewers[i].close()
            return [], None
        # renderer setup
        if not viewers:
            viewers = [None] * len(self.option)
            for i in range(len(self.option)):
                viewers[i] = Viewer(self.w * self.res, self.h * self.res)
                viewers[i].set_bounds(0, self.w, 0, self.h)
                self.bg_render_by_option[self.option[i]](viewers[i])

        # physical info
        self.rigid_pos_x, self.rigid_pos_y = self.sol.get_rgd_pos()
        self.rigid_theta = self.sol.get_rgd_theta()

        assert(len(sh_poss) == len(sh_thetas))
        for option, viewer in zip(self.option, viewers):
            if isinstance(self.sol, SmokeSol):
                null_ptr = ct.POINTER(ct.c_float)()
                # sz_c = (ct.c_int * 2)()
                # stride_c = (ct.c_int * 2)()
                # nrRho_c = (ct.c_int)()
                # self.fluid.getRhoSmoke(null_ptr, null_ptr, null_ptr, sz_c, stride_c, nrRho_c)
                # rho_cache_c = (ct.c_float * nrRho_c.value)()
                # posx_cache_c = (ct.c_float * sz_c[0])()
                # posy_cache_c = (ct.c_float * sz_c[1])()
                # self.fluid.getRhoSmoke(rho_cache_c, posx_cache_c, posy_cache_c, sz_c, stride_c, nrRho_c)
                # dens = DensityField(self.w, self.h, rho_cache_c, posx_cache_c, posy_cache_c, sz_c, stride_c, nrRho_c)
                # viewer.add_onetime(dens)

            self.fg_render_by_option[option](viewer, sh_poss, sh_thetas, mesh_v_size, mesh_i_size)
            result = viewer.render(return_rgb_array=mode == 'rgb_array')
            if self.png_path is not None and os.path.exists(self.png_path + option):
                viewer.take_screenshot(self.png_path + option + '/' + str(self.step_cnt))
        self.step_cnt += 1

        return viewers, result

    def good_render(self, viewer, sh_poss, sh_thetas, mesh_v_size, mesh_i_size):
        # mesh
        if isinstance(self.sol, LiquidSol) and mesh_v_size > 0 and mesh_i_size > 0:
            mesh_v, mesh_i = self.sol.get_mesh()
            viewer.draw_mesh(mesh_v, mesh_i, color=(53/255, 147/255, 241/255, 0.5))
        
        # object
        self.rigid_obj.good_render(viewer, translation=(self.rigid_pos_x.value, self.rigid_pos_y.value), rotation=(self.rigid_theta.value))

        # shooter
        for i in range(len(sh_poss)):
            self.sh_obj.good_render(viewer, translation=sh_poss[i], rotation=(sh_thetas[i]-90.0)*np.pi/180.0)

    def plain_render(self, viewer, sh_poss, sh_thetas, mesh_v_size, mesh_i_size):
        # liquid particles
        if isinstance(self.sol, LiquidSol):
            # nr_particle_c = (ct.c_int)()
            # null_ptr = ct.POINTER(ct.c_float)()
            # self.fluid.getParticlesLiquid(null_ptr, nr_particle_c)
            # particle_cache_c = (ct.c_float * (nr_particle_c.value * 2))()
            # self.fluid.getParticlesLiquid(particle_cache_c, nr_particle_c)
            particle_cache, _ = self.sol.get_pt()
            pts = Points(particle_cache)
            _add_attrs(pts, {'color': (0.5, 0.9, 0.4)})
            viewer.add_onetime(pts)

        # object
        self.rigid_obj.plain_render(viewer, translation=(self.rigid_pos_x, self.rigid_pos_y), rotation=(self.rigid_theta))

        # shooter
        for i in range(len(sh_poss)):
            self.sh_obj.plain_render(viewer, translation=sh_poss[i], rotation=(sh_thetas[i]-90.0)*np.pi/180.0)


class MusicRenderer(Renderer):

    def __init__(self, option, fluid_type, fluid, w, h, res, bg_obj, rigid_obj, sh_obj, nkey, hkey, png_path=None):
        Renderer.__init__(self, option, fluid_type, fluid, w, h, res, bg_obj, rigid_obj, sh_obj, png_path)
        self.bg_render_by_option['t'] = self.bg_obj.plain_render
        self.fg_render_by_option['t'] = self.good_render
        self.bg_render_by_option['c'] = self.bg_obj.good_render
        self.fg_render_by_option['c'] = self.cylinder_render
        self.nkey = nkey
        self.hkey = hkey
        self.key_map = {0: 'Do', 1: 'Re', 2: 'Mi', 3: 'Fa', 4: 'Sol', 5: 'La', 6: 'Ti'}
        self.cylinder = Cylinder(self.w, self.h, self.res, self.bg_obj.image)
        if png_path is not None:
            if not os.path.exists(self.png_path + 'c'):
                os.makedir(self.path + 'c')

    def render(self, viewers, mode, close, sh_poss, sh_thetas, target, hit_right, last_hit, time, mesh_v_size=0, mesh_i_size=0):
        self.target = target
        self.hit_right = hit_right
        self.last_hit = last_hit
        self.time = time
        return Renderer.render(self, viewers, mode, close, sh_poss, sh_thetas, mesh_v_size, mesh_i_size)

    def good_render(self, viewer, sh_poss, sh_thetas, mesh_v_size, mesh_i_size):
        Renderer.good_render(self, viewer, sh_poss, sh_thetas, mesh_v_size, mesh_i_size)
        for i in range(self.nkey):
            viewer.draw_polygon([(i * self.w / self.nkey, self.h - self.hkey),
                ((i + 1) * self.w / self.nkey, self.h - self.hkey), 
                ((i + 1) * self.w / self.nkey, self.h),
                (i * self.w / self.nkey, self.h)], color=(1, 1, 1))
            viewer.draw_line((i * self.w / self.nkey, self.h - self.hkey), (i * self.w / self.nkey, self.h), color=(0.7, 0.7, 0.7))
            t = Transform(translation=((i + 0.5) * self.w / self.nkey, self.h - 0.7 * self.hkey), scale=(0.0004, 0.0004))
            viewer.draw_text(text=self.key_map[i], size=24, color=(100, 100, 100, 255), dpi=1200).add_attr(t)
            viewer.draw_line((0, self.h), (self.w, self.h), color=(0.2, 0.2, 0.2), width=10)
        d = 0.1
        for i in range(self.nkey - 1):
            if self.nkey > 5 and (i == 2 or i == 6):
                continue
            viewer.draw_polygon([(self.w / self.nkey * (1 - d + i), self.h - self.hkey * 0.618),
                (self.w / self.nkey * (1 + d + i), self.h - self.hkey * 0.618),
                (self.w / self.nkey * (1 + d + i), self.h),
                (self.w / self.nkey * (1 - d + i), self.h)], color=(0.2, 0.2, 0.2))

    def plain_render(self, viewer, sh_poss, sh_thetas, mesh_v_size, mesh_i_size):
        Renderer.plain_render(self, viewer, sh_poss, sh_thetas, mesh_v_size, mesh_i_size)
        for i in range(self.nkey):
            if i == self.target:
                color = (0, 1, 0)
            elif not self.hit_right and i == self.last_hit:
                color = (1, 0, 0)
            else:
                color = (1, 1, 1)
            viewer.draw_polygon([(i * self.w / self.nkey, self.h - self.hkey),
                ((i + 1) * self.w / self.nkey, self.h - self.hkey), 
                ((i + 1) * self.w / self.nkey, self.h),
                (i * self.w / self.nkey, self.h)], color=color)
            viewer.draw_line((i * self.w / self.nkey, self.h - self.hkey), (i * self.w / self.nkey, self.h), color=(0.7, 0.7, 0.7))
            t = Transform(translation=((i + 0.5) * self.w / self.nkey, self.h - 0.7 * self.hkey), scale=(0.0004, 0.0004))
            viewer.draw_text(text=self.key_map[i], size=24, color=(100, 100, 100, 255), dpi=1200).add_attr(t)
            viewer.draw_line((0, self.h), (self.w, self.h), color=(0.2, 0.2, 0.2), width=10)
        t = Transform(translation=(5 * self.w / 6, self.h / 6), scale=(0.01, 0.01))
        viewer.draw_text(str(self.time), 36).add_attr(t)

    def cylinder_render(self, viewer, sh_poss, sh_thetas, mesh_v_size, mesh_i_size):
        self.cylinder.img = ImageOps.flip(viewer.take_screenshot_trans())
        viewer.geoms = [self.cylinder]


class BattleRenderer(Renderer):

    def __init__(self, option, fluid_type, fluid, w, h, res, bg_obj, rigid_obj, sh_obj, bar_obj, png_path=None):
        Renderer.__init__(self, option, fluid_type, fluid, w, h, res, bg_obj, rigid_obj, sh_obj, png_path)
        self.bar_obj = bar_obj

    def good_render(self, viewer, sh_poss, sh_thetas, mesh_v_size, mesh_i_size):
        Renderer.good_render(self, viewer, sh_poss, sh_thetas, mesh_v_size, mesh_i_size)
        self.bar_obj.good_render(viewer, translation=(self.w / 2, self.bar_obj.height / 2))

    def plain_render(self, viewer, sh_poss, sh_thetas, mesh_v_size, mesh_i_size):
        Renderer.plain_render(self, viewer, sh_poss, sh_thetas, mesh_v_size, mesh_i_size)
        self.bar_obj.plain_render(viewer, translation=(self.w / 2, self.bar_obj.height / 2))

