from pyglet.gl import *
import OpenGL.GL.shaders

phong_vertex_src = '''
    #version 400 core
    in layout(location = 0) vec3 position;
    in layout(location = 1) vec3 normal;

    out vec3 Normal;
    out vec3 FragPos;

    uniform mat4 model;
    uniform mat4 view;
    uniform mat4 projection;

    void main() {
        gl_Position = projection * view * model * vec4(position, 1.0f);
        FragPos = vec3(model * vec4(position, 1.0f));
        Normal = mat3(transpose(inverse(model))) * normal;
    }
    '''

phong_fragment_src = '''
    #version 400 core
    in vec3 FragPos;
    in vec3 Normal;

    out vec4 outColor;

    uniform vec3 viewPos;
    uniform vec3 lightPos;
    uniform vec3 lightColor;
    uniform vec3 objectColor; 

    void main() {
        // Ambient
        float ambientStrength = 0.3f;
        vec3 ambient = ambientStrength * lightColor;

        // Diffuse
        vec3 norm = normalize(Normal);
        vec3 lightDir = normalize(lightPos - FragPos);
        float diff = max(dot(norm, lightDir), 0.0);
        vec3 diffuse = diff * lightColor;

        // Specular
        float specularStrength = 0.48f;
        vec3 viewDir = normalize(viewPos - FragPos);
        vec3 halfwayDir = normalize(lightDir + viewDir);
        float spec = pow(max(dot(norm, halfwayDir), 0.0), 4);
        vec3 specular = specularStrength * spec * lightColor;

        vec3 result = (ambient + diffuse + specular) * objectColor;

        outColor = vec4(result, 1.0f);
    }
    '''

plain_vertex_src = '''
    #version 400 core
    in layout(location = 0) vec3 position;

    uniform mat4 model;
    uniform mat4 view;
    uniform mat4 projection;

    void main() {
        gl_Position = projection * view * model * vec4(position, 1.0f);
    }
    '''

plain_fragment_src = '''
    #version 400 core

    out vec4 outColor;

    uniform vec3 viewPos;
    uniform vec3 lightPos;
    uniform vec3 lightColor;

    void main() {
        // Ambient
        // float ambientStrength = 0.3f;
        // vec3 ambient = ambientStrength * lightColor;

        // vec3 result = ambient * vec3(0.0, 1.0, 0.0);

        // outColor = vec4(result, 1.0f);
        outColor = vec4(0.0, 1.0, 0.0, 1.0);
    }
    '''

skybox_vertex_src = '''
    #version 400 core
    in layout (location = 0) vec3 position;

    out vec3 TexCoords;

    uniform mat4 model;
    uniform mat4 view;
    uniform mat4 projection;

    void main() {
        gl_Position = projection * view * model * vec4(position, 1.0f);
        TexCoords = position;
    }
    '''

skybox_fragment_src = '''
    #version 400 core
    in vec3 TexCoords;

    out vec4 color;

    uniform samplerCube skybox;

    void main() {
        color = texture(skybox, TexCoords);
    }
    '''

depth_vertex_src = '''
    #version 400 core
    in layout (location = 0) vec3 position;

    uniform mat4 lightProjection;
    uniform mat4 lightView;
    uniform mat4 model;

    void main() {
        gl_Position = lightProjection * lightView * model * vec4(position, 1.0f);
    }
    '''

depth_fragment_src = '''
    #version 400 core

    void main() {
    }
    '''

shadow_vertex_src = '''
    #version 400 core
    in layout(location = 0) vec3 position;
    in layout(location = 1) vec3 normal;

    out vec3 Normal;
    out vec3 FragPos;
    out vec4 FragPosLightSpace;

    uniform mat4 model;
    uniform mat4 view;
    uniform mat4 projection;
    uniform mat4 lightProjection;
    uniform mat4 lightView;

    void main() {
        gl_Position = projection * view * model * vec4(position, 1.0);
        FragPos = vec3(model * vec4(position, 1.0));
        Normal = mat3(transpose(inverse(model))) * normal;
        FragPosLightSpace = lightProjection * lightView * vec4(FragPos, 1.0);
    }
    '''

shadow_fragment_src = '''
    #version 400 core
    in vec3 FragPos;
    in vec3 Normal;
    in vec4 FragPosLightSpace;

    out vec4 outColor;

    uniform vec3 viewPos;
    uniform vec3 lightPos;
    uniform vec3 lightColor;
    uniform vec3 objectColor;
    uniform sampler2D shadowMap;
    uniform bool shadows;

    float ShadowCalculation(vec4 fragPosLightSpace) {
        vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
        projCoords = projCoords * 0.5 + 0.5;
        float closestDepth = texture(shadowMap, projCoords.xy).r;
        float currentDepth = projCoords.z;

        vec3 normal = normalize(Normal);
        vec3 lightDir = normalize(lightPos - FragPos);
        float bias = max(0.05 * (1.0 - dot(normal, lightDir)), 0.005);

        float shadow = 0.0;
        vec2 texelSize = 1.0 / textureSize(shadowMap, 0);
        for (int x = -1; x <= 1; ++x) {
            for (int y = -1; y <= 1; ++y) {
                float pcfDepth = texture(shadowMap, projCoords.xy + vec2(x, y) * texelSize).r;
                shadow += currentDepth - bias > pcfDepth ? 1.0 : 0.0;
            }
        }
        shadow /= 9.0;

        if (projCoords.z > 1.0)
            shadow = 0.0;
        return shadow;
    }

    void main() {
        // Ambient
        float ambientStrength = 0.3f;
        vec3 ambient = ambientStrength * lightColor;

        // Diffuse
        vec3 norm = normalize(Normal);
        vec3 lightDir = normalize(lightPos - FragPos);
        float diff = max(dot(norm, lightDir), 0.0);
        vec3 diffuse = diff * lightColor;

        // Specular
        float specularStrength = 0.2f;
        vec3 viewDir = normalize(viewPos - FragPos);
        vec3 halfwayDir = normalize(lightDir + viewDir);
        float spec = pow(max(dot(norm, halfwayDir), 0.0), 8);
        vec3 specular = specularStrength * spec * lightColor;

        // Shadow
        float shadow = shadows ? ShadowCalculation(FragPosLightSpace) : 0.0;
        shadow = min(shadow, 0.75);

        vec3 result = (ambient + (1.0 - shadow) * (diffuse + specular)) * objectColor;

        outColor = vec4(result, 1.0f);
    }
    '''

screen_vertex_src = '''
    #version 400 core
    in layout(location = 0) vec2 position;
    in layout(location = 1) vec2 texCoords;

    out vec2 TexCoords;

    void main() {
        gl_Position = vec4(position.x, position.y, 0.0, 1.0);
        TexCoords = texCoords;
    }
    '''

screen_fragment_src = '''
    #version 400 core
    in vec2 TexCoords;

    out vec4 FragColor;

    uniform sampler2D screenTexture;

    void main() {
        vec3 col = texture(screenTexture, TexCoords).rgb;
        // float grayscale = 0.2126 * col.r + 0.7152 * col.g + 0.0722 * col.b;
        // FragColor = vec4(vec3(grayscale), 1.0);
        FragColor = vec4(col, 1.0);
    }
    '''

################

phong_shader = OpenGL.GL.shaders.compileProgram(
    OpenGL.GL.shaders.compileShader(phong_vertex_src, GL_VERTEX_SHADER),
    OpenGL.GL.shaders.compileShader(phong_fragment_src, GL_FRAGMENT_SHADER))

plain_shader = OpenGL.GL.shaders.compileProgram(
    OpenGL.GL.shaders.compileShader(plain_vertex_src, GL_VERTEX_SHADER),
    OpenGL.GL.shaders.compileShader(plain_fragment_src, GL_FRAGMENT_SHADER))

skybox_shader = OpenGL.GL.shaders.compileProgram(
    OpenGL.GL.shaders.compileShader(skybox_vertex_src, GL_VERTEX_SHADER),
    OpenGL.GL.shaders.compileShader(skybox_fragment_src, GL_FRAGMENT_SHADER))

depth_shader = OpenGL.GL.shaders.compileProgram(
    OpenGL.GL.shaders.compileShader(depth_vertex_src, GL_VERTEX_SHADER),
    OpenGL.GL.shaders.compileShader(depth_fragment_src, GL_FRAGMENT_SHADER))

shadow_shader = OpenGL.GL.shaders.compileProgram(
    OpenGL.GL.shaders.compileShader(shadow_vertex_src, GL_VERTEX_SHADER),
    OpenGL.GL.shaders.compileShader(shadow_fragment_src, GL_FRAGMENT_SHADER))

screen_shader = OpenGL.GL.shaders.compileProgram(
    OpenGL.GL.shaders.compileShader(screen_vertex_src, GL_VERTEX_SHADER),
    OpenGL.GL.shaders.compileShader(screen_fragment_src, GL_FRAGMENT_SHADER))