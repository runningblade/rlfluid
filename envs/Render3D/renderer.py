from pyglet.gl import *
from time import time
from .viewer import Viewer3D
from .shader import *
from .entity import *
from .utils import *

class Renderer3D():

    def __init__(self, w, h, res, camera_pos, world_up):
        self.w = w * res
        self.h = h * res
        self.light_color = (1, 1, 1)
        self.shader = phong_shader
        # self.depth_shader = depth_shader
        self.screen_shader = screen_shader
        self.last_time = time()
        self.viewer = Viewer3D(camera_pos=camera_pos, world_up=world_up, width=self.w, height=self.h)
        self.entities = []
        # self._init_depth()
        self._init_msaa(samples=4)

    def _init_depth(self):
        self.depth_map_fbo = GLuint(0)
        glGenFramebuffers(1, self.depth_map_fbo)
        self.shadow_w, self.shadow_h = 1024, 1024
        self.depth_map = GLuint(0)
        glGenTextures(1, self.depth_map)
        glBindTexture(GL_TEXTURE_2D, self.depth_map)
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, self.shadow_w, self.shadow_h, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)
        glBindFramebuffer(GL_FRAMEBUFFER, self.depth_map_fbo)
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, self.depth_map, 0)
        glDrawBuffer(GL_NONE)
        glReadBuffer(GL_NONE)
        glBindFramebuffer(GL_FRAMEBUFFER, 0)

        ####
        light_proj = ortho(-10.0, 10.0, -10.0, 10.0, 1.0, 7.5)
        # light_view = look_at(self.light_pos, (10, 0, 10), (0, 1, 0))
        light_view = look_at(self.light_pos, (0, 0, 0), (0, 0, 1))
        light_proj_c, light_view_c = self._get_c_mat(light_proj), self._get_c_mat(light_view)

        glUseProgram(self.depth_shader)
        light_proj_loc = glGetUniformLocation(self.depth_shader, 'lightProjection'.encode('utf-8'))
        light_view_loc = glGetUniformLocation(self.depth_shader, 'lightView'.encode('utf-8'))
        glUniformMatrix4fv(light_proj_loc, 1, GL_FALSE, light_proj_c)
        glUniformMatrix4fv(light_view_loc, 1, GL_FALSE, light_view_c)

        glUseProgram(self.shader)
        light_proj_loc = glGetUniformLocation(self.shader, 'lightProjection'.encode('utf-8'))
        light_view_loc = glGetUniformLocation(self.shader, 'lightView'.encode('utf-8'))
        glUniformMatrix4fv(light_proj_loc, 1, GL_FALSE, light_proj_c)
        glUniformMatrix4fv(light_view_loc, 1, GL_FALSE, light_view_c)

    def _init_msaa(self, samples):
        self.framebuffer = GLuint(0)
        glGenFramebuffers(1, self.framebuffer)
        glBindFramebuffer(GL_FRAMEBUFFER, self.framebuffer)
        
        textureColorBufferMultiSampled = GLuint(0)
        glGenTextures(1, textureColorBufferMultiSampled)
        glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, textureColorBufferMultiSampled)
        glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, samples, GL_RGB, self.w, self.h, GL_TRUE)
        glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, 0)
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D_MULTISAMPLE, textureColorBufferMultiSampled, 0)
        
        rbo = GLuint(0)
        glGenRenderbuffers(1, rbo)
        glBindRenderbuffer(GL_RENDERBUFFER, rbo)
        glRenderbufferStorageMultisample(GL_RENDERBUFFER, samples, GL_DEPTH24_STENCIL8, self.w, self.h)
        glBindRenderbuffer(GL_RENDERBUFFER, 0)
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo)
        if glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE:
            print('Error: Framebuffer is not complete!')
        glBindFramebuffer(GL_FRAMEBUFFER, 0)

        self.intermediateFBO = GLuint(0)
        glGenFramebuffers(1, self.intermediateFBO)
        glBindFramebuffer(GL_FRAMEBUFFER, self.intermediateFBO)
        self.screenTexture = GLuint(0)
        glGenTextures(1, self.screenTexture)
        glBindTexture(GL_TEXTURE_2D, self.screenTexture)
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, self.w, self.h, 0, GL_RGB, GL_UNSIGNED_BYTE, 0)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, self.screenTexture, 0)
        if glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE:
            print('Error: Intermediate framebuffer is not complete!')
        glBindFramebuffer(GL_FRAMEBUFFER, 0)

        glUseProgram(self.screen_shader)
        screen_texture_loc = glGetUniformLocation(self.shader, 'screenTexture'.encode('utf-8'))
        glUniform1i(screen_texture_loc, 0)

        quad = Quad(self.screen_shader)
        self.quad_vao = quad.vao

    def get_dt(self):
        curr_time = time()
        dt = curr_time - self.last_time
        self.last_time = curr_time
        return dt

    def update(self, *args, **kwargs):
        dt = self.get_dt()
        self.viewer.on_keyboard_activity(dt)
        self.light_pos = self.viewer.camera.pos + (-1, -1, 1)
        for entity in self.entities:
            entity.update(*args, **kwargs, dt=dt)
        # self.viewer.camera.print_debug()

    def _get_c_mat(self, mat):
        mat = mat.flatten()
        return (GLfloat * len(mat))(*mat)

    def _render(self):
        for entity in self.entities:
            entity.render()

    def render(self):
        self.viewer.on_draw()

        glBindFramebuffer(GL_FRAMEBUFFER, self.framebuffer)
        self.viewer.on_draw()
        glEnable(GL_DEPTH_TEST)

        glUseProgram(self.shader)
        view_loc = glGetUniformLocation(self.shader, 'view'.encode('utf-8'))
        proj_loc = glGetUniformLocation(self.shader, 'projection'.encode('utf-8'))
        view_pos_loc = glGetUniformLocation(self.shader, 'viewPos'.encode('utf-8'))

        view = self.viewer.camera.get_view_matrix()
        proj = perspective(self.viewer.camera.zoom, self.w / self.h, 0.1, 100.0)
        view_c, proj_c = self._get_c_mat(view), self._get_c_mat(proj)

        glUniform3f(view_pos_loc, *self.viewer.camera.pos)
        glUniformMatrix4fv(view_loc, 1, GL_FALSE, view_c)
        glUniformMatrix4fv(proj_loc, 1, GL_FALSE, proj_c)

        light_color_loc = glGetUniformLocation(self.shader, 'lightColor'.encode('utf-8'))
        light_pos_loc = glGetUniformLocation(self.shader, 'lightPos'.encode('utf-8'))
        glUniform3f(light_color_loc, *self.light_color)
        glUniform3f(light_pos_loc, *self.light_pos)

        self._render()

        glBindFramebuffer(GL_READ_FRAMEBUFFER, self.framebuffer)
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, self.intermediateFBO)
        glBlitFramebuffer(0, 0, self.w, self.h, 0, 0, self.w, self.h, GL_COLOR_BUFFER_BIT, GL_NEAREST)

        glBindFramebuffer(GL_FRAMEBUFFER, 0)
        glClearColor(1.0, 1.0, 1.0, 1.0)
        glClear(GL_COLOR_BUFFER_BIT)
        glDisable(GL_DEPTH_TEST)

        glUseProgram(self.screen_shader)
        glBindVertexArray(self.quad_vao)
        glActiveTexture(GL_TEXTURE0)
        glBindTexture(GL_TEXTURE_2D, self.screenTexture)
        glDrawArrays(GL_TRIANGLES, 0, 6)

        self.viewer.flip()

    def add_entity(self, entity):
        self.entities.append(entity)

    def remove_entity(self, entity_class):
        removal = None
        for entity in self.entities:
            if isinstance(entity, entity_class):
                removal = entity
        self.entities.remove(removal)