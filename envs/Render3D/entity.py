from pyglet.gl import *
import ctypes
import numpy as np
from .utils import *
import os, shutil

RESULT_PATH = '../results/'

class Entity():

    def __init__(self, name, shader, pos=None, ext=None, color=None):
        self.name = name
        self.shader = shader
        self.pos = pos
        self.ext = ext
        self.color = color

    def render(self):
        raise NotImplementedError

    def update(self, *args, **kwargs):
        raise NotImplementedError

    def bind_vao(self, obj_data, num_attr_list):
        glUseProgram(self.shader)

        vao, vbo = GLuint(0), GLuint(0)
        glGenVertexArrays(1, vao)
        glGenBuffers(1, vbo)
        glBindVertexArray(vao)
        glBindBuffer(GL_ARRAY_BUFFER, vbo)
        len_data = len(obj_data)
        glBufferData(GL_ARRAY_BUFFER, 4 * len_data, (GLfloat * len_data)(*obj_data), GL_STATIC_DRAW)
        
        num_attr_acc = 0
        for i in range(len(num_attr_list)):
            glVertexAttribPointer(i, num_attr_list[i], GL_FLOAT, GL_FALSE, 4 * sum(num_attr_list), ctypes.c_void_p(4 * num_attr_acc))
            glEnableVertexAttribArray(i)
            num_attr_acc += num_attr_list[i]
        glBindVertexArray(0)

        return vao


class Cube(Entity):

    def __init__(self, name, shader, pos, ext, color, pov=False, epi=0):
        super().__init__(name, shader, pos, ext, color)
        self.vao = self.bind_vao(cube_vertices, [3, 3])
        self.pov = pov
        if self.pov:
            self.pov_ite = self.write_pov(RESULT_PATH, epi)
            os.makedirs(RESULT_PATH + 'pov/' + self.name, exist_ok=True)

    def render(self):
        glUseProgram(self.shader)

        model_loc = glGetUniformLocation(self.shader, 'model'.encode('utf-8'))
        model = (translate(self.pos).T.dot(scale(self.ext))).T.flatten()
        model_c = (GLfloat * len(model))(*model)
        glUniformMatrix4fv(model_loc, 1, GL_FALSE, model_c)

        object_color_loc = glGetUniformLocation(self.shader, 'objectColor'.encode('utf-8'))
        glUniform3f(object_color_loc, *self.color)

        glBindVertexArray(self.vao)
        glDrawArrays(GL_TRIANGLES, 0, 36)
        glBindVertexArray(0)

    def update(self, *args, **kwargs):
        self.pos = kwargs[self.name + '_pos']
        if self.pov:
            next(self.pov_ite)

    def write_pov(self, path, epi):
        full_path = path + '/pov/' + self.name + '/' + str(epi) + '/'
        if os.path.exists(full_path):
            shutil.rmtree(full_path)
        os.makedirs(full_path, exist_ok=True)
        i = 0
        while True:
            with open(full_path + 'C%d.pov' % i, 'w') as file:
                file.write('box {<' + str(self.pos[0] - self.ext[0] / 2) + ',' + str(self.pos[1] - self.ext[1] / 2) + ',' + str(self.pos[2] - self.ext[2] / 2) + '>,<'+ 
                    str(self.pos[0] + self.ext[0] / 2) + ',' + str(self.pos[1] + self.ext[1] / 2) + ',' + str(self.pos[2] + self.ext[2] / 2) + '>}')
            i += 1
            yield


# class Light(Cube):

#     def render(self):
#         glUseProgram(self.shader)
#         light_color_loc = glGetUniformLocation(self.shader, 'lightColor'.encode('utf-8'))
#         light_pos_loc = glGetUniformLocation(self.shader, 'lightPos'.encode('utf-8'))
#         glUniform3f(light_color_loc, *self.color)
#         glUniform3f(light_pos_loc, *self.pos)
#         super().render()

#     def update(self, dt):
#         # self.pos = self.camera.pos
#         pass


class Skybox(Entity):

    def __init__(self, shader):
        self.shader = shader
        self.vao = self.bind_vao(skybox_vertices, [3])

    def render(self):
        pass


class Sphere(Entity):

    def __init__(self, name, shader, pos, ext, color, pov=False, epi=0):
        super().__init__(name, shader, pos, ext, color)
        assert self.ext[0] == self.ext[1] == self.ext[2]
        r = self.ext[0] * 4
        def F(u, v):
            return [np.cos(u) * np.sin(v) * r, np.cos(v) * r, np.sin(u) * np.sin(v) * r] * 2
        vertices = []

        ures, vres = 40, 80
        du, dv = np.pi * 2 / ures, np.pi / vres
        for i in range(ures):
            for j in range(vres):
                u, v = i * du, j * dv
                un = np.pi * 2 if i + 1 == ures else (i + 1) * du
                vn = np.pi if j + 1 == vres else (j + 1) * dv
                p0, p1, p2, p3 = F(u, v), F(u, vn), F(un, v), F(un, vn)
                vertices += p0 + p2 + p1 + p3 + p1 + p2
        self.vao = self.bind_vao(vertices, [3, 3])
        self.vertices_len = len(vertices)
        self.pov = pov
        if self.pov:
            self.pov_ite = self.write_pov('../results', epi)
            os.makedirs(RESULT_PATH + 'pov/' + self.name, exist_ok=True)

    def render(self):
        glUseProgram(self.shader)

        model_loc = glGetUniformLocation(self.shader, 'model'.encode('utf-8'))
        model = (translate(self.pos).T.dot(scale(self.ext))).T.flatten()
        model_c = (GLfloat * len(model))(*model)
        glUniformMatrix4fv(model_loc, 1, GL_FALSE, model_c)

        object_color_loc = glGetUniformLocation(self.shader, 'objectColor'.encode('utf-8'))
        glUniform3f(object_color_loc, *self.color)

        glBindVertexArray(self.vao)
        glDrawArrays(GL_TRIANGLES, 0, self.vertices_len // 6)
        glBindVertexArray(0)

    def update(self, *args, **kwargs):
        self.pos = kwargs[self.name + '_pos']
        if self.pov:
            next(self.pov_ite)

    def write_pov(self, path, epi):
        full_path = path + '/pov/' + self.name + '/' + str(epi) + '/'
        if os.path.exists(full_path):
            shutil.rmtree(full_path)
        os.makedirs(full_path, exist_ok=True)
        i = 0
        while True:
            with open(full_path + 'S%d.pov' % i, 'w') as file:
                file.write('sphere {<' + str(self.pos[0]) + ',' + str(self.pos[1]) + ',' + str(self.pos[2]) + '>,'+ str(self.ext[0] / 2) + '}')
            i += 1
            yield


class Mesh(Entity):

    def __init__(self, shader, sol, goodlooking):
        self.name = 'mesh'
        self.shader = shader
        self.sol = sol
        self.w = None
        self.b = None
        self.pos = (0, 0, 0)
        self.ext = (1, 1, 1)
        self.color = (0.25, 0.64, 0.87)
        self.goodlooking = goodlooking
        self.step = 0

    def render(self):
        glUseProgram(self.shader)
        model_loc = glGetUniformLocation(self.shader, 'model'.encode('utf-8'))
        model = (scale(self.ext)).flatten()
        model_c = (GLfloat * len(model))(*model)
        glUniformMatrix4fv(model_loc, 1, GL_FALSE, model_c)

        object_color_loc = glGetUniformLocation(self.shader, 'objectColor'.encode('utf-8'))
        glUniform3f(object_color_loc, *self.color)

        glBindVertexArray(self.vao)
        if self.goodlooking:
            glDrawArrays(GL_TRIANGLES, 0, len(self.w))
        else:
            glDrawArrays(GL_POINTS, 0, len(self.w))
        glBindVertexArray(0)

    def update(self, *args, **kwargs):
        ws = self.sol.getWS()
        ws_obj = ws.toObj(True)
        self.w = []

        if not self.goodlooking:
            for i in range(ws_obj.nrV()):
                v = ws_obj.getV(i)
                self.w += [v[0], v[1], v[2]]
            self.vao = self.bind_vao(self.w, [3])
        else:
            ws_obj.subdivide(1)
            n_v, n_i = ws_obj.nrV(), ws_obj.nrI()

            # generate normal
            norms = np.zeros((n_v + 1) * 3)

            for i in range(n_i):
                face = ws_obj.getI(i)
                v1, v2, v3 = ws_obj.getV(face[0]), ws_obj.getV(face[1]), ws_obj.getV(face[2])
                e1, e2 = v2 - v1, v3 - v1
                norm = e1.cross(e2)
                norms[face[0]*3:(face[0]+1)*3] += [norm[0], norm[1], norm[2]]
                norms[face[1]*3:(face[1]+1)*3] += [norm[0], norm[1], norm[2]]
                norms[face[2]*3:(face[2]+1)*3] += [norm[0], norm[1], norm[2]]

            for i in range(n_v):
                norms[i*3:(i+1)*3] = normalize(norms[i*3:(i+1)*3])
            
            for i in range(n_i):
                face = ws_obj.getI(i)
                v1, v2, v3 = ws_obj.getV(face[0]), ws_obj.getV(face[1]), ws_obj.getV(face[2])
                n1, n2, n3 = norms[face[0]*3:(face[0]+1)*3], norms[face[1]*3:(face[1]+1)*3], norms[face[2]*3:(face[2]+1)*3]
                self.w += [v1[0], v1[1], v1[2]] + n1.tolist() + \
                    [v2[0], v2[1], v2[2]] + n2.tolist() + \
                    [v3[0], v3[1], v3[2]] + n3.tolist()

            self.vao = self.bind_vao(self.w, [3, 3])

        self.step += 1


class Quad(Entity):

    def __init__(self, shader):
        self.shader = shader
        vertices = [
            -1.0, 1.0, 0.0, 1.0,
            -1.0, -1.0, 0.0, 0.0,
            1.0, -1.0, 1.0, 0.0,

            -1.0, 1.0, 0.0, 1.0,
            1.0, -1.0, 1.0, 0.0,
            1.0, 1.0, 1.0, 1.0
        ]
        self.vao = self.bind_vao(vertices, [2, 2])
