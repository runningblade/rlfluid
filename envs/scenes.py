import math
import numpy as np
import ctypes as ct
import os
import copy
from collections import OrderedDict

from numpy import shape

from sceneSetup import *
from fluidRender_new import *

from gym import Env, spaces
from gym.utils import seeding
from recorder_new import GameSingleRunner
from time import time

from Render3D.renderer import Renderer3D
from Render3D.entity import *

FPS = 100
RESULT_PATH = '../results'


class BaseFluid(Env):
    # implementation
    metadata = {
        'render.modes': ['human', 'rgb_array'],
        'video.frames_per_second': FPS
    }

    def __init__(self, *args, **kwargs):  # render when training
        # random seed
        self.np_random = None
        self.seed(kwargs['seed'])
        # basic
        self.viewers  = []
        self.now_step = 0
        self.max_step = kwargs['max_step']
        self.res      = kwargs['res']
        self.debug    = kwargs['debug']
        self.vtk      = kwargs['vtk']
        self.pov      = kwargs['pov']
        self.epi      = 0
        # geometry
        self.w      = kwargs['w']
        self.h      = kwargs['h']
        self.sz     = kwargs['sz']
        # physics
        self.sol     = None
        self.warp_on = kwargs['warp_on']
        self.ft_sz   = None
        self.rgd_rho = kwargs['rgd_rho']
        self.reward  = kwargs['reward']
        # render
        self.renderer = None
        self.obs_dict = None
        high = np.inf * np.ones(kwargs['obs_num'])
        self.observation_space = spaces.Box(-high, high, dtype=np.float32)
        self.action_space      = spaces.Box(
            -np.ones(kwargs['act_num']),
            np.ones(kwargs['act_num']),
            dtype=np.float32
        )
        self.reset()

    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def _reset_sh(self):
        """
        Here we reset shooters properties to default ones:
        sh_pos, sh_vel, sh_theta
        This func should be rewritten in each env
        :return: Nothing returned
        """
        pass

    def _clear_param(self):
        # should be overwritten, this is only the common part
        # in other word, extend this func if thr other params to clear
        self.now_step = 0
        self.ft_sz = self.sol.get_ft_sz(self.sz)
        self._reset_sh()
        self._set_sink()

    def _set_sink(self):
        raise NotImplementedError

    def _create_sol(self):
        raise NotImplementedError

    def _create_rgd(self):
        self.sol.init_solid()

    def _get_renderer(self):
        raise NotImplementedError

    def _init_domain(self):
        raise NotImplementedError

    def _init_sol(self):
        raise NotImplementedError

    def reset(self):
        self._create_sol()
        self.sol.update_sol()
        self._init_domain()
        self._create_rgd()
        self._init_sol()
        self._clear_param()
        if self.debug:
            self.renderer = self._get_renderer()
        if self.vtk:
            self.vtk_ite = self.sol.write_vtk(RESULT_PATH, self.epi)
        if self.pov:
            self.pov_ite = self.sol.write_pov(RESULT_PATH, self.epi)
        self.epi += 1
        return self.step(self.action_space.sample())[0]

    def step(self, action):
        # ensure its a valid action
        low = self.action_space.low
        high = self.action_space.high
        action = np.clip(action, low, high)
        assert self.action_space.contains(action), "%r (%s) invalid " % (action, type(action))
        # record the steps
        self.now_step += 1
        self._update_sh(action)

        # Advance
        self.sol.advance(1 / FPS)

        # Get Observations
        self.obs_dict = OrderedDict()
        self._get_obs(self.obs_dict)
        obs = np.array(list(self.obs_dict.values()))
        assert self.observation_space.contains(obs), "%r (%s) invalid " % (obs, type(obs))

        # Get Reward
        reward = self._get_reward()

        # Get Information
        info = dict()
        info['speed'] = None
        # self._get_info(info)

        # Is the game over?
        game_over = self._get_game_over()

        # Get Utilities
        self._get_util()

        if self.vtk:
            next(self.vtk_ite)

        if self.pov:
            next(self.pov_ite)

        if self.debug:
            # in case u wanna c the training process visually
            self.render()

        return obs, reward, game_over, info

    def _update_sh(self, action):
        raise NotImplementedError

    def _get_info(self, info):
        pass

    def _get_util(self):
        pass

    def _get_game_over(self):
        return self.now_step > self.max_step

    def _get_obs(self, obs_dict):
        pass

    def _get_reward(self):
        raise NotImplementedError

    def render(self, mode='human', close=False):
        return self.viewer.render(return_rgb_array=mode == 'rgb_array')

    def sample_speed(self):
        pass


class BaseSWE(BaseFluid):

    def __init__(self, *args, **kwargs):
        self.dim = 3
        self.src_num = kwargs['src_num']
        self.d    = kwargs['d']
        self.c_d  = kwargs['c_d']
        self.c_dl = kwargs['c_dl']
        self.c_l  = kwargs['c_l']
        self.sink_h = kwargs['sink_h']
        self.src_def_h = kwargs['src_def_h']
        self.f_mv = kwargs['f_mv']
        self.f_sh = kwargs['f_sh']
        self.water_d = kwargs['water_d']
        self.target_x_range = kwargs['target_x_range']
        self.target_y_range = kwargs['target_y_range']
        self.target_z_range = kwargs['target_z_range']
        self.target_threshold = kwargs['target_threshold']
        self.game_over = kwargs['game_over']
        self.first_time = True
        super().__init__(*args, **kwargs)
        self.first_time = False

    def _clear_param(self):
        super()._clear_param()
        self.sol.focus_on1(self.w, self.h)
        self.target_x = np.random.uniform(*self.target_x_range)
        self.target_y = np.random.uniform(*self.target_y_range)
        self.target_z = np.random.uniform(*self.target_z_range)

    def _create_sol(self):
        self.sol = SWESol()

    def _get_info(self, info):
        super()._get_info(info)

    def _get_util(self):
        super()._get_util()

    def _get_game_over(self):
        return super()._get_game_over()

    def _get_obs(self, obs_dict):
        super()._get_obs(obs_dict)
        obs_dict['target_x'] = self.target_x
        obs_dict['target_y'] = self.target_y
        obs_dict['target_z'] = self.target_z

    def _get_renderer(self):
        if self.first_time:
            renderer = Renderer3D(5, 3, self.res, camera_pos=(0.5, 0.5, 1.3), world_up=(0, 0, 1))
            renderer.add_entity(Mesh(renderer.shader, self.sol.sol, goodlooking=False))
            renderer.add_entity(Sphere('ball', renderer.shader, pos=(self.rgd_x, self.rgd_y, self.rgd_z),
                                       ext=(self.rgd_w, self.rgd_h, self.rgd_d), 
                                       color=(0, 1, 0), pov=self.pov))
            renderer.add_entity(Cube('target', renderer.shader, pos=(self.target_x, self.target_y, self.target_z),
                                       ext=(self.target_threshold * 2, self.target_threshold * 2, self.target_threshold * 2), 
                                       color=(1, 0, 0), pov=self.pov))
            return renderer
        else:
            self.renderer.remove_entity(Mesh)
            for entity in self.renderer.entities:
                entity.pov_ite = entity.write_pov(RESULT_PATH, self.epi)
            self.renderer.add_entity(Mesh(self.renderer.shader, self.sol.sol, goodlooking=False))
            return self.renderer

    def render(self, mode='human', close=False):
        self.renderer.update(ball_pos=(self.obs_dict['rgd_pos_x'], self.obs_dict['rgd_pos_y'], self.obs_dict['rgd_pos_z']),
                             target_pos=(self.target_x, self.target_y, self.target_z))
        self.renderer.render()
        return None

    def _init_sol(self):
        self.sol.init_sol(self.water_d, self.c_d, self.c_dl, self.c_l)

    def _init_domain(self):
        self.sol.init_domain(self.warp_on, self.sz, self.dim, self.w, self.h, self.d)

class BaseLiquid(BaseFluid):

    def __init__(self, *args, **kwargs):
        self.mesh_v_sz = 0
        self.mesh_i_sz = 0
        self.dim = 2
        self.sh_pos         = None
        self.sh_vel         = None
        self.sh_theta       = None
        self.sh_def_theta   = None
        self.sh_delta_theta = kwargs['sh_delta_theta']
        self.jets    = None
        self.f_mv    = kwargs['f_mv']
        self.f_sh    = kwargs['f_sh']
        self.omega   = kwargs['omega']
        self.sh_rad  = kwargs['sh_rad']
        super().__init__(*args, **kwargs)

    def _create_sol(self):
        self.sol = LiquidSol()

    def _get_info(self, info):
        info['ft'] = self.sol.get_ft()
        super()._get_info(info)

    def _get_util(self):
        self.mesh_v_sz, self.mesh_i_sz = self.sol.get_mesh()
        super()._get_util()

    def _get_game_over(self):
        if self.sol.is_ct():
            return True
        else:
            return super()._get_game_over()

    def _init_sol(self):
        self.sol.init_sol(True, False, True)

    def _set_sink(self):
        self.sol.add_sink_2D(self.w, self.h, self.sz)

    def _init_domain(self):
        self.sol.init_domain(self.warp_on, self.sz, self.dim, self.w, self.h)

class BaseSmoke(BaseFluid):

    def __init__(self, *args, **kwargs):
        self.dim = 2
        self.sh_pos         = None
        self.sh_vel         = None
        self.sh_theta       = None
        self.sh_def_theta   = None
        self.sh_delta_theta = kwargs['sh_delta_theta']
        self.jets   = None
        self.f_mv   = kwargs['f_mv']
        self.f_sh   = kwargs['f_sh']
        self.omega  = kwargs['omega']
        self.sh_rad = kwargs['sh_rad']
        super().__init__(*args, **kwargs)

    def _create_sol(self):
        self.sol = SmokeSol()

    def _reset_sh(self):
        super()._reset_sh()

    def _get_info(self, info):
        info['ft'] = self.sol.get_ft()
        super()._get_info(info)

    def _get_game_over(self):
        if self.sol.is_ct():
            return True
        else:
            return super()._get_game_over()

    def _set_sink(self):
        self.sol.add_sink_2D(self.w, self.h, self.sz)

    def _init_domain(self):
        self.sol.init_domain(self.warp_on, self.sz, self.dim, self.w, self.h)


class BaseSingle(BaseFluid):

    def __init__(self, *args, **kwargs):
        self.sh_rad = kwargs['sh_rad']
        self.sh_obj = Ball(self.sh_rad * 2, self.sh_rad * 2, '../envs/texs/pipe.png', (0.5, 0.4, 0.9))
        super().__init__(*args, **kwargs)

    def _reset_sh(self):
        self.sh_pos = np.array([self.w / 2], dtype=np.float32)
        self.sh_vel = np.array([0], dtype=np.float32)
        self.sh_def_theta = np.array([np.pi / 2], dtype=np.float32)
        self.jets = np.array([False], dtype=np.bool)
        s = SphereSrcSol(
            (self.w / 2, self.sh_rad, 0),
            self.sh_rad,
            self.dim,
            1 / self.sz,
            (0, 0, 0)
        )
        self.sol.add_srcs([s])
        self.sh_theta = copy.deepcopy(self.sh_def_theta)
        super()._reset_sh()

    def _update_sh(self, action):
        vol, omega, force = tuple(action.flatten())
        last_sh_pos = copy.deepcopy(self.sh_pos)
        self.sh_vel += force * self.f_mv / FPS
        self.sh_pos += self.sh_vel / FPS
        self.sh_pos = np.clip(self.sh_pos, 0, self.w)
        self.sh_vel = (self.sh_pos - last_sh_pos) * FPS
        self.sh_theta += omega * self.omega / FPS
        self.sh_theta = np.clip(
            self.sh_theta,
            self.sh_def_theta - self.sh_delta_theta,
            self.sh_def_theta + self.sh_delta_theta
        )
        self.jets[0] = np.random.rand() * 2 - 1 < vol
        self.sol.update_src_h(self.sh_theta, self.sh_pos, self.jets, self.f_sh)

    def _get_obs(self, obs_dict):
        obs_dict['sh_pos'] = self.sh_pos[0]
        obs_dict['sh_vel'] = self.sh_vel[0]
        obs_dict['sh_theta'] = self.sh_theta[0]
        super()._get_obs(obs_dict)


class BaseSWEMulti1Way(BaseFluid):

    def __init__(self, *args, **kwargs):
        self.sh_pos = None
        # self.sh_obj = Ball(self.sh_rad * 2, self.sh_rad * 2, '../envs/texs/pipe.png', (0.5, 0.4, 0.9))
        super().__init__(*args, **kwargs)

    def _reset_sh(self):
        src = [
            RectSrcSol(
                (i * self.w / self.src_num, 0, self.src_def_h),
                ((i + 1) * self.w / self.src_num, 2 / self.sz, self.src_def_h),
                self.dim,
                1.0 / self.sz,
                1
            ) for i in range(self.src_num)
        ]
        self.sh_pos = np.array([self.src_def_h for i in range(len(src))])
        self.sol.add_srcs(src)
        super()._reset_sh()

    def _update_sh(self, action):
        # action = [(2 * num) vels, (2 * num) spds]
        vels, spds = tuple(action.reshape((2, -1)))
        spds = np.clip((spds + 1) / 2, 0, 1)
        self.sh_pos += vels * self.f_mv
        self.sh_pos = np.clip(
            self.sh_pos,
            self.water_d,
            np.min([self.trap_h * 1.25, self.d])
        )
        self.sol.update_3Dsrc(self.sh_pos, spds * self.f_sh)

    def _get_obs(self, obs_dict):
        for i, pos in enumerate(self.sh_pos):
            obs_dict['sh_pos_%d' % i] = pos
        super()._get_obs(obs_dict)

    def _set_sink(self):
        self.sol.add_sink_3D_1Way(self.w, self.h, self.sink_h, self.sz)


class BaseSWEMulti2Way(BaseFluid):

    def __init__(self, *args, **kwargs):
        self.sh_pos = None
        # self.sh_obj = Ball(self.sh_rad * 2, self.sh_rad * 2, '../envs/texs/pipe.png', (0.5, 0.4, 0.9))
        super().__init__(*args, **kwargs)

    def _reset_sh(self):
        src = [
            RectSrcSol(
                (i * self.w / self.src_num, 0, self.src_def_h),
                ((i + 1) * self.w / self.src_num, 1 / self.sz, self.src_def_h),
                self.dim,
                1.0 / self.sz,
                1
            ) for i in range(self.src_num)
        ] + [
            RectSrcSol(
                (i * self.w / self.src_num, self.h - 1 / self.sz, self.src_def_h),
                ((i + 1) * self.w / self.src_num, self.h, self.src_def_h),
                self.dim,
                1.0 / self.sz,
                -1
            ) for i in range(self.src_num)
        ]
        self.sh_pos = np.array([self.src_def_h for i in range(len(src))])
        self.sol.add_srcs(src)
        super()._reset_sh()

    def _update_sh(self, action):
        # action = [(2 * num) vels, (2 * num) spds]
        vels, spds = tuple(action.reshape((2, -1)))
        spds = np.clip((spds + 1) / 2, 0, 1)
        self.sh_pos += vels * self.f_mv
        self.sh_pos = np.clip(
            self.sh_pos,
            self.water_d,
            np.min([self.trap_h * 1.2, self.d])
        )
        self.sol.update_3Dsrc(self.sh_pos, spds * self.f_sh)

    def _get_obs(self, obs_dict):
        for i, pos in enumerate(self.sh_pos):
            obs_dict['sh_pos_%d' % i] = pos
        super()._get_obs(obs_dict)

    def _set_sink(self):
        self.sol.add_sink_3D_2Way(self.w, self.h, self.sink_h, self.sz)


class Base3DRgd(BaseFluid):

    def __init__(self, *args, **kwargs):
        self.rgd_w, self.rgd_h, self.rgd_d = kwargs['rgd_w'], kwargs['rgd_h'], kwargs['rgd_d']
        self.rgd_x, self.rgd_y, self.rgd_z = kwargs['rgd_x'], kwargs['rgd_y'], kwargs['rgd_z']
        # self.rgd_pic = kwargs['rgd_pic']
        rgd_obj_map = {'cube': Cube, 'sphere': Sphere}
        self.rgd_type = kwargs['rgd_type']
        self.trap_h = kwargs['trap_h']
        self.trap_t = kwargs['trap_t']
        # need to be fixed
        # self.rgd_obj = rgd_obj_map[self.rgd_type](self.rgd_w, self.rgd_h, self.rgd_pic)
        super().__init__(*args, **kwargs)

    def _create_rgd(self):
        self.sol.add_bottom(self.w, self.h)
        self.sol.add_trap(self.trap_h, self.trap_t, self.w, self.h)
        self.sol.add_rgd(
            self.rgd_type,
            (self.rgd_w / 2, self.rgd_h / 2, self.rgd_d / 2),
            (self.rgd_x, self.rgd_y, self.rgd_z),
            self.rgd_rho,
        )
        super()._create_rgd()

    def _get_obs(self, obs_dict):
        obs_dict['rgd_pos_x'], obs_dict['rgd_pos_y'], obs_dict['rgd_pos_z'] = self.sol.get_rgd_pos(3)
        obs_dict['rgd_vel_x'], obs_dict['rgd_vel_y'], obs_dict['rgd_vel_z'] = self.sol.get_rgd_vel(3)
        obs_dict['rgd_frc_x'], obs_dict['rgd_frc_y'], obs_dict['rgd_frc_z'] = self.sol.get_rgd_frc(3)
        super()._get_obs(obs_dict)


class Base2DRgd(BaseFluid):
    
    def __init__(self, *args, **kwargs):
        self.rgd_w, self.rgd_h, self.rgd_pic = kwargs['rgd_w'], kwargs['rgd_h'], kwargs['rgd_pic']
        rgd_obj_map = {'ball': Ball, 'cross': Cross, 'box': Rectangle}
        self.rgd_type = kwargs['rgd_type']
        self.rgd_obj = rgd_obj_map[self.rgd_type](self.rgd_w, self.rgd_h, self.rgd_pic)
        super().__init__(*args, **kwargs)

    def _create_rgd(self):
        self.sol.add_rgd(
            self.rgd_type,
            (self.rgd_w / 2, self.rgd_h / 2, 0), 
            (self.w / 2, self.h / 2, 0),
            self.rgd_rho
        )
        super()._create_rgd()

    def _get_obs(self, obs_dict):
        obs_dict['rgd_pos_x'], obs_dict['rgd_pos_y'] = self.sol.get_rgd_pos(2)
        obs_dict['rgd_vel_x'], obs_dict['rgd_vel_y'] = self.sol.get_rgd_vel(2)
        obs_dict['rgd_frc_x'], obs_dict['rgd_frc_y'] = self.sol.get_rgd_frc(2)
        obs_dict['rgd_theta'] = self.sol.get_rgd_theta()
        obs_dict['rgd_omega'] = self.sol.get_rgd_omega()
        obs_dict['rgd_trq']   = self.sol.get_rgd_trq()
        super()._get_obs(obs_dict)


class LiquidSingle2DRgd(BaseLiquid, BaseSingle, Base2DRgd):
    def _get_reward(self):
        reward = self.reward(
            (self.obs_dict['rgd_pos_x'], self.obs_dict['rgd_pos_y']),
            (self.obs_dict['rgd_vel_x'], self.obs_dict['rgd_vel_y']),
            self.jets[0]
        )
        return reward

    def __init__(self, *args, **kwargs):
        self.bg_obj = Background(kwargs['w'], kwargs['h'], '../envs/texs/grey.jpg', (0, 0, 0))
        self.keymap = [{'w': 1, 'else': -1}, {'left': 1, 'right': -1}, {'d': 1, 'a': -1}]

        super().__init__(*args, **kwargs)

    def _get_renderer(self):
        return Renderer('p', self.sol, self.w, self.h, self.res, self.bg_obj, self.rgd_obj, self.sh_obj)

    def render(self, mode='human', close=False):
        self.viewers, result = self.renderer.render(
            self.viewers, mode, close, [(self.sh_pos[0], self.sh_rad / 2)], [self.sh_theta[0]]
        )
        return result


class SWEMulti1Way3DRgd(BaseSWE, BaseSWEMulti1Way, Base3DRgd):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.keymap = None

    def _get_reward(self):
        return self.reward(obs=self.obs_dict, time=self.now_step)

    def _get_game_over(self):
        # print(self.obs_dict['rgd_pos_x'], self.target_x, self.obs_dict['rgd_pos_y'], self.target_y)
        return super()._get_game_over() or self.game_over(obs=self.obs_dict)


class SWEMulti2Way3DRgd(BaseSWE, BaseSWEMulti2Way, Base3DRgd):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.keymap = None

    def _get_reward(self):
        return self.reward(obs=self.obs_dict, time=self.now_step)

    def _get_game_over(self):
        # print(self.obs_dict['rgd_pos_x'], self.target_x, self.obs_dict['rgd_pos_y'], self.target_y)
        return super()._get_game_over() or self.game_over(obs=self.obs_dict)


# TODO:
# LiquidMusicSingle
# LiquidBattle
# LiquidBottombattle
# Discrete vs Continuous
