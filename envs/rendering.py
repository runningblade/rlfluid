import gym.envs.classic_control.rendering
from gym.envs.classic_control.rendering import _add_attrs
from gym.envs.classic_control.rendering import *
import ctypes as ct

try:
    from pyglet.gl.glu import *
except ImportError as e:
    reraise(prefix="Error occured while running `from pyglet.gl.glu import *`")


def take_screenshot_trans(self, path=None):
    glReadBuffer(GL_FRONT)
    data = (GLubyte * (4 * self.width * self.height))(0)
    glReadPixels(0, 0, self.width, self.height, GL_RGBA, GL_UNSIGNED_BYTE, data)
    from PIL import Image, ImageOps
    image = Image.frombytes('RGBA', (self.width, self.height), data)
    image = ImageOps.flip(image)
    data = np.array(image)
    r, g, b, a = data.T
    trans = (r == 0) & (g == 0) & (b == 0)
    a[trans] = 0
    halftrans = (r == 255) & (g == 255) & (b == 255)
    a[halftrans] = 180
    image = Image.fromarray(np.array([r, g, b, a]).T)
    if path is not None:
        image.save(path, 'PNG')
    return image

def take_screenshot(self, path=None):
    glReadBuffer(GL_FRONT)
    data = (GLubyte * (4 * self.width * self.height))(0)
    glReadPixels(0, 0, self.width, self.height, GL_RGBA, GL_UNSIGNED_BYTE, data)
    from PIL import Image, ImageOps
    image = Image.frombytes('RGBA', (self.width, self.height), data)
    image = ImageOps.flip(image)
    if path is not None:
        image.save(path, 'PNG')
    return image

def draw_background_image(self, fname, width, height, **attrs):
    geom = Image(fname, width, height)
    _add_attrs(geom, attrs)
    self.add_geom(geom)
    return geom

def draw_background_polygon(self, v, filled=True, **attrs):
    geom = make_polygon(v=v, filled=filled)
    _add_attrs(geom, attrs)
    self.add_geom(geom)
    return geom

def draw_image(self, fname, width, height, **attrs):
    geom = Image(fname, width, height)
    _add_attrs(geom, attrs)
    self.add_onetime(geom)
    return geom

def draw_line(self, start, end, width=2, **attrs):
    geom = NewLine(start, end, width)
    _add_attrs(geom, attrs)
    self.add_onetime(geom)
    return geom

def draw_mesh(self, mesh_v, mesh_i, **attrs):
    geom = Mesh(mesh_v, mesh_i)
    _add_attrs(geom, attrs)
    self.add_onetime(geom)
    return geom

def draw_text(self, text, size, color=(255,255,255,255), dpi=96, **attrs):
    geom = Text(text, size, color, dpi)
    _add_attrs(geom, attrs)
    self.add_onetime(geom)
    return geom


Viewer.take_screenshot_trans = take_screenshot_trans
Viewer.take_screenshot = take_screenshot
Viewer.draw_background_image = draw_background_image
Viewer.draw_background_polygon = draw_background_polygon
Viewer.draw_image = draw_image
Viewer.draw_line = draw_line
Viewer.draw_mesh = draw_mesh
Viewer.draw_text = draw_text


def set_color(self, r, g, b, a = 1):
    self._color.vec4 = (r, g, b, a)


Geom.set_color = set_color


class Points(Geom):
    def __init__(self, cache):
        Geom.__init__(self)
        self.cache = cache

    def render1(self):
        glPointSize(2)
        glEnable(GL_POINT_SMOOTH)
        glBegin(GL_POINTS)
        for i in range(len(self.cache) // 2):
            glVertex2f(self.cache[i * 2], self.cache[i * 2 + 1])
        glEnd()


class Mesh(Geom):
    def __init__(self, mesh_v, mesh_i):
        Geom.__init__(self)
        self.mesh_v = mesh_v
        self.mesh_i = mesh_i

    def sigmoid_flipped(self, x):
        return 1 - 1 / (1 + math.exp(-x))

    def render1(self):
        glBegin(GL_TRIANGLES)
        for face_i in range(len(self.mesh_i)//3):
            pt1_i, pt2_i, pt3_i = self.mesh_i[face_i * 3], self.mesh_i[face_i * 3 + 1], self.mesh_i[face_i * 3 + 2]
            x1, y1 = self.mesh_v[pt1_i * 3], self.mesh_v[pt1_i * 3 + 1]
            x2, y2 = self.mesh_v[pt2_i * 3], self.mesh_v[pt2_i * 3 + 1]
            x3, y3 = self.mesh_v[pt3_i * 3], self.mesh_v[pt3_i * 3 + 1]
            glVertex2f(x1, y1)
            glVertex2f(x2, y2)
            glVertex2f(x3, y3)
        glEnd()


class DensityField(Geom):
    def __init__(self, x, y, rhoss, posxss, posyss, sz, stride, nrRho):
        Geom.__init__(self)
        k=0
        self.quadColors = (GLfloat*(4 * sz[0] * sz[1]))()
        self.quadCoords = (GLfloat*(3 * sz[0] * sz[1]))()
        self.quadIndices = (GLuint *((sz[0] - 1) * (sz[1] - 1) * 4))()
        for xx in range(sz[0]):
            for yy in range(sz[1]):
                off = xx * sz[1]+yy
                offS = xx * stride[0]+yy * stride[1]
                self.quadColors[off * 4+0]=self.quadColors[off * 4+1]=self.quadColors[off * 4+2]=0
                self.quadColors[off * 4+3]=max(min(rhoss[offS],1),0)
                self.quadCoords[off * 3+0]=posxss[xx]
                self.quadCoords[off * 3+1]=posyss[yy]
                self.quadCoords[off * 3+2]=0
                if xx<sz[0]-1 and yy<sz[1]-1:
                    self.quadIndices[k]=xx * sz[1]+yy
                    k=k+1
                    self.quadIndices[k]=(xx+1) * sz[1]+yy
                    k=k+1
                    self.quadIndices[k]=(xx+1) * sz[1]+(yy+1)
                    k=k+1
                    self.quadIndices[k]=xx * sz[1]+(yy+1)
                    k=k+1
    def render1(self):
        glVertexPointer(3,GL_FLOAT,0,self.quadCoords)
        glColorPointer(4,GL_FLOAT,0,self.quadColors)
        glEnableClientState(GL_VERTEX_ARRAY)
        glEnableClientState(GL_COLOR_ARRAY)
        glDrawElements(GL_QUADS,len(self.quadIndices),GL_UNSIGNED_INT,self.quadIndices)
        glDisableClientState(GL_VERTEX_ARRAY)
        glDisableClientState(GL_COLOR_ARRAY)


class NewLine(Line):
    def __init__(self, start=(0.0, 0.0), end=(0.0, 0.0), width=2):
        Geom.__init__(self)
        self.start = start
        self.end = end
        self.linewidth = LineWidth(width)
        self.add_attr(self.linewidth)


class Text(Geom):
    def __init__(self, text, size, color, dpi):
        Geom.__init__(self)
        if text[0] == '-':
            text = text[:5]
        else:
            text = text[:4]
        self.label = pyglet.text.Label(text,
            font_name='Times New Roman',
            font_size=size,
            color=color,
            dpi=dpi,
            anchor_x='center', anchor_y='center')
    def render1(self):
        self.label.draw()


class Object():

    def __init__(self, width, height, image, color=(1, 1, 1)):
        self.width = width
        self.height = height
        self.image = image
        self.color = color

    def good_render(self, viewer, translation, rotation=0):
        t = Transform(translation=translation, rotation=rotation)
        viewer.draw_image(self.image, self.width, self.height, color=(1, 1, 1, 1)).add_attr(t)

    def plain_render(self, viewer, translation, rotation=0):
        raise NotImplementedError


class Ball(Object):

    def plain_render(self, viewer, translation, rotation=0):
        t = Transform(translation=translation, rotation=rotation)
        viewer.draw_circle(self.width / 2, 20, color=self.color).add_attr(t)
        viewer.draw_circle(self.width / 2, 20, color=tuple(0.9 * i for i in self.color), filled=False, linewidth=2).add_attr(t)


class Rectangle(Object):

    def plain_render(self, viewer, translation, rotation=0):
        t = Transform(translation=translation, rotation=rotation)
        viewer.draw_polygon([(-self.width / 2, -self.height / 2), (self.width / 2, -self.height / 2), 
            (self.width / 2, self.height / 2), (-self.width / 2, self.height / 2)], color=self.color).add_attr(t)


class Cross(Object):

    def __init__(self, width, height, image, color=(1, 1, 1)):
        self.rect1 = Rectangle(width, height, image, color)
        self.rect2 = Rectangle(height, width, image, color)

    def good_render(self, viewer, translation, rotation=0):
        self.rect1.good_render(viewer, translation, rotation)
        self.rect2.good_render(viewer, translation, rotation)

    def plain_render(self, viewer, translation, rotation=0):
        self.rect1.plain_render(viewer, translation, rotation)
        self.rect2.plain_render(viewer, translation, rotation)


class Background(Object):

    def good_render(self, viewer):
        t = Transform(translation=(self.width / 2, self.height / 2))
        viewer.draw_background_image(self.image, self.width, self.height, color=(1, 1, 1, 1)).add_attr(t)

    def plain_render(self, viewer):
        viewer.draw_background_polygon([(0, 0), (self.width, 0), (self.width, self.height), (0, self.height)], color=self.color)


class Cylinder():

    def __init__(self, width, height, res, bg_img):
        self.width = width
        self.height = height
        self.dist = 3.5
        self.distz = 1.8
        self.rad = self.width / (np.pi * 2)
        self.slice = 512

        from PIL import Image, ImageOps

        bg = ImageOps.flip(Image.open(bg_img))
        bg_x, bg_y = bg.size
        bg = bg.convert('RGB').tobytes('raw', 'RGB')

        bg_tex_c, img_tex_c = (ct.c_uint)(), (ct.c_uint)()

        glGenTextures(1, bg_tex_c)
        self.bg_tex = bg_tex_c.value
        glBindTexture(GL_TEXTURE_2D, self.bg_tex)
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1)
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, bg_x, bg_y, 0, GL_RGB, GL_UNSIGNED_BYTE, bg)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE)

        glGenTextures(1, img_tex_c)
        self.img_tex = img_tex_c.value
        glBindTexture(GL_TEXTURE_2D, self.img_tex)
        glPixelStorei(GL_UNPACK_ALIGNMENT,1)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE)

        self.vel = -0.02
        self.timestep = 0
        self.img = None

    def render(self):
        glEnable(GL_TEXTURE_2D)
        glDisable(GL_DEPTH_TEST)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()
        glBindTexture(GL_TEXTURE_2D, self.bg_tex)
        glBegin(GL_QUADS)
        glTexCoord2f(0,0)
        glVertex2f(-1,-1)
        glTexCoord2f(1,0)
        glVertex2f(1,-1)
        glTexCoord2f(1,1)
        glVertex2f(1,1)
        glTexCoord2f(0,1)
        glVertex2f(-1,1)
        glEnd()
        glEnable(GL_DEPTH_TEST)

        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(45.0, float(self.width) / float(self.height), 0.1, 100.0)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()

        gluLookAt(self.dist, self.dist, self.distz, 0, 0, self.height / 2, 0, 0, 1)

        glLineWidth(2.0)
        # little hack
        for side in range(2):
            glBegin(GL_LINES)
            for i in range(self.slice):
                if i < 9 * self.slice / 20:
                    glColor3f(0, 0, 0)
                else:
                    glColor3f(0.95, 0.95, 0.95)
                cr0 = float(i) / float(self.slice)
                cr1 = float(i + 1) / float(self.slice)
                theta0 = (cr0 - 0.110) * np.pi * 2
                theta1 = (cr1 - 0.110) * np.pi * 2
                glVertex3f(self.rad * np.cos(theta0), self.rad * np.sin(theta0), 0)
                glVertex3f(self.rad * np.cos(theta1), self.rad * np.sin(theta1), 0)
            glEnd()

        glLineWidth(0.5)
        glColor3f(0.9, 0.9, 0.9)
        glBegin(GL_LINES)
        for i in range(self.slice):
            cr0 = float(i) / float(self.slice)
            cr1 = float(i + 1) / float(self.slice)
            theta0 = (cr0 - 0.1 + self.timestep * self.vel) * np.pi * 2
            theta1 = (cr1 - 0.1 + self.timestep * self.vel) * np.pi * 2
            # check side
            e = np.array([self.dist, self.dist, self.distz])
            n0 = np.array([np.cos(theta0), np.sin(theta0), 0])
            n1 = np.array([np.cos(theta1), np.sin(theta1), 0])
            p0 = np.array([self.rad * np.cos(theta0), self.rad * np.sin(theta0), 0])
            p1 = np.array([self.rad * np.cos(theta1), self.rad * np.sin(theta1), 0])
            if np.dot(n0, np.subtract(p0,e)) * np.dot(n1, np.subtract(p1,e)) < 0:
                glVertex3f(self.rad * np.cos(theta0), self.rad * np.sin(theta0), 0)
                glVertex3f(self.rad * np.cos(theta0), self.rad * np.sin(theta0), self.height)
        glEnd()
        glColor3f(1.0, 1.0, 1.0)

        assert self.img is not None
        img_x, img_y = self.img.size
        self.img = self.img.convert('RGBA').tobytes('raw', 'RGBA')

        glBindTexture(GL_TEXTURE_2D, self.img_tex)
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1)
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, img_x, img_y, 0, GL_RGBA, GL_UNSIGNED_BYTE, self.img)

        glBegin(GL_TRIANGLES)
        for side in range(2):
            sgn = 1.0 if side == 0 else -1.0
            for i in range(self.slice):
                cr0 = float(i) / float(self.slice)
                cr1 = float(i + 1) / float(self.slice)
                theta0 = (cr0 - 0.1 + self.timestep * self.vel) * np.pi * 2
                theta1 = (cr1 - 0.1 + self.timestep * self.vel) * np.pi * 2
                # check side
                e = np.array([self.dist, self.dist, self.distz])
                n = np.array([np.cos(theta0) ,np.sin(theta0), 0])
                p = np.array([self.rad * np.cos(theta0), self.rad * np.sin(theta0), 0])
                if np.dot(n, np.subtract(p, e)) * sgn < 0:
                    continue
                # triangle 0
                glTexCoord2f(cr0, 0)
                glNormal3f(n[0], n[1], n[2])
                glVertex3f(self.rad * np.cos(theta0), self.rad * np.sin(theta0), 0)
                glTexCoord2f(cr1, 0)
                glNormal3f(n[0], n[1], n[2])
                glVertex3f(self.rad * np.cos(theta1), self.rad * np.sin(theta1), 0)
                glTexCoord2f(cr1, 1)
                glNormal3f(n[0], n[1], n[2])
                glVertex3f(self.rad * np.cos(theta1), self.rad * np.sin(theta1), self.height)
                # triangle 1
                glTexCoord2f(cr0, 0)
                glNormal3f(n[0], n[1], n[2])
                glVertex3f(self.rad * np.cos(theta0), self.rad * np.sin(theta0), 0)
                glTexCoord2f(cr1, 1)
                glNormal3f(n[0], n[1], n[2])
                glVertex3f(self.rad * np.cos(theta1), self.rad * np.sin(theta1), self.height)
                glTexCoord2f(cr0, 1)
                glNormal3f(n[0], n[1], n[2])
                glVertex3f(self.rad * np.cos(theta0), self.rad * np.sin(theta0), self.height)
        glEnd()
    
        self.timestep += 1