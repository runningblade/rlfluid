#!/usr/bin/env python
# noinspection PyUnresolvedReferences
import os
import sys
sys.path.append('..')
sys.path.append('../envs')
sys.path.append('../trpo')
from mpi4py import MPI
from baselines.common import set_global_seeds, tf_util as U
import os.path as osp
import gym
import logging
import argparse
from baselines import logger
from mlp_policy import MlpPolicy
import tensorflow as tf
from baselines import bench
import matplotlib.pyplot as plt
import datetime
import json
import signal
import numpy as np
import trpo_mpi as trpo

from env_funcs import *
from utils import *
from scenes import *
from recorder import GameSingleRunner

from env_ins import *

RESULT_PATH = '../results/'

def train(identifier, env, mode, seed, rollout_length, total_steps, 
    policy_hid_size, vf_hid_size, activation_policy, activation_vf, 
    load_ckpt, make_video):

    sess = make_growable_session(1)
    sess.__enter__()

    rank = MPI.COMM_WORLD.Get_rank()
    if rank != 0:
        logger.set_level(logger.DISABLED)
    workerseed = seed + 10000 * MPI.COMM_WORLD.Get_rank()
    set_global_seeds(workerseed)

    def policy_fn(name, ob_space, ac_space):
        return MlpPolicy(name=name, ob_space=env.observation_space, ac_space=env.action_space,
                         policy_hid_size=policy_hid_size, vf_hid_size=vf_hid_size, activation_policy=activation_policy,
                         activation_vf=activation_vf, cnn_input=None)

    env.seed(workerseed)
    gym.logger.setLevel(logging.WARN)

    signal.signal(signal.SIGTERM, handler)

    if mode == 'train':
        trpo.learn(env, policy_fn, timesteps_per_batch=int(rollout_length), max_kl=0.01, cg_iters=20, cg_damping=0.1,
            max_timesteps=int(total_steps), gamma=0.995, lam=0.97, vf_iters=5, vf_stepsize=1e-3,
            identifier=identifier, load_ckpt=load_ckpt, reward_list=reward_list)
    else:   
        # enjoy
        runner = GameSingleRunner(env, policy_fn, max_timesteps=int(total_steps), identifier=identifier, record_video=make_video)
        runner.run('n')

    env.close()

    if rank == 0:
        curr_time = datetime.datetime.now().strftime('%y%m%d%H%M%S')
        save_rewards(reward_list, curr_time)
        save_state(RESULT_PATH + 'saved_model/model_data/' + identifier + '.ckpt')


def main():

    global IDENTIFIER

    parser = argparse.ArgumentParser(description='TRAIN.')
    parser.add_argument('--mode', type=str, default='enjoy')
    parser.add_argument('--id', type=str, default='')

    parser.add_argument('--debug', dest='debug', action='store_true')
    parser.set_defaults(debug=False)
    parser.add_argument('--load_ckpt', dest='load_ckpt', action='store_true')
    parser.set_defaults(load_ckpt=False)
    parser.add_argument('--make_video', dest='make_video', action='store_true')
    parser.set_defaults(make_video=False)

    parser.add_argument('--rollout_length', type=int, default=500)
    parser.add_argument('--total_steps', type=int, default=3e7)

    parser.add_argument('--policy_size', type=int, nargs='+', default=(128, 64, 64, 32))
    parser.add_argument('--value_func_size', type=int, nargs='+', default=(128, 64, 64, 32))

    parser.add_argument('--activation_vf', type=str, default='relu')
    parser.add_argument('--activation_policy', type=str, default='relu')
    parser.add_argument('--seed', type=int, default=0)
    parser.add_argument('--log_dir', type=str, default=RESULT_PATH + 'logs/')

    parser.add_argument('--vtk', dest='vtk', action='store_true')
    parser.set_defaults(vtk=False)
    parser.add_argument('--pov', dest='pov', action='store_true')
    parser.set_defaults(pov=False)

    parser.add_argument('--src_num', type=int, default=5)

    args = parser.parse_args()

    IDENTIFIER[0] = args.id

    assert args.mode in ['train', 'enjoy']

    if args.load_ckpt or args.mode == 'enjoy':
        params = load_params(args.id)
        args.rollout_length = params['rollout_length']
        args.total_steps = params['total_steps']
        args.policy_size = tuple(params['policy_size'])
        args.value_func_size = tuple(params['value_func_size'])
    else:
        params = {'rollout_length': args.rollout_length,
            'total_steps': args.total_steps,
            'policy_size': args.policy_size,
            'value_func_size': args.value_func_size}
        save_params(params, args.id)

    if args.mode == 'enjoy':
        args.debug = True

    activation_map = {'relu': tf.nn.relu, 'leaky_relu': U.lrelu, 'tanh': tf.nn.tanh}

    logger.configure(dir=args.log_dir)
    activation_policy = activation_map[args.activation_policy]
    activation_vf = activation_map[args.activation_vf]

    env = globals()['get_' + args.id](debug=args.debug, vtk=args.vtk, pov=args.pov, src_num=args.src_num, target_threshold=0.05)

    train(args.id, env, args.mode, args.seed, args.rollout_length, args.total_steps, 
        args.policy_size, args.value_func_size, activation_policy, activation_vf, 
        args.load_ckpt, args.make_video)


if __name__ == '__main__':
    main()

