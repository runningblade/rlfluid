import sys
import os
sys.path.append('..')
sys.path.append('../envs')
import json
from mpi4py import MPI
import datetime
from baselines.common import tf_util as U
import numpy as np
import tensorflow as tf
import multiprocessing
from env_funcs import *

RESULT_PATH = '../results/'


def get_session():
    return tf.get_default_session()


def load_state(fname):
    saver = tf.train.Saver()
    saver.restore(get_session(), fname)


def save_state(fname):
    os.makedirs(os.path.dirname(fname), exist_ok=True)
    saver = tf.train.Saver()
    saver.save(get_session(), fname)


def save_params(params, env_id):
    save_path = RESULT_PATH + "saved_model/params/"
    os.makedirs(save_path, exist_ok=True)
    with open(save_path + env_id + ".json", "w") as f:
        json.dump(params, f)


def load_params(env_id):
    with open(RESULT_PATH + "saved_model/params/" + env_id + ".json", "r") as f:
        params = dict(json.load(f))
    return params


def handler(signum, frame):
    rank = MPI.COMM_WORLD.Get_rank()
    if rank == 0:
        print("saving last terminal")
        curr_time = datetime.datetime.now().strftime("%y%m%d%H%M%S")
        save_rewards(reward_list, curr_time)
        save_state(RESULT_PATH + "saved_model/model_data/" + IDENTIFIER[0] + ".ckpt")
    sys.exit(1)


def save_rewards(reward_list, curr_time):
    reward_list = np.array(reward_list).tolist()
    save_path = RESULT_PATH + "saved_model/plot_jsons/"
    os.makedirs(save_path, exist_ok=True)
    with open(save_path + IDENTIFIER[0] + '_' + curr_time + ".json", "w") as f:
        json.dump(reward_list, f)


def plot_rewards(reward_json, title):
    import matplotlib.pyplot as plt
    with open(reward_json, 'r') as file:
        reward_list = list(json.load(file))
    x = np.arange(len(reward_list))
    plt.figure('Training Result')

    order = 6

    fit = np.polyfit(x, reward_list, order)
    fit = np.polyval(fit, x)

    plt.plot(x, reward_list, color='r', alpha=0.5)
    plt.plot(x, fit, lw=2, label=title, color='r')

    plt.legend(loc='lower right')
    plt.xlabel('Iteration Number')
    plt.ylabel('Episode Reward')
    plt.grid(True)

    save_path = RESULT_PATH + "saved_model/plot_images/"
    os.makedirs(save_path, exist_ok=True)
    plt.savefig(save_path + reward_json.split('/')[-1] + ".png")
    plt.close()


def make_growable_session(num_cpu=None, make_default=False):
    """Returns a session that will use <num_cpu> CPU's only"""
    if num_cpu is None:
        num_cpu = int(os.getenv('RCALL_NUM_CPU', multiprocessing.cpu_count()))
    tf_config = tf.ConfigProto(
        inter_op_parallelism_threads=num_cpu,
        intra_op_parallelism_threads=num_cpu)
    tf_config.gpu_options.allocator_type = 'BFC'
    tf_config.gpu_options.allow_growth = True
    if make_default:
        return tf.InteractiveSession(config=tf_config)
    else:
        return tf.Session(config=tf_config)
