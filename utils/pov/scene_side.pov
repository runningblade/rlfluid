#include "colors.inc"
#include "textures.inc"
#include "math.inc"

#declare PATH="../../results/pov/";
#declare ITER=1;

global_settings {assumed_gamma 1.0}
#default {finish {ambient 0.2 diffuse 0.5 specular 0.2}}

#macro Container(C_L,C_H,C_W,C_T)
difference{
    box {<0,0,0>,<C_L,C_H,C_W>}
    box {<C_T,C_T,C_T>,<C_L+0.1,C_H+0.1,C_W-C_T>}
}
translate <-0.5*C_L,0,-0.5*C_W>
#end

#macro Bottom(C_L,C_H,C_W,TRAP_H,TRAP_T)
union {
    polygon {
        5,
        <0,0,0> <0,0,0.5*(C_W-TRAP_T-2*TRAP_H)>
        <C_L,0,0.5*(C_W-TRAP_T-2*TRAP_H)> <C_L,0,0>
        <0,0,0>
    }
    polygon {
        5,
        <0,0,0.5*(C_W-TRAP_T-2*TRAP_H)> <0,TRAP_H,0.5*(C_W-TRAP_T)>
        <C_L,TRAP_H,0.5*(C_W-TRAP_T)> <C_L,0,0.5*(C_W-TRAP_T-2*TRAP_H)>
        <0,0,0.5*(C_W-TRAP_T-2*TRAP_H)>
    }
    polygon {
        5,
        <0,TRAP_H,0.5*(C_W-TRAP_T)> <0,TRAP_H,0.5*(C_W+TRAP_T)>
        <C_L,TRAP_H,0.5*(C_W+TRAP_T)> <C_L,TRAP_H,0.5*(C_W-TRAP_T)>
        <0,TRAP_H,0.5*(C_W-TRAP_T)>
    }
    polygon {
        5,
        <0,TRAP_H,0.5*(C_W+TRAP_T)> <0,0,0.5*(C_W+TRAP_T+2*TRAP_H)>
        <C_L,0,0.5*(C_W+TRAP_T+2*TRAP_H)> <C_L,TRAP_H,0.5*(C_W+TRAP_T)>
        <0,TRAP_H,0.5*(C_W+TRAP_T)>
    }
    polygon {
        5,
        <0,0,0.5*(C_W+TRAP_T+2*TRAP_H)> <C_L,0,0>
        <C_L,0,C_W> <C_L,0,0.5*(C_W+TRAP_T+2*TRAP_H)>
        <0,0,0.5*(C_W+TRAP_T+2*TRAP_H)>
    }
}
translate <-0.5*C_L,-0.5*C_H,-0.5*C_W>
#end

#macro calcCtr(frame)
  #local path=concat(PATH,"ball/",str(ITER,1,0),"/S",str(frame,1,0),".pov");
  #local body=#include path;
  (min_extent(body)+max_extent(body))*0.5
#end
  
#macro sweTrans()
    scale <1,1,1> rotate <90,0,180> translate <0.75,0,-1.25>
#end

background {color White}

camera {location <0.7,2.0,0> look_at <-0.1,0.4,0>
    up <0,1,0> right<1.78,0,0>}

light_source {<0,1000,0> color White}

// sky -------------------------------------
plane {
    <0,1,0>,1 hollow
    texture {
        pigment {bozo turbulence 0.92
            color_map {
                 [0.00 rgb <0.05,0.15,0.45>]
                 [0.50 rgb <0.05,0.15,0.45>]
                 [0.70 rgb <1,1,1>]
                 [0.85 rgb <0.2,0.2,0.2>]
                 [1.00 rgb <0.5,0.5,0.5>]
           }
           scale <1,1,1.5>*2.5
           translate <0,0,0>
        }
        finish {ambient 1 diffuse 0}
    }
    scale 10000
}

// fog on the ground -----------------------
fog {
    fog_type   2
    distance   50
    color      rgb <1,1,1>*0.8
    fog_offset 0.1
    fog_alt    1.5
    turbulence 1.8
}

// ground ----------------------------------
plane {<0,1,0>,0
    texture {
        pigment {color rgb <0.22,0.45,0>}
        normal {bumps 0.75 scale 0.015}
        finish {phong 0.1}
    }
}

// ball -----------------------------------
object {
    #include concat(PATH,"ball/",str(ITER,1,0),"/S", str(frame_number*2,1,0),".pov")
    pigment {rgb <1,0,0>}
    sweTrans()
}

// target ---------------------------------
object {
    #include concat(PATH,"target/",str(ITER,1,0),"/C0.pov")
    pigment {rgb <0,1,0>}
    sweTrans()
}

// trace ----------------------------------
#declare i=2;
#declare last_i=0;
#declare RAD_TRACE=0.005;
#while(i<frame_number*2)
    #if (VEq(calcCtr(i), calcCtr(last_i)))
    #else
        object {
            cylinder {calcCtr(last_i),calcCtr(i),RAD_TRACE pigment {rgb <0.5,0,0>} no_shadow}
            sweTrans()
        }
        object {
            sphere   {calcCtr(last_i),RAD_TRACE pigment {rgbf <1,0,0,0.5>} no_shadow}
            sweTrans()
        }
        #declare last_i=i;
    #end
  #declare i=i+2;
#end

// container ------------------------------
object {
    Container(1.55,1.0,2.55,0.05)
    pigment {checker
        color rgb <1,1,1>
        color rgb <1,1,1>*0.2
        scale <0.25,0.125,0.25>
    }
}

// bottom ---------------------------------
object {
    Bottom(1.5,-0.001,2.5,0.6,0.2)
    pigment {checker
        color rgb <1,1,1>
        color rgb <1,1,1>*0.2
        scale <0.25,0.125,0.25>
    }
}

// mesh -----------------------------------
object {
    #include concat(PATH,"mesh/",str(ITER,1,0),"/W",str(frame_number,1,0),".pov")
    texture {
        pigment {rgbf <.93,.95,.98,0.825>*0.99}
        finish {
            ambient    0.35
            diffuse    0.45
            specular   0.2
            reflection 0.3
        }
    }
    sweTrans()
    interior {
        ior           1.33
        fade_power    1001
        fade_distance 0.5
        fade_color    <0.8,0.8,0.8>
        caustics      0.16
    }
}
