# read from 'utils/music_notes.py' and save json to 'utils/music_jsons/'

from os import makedirs, listdir
import json
import sys
sys.path.append('../envs/')
from argparse import ArgumentParser
from music_notes import *
from music_params import TBASE

key_map = {'c': 0, 'd': 1, 'e': 2, 'f': 3, 'g': 4, 'a': 5, 'b': 6, 'c5': 7}


def make_json(name, music):
	music_mapped = []
	for note in music:
		music_mapped.append([key_map[note[0]],  4 * TBASE / note[1]])

	save_path = './music_jsons/'
	makedirs(save_path, exist_ok=True)
	with open(save_path + name + '.json', 'w') as f:
		json.dump(music_mapped, f)


def main():
	parser = ArgumentParser(description='MAKE_MUSIC_JSON.')
	parser.add_argument('--name', type=str, default='_all')
	args = parser.parse_args()
	name = args.name

	if name == '_all':
		for notes_name, notes_music in all_notes:
			make_json(notes_name, notes_music)
	else:
		make_json(name, globals()[name])


if __name__ == '__main__':
	main()