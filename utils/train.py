#!/usr/bin/env python
# noinspection PyUnresolvedReferences
import os
import sys
sys.path.append('..')
sys.path.append('../envs')
sys.path.append('../trpo')
from mpi4py import MPI
from baselines.common import set_global_seeds, tf_util as U
import os.path as osp
import gym
import logging
import argparse
from baselines import logger
from mlp_policy import MlpPolicy
import tensorflow as tf
from baselines import bench
import matplotlib.pyplot as plt
import datetime
import json
import signal
from trpo_cnn import TrpoCnn
import numpy as np

from env_funcs import *
from utils import *
from recorder import GameSingleRunner, GameDoubleRunner

RESULT_PATH = '../results/'
MUSIC_PATH = './music_jsons/'
RECORD_PATH = RESULT_PATH + 'saved_recording/jsons/'

# Parameters

# CNN_PARAMS = {
#     'filter_sizes': [7, 7, 5, 5, 3, 3, 3],
#     'strides': [2, 2, 2, 2, 1, 1, 1],
#     'filter_nums': [64, 64, 128, 128, 256, 256, 256],
#     'pads': ["SAME", "SAME", "SAME", "SAME", "VALID", "VALID", "VALID"]
# }

CNN_PARAMS = {
    'filter_sizes': None,
    'strides': None,
    'filter_nums': None,
    'pads': None
}

def train(identifier, mode, seed, max_step, rollout_length, total_steps, 
    policy_hid_size, vf_hid_size, activation_policy, activation_vf, 
    music, make_recording, load_ckpt, make_video):

    import baselines.common.tf_util as U
    sess = U.make_session(2)
    sess.__enter__()

    rank = MPI.COMM_WORLD.Get_rank()
    if rank != 0:
        logger.set_level(logger.DISABLED)
    workerseed = seed + 10000 * MPI.COMM_WORLD.Get_rank()
    set_global_seeds(workerseed)

    if make_recording:
        assert music != ''
        recording = RECORD_PATH + music + '.json'
    else:
        recording = None

    if music == '':
        music = None
    else:
        music = MUSIC_PATH + music + '.json'

    cnn_input = False
    cnn = None
    if mode in ['train', 'train_multi']:
        env = func[identifier](max_step=max_step, music=music)
    elif mode in ['train_cnn', 'train_multi_cnn']:
        env = func[identifier](max_step=max_step, music=music)
        cnn_input = True
    elif mode in ['play', 'enjoy', 'battle']:
        env = func[identifier](max_step=np.inf, music=music, recording=recording)
    elif mode in ['enjoy_cnn', 'battle_cnn']:
        env = func[identifier](max_step=np.inf, music=music, recording=recording)
        cnn_input = True
        cnn = TrpoCnn(env.field_size)
    elif mode == 'build_cnn':
        env = func[identifier](max_step=np.inf)
        cnn = TrpoCnn(env.field_size, filter_sizes=CNN_PARAMS['filter_sizes'], strides=CNN_PARAMS['strides'],
            filter_nums=CNN_PARAMS['filter_nums'], pads=CNN_PARAMS['pads'])
    else:
        print('Mode argument error!')
        return

    if identifier.endswith('b'):    # be cautious when naming non-battle env
        import trpo_multi_mpi as trpo
        battle = True
        GameRunner = GameDoubleRunner
    else:
        import trpo_mpi as trpo
        battle = False
        GameRunner = GameSingleRunner

    def policy_fn(name, ob_space, ac_space):
        return MlpPolicy(name=name, ob_space=env.observation_space, ac_space=env.action_space,
                         policy_hid_size=policy_hid_size, vf_hid_size=vf_hid_size, activation_policy=activation_policy,
                         activation_vf=activation_vf, cnn_input=cnn_input)

    # env = bench.Monitor(env, logger.get_dir() and
    #                     osp.join(logger.get_dir(), '%i.monitor.json' % rank))
    env.seed(workerseed)
    gym.logger.setLevel(logging.WARN)

    signal.signal(signal.SIGTERM, handler)

    finetune = False
    if mode.endswith('_cnn'):
        IDENTIFIER[0] += '_cnn'
        identifier += '_cnn'
        finetune = True

    if mode.startswith('train'):
        trpo.learn(env, policy_fn, timesteps_per_batch=int(rollout_length), max_kl=0.01, cg_iters=20, cg_damping=0.1,
            max_timesteps=int(total_steps), gamma=0.995, lam=0.97, vf_iters=5, vf_stepsize=1e-3,
            identifier=identifier, load_ckpt=load_ckpt, reward_list=reward_list, finetune=finetune)
    elif mode == 'build_cnn':
        trpo.build_equal_cnn(env, policy_fn, cont=False, timesteps_per_batch=500,
                               max_iters=700, identifier=identifier, cnn=cnn, reward_list=reward_list)
    else:
        runner = GameRunner(env, policy_fn, max_timesteps=int(total_steps), identifier=identifier, record_video=make_video, cnn=cnn)
        run_option = {'play': 'h', 'enjoy': 'n', 'enjoy_cnn': 'n',
            'battle': 'hn', 'battle_cnn': 'hn'}
        runner.run(run_option[mode])
    
    env.close()

    if rank == 0:
        curr_time = datetime.datetime.now().strftime('%y%m%d%H%M%S')
        save_rewards(reward_list, curr_time)
        save_state(RESULT_PATH + 'saved_model/model_data/' + identifier + '.ckpt')

def main():

    global IDENTIFIER

    parser = argparse.ArgumentParser(description='TRAIN.')
    parser.add_argument('--mode', type=str, default='enjoy')
    parser.add_argument('--id', type=str, default='')

    parser.add_argument('--load_ckpt', dest='load_ckpt', action='store_true')
    parser.set_defaults(load_ckpt=False)
    parser.add_argument('--make_video', dest='make_video', action='store_true')
    parser.set_defaults(make_video=False)

    parser.add_argument('--max_step', type=int, default=500)
    parser.add_argument('--rollout_length', type=int, default=500)
    parser.add_argument('--total_steps', type=int, default=3e7)

    parser.add_argument('--policy_size', type=int, nargs='+', default=(128, 64, 64, 32))
    parser.add_argument('--value_func_size', type=int, nargs='+', default=(128, 64, 64, 32))

    parser.add_argument('--activation_vf', type=str, default='relu')
    parser.add_argument('--activation_policy', type=str, default='relu')
    parser.add_argument('--seed', type=int, default=0)
    parser.add_argument('--log_dir', type=str, default=RESULT_PATH + 'logs/')

    parser.add_argument('--music', type=str, default='')
    parser.add_argument('--make_recording', dest='make_recording', action='store_true')
    parser.set_defaults(make_recording=False)

    args = parser.parse_args()

    assert args.id in func
    IDENTIFIER[0] = args.id

    if args.load_ckpt or args.mode in ['play', 'enjoy', 'enjoy_cnn', 'battle', 'battle_cnn', 'build_cnn']:
        params = load_params(args.id)
        args.max_step = params['max_step']
        args.rollout_length = params['rollout_length']
        args.total_steps = params['total_steps']
        args.policy_size = tuple(params['policy_size'])
        args.value_func_size = tuple(params['value_func_size'])
    else:
        params = {'max_step': args.max_step,
            'rollout_length': args.rollout_length,
            'total_steps': args.total_steps,
            'policy_size': args.policy_size,
            'value_func_size': args.value_func_size}
        save_params(params, args.id)

    activation_map = {'relu': tf.nn.relu, 'leaky_relu': U.lrelu, 'tanh': tf.nn.tanh}

    logger.configure(dir=args.log_dir)
    activation_policy = activation_map[args.activation_policy]
    activation_vf = activation_map[args.activation_vf]

    train(args.id, args.mode, args.seed, args.max_step, args.rollout_length, args.total_steps, 
        args.policy_size, args.value_func_size, activation_policy, activation_vf, 
        args.music, args.make_recording, args.load_ckpt, args.make_video)


if __name__ == '__main__':
    main()

